/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: basis
 *
 * Created on 22. März 2018, 13:55
 */

#include <cstdlib>

#include <gmp.h>
#include <gmpxx.h>

#include <iostream>
#include <sstream>
#include <string>
#include <tuple>
#include <vector>

using namespace std;



class paper{
    public:
    vector<vector<mpf_class>> m_fields;

    int m_width;
    int m_height;

    mpf_class m_prob_edge;
    mpf_class m_prob_side;
    mpf_class m_prob_middle;

    paper(int width, int height, int defaultValue){

        this->m_prob_edge = mpf_class(0.5, 1024);
        this->m_prob_side = mpf_class(1, 1024);
        this->m_prob_side /= 3;
        this->m_prob_middle = mpf_class(0.25, 1024);

        this->m_width = width;
        this->m_height = height;

        for(int i=0;i<height;i++){
            vector<mpf_class> elem;
            for(int j=0;j<width;j++){
                elem.push_back({defaultValue});
            }
            this->m_fields.push_back(elem);
        }
    }

    paper(const paper &orig){
        this->m_prob_edge = orig.m_prob_edge;
        this->m_prob_side = orig.m_prob_side;
        this->m_prob_middle = orig.m_prob_middle;

        this->m_width = orig.m_width;
        this->m_height = orig.m_height;
        this->m_fields = orig.m_fields;
    }

    paper& operator=(const paper& rhs){
       this->m_prob_edge = rhs.m_prob_edge;
       this->m_prob_side = rhs.m_prob_side;
       this->m_prob_middle = rhs.m_prob_middle;

       this->m_width = rhs.m_width;
       this->m_height = rhs.m_height;
       this->m_fields = rhs.m_fields;

       return *this;
    }

    vector<tuple<int,int, mpf_class>> getNeighbors(int x, int y){
        vector<tuple<int,int, mpf_class>> result;

        bool noUp = y == 0;
        bool noDown = y == (this->m_height -1);
        bool noLeft = x == 0;
        bool noRight = x == (this->m_width -1);


        bool sideX = noLeft || noRight;
        bool sideY = noUp || noDown;

        bool isEdge = sideX && sideY;
        bool isSide = sideX || sideY;

        auto prob = isEdge?this->m_prob_edge:isSide?this->m_prob_side:this->m_prob_middle;
        
        prob = prob * this->m_fields.at(y).at(x);

        if(!noUp) result.push_back({x, y-1, prob});
        if(!noDown) result.push_back({x, y+1, prob});
        if(!noLeft) result.push_back({x-1, y, prob});
        if(!noRight) result.push_back({x+1, y, prob});

        return result;
    }

    string toString(){
        stringstream ss_result;
        for(auto row : this->m_fields){
            for(auto elem : row){
                ss_result << elem.get_d() << "\t";
            }
            ss_result << endl;
        }
        return ss_result.str();
    }

    static paper generate(paper from){
        paper result(from.m_width, from.m_height, 1);

        for(int y=0;y<from.m_height;y++){
            for(int x=0;x<from.m_width;x++){

                auto neighbors = from.getNeighbors(x,y);

                for(auto neighbor : neighbors){
                    auto prob = get<2>(neighbor);
                    auto &field = result.m_fields.at(get<1>(neighbor)).at(get<0>(neighbor));
                    field *= prob;
                }
            }
        }


        for(int y=0;y<from.m_height;y++){
            for(int x=0;x<from.m_width;x++){
                auto &field_ref = result.m_fields.at(y).at(x);

                field_ref = 1 - field_ref;
            }
        }

        return result;
    }
    
    mpf_class expectedUnoccupied(){
        mpf_class result = 0;

        for(auto row : this->m_fields){
            for(auto elem : row){
                
                result += (1- elem);
            }
        }

        return result;
    }

    
};


/*
 * 
 */
int main(int argc, char** argv) {

    paper old(3,2,0);

    old.m_fields[0][0] = 1;
    old.m_fields[1][1] = 1;

    cout << old.toString() << endl;


    for(int i=0;i<3;i++){
        paper next = paper::generate(old);
        old = next;
        cout << old.toString() << endl;
    }

    
    cout << old.expectedUnoccupied() << endl;
    return 0;
}






