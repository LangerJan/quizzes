require 'set'


def isPandigital(number, digitrange)
	return false if number.to_s.size != digitrange.size
	digitrange.each {|digit|
		return false if number.to_s.count(digit.to_s) != 1
	}
	return true
end



digits = (1..9).to_a


def array_to_num(array)

	res = 0
	fak = 1
	array.each {|n|
		tmp = fak * n
		res = res + tmp
		fak = fak * 10
	}
	res
end

def dings(prod_a, prod_b)

	result = Set.new

	digits = (1..9).to_a
	digits.combination(prod_a).each {|comb_a|
		rest_digits = Array.new(digits)
		comb_a.each {|x| rest_digits.delete(x) }
		rest_digits.combination(prod_b).each {|comb_b|
			comb_a.permutation(comb_a.size()).each {|comb_a_perm|
				comb_b.permutation(comb_b.size()).each{|comb_b_perm|
					a = array_to_num(comb_a_perm)
					b = array_to_num(comb_b_perm)
					c = a * b
	
					if isPandigital((a.to_s+b.to_s+c.to_s).to_i, (1..9).to_a)
						result << c
						p a.to_s + " * " + b.to_s + " = " + c.to_s
					end
				}
			}


		}

	}
	result
end


set = Set.new

1.upto(9) {|x|
	1.upto(9) {|y|
		set.merge(dings(x,y))
	}
}


res = 0
p "---- Results ----"
set.each{|s| 
	p s
	res = res + s
}
p "---- End ----"
p res






