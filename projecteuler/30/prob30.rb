def digits(number)
	digits = Array.new

	while number != 0
		digits << (number%10)
		number = number / 10
	end
	digits
end

def predicate(number, power)
	digs = digits(number)
	
	probe = 0
	
	digs.each { |n|
		probe = probe + n**power
	}
	
	return number == probe
end

res = 0

10.upto(1000000) { |n|

	res += n if predicate(n,5)

}

p res






