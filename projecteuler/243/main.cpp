

#include <gmp.h>
#include <gmpxx.h>
#include <primesieve.hpp>

#include "../libeuler/include/libeuler.hpp"

#include <cstdint>
#include <cstdlib>
#include <iostream>
#include <set>
#include <vector>

using namespace std;

int main(){

	//mpq_class limit{4,10};
	mpq_class limit{15499,94744};
	mpq_class tst;

	uint64_t den=1;
	uint64_t denPhi;
	do {
		den++;
		denPhi = eulerPhi(den);

		tst = mpq_class{denPhi, den-1};

		// cout << den << " " << tst << endl;
	}
	while(!(tst < limit));

	std::cout << "d = " << den << std::endl;
	return EXIT_SUCCESS;
}
