#include "../include/libeuler.hpp"


#include <cstdint>
#include <tuple>
#include <vector>

#include <gmp.h>
#include <gmpxx.h>

#include <primesieve.hpp>

using namespace std;


vector<tuple<uint64_t, uint64_t>> factorize_naively(uint64_t value){

	vector<tuple<uint64_t, uint64_t>> result;
	primesieve::iterator it(0);
	uint64_t prime;	
	uint64_t exponent;

	while(value > 1){
		prime = it.next_prime();		
		exponent = 0;
		
		if(value % prime == 0){
			while(value % prime == 0){
				value = value / prime;
				exponent++;
			}
			result.push_back({prime, exponent});
		}
	}

	return result;
}


uint64_t eulerPhi(uint64_t value){
	uint64_t result = 0;
	auto factors = factorize_naively(value);
	
	mpz_class res{1};

	for(auto factor : factors){
		mpz_class result;
		mpz_ui_pow_ui(result.get_mpz_t(), std::get<0>(factor), std::get<1>(factor)-1);
		result = result * (std::get<0>(factor) - 1);

		res *= result;
	}

	return res.get_si();
}




