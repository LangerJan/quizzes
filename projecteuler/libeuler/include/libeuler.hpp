#ifndef LIBEULER_HPP
#define LIBEULER_HPP

#include <cstdint>
#include <tuple>
#include <vector>

uint64_t eulerPhi(uint64_t value);
std::vector<std::tuple<uint64_t, uint64_t>> factorize_naively(uint64_t value);


#endif
