#include <iostream>
#include <vector>
#include <tuple>
#include <set>
#include <map>
#include <string>
#include <ctype.h>
#include <cstdint>
#include <algorithm>
#include <memory>

#include <boost/algorithm/string.hpp>

using namespace std;

/**
 * Approach: Divide and conquer
 *
 * Observations:
 *
 * 1) Most of the seating is irrelevant once the people are placed
 * in accordance with the rules. We only need to now the last three on each side.
 *
 * 2) To avoid running into duplicates, someone needs to be seated first, fixed.
 *    This works without any loss of generality.
 */

/**
 * Coding strategies:
 *
 * a,b,c,d,e,f,g,h,i,j Male member of a different family
 * A,B,C,D,E,F,G,H,I,J Female member of a different family
 *
 */

/**
 * A family is a tuple of females and males which are not yet seated
 */
typedef tuple<int,int> family;

/**
 * This describes a number of families which are all alike.
 */
typedef tuple<family, int> identical_families;

inline bool noMorePeopleToSeat(map<uint16_t, family> &families){
    for(auto &kv : families){
        if(get<0>(kv.second) + get<1>(kv.second) > 0) return false;
    }
    return true;
}

inline bool rule_edgeCheck(vector<uint16_t> &seating){
    auto ch = seating.at(0);
    if(seating.size() < 4) return true;

    size_t tmp = 3;

    std:vector<int> y(seating.end() - std::min(seating.size(), tmp), seating.end());
    y.push_back(seating.at(0));
    y.push_back(seating.at(1));
    y.push_back(seating.at(2));

    int count = 0;
    for(auto c : y){
        if(c == ch) count ++;
        else count = 0;

        if(count == 4) return false;
    }

    return true;
}

inline bool ruleCheck(const vector<uint16_t> &seating){
    if(seating.size() < 4) return true;
    auto ch = seating.at(seating.size()-1);
    auto cnt = std::count_if(seating.end()-4, seating.end(), [ch](auto x){return x == ch;});
    return cnt != 4;
}

vector<uint16_t> create_context(const vector<uint16_t> &seating){
    vector<int32_t> result;

    map<uint16_t, int> occurrences;

    vector<uint16_t> result2(seating.begin(), seating.end());

    uint16_t nextFreeNumber = 10000;
    for(auto v : result2){
        if(v < 10000){
            std::replace(result2.begin(), result2.end(), v, nextFreeNumber);
            nextFreeNumber++;
        }
    }
    for(auto &v : result2){
        v -= 10000;
    }

    return result2;

}

map<vector<uint16_t>, uint64_t> result_cache;

uint64_t m_recursive(vector<uint16_t> &seating_so_far,
                bool femalesTurn,
                map<uint16_t, family> &families
                ){

    uint64_t result = 0;

    auto context = create_context(seating_so_far);

    auto it = result_cache.find(context);
    if(it != result_cache.end()){
        return it->second;
    }


    if(noMorePeopleToSeat(families)){
        result = rule_edgeCheck(seating_so_far)?1:0;
    }
    else
    {
        uint64_t res2 = 0;
        for(auto &kv : families){

            auto &familyMember = femalesTurn?get<0>(kv.second):get<1>(kv.second);
            auto familyMember_backup = familyMember;
            if(familyMember != 0){
                seating_so_far.push_back(kv.first);

                if(!ruleCheck(seating_so_far))
                {
                    seating_so_far.pop_back();
                    continue;
                }

                familyMember--;
                res2 += familyMember_backup * m_recursive(seating_so_far, !femalesTurn, families);
                res2 = res2 % 1000000007ULL;
                seating_so_far.pop_back();
            }
            familyMember = familyMember_backup;
        }
        result = res2;

    }

    result_cache[context] = result;

    return result;
}

uint64_t m(int numFamilies){
    uint64_t result = 0;
    map<uint16_t, family> fam;

    for(uint16_t i=0;i<numFamilies;i++) {
        fam[i] = {2,2};
    }

    // Seat the first person.
    {
        get<0>(fam[0])--;

        vector<uint16_t> seating = {0};
        result += 2 /* Per Gender */ *
                numFamilies *
                2 /* Parent and child */ *
                m_recursive(seating, false, fam);
        get<0>(fam[0])++;
    }

    return result;
}



int main(int argc, char* argv[]) {
    cout << m(3) << endl;
    cout << "cache size:" << result_cache.size() << endl;

#if 1
    for( auto kv : result_cache){
        for(auto str : kv.first){
            cout << str << ",";
        }
        cout << ":" << kv.second << endl;
    }
#endif
    return 0;
}
