
require 'prime'
require 'set'


def factors_of(number)
  primes, powers = number.prime_division.transpose
  exponents = powers.map{|i| (0..i).to_a}
  divisors = exponents.shift.product(*exponents).map do |powers|
    primes.zip(powers).map{|prime, power| prime ** power}.inject(:*)
  end
  divisors.sort.map{|div| [div, number / div]}
end


def d(number)
	res = 0
	facs = factors_of(number)
	facs.each { |n|
		res = res + n[0]
	}
	res = res - facs[-1][0]
	return res
end



def amiCheck(number)
	second = d(number)
	if d(second) == number and number != second
		return [number, second]
	else
		return []
	end
end


p amiCheck(284)

res = Set.new

2.upto(10000) { |n|

	unless Prime.prime?(n)
		a = amiCheck(n)
		res.merge(a)
		p a unless a.empty?
	end
}


resInt = 0

res.each { |n|
	resInt = resInt + n
}



p resInt











