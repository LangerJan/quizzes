

#include <gmp.h>
#include <gmpxx.h>
#include <primesieve.hpp>

#include "../libeuler/include/libeuler.hpp"

#include <cstdint>
#include <cstdlib>
#include <iostream>
#include <set>
#include <vector>


int main(){
	uint64_t limit = 1000000;
	uint64_t res = 0;
	for(uint64_t den=2;den<=limit;den++){
		std::cout << den << std::endl;

		res += eulerPhi(den);
	}

	std::cout << res << std::endl;
	return EXIT_SUCCESS;
}
