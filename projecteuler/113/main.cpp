#include <iostream>
#include <map>
#include <stdexcept>
#include <set>
#include <cstdint>


using namespace std;


set<int>& getIncreasing(int x){
	static set<int>
		result0{1,2,3,4,5,6,7,8,9},
		result1{1,2,3,4,5,6,7,8,9},
		result2{2,3,4,5,6,7,8,9},
		result3{3,4,5,6,7,8,9},
		result4{4,5,6,7,8,9},
		result5{5,6,7,8,9},
		result6{6,7,8,9},
		result7{7,8,9},
		result8{8,9},
		result9{9};	

	switch(x){
		case 0: return result0;
		case 1: return result1;
		case 2: return result2;
		case 3: return result3;
		case 4: return result4;
		case 5: return result5;
		case 6: return result6;
		case 7: return result7;
		case 8: return result8;
		case 9: return result9;
		default: throw std::runtime_error("Illegal value");
	}
}



set<int>& getDecreasing(int x){
	static set<int>
		result0{0},
		result1{0,1},
		result2{0,1,2},
		result3{0,1,2,3},
		result4{0,1,2,3,4},
		result5{0,1,2,3,4,5},
		result6{0,1,2,3,4,5,6},
		result7{0,1,2,3,4,5,6,7},
		result8{0,1,2,3,4,5,6,7,8},
		result9{0,1,2,3,4,5,6,7,8,9};	

	switch(x){
		case 0: return result0;
		case 1: return result1;
		case 2: return result2;
		case 3: return result3;
		case 4: return result4;
		case 5: return result5;
		case 6: return result6;
		case 7: return result7;
		case 8: return result8;
		case 9: return result9;
		default: throw std::runtime_error("Illegal value");
	}
}
	

map<int, uint64_t> getAllIncreasing(map<int, uint64_t> howMuchOfEach){

	map<int, uint64_t> result;
	
	for(auto digit_count_pair : howMuchOfEach){
		int digit = digit_count_pair.first;
		uint64_t count = digit_count_pair.second;

		set<int> & increasing = getIncreasing(digit);
		for(auto incDigit : increasing){
			result[incDigit] += count;
		}
	}

	return result;
}


map<int, uint64_t> getAllDecreasing(map<int, uint64_t> howMuchOfEach){

	map<int, uint64_t> result;
	
	for(auto digit_count_pair : howMuchOfEach){
		int digit = digit_count_pair.first;
		uint64_t count = digit_count_pair.second;

		set<int> & decreasing = getDecreasing(digit);
		for(auto incDigit : decreasing){
			result[incDigit] += count;
		}
	}

	return result;
}


uint64_t countIncreasing(int digits){
	uint64_t result = 10;
	map<int, uint64_t> prevDigits;

	for(int i=1;i<10;i++) prevDigits[i]=1;

	for(int i=2;i<=digits;i++){
		map<int, uint64_t> followingDigits = getAllIncreasing(prevDigits);
		
		for(auto digit_count_pair : followingDigits) result += digit_count_pair.second;
		prevDigits = followingDigits;
	}

	return result;

}


uint64_t countDecreasing(int digits){
	uint64_t result = 9;
	map<int, uint64_t> prevDigits;

	for(int i=0;i<10;i++) prevDigits[i]=1;

	for(int i=2;i<=digits;i++){
		map<int, uint64_t> followingDigits = getAllDecreasing(prevDigits);
		
		for(auto digit_count_pair : followingDigits) result += digit_count_pair.second;
		prevDigits = followingDigits;
	}

	return result;

}

uint64_t solveFor(int digits){
	uint64_t increasing = countIncreasing(digits);
	uint64_t decreasing = countDecreasing(digits);
	uint64_t nonbouncy = increasing + decreasing;
	uint64_t doublettes = 0;
	if(digits >= 1)doublettes += digits * 10;
	
	uint64_t result = nonbouncy - doublettes;
	return result;
}

int main(){
	cout << "result: " << solveFor(100) << endl;
	return 0;
}


