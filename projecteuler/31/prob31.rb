
def kombinationen(zielwert, muenzauswahl)

	res = 0
	
	return 0 if zielwert < 0
	return 1 if zielwert == 0


	0.upto(muenzauswahl.size() -1){ |n|
		neuauswahl = muenzauswahl[0..n]
		res = res + kombinationen(zielwert - muenzauswahl[n], neuauswahl);
	}

	return res
end

auswahl = [200,100,50,20,10,5,2,1]
p kombinationen(200, auswahl)
