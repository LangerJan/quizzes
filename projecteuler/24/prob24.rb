require 'set'


# Implementierung der schriftlichen Schuldivision
# gibt die groesse der unendlichen periode zurück
def findCycle(number)
		
	endset = Set.new

	resteliste = Array.new
	dezimalliste = Array.new

	dezRest = Array.new

	rest = 1

	while rest != 0
		rest = rest * 10
		
		dezimalstelle = rest / number
		rest = rest % number

		if endset.include?([dezimalstelle, rest])
			ind = dezRest.rindex([dezimalstelle, rest])
			return dezRest.drop(ind).transpose[0].size
		end

		dezRest << [dezimalstelle, rest]

		dezimalliste << dezimalstelle
		resteliste << rest
		endset << [dezimalstelle, rest]

	end
	return 0
end


res = 0
resNum = 0

1.upto(999) { |n|


	x = findCycle(n)
	res = x unless x < res
	resNum = n if res == x
}

p res

p resNum
