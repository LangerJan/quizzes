
require 'prime'
require 'set'


#Wikipedia: Teilersumme. 4.Satz
def teilersumme(number)
	primfaktoren = number.prime_division
	teilersumme = 1
	primfaktoren.each { |n|
		teilersumme *= ((n[0]**(n[1]+1) - 1) / (n[0] -1))
	}
	teilersumme -= number
	return teilersumme
end

def isAbundant(number)
	return false if Prime.prime?(number)
	return teilersumme(number) > number
end



abundantNumbers = Array.new




1.upto(28123) { |n|
	unless Prime.prime?(n)
		abundantNumbers << n if isAbundant(n)
	end
}

sumAbundant = Set.new

abundantNumbers.combination(2).each { |n|
	sum = n[0] + n[1]
	sumAbundant << sum
}

abundantNumbers.each { |n|
	sumAbundant << (n*2)
}

res = 0

1.upto(28123) { |n|
	res +=  n unless sumAbundant.include?(n)
}

p res




