#include <iostream>
#include <vector>
#include <set>
#include <string>
#include <sstream>
#include <stdexcept>
#include <algorithm>


class dartThrow{
	public:
		typedef enum multiplier {SINGLE=1, DOUBLE=2, TRIPLE=3} multiplier;
	private:
		int m_basic_value;
		multiplier m_multiplier;
	public:
		dartThrow(int basic_value, multiplier multiplier)
			: m_basic_value(basic_value), m_multiplier(multiplier){};
		
		std::string toString() const {
			std::stringstream ss;
			switch(this->m_multiplier){
				case SINGLE:
					ss << "S";
					break;
				case DOUBLE:
					ss << "D";
					break;
				case TRIPLE:
					ss << "T";
					break;
				default:
					throw std::runtime_error("Unknown multiplier");
			}
			ss << this->m_basic_value;
			return ss.str();
		}

		bool operator<(const dartThrow& other) const {
			if(this->m_basic_value != other.m_basic_value){
				return this->m_basic_value < other.m_basic_value;
			}
			else
			{
				auto thisvalue = static_cast<std::underlying_type<multiplier>::type>(this->m_multiplier);
				auto othervalue = static_cast<std::underlying_type<multiplier>::type>(other.m_multiplier);
				return thisvalue < othervalue;
			}
		}

		int getValue(){
			return this->m_basic_value * static_cast<std::underlying_type<multiplier>::type>(this->m_multiplier);
		}
};




static std::vector<dartThrow> all_possible_throws;
static std::vector<dartThrow> all_possible_endthrows;

void init(){
	for(int i=1;i<=20;i++){
		all_possible_throws.push_back({i, dartThrow::SINGLE});
		all_possible_throws.push_back({i, dartThrow::DOUBLE});
		all_possible_throws.push_back({i, dartThrow::TRIPLE});
		all_possible_endthrows.push_back({i, dartThrow::DOUBLE});
	}

	all_possible_throws.push_back({25, dartThrow::SINGLE});
	all_possible_throws.push_back({25, dartThrow::DOUBLE});
	all_possible_endthrows.push_back({25, dartThrow::DOUBLE});
}

const std::vector<dartThrow> findAllThrows(int value, bool doubleOnly=false){
	std::vector<dartThrow> result;

	std::vector<dartThrow> *possible_throws;
	if(doubleOnly) possible_throws = &all_possible_endthrows;
	else possible_throws = &all_possible_throws;

	for(auto dthrow : *possible_throws){
		if(dthrow.getValue() == value){
			result.push_back(dthrow);
		}
	}

	return result;
}

std::set<std::vector<dartThrow>> findAllPossibleThrows(int points){
	std::set<std::vector<dartThrow>> combinations;


	{
		std::vector<dartThrow> dthrows = findAllThrows(points, true);
		for(auto throws : dthrows){
			combinations.insert({std::vector<dartThrow>({throws})});
		}
	}

	/*
	 * Start with the end: All doubles
	 */

	for(auto dthrow : all_possible_endthrows){

		int valSoFar = points - dthrow.getValue();
		if(valSoFar <= 0) continue;

		std::vector<dartThrow> currentThrows{dthrow};
		
		
		
		for(auto secondThrow : all_possible_throws){
			std::vector<dartThrow> inThrow{secondThrow, dthrow};
			int valAfterSecond = points - dthrow.getValue() - secondThrow.getValue();
			if(valAfterSecond < 0){
				continue;
			}
			else if(valAfterSecond == 0){
				combinations.insert(inThrow);
			}
			else {
				/*
				 * Continue with a win in 3 rounds
				 */
				for(auto thirdThrow : all_possible_throws){
					std::vector<dartThrow> thrdThrow{thirdThrow, secondThrow, dthrow};
					std::sort(thrdThrow.begin(), thrdThrow.end()-1);


					int valAfterThird = points - dthrow.getValue() - secondThrow.getValue() - thirdThrow.getValue();
					if(valAfterThird != 0){
						continue;
					}
					else{
						combinations.insert(thrdThrow);
					}
				}
			}
		}
	}


	return combinations;
}


int main(){
	init();

	std::set<std::vector<dartThrow>> ending;

	for(int i=2;i<100;i++){
		auto curEnding = findAllPossibleThrows(i);
		ending.insert(curEnding.begin(), curEnding.end());
	}


	std::cout << ending.size() << std::endl;

	return 0;
}
























