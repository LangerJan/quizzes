#include <iostream>
#include <string>
#include <sstream>
#include <vector>

typedef struct node node;

struct node{
	int head;
	node *left;
	node *right;
};

using namespace std;


void printElements(node *n){
	cout << "," << n->head << ",";
	if(n->left != NULL) printElements(n->left);
	if(n->right != NULL) printElements(n->right);
}

int maxSize(node *n){

	if(n == NULL) return 0;

	int max_left = maxSize(n->left);
	int max_right = maxSize(n->right);
	
	return n->head + (max_left>max_right?max_left:max_right);
}





void maxSizeNew(vector<vector<node>> *triangle){

	
	for(int j=triangle->size()-1; j > 0; j--  ){

		vector<node> &line = triangle->at(j);

		for(int i = 0; i < line.size() -1; i++){
			
			int arg0 = line[i].head;
			int arg1 = line[i+1].head;

			int bigger = arg0 > arg1?arg0:arg1;

			triangle->at(j-1)[i].head += bigger;
		}
	}
}

int main(){
	
	vector<vector<node>> triangle;

	string line;
	while(getline(cin, line)){
		vector<node> triline;
		stringstream linein(line);
		string number;
		while(getline(linein, number, ' ')){
			int x;
			x = stoi(number);
			node res = {x, NULL, NULL};
			triline.push_back(res);
		}
		triangle.push_back(triline);
	}


	
	int i = 0;
	for(auto &triline:triangle){
		int j = 0;
		for(node &head:triline){
			cout << "inspecting " << head.head << endl;
			if(i < triangle.size() - 1){
				cout << "put " << triangle[i+1][j].head << endl;
				head.left 	= &(triangle[i+1][j]);
				head.right 	= &(triangle[i+1][j+1]);
			}
			j++;
		}

		i++;
	}

	maxSizeNew(&triangle);

	cout << triangle[0][0].head << endl;

	return 0;
}















