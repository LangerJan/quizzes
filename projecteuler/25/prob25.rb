require 'prime'


def bruteForceQuadrat(a, b)
	res = 2
	n = 0
	
	its = -1
	while (Prime.prime?(res))
		res = n**2 + a * n + b
		n = n + 1
		its = its + 1
	end
	its
end


pair = nil

res = 0
(-1000..1000).each { |a|
	(-1000..1000).each { |b|
		kan = bruteForceQuadrat(a,b)
		res = kan if kan > res
		pair = [a,b] if kan == res
	}
	p pair.to_s + " = " + res.to_s + " = " + (pair[0]*pair[1]).to_s
}




