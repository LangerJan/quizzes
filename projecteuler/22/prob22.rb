

def namescore(name)
	name.downcase!
	
	res = 0
	
	name.each_byte { |b|
		if b > 96
			res = res + (b.to_i - 96)
		end
	}	
	return res

end



names = Array.new


File.open("sortednames.txt", "r") do |infile|
	while line = infile.gets
		names << line
	end
end


result = 0

i = 1
names.each { |name|

	result = result + (i * namescore(name))
	i = i +1

}

p result


