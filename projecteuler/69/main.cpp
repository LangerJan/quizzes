#include <iostream>
#include <gmp.h>
#include <gmpxx.h>
#include <primesieve.hpp>
#include <vector>
#include <tuple>
#include <cstdint>
#include <stdexcept>

std::vector<std::tuple<uint64_t, uint64_t>> factorize_naively(uint64_t value){

	std::vector<std::tuple<uint64_t, uint64_t>> result;
	primesieve::iterator it(0);
	uint64_t prime;	
	uint64_t exponent;


	while(value > 1){
		prime = it.next_prime();		
		exponent = 0;

		
		if(value % prime == 0){
			while(value % prime == 0){
				value = value / prime;
				exponent++;
			}
			result.push_back(std::make_tuple(prime, exponent));
		}
		
	}


	return result;
}

uint64_t euler_phi(uint64_t arg){

	uint64_t result = 1;

	std::vector<std::tuple<uint64_t,uint64_t>> primefactors = factorize_naively(arg);


	mpz_class power;
	mpz_class res = 1;

	for(auto factor : primefactors){
		uint64_t &prime = std::get<0>(factor);
		uint64_t &exponent = std::get<1>(factor);

		mpz_ui_pow_ui(power.get_mpz_t(), prime, exponent-1);	

		res *= power * (prime -1);


	}
	
	
	return res.get_ui();
}



int main(){

	uint64_t max_n_phi_i = 0;
	double max_n_phi = 0;

	for(uint64_t n=2;n<=1000000;n++){
		uint64_t cur_phi = euler_phi(n);
		double n_phi = (double)((double)n / cur_phi);
		if(n_phi > max_n_phi){
			max_n_phi = n_phi;
			max_n_phi_i = n;
		}
	}

	std::cout << std::endl << max_n_phi_i << std::endl;

	return 0;
}

