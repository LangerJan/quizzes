


def curiosity(number)
	res = 0
	number.to_s.each_char {|digit|
		dig = digit.to_i
		fac = 1
		if dig > 0
			fac = dig.downto(1).inject(:*)
		end
		res = res + fac
	}

	return res == number
end

res = 0

3.upto(1000000) {|n|

	if curiosity(n)
		p n
		res = res + n
	end
}



