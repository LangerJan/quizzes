

A,E,F,R

FREE
FARE
AREA
REEF


FREEFAREA
f(9) = 1

# 2-er Kombinationen:
* FREEF
* FAREA
* FAREEF
* REEFREE
* REEFARE

* FREEAREA
* FREEREEF

* FAREFREE
* FAREREEF

* AREAFREE
* AREAFARE
* AREAREEF

* REEFFREE
* REEFFARE


# 3-er Kombinationen

* FREEFARE (FREE REEF FARE)
* REEFAREA (REEF FARE AREA)
* FAREEFREE (FARE REEF FREE), darf nicht mit F weitergehen

* FREEAREAFARE (FREE AREA FARE)

FREE
FARE
AREA
REEF

* FREEF

# 4-er Kombinationen
* FREEFAREA (FREE REEF FARE AREA)

## Gültige Wörter

* *FREE*FARE*AREA*REEF*
* *FREEF*FAREA*
* *REEFREE[!F]*FAREA*
* *FREEF*FARE*AREA*
* *


