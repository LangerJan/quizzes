#include <iostream>
#include <set>
#include <string>
#include <vector>
#include <boost/algorithm/string.hpp>
#include <boost/range/algorithm/count.hpp>

#include <boost/regex.hpp>

using namespace std;
using namespace boost;


const array<const char, 4> letters = {'A', 'E', 'F', 'R'};
const set<string> keywords = {"FREE", "REEF", "AREA", "FARE"};

int countOccurrences(const string &needle, const string &haystack){
	int result = 0;

#if 0
	boost::regex expression(needle);

	boost::sregex_iterator m2;
	for(boost::sregex_iterator m1(haystack.begin(), haystack.end(), expression);m1!=m2;m1++){
		cout << *m1 << endl;
		result++;
	}
	return result;
#endif
	for(size_t pos = 0; pos!=string::npos;){
		pos = haystack.find(needle, pos);
		if(pos !=string::npos){
			result++;
			pos++;
		}
	}
	return result;
}

bool allOccurExactlyOnce(string word, const set<string> &keywords){
	bool result = true;
	for(const string &keyword : keywords){
		int cnt = countOccurrences(keyword, word);
		if(cnt != 1){
			result = false;
			break;
		}
	}
	return result;
}

bool increaseWord(string &placebed){
	// Increase to next word
	bool increased = false;
	for(size_t i=0;i<placebed.size() && !increased;i++){
		char currLetter = placebed.at(i);
		int lettersPos = -2;
		for(size_t j=0;j<letters.size();j++){
			if(letters.at(j) == currLetter){
				lettersPos = j;
				break;
			}
		}

		placebed.at(i) = letters.at((lettersPos+1)%letters.size());

		if(lettersPos == letters.size()-1){
			placebed.at(i) = letters.at(0);
		}
		else {
			placebed.at(i) = letters.at(lettersPos+1);
			increased = true;
		}
	}
	return increased;
}

set<string> findAllWordsWith(int length, const set<string> keywords){
	set<string> result;
	
	string placebed;
	for(int i=0;i<length;i++) placebed.push_back(letters.at(0));
	
	bool increased = false;
	do{

		if(allOccurExactlyOnce(placebed, keywords)){
			result.insert(placebed);
		}
		increased = increaseWord(placebed);
	}while(increased);

	return result;
}

/*
 * 4-words:
 * FREEFAREA
 */
int main(){

	set<string> keywordSet = {"FARE"};
	for(string x : findAllWordsWith(10, keywords)){
		cout << x << endl;
	}
	
	

	return 0;
}
