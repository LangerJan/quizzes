#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <numeric>
#include <iomanip>
#include <sstream>
#include <bitset>

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/numeric/conversion/cast.hpp>

using namespace std;

vector<int> getInput(std::string line){
    boost::trim(line);
    vector<int> result;
    for(char c : line) {
        result.push_back(c);
    }

    vector<int> addThis{17, 31, 73, 47, 23};
    result.insert(result.end(), addThis.begin(), addThis.end());

    return result;
}

vector<int> knotHash(vector<int> input, vector<int> lengths, int &position, int &skip_size){
    for(int length : lengths){
        vector<int> subInput;
        for(int i=0;i<length;i++){
            subInput.push_back(input.at((i+position)%input.size()));
        }
        reverse(begin(subInput), end(subInput));
        for(int i=0;i<length;i++){
            input.at((i+position)%input.size()) = subInput.at(i);
        }

        position = (position + length + skip_size) % input.size();
        skip_size = (skip_size+1)%input.size();
    }
    return input;
}

string solvePartTwo(string line){
    vector<int> lengths = getInput(line);

    vector<int> input(256);
    std::iota (std::begin(input), std::end(input), 0);

    int position = 0;
    int skip_size = 0;
    for(int i=0;i<64;i++){
        vector<int> output = knotHash(input, lengths, position, skip_size);
        input = output;
    }

    stringstream result;

    for(int i=0;i<16;i++){
        int dense = 0;
        for(int j=0;j<16;j++){
            dense = dense ^ input.at((i*16)+j);
        }
        result << setfill('0') << setw(2) << hex << dense;
    }

    return result.str();
}

string toBits(string input){
    stringstream result;

    for(char c : input){
        stringstream ss;
        ss << hex << string(&c, 1);
        unsigned n;
        ss >> n;
        bitset<4> b(n);
        result << b.to_string();
   }

   return result.str();
}


void colorDiskFill(vector<string> &disk, int x, int y, char color){
    if(disk.at(y).at(x) != '1') return;
    else
    {
        disk.at(y).at(x) = color;
        if(x > 0) colorDiskFill(disk, x-1, y, color);
        if((x+1) < disk.at(y).size()) colorDiskFill(disk, x+1, y, color);
        if(y > 0) colorDiskFill(disk, x, y-1, color);
        if((y+1) < disk.size()) colorDiskFill(disk, x, y+1, color);
    }
}


int main(){

    string line;
    getline(cin, line);

    int result = 0;

    int regions = 0;

    vector<string> disk;

    for(int i=0;i<128;i++){
        string input = line + "-" + to_string(i);

        string hash = solvePartTwo(input);
        string bits = toBits(hash);

        result += std::count(bits.begin(), bits.end(), '1');

        disk.push_back(bits);
    }

    for(int y=0;y<disk.size();y++){
        string &line = disk.at(y);
        for(int x=0;x<line.size();x++){
            if(disk.at(y).at(x) == '1'){
                static char color = 'a';
                colorDiskFill(disk, x, y, color);
                color = color == 'z'?'a':color + 1;
                
                regions++;
            }
        }
    }


    cout << result << endl;
    cout << regions << endl;

    return 0;
}





