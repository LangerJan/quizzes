#include <cstdint>
#include <functional>
#include <iostream>
#include <stdexcept>
#include <string>
#include <map>
#include <vector>

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

using namespace std;

class cpu{
	public:

	map<char, int64_t> m_registers;
	int64_t m_pc;
	int64_t m_mulCnt;

	cpu(){
		m_registers['a'] = 0;
		m_registers['b'] = 0;
		m_registers['c'] = 0;
		m_registers['d'] = 0;
		m_registers['e'] = 0;
		m_registers['f'] = 0;
		m_registers['g'] = 0;
		m_registers['h'] = 0;

		m_pc = 0;
		m_mulCnt = 0;
	}

	int64_t getValueOf(string numOrReg){
		auto it = m_registers.find(numOrReg.at(0));
		if(it == m_registers.end()){
			return boost::lexical_cast<int64_t>(numOrReg);
		}
		else
		{
			return it->second;
		}
	}

	void setRegister(string numOrReg0, string numOrReg1){
		if(numOrReg0.empty()){
			throw runtime_error("invalid arg0");
		}

		if(m_registers.find(numOrReg0[0]) == m_registers.end()){
			cerr << numOrReg0 << endl;
			throw runtime_error("invalid arg0 num");
		}

		m_registers[numOrReg0[0]] = getValueOf(numOrReg1);

	}

	void setRegister(string numOrReg0, int64_t num1){
		if(numOrReg0.empty()){
			throw runtime_error("invalid arg0");
		}

		if(m_registers.find(numOrReg0[0]) == m_registers.end()){
			cerr << numOrReg0 << endl;
			throw runtime_error("invalid arg0 num");
		}

		m_registers[numOrReg0[0]] = num1;

	}

};



vector<function<void(cpu&)>> readInput(){
	
	vector<function<void(cpu&)>> result;


	for(string line;getline(cin, line);){
		
		vector<string> strs;
		boost::split(strs,line,boost::is_any_of(" "));

		if(strs.size() != 3){
			cerr << line << endl;
			throw runtime_error("Invalid input");
		}

		string arg0 = strs.at(1);
		string arg1 = strs.at(2);

		if(boost::equals(strs.at(0), "set")){
			result.push_back([arg0, arg1](cpu &l_cpu){
				l_cpu.setRegister(arg0, arg1);
				l_cpu.m_pc++;
			});
		}
		else if(boost::equals(strs.at(0), "sub")){
			result.push_back([arg0, arg1](cpu &l_cpu){
				int64_t op1 = l_cpu.getValueOf(arg0);
				int64_t op2 = l_cpu.getValueOf(arg1);
				l_cpu.setRegister(arg0, op1-op2);
				l_cpu.m_pc++;
			});

		}
		else if(boost::equals(strs.at(0), "mul")){
			result.push_back([arg0, arg1](cpu &l_cpu){
				int64_t op1 = l_cpu.getValueOf(arg0);
				int64_t op2 = l_cpu.getValueOf(arg1);
				l_cpu.setRegister(arg0, op1*op2);
				l_cpu.m_pc++;
				l_cpu.m_mulCnt++;
			});

		}
		else if(boost::equals(strs.at(0), "jnz")){
			result.push_back([arg0, arg1](cpu &l_cpu){
				int64_t op1 = l_cpu.getValueOf(arg0);
				int64_t op2 = l_cpu.getValueOf(arg1);

				if(op1 != 0){
					l_cpu.m_pc += op2;
				}
				else
				{
					l_cpu.m_pc++;
				}
			});
		}
		else
		{
			cerr << line << endl;
			throw runtime_error("Invalid op");
		}

	}

	return result;
}


int main(void){
	
	vector<function<void(cpu&)>> instructions = readInput();

	cpu l_cpu;

	while(l_cpu.m_pc >= 0 && l_cpu.m_pc < instructions.size()){

		instructions.at(l_cpu.m_pc)(l_cpu);

	}

	cout << l_cpu.m_mulCnt << endl;
}




