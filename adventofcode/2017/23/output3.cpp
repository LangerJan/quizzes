#include <iostream>
#include <cstdint>

#include <primesieve.hpp>
#include <vector>
#include <set>

using namespace std;

int main(void){

	vector<int64_t> primes;
	primesieve::generate_n_primes(40000, &primes);
	set<int64_t> primeSet(primes.begin(), primes.end());

	int64_t a = 1;
	int64_t b = 0;
	int64_t c = 0;
	int64_t d = 0;
	int64_t e = 0;
	int64_t f = 0;
	int64_t g = 0;
	int64_t h = 0;
	
	b = 109900;
	c = 126900;
	
	do {
		f = 1;
		d = 2;

		int64_t b_vorher = b;
		int64_t d_vorher = d;
		int64_t e_vorher = e;
		int64_t f_vorher = f;

		f = (primeSet.find(b) == primeSet.end())?0:1;


		if(f == 0){
			h++;
		}
		g = b;
		g = g - c;

		if(g != 0){
			b = b + 17;
		}
		else {
			break;
		}
	} while(true);
	std::cout << h << std::endl;
	return 0;
}
