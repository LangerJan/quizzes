#include <cstdint>
#include <functional>
#include <iostream>
#include <stdexcept>
#include <string>
#include <map>
#include <vector>

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

using namespace std;

void transpileInput(){
	cout << "#include <iostream>" << endl;
	cout << "#include <cstdint>" << endl;
	cout << "int main(void){" << endl;
	
	cout << "int64_t a = 1;" << endl;
	cout << "int64_t b = 0;" << endl;
	cout << "int64_t c = 0;" << endl;
	cout << "int64_t d = 0;" << endl;
	cout << "int64_t e = 0;" << endl;
	cout << "int64_t f = 0;" << endl;
	cout << "int64_t g = 0;" << endl;
	cout << "int64_t h = 0;" << endl;




	int pc_num = 0;
	for(string line;getline(cin, line);){
		cout << "label_line_" << pc_num << ": \t";


		vector<string> strs;
		boost::split(strs,line,boost::is_any_of(" "));

		if(strs.size() != 3){
			cerr << line << endl;
			throw runtime_error("Invalid input");
		}

		string arg0 = strs.at(1);
		string arg1 = strs.at(2);

		if(boost::equals(strs.at(0), "set")){
			cout << arg0 << " = " << arg1 << ";"; 
		}
		else if(boost::equals(strs.at(0), "sub")){
			cout << arg0 << " = " << arg0 << " - " << arg1 << ";";
		}
		else if(boost::equals(strs.at(0), "mul")){
			cout << arg0 << " = " << arg0 << " * " << arg1 << ";";
		}
		else if(boost::equals(strs.at(0), "jnz")){
			int64_t offset = boost::lexical_cast<int64_t>(arg1);
			cout << "if(" << arg0 << " != 0){ goto label_line_" << pc_num + offset << ";}";
		}
		else
		{
			cerr << line << endl;
			throw runtime_error("Invalid op");
		}
		cout << endl;
		pc_num++;
	}
	cout << "label_line_" << pc_num << ":" << endl;

	cout << "std::cout << h << std::endl;" << endl;

	cout << "return 0;" << endl << "}" << endl;

}


int main(void){
	transpileInput();
	return 0;
}




