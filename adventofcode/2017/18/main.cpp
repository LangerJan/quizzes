#include <iostream>
#include <map>
#include <functional>
#include <string>
#include <regex>
#include <vector>
#include <stdexcept>
#include <cstdint>

#include <boost/lexical_cast.hpp>

using namespace std;
using namespace boost;

class soundcard{
	public:
	
	bool m_halted = false;
	int m_pc = 0;
	int m_lastPlayed = -1;
	map<string, int64_t> m_reg;

	vector<function<void()>> m_instructions;
	


	int64_t getValue(string arg){
		try{
			return lexical_cast<int64_t>(arg);
		}
		catch(bad_lexical_cast &ex){
			auto it = this->m_reg.find(arg);
			if(it == this->m_reg.end()){
				this->m_reg[arg] = 0;
			}

			return m_reg[arg];
		}
	}

	void handleSND(vector<string> args){
		this->m_lastPlayed = getValue(args.at(0));
		this->m_pc++;
	}
	
	void handleSET(vector<string> args){
		this->m_reg[args.at(0)] = getValue(args.at(1));
		this->m_pc++;
	}

	void handleADD(vector<string> args){
		this->m_reg[args.at(0)] += getValue(args.at(1));
		this->m_pc++;
	}

	void handleMUL(vector<string> args){
		this->m_reg[args.at(0)] *= getValue(args.at(1));
		this->m_pc++;
	}
	
	void handleMOD(vector<string> args){
		this->m_reg[args.at(0)] %= getValue(args.at(1));
		this->m_pc++;
	}
	
	void handleRCV(vector<string> args){
		if(getValue(args.at(0)) != 0){
			cout << "RCV: " << this->m_lastPlayed << endl;
			this->m_halted = true;
		}
		this->m_pc++;
	}

	void handleJGZ(vector<string> args){
		if(getValue(args.at(0)) > 0){
			this->m_pc += getValue(args.at(1));
		}
		else {
			this->m_pc++;
		}
	}

	void readInstructions(){
		for(string line;getline(cin, line);){
			regex rex("(\\w*) ([-\\w*])\\ ?([-\\w]*)");
			smatch smat;
			if(regex_match(line, smat, rex)){
				std::string op = smat[1];
				vector<string> args;
				for(int i=2;i<smat.size();i++) args.push_back(smat[i]);
				
				if(op.compare("snd") == 0) m_instructions.push_back(bind(&soundcard::handleSND, this, args));
				else if(op.compare("set") == 0) m_instructions.push_back(bind(&soundcard::handleSET, this, args));
				else if(op.compare("add") == 0) m_instructions.push_back(bind(&soundcard::handleADD, this, args));
				else if(op.compare("mul") == 0) m_instructions.push_back(bind(&soundcard::handleMUL, this, args));
				else if(op.compare("mod") == 0) m_instructions.push_back(bind(&soundcard::handleMOD, this, args));
				else if(op.compare("rcv") == 0) m_instructions.push_back(bind(&soundcard::handleRCV, this, args));
				else if(op.compare("jgz") == 0) m_instructions.push_back(bind(&soundcard::handleJGZ, this, args));
				else{
					throw runtime_error("unknown opcode");
				}
			}
			else
			{
				cerr << line << endl;
				throw runtime_error("invalid op");
			}

		}
	}

	void run(){
		while(this->m_pc >= 0 && this->m_pc < this->m_instructions.size() && !this->m_halted){
			this->m_instructions.at(this->m_pc)();
		}
	}
};

int main(){
	soundcard sc;
	sc.readInstructions();
	sc.run();

	for(auto pair : sc.m_reg){
		cout << pair.first << ":" << pair.second << endl;
	}

	return 0;
}




