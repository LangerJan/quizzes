#include <iostream>
#include <map>
#include <functional>
#include <string>
#include <regex>
#include <vector>
#include <stdexcept>
#include <cstdint>
#include <memory>
#include <deque>

#include <boost/lexical_cast.hpp>

using namespace std;
using namespace boost;

class soundcard{
	public:
	
	soundcard(int64_t id){
		this->m_reg["p"] = id;
		this->m_id = id;
	}

	int m_id;	
	bool m_halted = false;
	int m_pc = 0;
	int m_lastPlayed = -1;
	map<string, int64_t> m_reg;


	bool m_waiting = false;

	int m_snd_cnt = 0;

	vector<function<void()>> m_instructions;
	
	std::shared_ptr<soundcard> m_partner;
	deque<int64_t> m_msgQueue;
	

	int64_t getValue(string arg){

		if(arg[0] >= 'a' && arg[0] <= 'z'){
			auto it = this->m_reg.find(arg);
			if(it == this->m_reg.end()){
				this->m_reg[arg] = 0;
			}
			return m_reg[arg];
		}
		else
		{
			return lexical_cast<int64_t>(arg);
		}
	}

	void handleSND(vector<string> args){
		this->m_partner->m_msgQueue.push_front(getValue(args.at(0)));
		this->m_partner->m_waiting = false;
		this->m_snd_cnt++;
		this->m_pc++;
	}

	void handleRCV(vector<string> args){
		if(this->m_msgQueue.empty()){
			this->m_waiting = true;
		}
		else
		{
			this->m_reg[args.at(0)] = this->m_msgQueue.back();
			this->m_msgQueue.pop_back();
			this->m_pc++;
		}
	}
	
	void handleSET(vector<string> args){
		this->m_reg[args.at(0)] = getValue(args.at(1));
		this->m_pc++;
	}

	void handleADD(vector<string> args){
		this->m_reg[args.at(0)] += getValue(args.at(1));
		this->m_pc++;
	}

	void handleMUL(vector<string> args){
		this->m_reg[args.at(0)] *= getValue(args.at(1));
		this->m_pc++;
	}
	
	void handleMOD(vector<string> args){
		this->m_reg[args.at(0)] %= getValue(args.at(1));
		this->m_pc++;
	}

	void handleJGZ(vector<string> args){
		if(getValue(args.at(0)) > 0){
			this->m_pc += getValue(args.at(1));
		}
		else {
			this->m_pc++;
		}
	}

	void readInstructions(vector<string> instructions){
		for(string line : instructions){
			regex rex("(\\w*) ([-\\w*])\\ ?([-\\w]*)");
			smatch smat;
			if(regex_match(line, smat, rex)){
				std::string op = smat[1];
				vector<string> args;
				for(int i=2;i<smat.size();i++) args.push_back(smat[i]);
				
				if(op.compare("snd") == 0) m_instructions.push_back(bind(&soundcard::handleSND, this, args));
				else if(op.compare("set") == 0) m_instructions.push_back(bind(&soundcard::handleSET, this, args));
				else if(op.compare("add") == 0) m_instructions.push_back(bind(&soundcard::handleADD, this, args));
				else if(op.compare("mul") == 0) m_instructions.push_back(bind(&soundcard::handleMUL, this, args));
				else if(op.compare("mod") == 0) m_instructions.push_back(bind(&soundcard::handleMOD, this, args));
				else if(op.compare("rcv") == 0) m_instructions.push_back(bind(&soundcard::handleRCV, this, args));
				else if(op.compare("jgz") == 0) m_instructions.push_back(bind(&soundcard::handleJGZ, this, args));
				else{
					throw runtime_error("unknown opcode");
				}
			}
			else
			{
				cerr << line << endl;
				throw runtime_error("invalid op");
			}

		}
	}

	void run(){
		while(this->m_pc >= 0 && this->m_pc < this->m_instructions.size() && !this->m_halted){
			this->step();
		}
	}

	void step(){
		if(!this->m_waiting && !this->m_halted && this->m_pc >=0 && this->m_pc < this->m_instructions.size()){
			this->m_instructions.at(this->m_pc)();
		}
	}

	bool isStopped(){
		return (this->m_waiting || this->m_halted || this->m_pc < 0 || this->m_pc >= this->m_instructions.size());

	}

	string toString(){
		stringstream ss;
		ss << "soundcard " << this->m_id << "(" << (isStopped()?"stopped":"running") << "):" << endl;
		for(auto pair : this->m_reg){
			ss << pair.first << ": " << pair.second << endl;
		}
		ss << "msgQueueWait: " << (this->m_waiting?"yes":"no") << endl
		   << "halted: " << (this->m_halted?"yes":"no") << endl
		   << "pc: " << this->m_pc << endl
		   << "send count: "<< this->m_snd_cnt << endl;
		return ss.str();
	}
};

int main(){
	std::shared_ptr<soundcard> sc_one = std::make_shared<soundcard>(0);
	std::shared_ptr<soundcard> sc_two = std::make_shared<soundcard>(1);
	sc_one->m_partner = sc_two;
	sc_two->m_partner = sc_one;

	vector<string> instr;
	for(string line;getline(cin, line);)instr.push_back(line);

	sc_one->readInstructions(instr);
	sc_two->readInstructions(instr);

	while(!sc_one->isStopped() || !sc_two->isStopped()){
		sc_one->step();
		sc_two->step();
	}

	cout << sc_one->toString() << endl << sc_two->toString() << endl;

	return 0;
}




