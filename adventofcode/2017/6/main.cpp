#include <vector>
#include <set>
#include <iostream>
#include <algorithm>
#include <iterator>

using namespace std;

void redistribute(vector<int> &layout){
    auto maxelem = max_element(layout.begin(),layout.end());
    int val = *maxelem;
    *(maxelem++) = 0;

    while(val-- > 0){   
        if(maxelem == layout.end()) maxelem = layout.begin();
        (*maxelem++)++;
    }
}

int countCycles(vector<int> &layout){
    set<vector<int>> seenBefore;
    int iterations = 0;
    for(;seenBefore.find(layout) == seenBefore.end();iterations++){
        seenBefore.insert(layout);
        redistribute(layout);
    }
    return iterations;
}   

int main(){
    vector<int> layout{std::istream_iterator<int>{std::cin}, {}};
    cout << countCycles(layout) << "\t";
    cout << countCycles(layout) << endl;
    return 0;
}

