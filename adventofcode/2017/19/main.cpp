#include <vector>
#include <string>
#include <tuple>
#include <stdexcept>
#include <iostream>


using namespace std;

typedef tuple<int, int> direction;
typedef tuple<int, int> position;


vector<string> readInput(){
	vector<string> result;
	
	for(string line; getline(cin, line);){
		result.push_back(line);
	}

	return result;
}

position findStart(vector<string> &map){
	int i=0;
	for(auto ch : map[0]){
		if(ch == '|') return {0, i};
		i++;
	}
	return {-1, -1};
}

bool handleNextPos(vector<string> &map, position &posYX, direction &dirYX, string &str){
	int posx = get<1>(posYX);
	int posy = get<0>(posYX);

	int dirX = get<1>(dirYX);
	int dirY = get<0>(dirYX);

	int nextPosx, nextPosy;
	int nextDirx = dirX;
	int nextDiry = dirY;

#if 0
	cout << posy << "/" << posx << ":" << " '" << map[posy][posx] << "'" << endl;
#endif
	switch(map[posy][posx]){
		case ' ':
		{
			cerr << posy << "/" << posx << endl;
			throw std::runtime_error("Took a wrong turn");
		}
		case '|':
		case '-':
			nextPosx = posx + dirX;
			nextPosy = posy + dirY;
		break;
		case '+':
		{

			vector<direction> directions{ {-1,0},{1,0},{0,-1},{0,1} };
			bool found = false;
			for(direction nextDir : directions){
				nextDirx = get<1>(nextDir);
				nextDiry = get<0>(nextDir);
				if(nextDiry == -dirY && nextDirx == -dirX) continue;

				nextPosx = posx + nextDirx;
				nextPosy = posy + nextDiry;

				if(nextPosy < 0 || nextPosy >= map.size()) continue;
				if(nextPosx < 0 || nextPosx >= map[nextPosy].size()) continue;
				if(map[nextPosy][nextPosx] == ' ') continue;
#if 0
				cout << "suitable pos: " << nextPosy << "/" << nextPosx << ":" << " '" << map[nextPosy][nextPosx] << "'" << endl;
				cout << "suitable dir: " << nextDiry << "/" << nextDirx << ":" << " '" << map[nextPosy][nextPosx] << "'" << endl;
#endif
				found = true;
				break;
			}
			if(!found) return false;

			break;
		}
		default:
			cout << "d:" << map[posy][posx] << endl;
			nextPosx = posx + dirX;
			nextPosy = posy + dirY;
			str.append(&map[posy][posx], 1);
			break;
	}

	get<1>(posYX) = nextPosx;
	get<0>(posYX) = nextPosy;

	get<1>(dirYX) = nextDirx;
	get<0>(dirYX) = nextDiry;

#if 0
	cout << "next direction "  << nextDiry << "/" << nextDirx << endl;
	cout << "next position "   << nextPosy << "/" << nextPosx << endl;
#endif
	if(map[nextPosy][nextPosx] == ' ') return false;
	return true;
}



int main(){
	vector<string> map = readInput();

	string result;
	direction currDir_y_x{1,0};
	position  currPos_y_x = findStart(map);

	int steps = 1;
	while(handleNextPos(map, currPos_y_x, currDir_y_x, result)){
		steps++;
	};	

	cout << result << "\t" << steps << endl;

	return 0;
}










