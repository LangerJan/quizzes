#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <algorithm>
#include <set>
#include <stdexcept>

std::vector<std::vector<int>> collectNumbers(){
	std::vector<std::vector<int>> result;

	std::string line;
	while(std::getline(std::cin, line)){
		std::stringstream linestream(line);
		std::vector<int> numbers;
		while(linestream){
			int num;
			linestream >> num;
			if(linestream){
				numbers.push_back(num);
			}
		}
		result.push_back(numbers);
	}
	
	return result;
}

int calc_division(std::vector<int> &row){
	for(auto i=row.begin();i!=row.end();i++){
		for(auto j=i+1;j<row.end();j++){
			std::cout << *i << "/" << *j << std::endl;
			if((*j) % (*i) == 0) return (*j) / (*i);
		}
	}
	throw std::runtime_error("invalid situation");
}


int main(){
	std::vector<std::vector<int>> numbers = collectNumbers();

	int sum = 0;
	int calc_sum = 0;
	for(auto row : numbers){
		std::sort(row.begin(), row.end());
	
		calc_sum += calc_division(row);
		sum += (row[row.size()-1] - row[0]);

	}
	
	std::cout << sum << "\t" << calc_sum << std::endl;
	return 0;
}


