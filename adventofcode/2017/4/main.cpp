#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <algorithm>
#include <boost/algorithm/string.hpp>

bool containsNoDoubleWord(std::vector<std::string> words){
    return std::set<std::string>(words.begin(), words.end()).size() == words.size();
}

bool containsNoAnagrams(std::vector<std::string> words){
    std::for_each(words.begin(), words.end(), [](std::string &x){std::sort(x.begin(),x.end());});
    return containsNoDoubleWord(words);
}

int main(){
    int result = 0;
    int result_new_policy = 0;
    std::string line;
    while(std::getline(std::cin, line)){
        std::vector<std::string> words;
        boost::split(words, line, boost::is_any_of(" "));
        if(containsNoDoubleWord(words)) result++;
        if(containsNoAnagrams(words)) result_new_policy++;
    }
    std::cout << result << "\t" << result_new_policy << std::endl;
    return 0;
}
