#include <iostream>
#include <string>

using namespace std;

int main(){

	string line;
	getline(cin, line);

	bool garbageMode = false;
	bool ignoreNextLine = false;
	
	int level = 0;
	int result = 0;
	int garbage_count = 0;

	for(auto c : line){
		
		if(garbageMode){
			if(ignoreNextLine)ignoreNextLine = false;
			else if(c == '!')ignoreNextLine = true; 
			else if(c == '>')garbageMode = false;
			else garbage_count++;
		}
		else {
			if(c == '{')result += ++level;
			else if(c == '<')garbageMode = true;
			else if(c == '}') level--;
		}
	}

	cout << result << "\t" << garbage_count << endl;
	return 0;
}
