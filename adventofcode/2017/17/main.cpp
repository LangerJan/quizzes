#include <deque>
#include <iostream>

using namespace std;

class spinlock{
    public:
    spinlock(size_t stepsize) : m_stepSize(stepsize){}

    size_t m_stepSize;
    size_t m_currentPosition = 0;
    size_t m_nextValue = 1;
    deque<int> m_deque{0};

    void step(){
        this->m_currentPosition = (this->m_currentPosition + this->m_stepSize) % this->m_deque.size();
        this->m_deque.insert(this->m_deque.begin()+m_currentPosition+1, this->m_nextValue++);
        this->m_currentPosition++;
    }

    void print(){
        for(size_t i=0;i<this->m_deque.size();i++){
            if(i == this->m_currentPosition) cout << "(";
            cout << this->m_deque[i];
            if(i == this->m_currentPosition) cout << ")";
            cout << " ";
        }
        cout << endl;
    }
};

class pseudo_spinlock{
    public:
    pseudo_spinlock(size_t stepSize) : m_stepSize(stepSize){}

    size_t m_stepSize;
    size_t m_currentPosition = 0;
    size_t m_nextValue = 1;
    size_t m_dequeSize = 1;
    size_t m_valAtPosOne = 0;


    void step(){
        this->m_currentPosition = (this->m_currentPosition + this->m_stepSize) % this->m_dequeSize;
        
        {
            if(this->m_currentPosition == 0) this->m_valAtPosOne = this->m_nextValue;
            this->m_nextValue++;
            this->m_dequeSize++;
        }

        this->m_currentPosition++;
    }

};


void solveOne(){
    spinlock spinner{356};

    for(int i=0;i<2017;i++){
        spinner.step();
    }

    cout << spinner.m_deque[spinner.m_currentPosition+1] << endl;
}

void solveTwo(){
    pseudo_spinlock spinner{356};

    for(int i=0;i<50000000;i++){
        spinner.step();
    }

    cout << spinner.m_valAtPosOne << endl;
}
int main(){

    solveOne();
    solveTwo();   
    return 0;
}

