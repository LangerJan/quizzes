#include <iostream>
#include <string>
#include <regex>
#include <limits>
#include <set>

#include <boost/lexical_cast.hpp>

using namespace std;
using namespace boost;

map<int, int> readInput(){
    map<int,int> result;
    regex input_rex("(\\d+): (\\d+)");
    for(string line; getline(cin, line);){
        smatch match;
        if(regex_match(line, match, input_rex)){
            int layer = lexical_cast<int>(match[1]);
            int depth = lexical_cast<int>(match[2]);
            result[layer] = depth;
        }
    }

    return result;
}

tuple<int, bool> severity(map<int, int> &firewall, int time){

    bool everCaught = false;
    int result = 0;

    for(auto pair : firewall){

        int layer = pair.first;
        int depth = pair.second;

        int interval = (depth-1)*2;
        int timeAtLayer = time + layer;

        bool scannerPosZero = (timeAtLayer % interval == 0);

        if(scannerPosZero){
            result += (layer*depth);
            everCaught = true;
        }

    }

    return {result, everCaught};
}


int calcDelay(map<int,int> &firewall){

    int result;
    for(result=0;result<numeric_limits<int>::max();result++){
        if(get<1>(severity(firewall, result)) == false) break;
    }
    return result;
}


int main(){

    map<int,int> firewall = readInput();

    cout << get<0>(severity(firewall, 0)) << endl;
    cout << calcDelay(firewall) << endl;
    return 0;
}

