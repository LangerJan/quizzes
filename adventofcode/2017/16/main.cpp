#include <string>
#include <iostream>
#include <functional>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <vector>
#include <regex>
#include <stdexcept>
#include <algorithm>

using namespace std;
using namespace std::placeholders;

void spinFunc(string &str, int arg0){
		string subs = str.substr(str.size()-arg0);
		string keep = str.substr(0,str.size()-arg0);
		str.clear();
		str.append(subs);
		str.append(keep);
}

void exchangeFunc(string &str, int arg0, int arg1){
    swap(str[arg0], str[arg1]);
}

void partnerFunc(string &str, char arg0, char arg1){
	swap(str[str.find(arg0)], str[str.find(arg1)]);
}

function<void(string&)> spin(string instr){
	regex reg("s(\\d+)");
	smatch mat;
	if(regex_match(instr, mat, reg)){
		int arg0 = boost::lexical_cast<int>(mat[1]);
        return bind(spinFunc, _1, arg0);
	}
	else
	{
		throw runtime_error("spin error");
	}
}

function<void(string&)> exchange(string instr){
	regex reg("x(\\d+)/(\\d+)");
	smatch mat;
	if(regex_match(instr, mat, reg)){
		int arg0 = boost::lexical_cast<int>(mat[1]);
		int arg1 = boost::lexical_cast<int>(mat[2]);
        return bind(exchangeFunc, _1, arg0, arg1);
	}
	else
	{
		throw runtime_error("exchange error");
	}
}

function<void(string&)> partner(string instr){
    return bind(partnerFunc, _1, instr[1], instr[3]);
}

vector<function<void(string&)>> parse(vector<string> instr){
    vector<function<void(string&)>> result;
	for(auto ins : instr){
		switch(ins[0]){
			case 's':
				result.push_back(spin(ins));
				break;
			case 'x':
				result.push_back(exchange(ins));
				break;
			case 'p':
				result.push_back(partner(ins));
				break;
			default:
				throw runtime_error("Invalid opcode");
		}
	}
    return result;
}


void solveOne(string start, vector<string> instr){
    for(auto fkt : parse(instr)){
        fkt(start);
    }
	cout << start << endl;
}

int64_t determineLoop(string start, vector<function<void(string&)>> fkts, int64_t maxRounds){
    string round0 = start;

    for(int64_t i=1;i<=maxRounds;i++){
        for(auto fkt : fkts) fkt(start);

        if(boost::equals(start, round0)){
            return i;
        }
    }
    return maxRounds+1;
}

string loop(string start, vector<function<void(string&)>> fkts, int64_t rounds){
    for(int64_t i=0;i<rounds;i++){
        for(auto fkt : fkts) fkt(start);
    }
    return start;
}

void solveTwo(string start, vector<string> instr){

    auto fkts = parse(instr);
    int64_t maxRounds = determineLoop(start, fkts, 1000000000);

    cout << "found loop at " << maxRounds << endl;
    
    int64_t remainder = 1000000000 % maxRounds;

    cout << "So loop only " << remainder << " times " << start << endl;

    cout << loop(start, fkts, remainder) << endl;
}


int main(){

	string start("abcdefghijklmnop");
    //string start("abcde");

	string input;
	getline(cin, input);

	vector<string> instr;
	boost::algorithm::split(instr, input, boost::is_any_of(","));

	solveOne(start, instr);
	solveTwo(start, instr);

	return 0;
}
