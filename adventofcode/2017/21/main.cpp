#include <algorithm>
#include <iostream>
#include <map>
#include <memory>
#include <regex>
#include <set>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

#include <boost/algorithm/string.hpp>

using namespace std;


string flip_h_2x2(string input);
string flip_v_2x2(string input);
string flip_h_3x3(string input);
string flip_v_3x3(string input);

string rotate_clk_2x2(string input);
string rotate_clk_3x3(string input);

set<string> allFlips_2x2(string input);
set<string> allFlips_3x3(string input);

set<string> allRotations_2x2(string input);
set<string> allRotations_3x3(string input);

set<string> allExtraKeys(string input);

map<string, string> readRulebook();

string iterate(string input, map<string, string> &rulebook);

void print_it(string input){
	replace(input.begin(), input.end(), '/', '\n');
	cout << input << endl;
}

int main(void){

	string round = ".#./..#/###";
	map<string, string> rulebook = readRulebook();
	
	for(auto rule : rulebook){
		cout << rule.first << " -> " << rule.second << endl;
	}


	for(int i=0;i<18;i++){
		round = iterate(round, rulebook);
		cout << i << endl;
	}

	cout << count(round.begin(), round.end(), '#') << endl;

	return 0;
}


string cutBlock(int lineStart, int colStart, int blocksize, vector<string> lines){
	int maxlength = blocksize*blocksize+(blocksize-1);
	string result(maxlength, '?');

	int idx = 0;
	int y_origin = lineStart;
	for(int y = 0;y<blocksize;y++){
		int x_origin = colStart;
		for(int x = 0;x<blocksize;x++){
			result.at(idx) = lines.at(y_origin).at(x_origin);
			idx++;
			x_origin++;
			
		}
		y_origin++;
		if(idx < maxlength){
			result.at(idx) = '/';
			idx++;
		}
	}

	return result;
}


string merge_blocklist(vector<vector<string>> blockList){

	if(blockList.empty()){
		throw runtime_error("Invlaid blocklist");
	}
	if(blockList.at(0).empty()){
		throw runtime_error("Invalid sub-blocklist");
	}

	int blockSize;
	{
		string sampleBlock = blockList.at(0).at(0);
		auto cnt = count(sampleBlock.begin(), sampleBlock.end(), '/');
		if(cnt == 2){
			blockSize = 3;
		}
		else if(cnt == 3){
			blockSize = 4;
		}
		else {
			cerr << sampleBlock << endl;
			cerr << cnt << endl;
			throw runtime_error("invalid sample block");
		}
	}

	stringstream result;
	bool first = true;

	for(auto list_of_blocks : blockList){

		vector<shared_ptr<stringstream>> subresult;
		for(int i=0;i<blockSize;i++){
			subresult.push_back(make_shared<stringstream>());
		}

		for(auto block : list_of_blocks){
			
			vector<string> lines;
			boost::split(lines, block, boost::is_any_of("/"));

			for(int i=0;i<lines.size();i++){
				(*subresult.at(i)) << lines.at(i);
			}
		}

		if(!first){
			result << "/";
		}
		first = false;

		for(int i=0;i<subresult.size();i++){
			auto stream = subresult.at(i);

			result << stream->str();
			if(i+1 < subresult.size()){
				result << "/";
			}
		}

	}



	return result.str();
}


string iterate(string input, map<string, string> &rulebook){
	string result;

	auto size = count(input.begin(),input.end(), '/');

	if(size == 0){
		throw runtime_error("invalid size");
	}

	vector<string> lines;
	boost::split(lines, input, boost::is_any_of("/"));
	
	vector<vector<string>> blocks;
	int blocksize;
	if(size % 2 == 0){
		// Split into 3x3
		blocksize = 3;	
	}
	else {
		// Split into 2x2
		blocksize = 2;
	}

	for(int currentLineIdx = 0;currentLineIdx < lines.size(); currentLineIdx += blocksize){
		string &currentLine = lines.at(currentLineIdx);

		vector<string> block_line;
		
		for(int currentColIdx = 0;currentColIdx < currentLine.size();currentColIdx += blocksize){
			string block = cutBlock(currentLineIdx, currentColIdx, blocksize, lines);
			block_line.push_back(block);
		}
		
		blocks.push_back(block_line);
	}


	vector<vector<string>> result_blocks;

	for(auto list_of_blocks : blocks){
		vector<string> result_blocklist;

		for(auto bl : list_of_blocks){

			auto found = rulebook.find(bl);

			if(found == rulebook.end()){
				cerr << bl << endl;
				throw runtime_error("No rule found");
			}

			result_blocklist.push_back(rulebook[bl]);
		}

		result_blocks.push_back(result_blocklist);
	}

	result = merge_blocklist(result_blocks);


	return result;
}

map<string, string> readRulebook(){
	map<string, string> result;

	regex re("([\\./#]+) => ([\\./#]+)");
	for(string line;getline(cin, line);){
		smatch match;
		if(regex_match(line, match, re)){
			set<string> keys = allExtraKeys(match[1]);
			for(string key : keys){
				result[key] = match[2];
			}
		}
	}

	return result;
}

std::string flip_h_2x2(std::string input){
	string result(input);
	result.at(0) = input.at(1);
	result.at(1) = input.at(0);
	result.at(3) = input.at(4);
	result.at(4) = input.at(3);
	return result;
}


std::string flip_v_2x2(std::string input){
	string result(input);
	result.at(0) = input.at(3);
	result.at(1) = input.at(4);
	result.at(3) = input.at(0);
	result.at(4) = input.at(1);
	return result;
}

std::string flip_h_3x3(std::string input){
	string result(input);
	result.at(0) = input.at(8);
	result.at(1) = input.at(9);
	result.at(2) = input.at(10);
	
	result.at(4) = input.at(4);
	result.at(5) = input.at(5);
	result.at(6) = input.at(6);
	
	result.at(8) = input.at(0);
	result.at(9) = input.at(1);
	result.at(10) = input.at(2);
	return result;
}


std::string flip_v_3x3(std::string input){
	string result(input);
	result.at(0) = input.at(2);
	result.at(1) = input.at(1);
	result.at(2) = input.at(0);
	
	result.at(4) = input.at(6);
	result.at(5) = input.at(5);
	result.at(6) = input.at(4);
	
	result.at(8) = input.at(10);
	result.at(9) = input.at(9);
	result.at(10) = input.at(8);
	return result;
}


string rotate_clk_2x2(string input){
	string result(input);

	result.at(0) = input.at(3);
	result.at(1) = input.at(0);
	
	result.at(3) = input.at(4);
	result.at(4) = input.at(1);

	return result;
}

set<string> allRotations_2x2(string input){
	set<string> result{input};
	for(int i=0;i<4;i++){
		input = rotate_clk_2x2(input);
		result.insert(input);
	}
	return result;
}

string rotate_clk_3x3(string input){
	string result(input);

	result.at(0) = input.at(8);
	result.at(1) = input.at(4);
	result.at(2) = input.at(0);
	
	result.at(4) = input.at(9);
	result.at(5) = input.at(5);
	result.at(6) = input.at(1);

	result.at(8) = input.at(10);
	result.at(9) = input.at(6);
	result.at(10) = input.at(2);

	return result;
}


set<string> allRotations_3x3(string input){
	set<string> result{input};
	for(int i=0;i<4;i++){
		input = rotate_clk_3x3(input);
		result.insert(input);
	}
	return result;
}

set<string> allFlips_2x2(string input){
	return {flip_h_2x2(input), flip_v_2x2(input)};
}

set<string> allFlips_3x3(string input){
	return {flip_h_3x3(input), flip_v_3x3(input)};
}

set<string> allExtraKeys(string input){
	set<string> result{input};

	auto size = count(input.begin(), input.end(), '/');

	if(size == 1){
		set<string> flips, rotations;
		flips = allFlips_2x2(input);
		for(auto flip : flips){
			rotations = allRotations_2x2(flip);
			result.insert(rotations.begin(), rotations.end());
		}
		{
			rotations = allRotations_2x2(input);
			result.insert(rotations.begin(), rotations.end());
		}
	}
	else if(size == 2){
		set<string> flips, rotations;
		flips = allFlips_3x3(input);
		for(auto flip : flips){
			rotations = allRotations_3x3(flip);
			result.insert(rotations.begin(), rotations.end());
		}
		{
			rotations = allRotations_3x3(input);
			result.insert(rotations.begin(), rotations.end());
		}
	}
	else {
		cerr << size << endl;
		throw std::runtime_error("Invalid input");
	}


	return result;
}


