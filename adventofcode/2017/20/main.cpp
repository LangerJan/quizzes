#include <iostream>
#include <string>
#include <regex>
#include <tuple>
#include <vector>
#include <cmath>
#include <cstdint>
#include <limits>
#include <sstream>
#include <algorithm>

#include <boost/lexical_cast.hpp>

using namespace std;


typedef tuple<int64_t,int64_t,int64_t> matrix;
typedef tuple<matrix, matrix, matrix, int, bool> particle;

vector<particle> readInput(){
	vector<particle> result;

	int i=0;
	for(string line; getline(cin, line);){
		regex rex("p=<([-\\d]*),([-\\d]*),([-\\d]*)>, v=<([-\\d]*),([-\\d]*),([-\\d]*)>, a=<([-\\d]*),([-\\d]*),([-\\d]*)>");
		smatch smat;
		if(regex_match(line, smat, rex)){
			matrix position{
				boost::lexical_cast<int>(smat[1]),
				boost::lexical_cast<int>(smat[2]),
				boost::lexical_cast<int>(smat[3])
			};
			matrix velocity{
				boost::lexical_cast<int>(smat[4]),
				boost::lexical_cast<int>(smat[5]),
				boost::lexical_cast<int>(smat[6])
			};
			matrix acceleration{
				boost::lexical_cast<int>(smat[7]),
				boost::lexical_cast<int>(smat[8]),
				boost::lexical_cast<int>(smat[9])
			};
			result.push_back({position, velocity, acceleration, i, false});
		}
		i++;
	}

	return result;
}

int abssum(matrix const &mat){
	return abs(get<0>(mat)) + abs(get<1>(mat)) + abs(get<2>(mat));
}

string toString(matrix const &mat){
	stringstream ss;
	ss << get<0>(mat) << "," << get<1>(mat) << "," << get<2>(mat);
	return ss.str();
}

string toString(particle const &part){
	stringstream ss;
	if(get<4>(part)) return "------destroyed by collision------";

	ss << get<3>(part) << ": p:<" << toString(get<0>(part)) << ">, v=<" << toString(get<1>(part)) << ">, a=<" << toString(get<2>(part)) << ">";
	return ss.str();	
}

int64_t calcDistance(particle &arg0, particle &arg1){
	matrix &arg0_pos = get<0>(arg0);
	matrix &arg1_pos = get<0>(arg1);
	
	int64_t dist_x = llabs(get<0>(arg0_pos) - get<0>(arg1_pos));
	int64_t dist_y = llabs(get<1>(arg0_pos) - get<1>(arg1_pos));
	int64_t dist_z = llabs(get<2>(arg0_pos) - get<2>(arg1_pos));

	return dist_x + dist_y + dist_z;
}

void stepForward(particle &arg0){

	matrix &arg0_pos = get<0>(arg0);
	matrix &arg0_vel = get<1>(arg0);
	matrix &arg0_acc = get<2>(arg0);

	get<0>(arg0_vel) += get<0>(arg0_acc);
	get<1>(arg0_vel) += get<1>(arg0_acc);
	get<2>(arg0_vel) += get<2>(arg0_acc);

	get<0>(arg0_pos) += get<0>(arg0_vel);
	get<1>(arg0_pos) += get<1>(arg0_vel);
	get<2>(arg0_pos) += get<2>(arg0_vel);
}

bool collides(particle &arg0, particle &arg1){
	if(get<4>(arg0) || get<4>(arg1)) return false;

	matrix &arg0_pos = get<0>(arg0);
	matrix &arg1_pos = get<0>(arg1);

	return 
		get<0>(arg0_pos) == get<0>(arg1_pos) &&
		get<1>(arg0_pos) == get<1>(arg1_pos) &&
		get<2>(arg0_pos) == get<2>(arg1_pos);

}

void solveOne(vector<particle> input){
	for(auto &part : input){
		cout << toString(part) << endl;
	}


	sort(input.begin(), input.end(), [](particle const &arg0, particle const &arg1){
		if(abssum(get<2>(arg0)) != abssum(get<2>(arg1))){
			return abssum(get<2>(arg0)) > abssum(get<2>(arg1));
		}
		
		if(abssum(get<1>(arg0)) != abssum(get<1>(arg1))){
			return abssum(get<1>(arg0)) != abssum(get<1>(arg1));
		}

		return abssum(get<0>(arg0)) > abssum(get<0>(arg0));
	});


	for(auto &part : input){
		cout << toString(part) << endl;
	}
}

void eliminate(vector<particle> &input){

	for(int i=0;i<input.size();i++){
		if(get<4>(input[i])) continue;

		bool collided = false;
		for(int j=i+1;j<input.size();j++){
			if(collides(input[i], input[j])){
				collided = true;
				get<4>(input[j]) = true;
			}
		}
		get<4>(input[i]) = collided;
	}

}

void progress(vector<particle> &input){
	for(particle &part : input){
		stepForward(part);
	}
}


int countAlive(vector<particle> &input){
	return count_if(input.begin(), input.end(), [](particle &part){return !get<4>(part);});
}

void solveTwo(vector<particle> input){
	for(uint64_t i=0;i<10000;i++){
		eliminate(input);
		progress(input);
	}


	for(auto &part : input){
		cout << toString(part) << endl;
	}

	cout << countAlive(input) << endl;
}



int main(){

	vector<particle> input = readInput();
	
	solveOne(input);
	solveTwo(input);
	

	return 0;
}



