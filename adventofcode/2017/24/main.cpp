#include <algorithm>
#include <functional>
#include <iostream>
#include <memory>
#include <numeric>
#include <set>
#include <stdexcept>
#include <string>
#include <vector>

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

using namespace std;

class block{
	public:
	int m_first;
	bool m_firstConnected;
	int m_second;
	bool m_secondConnected;

	block(int first, int second) : 
		m_first(first), 
		m_firstConnected(false), 
		m_second(second),
		m_secondConnected(false)
	{

	}

	bool canConnect(int input){
		return (input == m_first) || (input == m_second);
	}

	void connect(int input){
		if(m_first == input){
			m_firstConnected = true;
		}
		else if(m_second == input){
			m_secondConnected = true;
		}
		else {
			throw runtime_error("connection error");
		}
	}
	
	int getRemainingConnector(){
		if(m_firstConnected == false && m_secondConnected == true){
			return m_first;
		}
		else if(m_firstConnected == true && m_secondConnected == false){
			return m_second;
		}
		else {
			cerr << this->toString() << endl;
			throw runtime_error("unable to determine remaining connector");
		}
	}


	void disconnect(int input){
		if(m_first == input){
			m_firstConnected = false;
		}
		else if(m_second == input){
			m_secondConnected = false;
		}
		else {
			throw runtime_error("connection error");
		}
	}
	
	int getStrength(){
		return m_first + m_second;
	}

	string toString(){
		stringstream ss;
		ss << m_first << "/" << m_second;
		return ss.str();
	}
};

set<shared_ptr<block>> readBlocks(){
	set<shared_ptr<block>> result;

	for(string line;getline(cin, line);){
		vector<string> strs;
		boost::split(strs,line,boost::is_any_of("/"));

		result.insert(make_shared<block>(boost::lexical_cast<int>(strs[0]), boost::lexical_cast<int>(strs[1])));
	}

	return result;
}


set<shared_ptr<block>> getSuitableFor(set<shared_ptr<block>> &source, int input){
	set<shared_ptr<block>> result;

	for(auto bl : source){
		if(bl->canConnect(input)){
			result.insert(bl);
		}
	}

	return result;
}

int getTotalStrength(vector<shared_ptr<block>> usedBlocks){
	return std::accumulate(usedBlocks.begin(), usedBlocks.end(), 0, 
			[](int current_sum, shared_ptr<block> &arg1){
				return current_sum + arg1->getStrength();
			}
			);
}


int strongestBridge(set<shared_ptr<block>> availableBlocks, vector<shared_ptr<block>> usedBlocks){

	shared_ptr<block> currentLastBlock = usedBlocks.at(usedBlocks.size()-1);
	int remainingConn = currentLastBlock->getRemainingConnector();

	set<shared_ptr<block>> suitable = getSuitableFor(availableBlocks, remainingConn);

	

	if(suitable.empty()){
		return getTotalStrength(usedBlocks);
	}
	else
	{
		vector<int> results;

		currentLastBlock->connect(remainingConn);
		for(auto bl : suitable){
			availableBlocks.erase(bl);
			usedBlocks.push_back(bl);

			bl->connect(remainingConn);
			results.push_back(strongestBridge(availableBlocks, usedBlocks));		
			bl->disconnect(remainingConn);

			usedBlocks.pop_back();
			availableBlocks.insert(bl);
		}
		currentLastBlock->disconnect(remainingConn);

		return *max_element(results.begin(), results.end());
	}
}

vector<shared_ptr<block>> longestBridge(set<shared_ptr<block>> availableBlocks, vector<shared_ptr<block>> usedBlocks){
	shared_ptr<block> currentLastBlock = usedBlocks.at(usedBlocks.size()-1);
	int remainingConn = currentLastBlock->getRemainingConnector();

	set<shared_ptr<block>> suitable = getSuitableFor(availableBlocks, remainingConn);

	

	if(suitable.empty()){
		return usedBlocks;
	}
	else
	{
		vector<vector<shared_ptr<block>>> results;

		currentLastBlock->connect(remainingConn);
		for(auto bl : suitable){
			availableBlocks.erase(bl);
			usedBlocks.push_back(bl);

			bl->connect(remainingConn);
			results.push_back(longestBridge(availableBlocks, usedBlocks));		
			bl->disconnect(remainingConn);

			usedBlocks.pop_back();
			availableBlocks.insert(bl);
		}
		currentLastBlock->disconnect(remainingConn);

		return *max_element(results.begin(), results.end(), 
			[](vector<shared_ptr<block>> &lhs, vector<shared_ptr<block>> &rhs){
				int len_lhs = lhs.size();
				int len_rhs = rhs.size();

				if(len_lhs != len_rhs){
					return len_lhs < len_rhs;
				}
				else
				{
					int str_lhs = getTotalStrength(lhs);
					int str_rhs = getTotalStrength(rhs);
					return str_lhs < str_rhs;
				}
			}
			);
	}

}


int main(void){
	set<shared_ptr<block>> availableBlocks = readBlocks();

	{
		shared_ptr<block> startBlock = make_shared<block>(0,0);
		startBlock->m_firstConnected = true;
		cout << "Strongest Bridge: " << strongestBridge(availableBlocks, {startBlock}) << endl;
	}
	{
		shared_ptr<block> startBlock = make_shared<block>(0,0);
		startBlock->m_firstConnected = true;
		cout << "Longest Strongest Bridge: " << getTotalStrength(longestBridge(availableBlocks, {startBlock})) << endl;
	}

	return 0;
}

