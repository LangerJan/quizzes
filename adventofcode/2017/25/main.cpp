#include <iostream>
#include <stdexcept>
#include <string>
#include <set>
#include <map>
#include <tuple>

using namespace std;

class turingM {
	public:

	set<int64_t> m_band_1;
	char m_state;
	int64_t m_band_pos;

	map<tuple<char, int>, tuple<char, int, int>> m_transitions;

	int read(){
		if(m_band_1.find(m_band_pos) == m_band_1.end()){
			return 0;
		}
		else
		{
			return 1;
		}
	}

	void step(){
		int rd = read();

		auto followUp = m_transitions.find({m_state, rd});

		if(followUp == m_transitions.end()){
			cerr << "I read a " << rd << " in state " << m_state << endl;
			cerr << "I have a ruleset of " << m_transitions.size() << " rules" << endl;
			throw runtime_error("No valid rule found");
		}


		if(get<1>(followUp->second) == 0){
			m_band_1.erase(m_band_pos);
		}
		else
		{
			m_band_1.insert(m_band_pos);
		}
		m_state = get<0>(followUp->second);
		m_band_pos += get<2>(followUp->second);
	}

};

turingM readTestTuringMachine(){

	turingM machine;

	machine.m_state = 'A';
	machine.m_band_pos = 0;

	map<tuple<char,int>, tuple<char, int, int>> transitions{
		{{'A',0},{'B',1, 1}},
		{{'A',1},{'B',0,-1}},

		{{'B',0},{'A',1,-1}},
		{{'B',1},{'A',1, 1}},
		
	};


	machine.m_transitions = transitions;	
	return machine;
}

turingM readTuringMachine(){

	turingM machine;

	machine.m_state = 'A';
	machine.m_band_pos = 0;

	map<tuple<char,int>, tuple<char, int, int>> transitions{
		{{'A',0},{'B',1, 1}},
		{{'A',1},{'C',0,-1}},

		{{'B',0},{'A',1,-1}},
		{{'B',1},{'D',1, 1}},
		
		{{'C',0},{'B',0,-1}},
		{{'C',1},{'E',0,-1}},
		
		{{'D',0},{'A',1, 1}},
		{{'D',1},{'B',0, 1}},

		{{'E',0},{'F',1,-1}},
		{{'E',1},{'C',1,-1}},
		
		{{'F',0},{'D',1, 1}},
		{{'F',1},{'A',1, 1}}

	};
	
	machine.m_transitions = transitions;	

	return machine;
}


int main(void)
{
	turingM tm = readTuringMachine();

	for(int i=0;i<12481997;i++){
		tm.step();
	}

	cout << tm.m_band_1.size() << endl;


	return 0;
}
