#include <cstdint>
#include <iostream>
#include <map>
#include <set>
#include <stdexcept>
#include <string>
#include <tuple>
#include <vector>

using namespace std;

const int CLEAN = 0;
const int WEAKENED = 1;
const int INFECTED = 2;
const int FLAGGED = 3;
const int FLAG_MODULO = 4;

int getStatus(tuple<int32_t, int32_t> position, map<tuple<int32_t, int32_t>, int> &infMap){
	auto it = infMap.find(position);
	if(it == infMap.end()){
		return CLEAN;
	}
	else {
		return it->second;
	}
}

int setStatus(tuple<int32_t, int32_t> position, map<tuple<int32_t, int32_t>, int> &infMap, int status){
	if(status == CLEAN){
		infMap.erase(position);
	}
	else
	{
		infMap[position] = status;
	}
}

map<tuple<int32_t, int32_t>, int> readInfected(){
	map<tuple<int32_t,int32_t>, int> result;

	vector<string> lines;
	for(string line;getline(cin, line);){
		lines.push_back(line);
	}

	int32_t center_x, center_y;

	center_y = lines.size() / 2;
	center_x = lines.at(0).size() / 2;

	int32_t current_y = center_y;
	for(string line : lines){
		int32_t current_x = -center_x;

		for(char ch : line){
	
			if(ch == '#'){
				result[{current_x, current_y}] = INFECTED;
			}

			current_x++;
		}
		current_y--;
	}


	return result;
}

void printMap(map<tuple<int32_t,int32_t>, int> infMap, int32_t minX, int32_t maxY, int32_t size, tuple<int32_t, int32_t> curPos){

	int32_t cur_xPos = get<0>(curPos);
	int32_t cur_yPos = get<1>(curPos);

	int32_t yPos = maxY;
	for(int32_t y = 0;y<size;y++){

		int32_t xPos = minX;
		for(int32_t x=0;x<size;x++){

			tuple<int32_t,int32_t> pos{xPos, yPos};

			if(cur_xPos == xPos && cur_yPos == yPos){
				cout << "[";
			}

			int status = getStatus(pos, infMap);
			switch(status){
				case INFECTED: cout << "#"; break;
				case WEAKENED: cout << "W"; break;
				case FLAGGED: cout << "F"; break;
				case CLEAN: cout << "."; break;
				default: cout << "?"; break;
			}

			if(cur_xPos == xPos && cur_yPos == yPos){
				cout << "]";
			}
			else
			{
				if(xPos+1 == cur_xPos && cur_yPos == yPos){

				}
				else
				{
					cout << " ";
				}
			}

			xPos++;
		}
		yPos--;
		cout << endl;
	}
}

int main(void){

	map<tuple<int32_t,int32_t>, int> infMap = readInfected();

	tuple<int32_t,int32_t> currentPosition {0,0};
	
	// printMap(infected, -4, -4, 9, currentPosition);

	vector<tuple<int32_t,int32_t>> directions{
		{0,1}, // up
		{1,0}, // right
		{0,-1}, // down
		{-1,0} // left
	};

	vector<string> directions_string{
		"up",
		"right",
		"down",
		"left"
	};

	int currentDirectionIdx = 0;

	int32_t infectionCount = 0;

	for(int burstIdx=0;burstIdx<10000000;burstIdx++){
		// printMap(infMap, -4, 4, 9, currentPosition);
		// cout << "The virus carrier is on ";

		int status = getStatus(currentPosition, infMap);
		switch(status){
			case INFECTED:
			{
				currentDirectionIdx = (currentDirectionIdx == (directions.size()-1))?0:currentDirectionIdx+1;
				break;
			}
			case WEAKENED:
			{
				break;
			}
			case FLAGGED:
			{

				currentDirectionIdx = (currentDirectionIdx+2)%directions.size();
				break;
			}
			case CLEAN:
			{
				currentDirectionIdx = (currentDirectionIdx == 0)?(directions.size()-1):currentDirectionIdx-1;
				break;
			}
			default:
			{
				throw runtime_error("wtf?");
				break;
			}
		}

		status = (status + 1) % FLAG_MODULO;

		if(status == INFECTED) infectionCount++;


		setStatus(currentPosition, infMap, status);

#if 0
		cout << ", so it turns " << (left?"left":"right") << ", " 
			<< (infects?"infects":"cleans") << " the node and moves "
			<< directions_string.at(currentDirectionIdx) << endl;
#endif 

		// cout << get<0>(currentPosition) << "/" << get<1>(currentPosition) << endl;
		get<0>(currentPosition) += get<0>(directions.at(currentDirectionIdx));
		get<1>(currentPosition) += get<1>(directions.at(currentDirectionIdx));
		// cout << get<0>(currentPosition) << "/" << get<1>(currentPosition) << endl;

		// cout << endl;
	}



	printMap(infMap, -20, 20, 40, currentPosition);

	cout << infectionCount << " infections" << endl;
	return 0;
}







