#include <cstdint>
#include <iostream>
#include <set>
#include <string>
#include <tuple>
#include <vector>

using namespace std;

set<tuple<int32_t, int32_t>> readInfected(){
	set<tuple<int32_t,int32_t>> result;

	vector<string> lines;
	for(string line;getline(cin, line);){
		lines.push_back(line);
	}

	int32_t center_x, center_y;

	center_y = lines.size() / 2;
	center_x = lines.at(0).size() / 2;

	int32_t current_y = center_y;
	for(string line : lines){
		int32_t current_x = -center_x;

		for(char ch : line){
	
			if(ch == '#'){
				result.insert({current_x, current_y});
			}

			current_x++;
		}
		current_y--;
	}


	return result;
}

void printMap(set<tuple<int32_t,int32_t>> infected, int32_t minX, int32_t maxY, int32_t size, tuple<int32_t, int32_t> curPos){

	int32_t cur_xPos = get<0>(curPos);
	int32_t cur_yPos = get<1>(curPos);

	int32_t yPos = maxY;
	for(int32_t y = 0;y<size;y++){

		int32_t xPos = minX;
		for(int32_t x=0;x<size;x++){

			tuple<int32_t,int32_t> pos{xPos, yPos};

			if(cur_xPos == xPos && cur_yPos == yPos){
				cout << "[";
			}

			if(infected.find(pos) != infected.end()){
				cout << "#";
			}
			else
			{
				cout << ".";
			}
			
			if(cur_xPos == xPos && cur_yPos == yPos){
				cout << "]";
			}
			else
			{
				if(xPos+1 == cur_xPos && cur_yPos == yPos){

				}
				else
				{
					cout << " ";
				}
			}

			xPos++;
		}
		yPos--;
		cout << endl;
	}
}

int main(void){

	set<tuple<int32_t,int32_t>> infected = readInfected();


	tuple<int32_t,int32_t> currentPosition {0,0};
	
	// printMap(infected, -4, -4, 9, currentPosition);

	vector<tuple<int32_t,int32_t>> directions{
		{0,1}, // up
		{1,0}, // right
		{0,-1}, // down
		{-1,0} // left
	};

	vector<string> directions_string{
		"up",
		"right",
		"down",
		"left"
	};

	int currentDirectionIdx = 0;

	int32_t infectionCount = 0;

	for(int burstIdx=0;burstIdx<10000;burstIdx++){
		// printMap(infected, -4, 4, 9, currentPosition);
		// cout << "The virus carrier is on ";

		bool left = true;
		bool infects = true;

		// Infected?
		if(infected.find(currentPosition) != infected.end()){
			// cout << "an infected node";

			currentDirectionIdx = (currentDirectionIdx == (directions.size()-1))?0:currentDirectionIdx+1;
			infected.erase(currentPosition);
			infects = false;
			left = false;
		}
		else
		{
			// cout << "a clean node";

			currentDirectionIdx = (currentDirectionIdx == 0)?(directions.size()-1):currentDirectionIdx-1;
			infected.insert(currentPosition);
			infects = true;
			left = true;

			infectionCount++;
		}

#if 0
		cout << ", so it turns " << (left?"left":"right") << ", " 
			<< (infects?"infects":"cleans") << " the node and moves "
			<< directions_string.at(currentDirectionIdx) << endl;
#endif 

		// cout << get<0>(currentPosition) << "/" << get<1>(currentPosition) << endl;
		get<0>(currentPosition) += get<0>(directions.at(currentDirectionIdx));
		get<1>(currentPosition) += get<1>(directions.at(currentDirectionIdx));
		// cout << get<0>(currentPosition) << "/" << get<1>(currentPosition) << endl;

		cout << endl;
	}



	printMap(infected, -4, 4, 9, currentPosition);

	cout << infectionCount << " infections" << endl;
	return 0;
}







