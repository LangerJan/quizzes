#include <string>
#include <iostream>
#include <regex>
#include <map>
#include <vector>
#include <stdexcept>
#include <functional>

#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>

using namespace std;
using namespace boost;

int main(){
    map<string, int> registers;

    int during_maximum = 0;
    for(string line;getline(cin, line);){

        regex instr_regex("(\\w*) (\\w*) ([-]?\\d*) if (\\w*) ([\\<\\=\\>\\!]*) ([-]?\\d*)");
        smatch match;
        
        if(regex_match(line, match, instr_regex)){
            string reg = match[1];

            string instr = match[2];
            int arg = boost::lexical_cast<int>(match[3]);
            
            string comp_reg = match[4];
            string comp_str = match[5];
            int comp_arg = boost::lexical_cast<int>(match[6]);
            
            std::function<bool(int,int)> fkt_cmp;
            std::function<int(int,int)> fkt_do;

            if(equals(comp_str, "==")) fkt_cmp = equal_to<int>();
            else if(equals(comp_str, "!=")) fkt_cmp = not_equal_to<int>();
            else if(equals(comp_str, ">=")) fkt_cmp = greater_equal<int>();
            else if(equals(comp_str, "<=")) fkt_cmp = less_equal<int>();
            else if(equals(comp_str, ">")) fkt_cmp = greater<int>();
            else if(equals(comp_str, "<")) fkt_cmp = less<int>();
            else throw runtime_error("no comp found");

            if(equals(instr, "inc")) fkt_do = plus<int>();
            else if(equals(instr, "dec")) fkt_do = minus<int>();
            else throw runtime_error("no instr found");

            
            if(fkt_cmp(registers[comp_reg], comp_arg)) registers[reg] = fkt_do(registers[reg],arg);
            during_maximum = std::max(registers[reg], during_maximum);

        }
    }

    int maximum = std::max_element(
        std::begin(registers), std::end(registers),
            [] (const auto & p1, const auto & p2) {return p1.second < p2.second;}
    )->second;

    cout << maximum << endl;
    cout << during_maximum << endl;
    return 0;
}

