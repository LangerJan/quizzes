#include <string>
#include <iostream>
#include <vector>
#include <iterator>

using namespace std;
using namespace boost;

int main(){
    int programCounter = 0;
    int steps = 0;
    vector<int> jumpcodes{std::istream_iterator<int>{std::cin}, {}};
    while(programCounter < jumpcodes.size()){
#ifdef PART1
        programCounter += jumpcodes[programCounter]++;
#else
        programCounter += jumpcodes[programCounter]>=3?jumpcodes[programCounter]--:jumpcodes[programCounter]++;
#endif
        steps++;
    }
    cout << steps << endl;
    return 0;
}

