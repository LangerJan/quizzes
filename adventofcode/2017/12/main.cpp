#include <iostream>
#include <regex>
#include <string>
#include <tuple>
#include <set>
#include <map>
#include <vector>
#include <sstream>
#include <memory>

#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>

using namespace std;
using namespace boost;


class Knoten;
class Knoten{
    public:

    int m_key;
    vector<std::shared_ptr<Knoten> > m_children;

    map<int, int> m_distances;

    Knoten(int key) : m_key(key){};
 
    string toString(){
        stringstream ss;
        ss << m_key << " <-> ";
        for(auto knowsPair : m_distances){
            ss << knowsPair.first << "[" << knowsPair.second << "] ";
        }

        return ss.str();
    }

    void propagate(int key, int distance){
        if(this->m_distances[key] == 0 || (this->m_distances[key] > distance) ){
            this->m_distances[key] = distance;
            for(auto child : m_children){
                child->propagate(key, distance+1);
            }
        }
    }

};


vector<std::shared_ptr<Knoten>> readNodes(){

    vector<std::shared_ptr<Knoten>> result;
    vector<vector<int>> childs_per_result;

    regex rex("(\\d+) <-> ((\\d+)(, (\\d+))*)");
    for(string line;getline(cin, line);){
        smatch match;
        if(regex_search(line, match, rex)){
            int key = lexical_cast<int>(match[1]);
            vector<string> nums;
            vector<int> ints;
            string commas = match[2];
            algorithm::split(nums, commas, is_any_of(","));
            for(auto num : nums){
                trim(num);
                ints.push_back(lexical_cast<int>(num));
            }

            
            result.push_back(make_shared<Knoten>(key));
            childs_per_result.push_back(ints);
        }
    }

    for(int i=0;i<result.size();i++){
        std::shared_ptr<Knoten> knot = result[i];
        for(int knotID : childs_per_result[i]){
            knot->m_children.push_back(result[knotID]);
        }
    }

    for(auto knot : result){
        knot->propagate(knot->m_key, 0);
    }

    return result;
}



int main(){
    vector<std::shared_ptr<Knoten>> nodes = readNodes();

    cout << nodes[0]->m_distances.size() << endl;



    set<std::shared_ptr<Knoten>> result2;

    for(auto knot : nodes){
        
        bool foundInside = false;
        for(auto groupknot : result2){
            if(groupknot->m_distances.find(knot->m_key) != groupknot->m_distances.end()){
                foundInside = true;
                break;
            }
        }
        if(!foundInside){
            result2.insert(knot);
        }
    }


    cout << result2.size() << endl;

    return 0;
}

