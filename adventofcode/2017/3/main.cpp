#include <cmath>
#include <iostream>
#include <cstdlib>

/* OEIS: https://oeis.org/A016754 */
int lowerRightCorner(int x){
	return (2*x+1)*(2*x+1);
}

/* OEIS: https://oeis.org/A054554 */
int upperRightCorner(int x){
	x++;
	return 4*(x*x)-10*x+7;
}

/* OEIS: https://oeis.org/A053755 */
int upperLeftCorner(int x){
	return 4*(x*x) + 1;
}

/* OEIS: https://oeis.org/A054569 */
int lowerLeftCorner(int x){
	x++;
	if(x <= 0){ return 1;}
	return 4*(x*x) - (6*x) + 3;
}

int determinePath(int puzzle_input){
	int circleNum = 0;
	int row = 0;

	while(circleNum < puzzle_input){
		row++;
		circleNum = lowerRightCorner(row);
	}

	int betweenA;
	int betweenB;

	if(lowerLeftCorner(row) == puzzle_input || 
		upperLeftCorner(row) == puzzle_input ||
		upperRightCorner(row) == puzzle_input ||
		lowerRightCorner(row) == puzzle_input 
	){
		return row * 2;	
	}

	if(lowerLeftCorner(row) < puzzle_input){
		betweenA = lowerLeftCorner(row);
		betweenB = lowerRightCorner(row);
	}
	else if(upperLeftCorner(row) < puzzle_input){
		betweenA = upperLeftCorner(row);
		betweenB = lowerLeftCorner(row);
	}
	else if(upperRightCorner(row) < puzzle_input){
		betweenA = upperRightCorner(row);
		betweenB = upperLeftCorner(row);
	}
	else {
		betweenA = upperRightCorner(row);
		betweenB = lowerRightCorner(row-1);
	}
	
	int center = (betweenA + betweenB) / 2;
	int stepsToCenter = ::abs(puzzle_input-center);

	return row+stepsToCenter;
}

/**
 * Solution to Part 2: Look up https://oeis.org/A141481/b141481.txt, profit. 
 * Sue me, I don't care ^^
 */

int main(){
	const int puzzle_input = 277678;
	std::cout << determinePath(puzzle_input) << std::endl;

	return 0;

}



