#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <numeric>
#include <iomanip>
#include <sstream>

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/numeric/conversion/cast.hpp>

using namespace std;


vector<int> getInputPartOne(std::string line){
    vector<string> split_str;
    vector<int> result;

    boost::algorithm::split(split_str, line, boost::is_any_of(","));
    for(auto str : split_str)result.push_back(boost::lexical_cast<int>(str));
    return result;
}

vector<int> getInputPartTwo(std::string line){
    boost::trim(line);
    vector<int> result;
    for(char c : line) {
        result.push_back(c);
    }

    vector<int> addThis{17, 31, 73, 47, 23};
    result.insert(result.end(), addThis.begin(), addThis.end());

    return result;
}

void printSituation(vector<int> &input, int position, int length){
        int endPosition = (position+(length-1))%input.size();
        for(int i=0;i<input.size();i++){
            if(i == position){
                cout << "([";
            }
            cout << input.at(i);

            if(i == position){
                cout << "]";
            }

            if(i == endPosition){
                cout << ")";
            }

            cout << " ";
        }
        cout << endl;
}

vector<int> knotHash(vector<int> input, vector<int> lengths, int &position, int &skip_size){
    for(int length : lengths){
        vector<int> subInput;
        for(int i=0;i<length;i++){
            subInput.push_back(input.at((i+position)%input.size()));
        }
        reverse(begin(subInput), end(subInput));
        for(int i=0;i<length;i++){
            input.at((i+position)%input.size()) = subInput.at(i);
        }

        position = (position + length + skip_size) % input.size();
        skip_size = (skip_size+1)%input.size();
    }
    return input;
}


void solvePartOne(string line){
    vector<int> lengths = getInputPartOne(line);
    vector<int> input(256);
    std::iota (std::begin(input), std::end(input), 0);

    int position = 0;
    int skip_size = 0;
    auto part1 = knotHash(input, lengths, position, skip_size);
    cout << (part1[0] * part1[1]) << endl;
}

void solvePartTwo(string line){
    vector<int> lengths = getInputPartTwo(line);

    vector<int> input(256);
    std::iota (std::begin(input), std::end(input), 0);

    int position = 0;
    int skip_size = 0;
    for(int i=0;i<64;i++){
        vector<int> output = knotHash(input, lengths, position, skip_size);
        input = output;
    }

    stringstream result;

    for(int i=0;i<16;i++){
        int dense = 0;
        for(int j=0;j<16;j++){
            dense = dense ^ input.at((i*16)+j);
        }
        result << setfill('0') << setw(2) << hex << dense;
    }

    cout << result.str() << endl;
}

int main(){

    std::string line;
    getline(cin, line);

    solvePartOne(line);
    solvePartTwo(line);



    return 0;
}
