#include <iostream>
#include <string>
#include <boost/lexical_cast.hpp>

int main(){
    int result_one = 0;
    int result_two = 0;

    std::string line;
    while(std::getline(std::cin, line)){
        for(int i=0;i<line.size();i++){
            int j=(i+1)%line.size();
            int k=(i+(line.size()/2))%line.size();

            if(line[i] == line[j]) result_one += boost::lexical_cast<int>(line[i]);
            if(line[i] == line[k]) result_two += boost::lexical_cast<int>(line[i]);
        }
        std::cout << result_one << "\t" << result_two << std::endl;
    }
    return 0;
}

