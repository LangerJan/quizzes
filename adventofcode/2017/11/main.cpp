#include <iostream>
#include <tuple>
#include <algorithm>
#include <vector>
#include <stdexcept>
#include <cmath>
#include <map>

#include <boost/algorithm/string.hpp>

using namespace std;
using namespace boost;


vector<string> getInput(){
    string line;
    getline(cin, line);
    vector<string> splitted;
    boost::algorithm::split(splitted, line, boost::is_any_of(","));
    return splitted;
}


// int calcDistance(vector<string> dirs){
int calcDistance(map<string, int> directions){
    //map<string, int> directions;
    //for(string dir : dirs){directions[dir]++;}

    // (N,S) -> ()
    int n_s = directions["n"] - directions["s"];
    // (NE,SW) -> ()
    int ne_sw = directions["ne"] - directions["sw"];
    // (NW,SE) -> ()
    int nw_se = directions["nw"] - directions["se"];

    // (NW, NE) -> N, (SW,SE) -> S
    if((nw_se >= 0) ^ (ne_sw < 0)){ // fancy way of saying "if both parameters have the same sign
        int tmp = (abs(nw_se) < abs(ne_sw))?nw_se:ne_sw;
        n_s += tmp;
        nw_se -= tmp;
        ne_sw -= tmp;
    }
    
    // (NW, S) -> (SW)
    if(nw_se > 0 && n_s < 0){
        int tmp = min(nw_se, abs(n_s));
        n_s += tmp;
        nw_se -= tmp;
        ne_sw -= tmp;
    }
    // (N, SE) -> (NE)
    if(nw_se < 0 && n_s > 0){
        int tmp = min(abs(nw_se), abs(n_s));
        n_s -= tmp;
        nw_se += tmp;
        ne_sw += tmp;
    }

    // (NE, S) -> (SE)
    if(ne_sw > 0 && n_s < 0){
        int tmp = min(ne_sw, abs(n_s));
        n_s += tmp;
        ne_sw -= tmp;
        nw_se -= tmp;
    }
    // (N, SW) -> (NW)
    if(ne_sw < 0 && n_s > 0){
        int tmp = min(abs(ne_sw), abs(n_s));
        n_s -= tmp;
        ne_sw += tmp;
        nw_se += tmp;
    }

    return abs(n_s) + abs(ne_sw) + abs(nw_se);
}


int main(){

    vector<string> input = getInput();

    int farthest = 0;
    map<string, int> directions;
    for(int i=0;i<input.size();i++){
        directions[input[i]]++;
        farthest = max(calcDistance(directions), farthest);
    }

    cout << calcDistance(directions) << endl;
    cout << farthest << endl;
    return 0;
}
