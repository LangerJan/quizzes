#include <algorithm>
#include <bitset>
#include <cstdint>
#include <functional>
#include <iostream>
#include <string>
#include <tuple>


using namespace std;

typedef tuple<uint64_t, uint64_t, uint8_t> generator;

uint64_t generatePartOne(generator &gen){
	get<0>(gen) = (get<0>(gen) * get<1>(gen))%2147483647;
	return get<0>(gen);
}

uint64_t generatePartTwo(generator &gen){
	uint64_t val;
	do{
		val = generatePartOne(gen);
	}while(val%get<2>(gen) != 0);
	return val;
}


int judge(generator &genA, generator &genB, function<uint64_t(generator&)> fkt){
	uint64_t valA = fkt(genA);
	uint64_t valB = fkt(genB);
	
	const uint64_t mask = 0x0000'0000'0000'FFFF;

	valA &= mask;
	valB &= mask;

	return (valA == valB?1:0);
}

void solveOne(generator genA, generator genB){
	int result = 0;
	for(int i=0;i<40000000;i++){
		result += judge(genA, genB, generatePartOne);
	}
	cout << result << endl;
}

void solveTwo(generator genA, generator genB){
	int result = 0;
	for(int i=0;i<5000000;i++){
		result += judge(genA, genB, generatePartTwo);
	}
	cout << result << endl;
}

int main(){
	generator genA{512, 16807, 4}, genB{191, 48271, 8};
	//generator genA{65, 16807, 4}, genB{8921, 48271, 8};

	solveOne(genA, genB);
	solveTwo(genA, genB);
	return 0;
}


