#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <memory>
#include <regex>
#include <boost/optional.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>

class node{
	public:
	virtual void receive(int chip) = 0;
};

class output : public node {
	public:
	std::string name;
	int lastGiven;

	void receive(int chip) override{
		this->lastGiven = chip;
		std::cout << name << ": " << chip << std::endl;
	}

	output(std::string name){
		this->name = name;
	}
};

class bot : public node {
	public:
	std::string name;
	boost::optional<int> firstChip, secondChip;

	std::string nameLower;
	std::string nameHigher;
	
	std::shared_ptr<std::map<std::string,std::shared_ptr<node>>> nodeMap;
	
	void receive(int chip) override{
		if(!firstChip){
			firstChip = chip;
			return;
		}
		else {
			secondChip = chip;
		}


		int min = std::min(*firstChip, *secondChip);
		int max = std::max(*firstChip, *secondChip);

		std::cout << this->name << ":" << "comparing " << min << " with " << max << std::endl;
		
		if(this->nodeMap->find(nameLower) == this->nodeMap->end()) throw std::runtime_error(std::string("cannot find ") + nameLower);
		(*this->nodeMap)[nameLower]->receive(min);
		if(this->nodeMap->find(nameHigher) == this->nodeMap->end()) throw std::runtime_error(std::string("cannot find ") + nameHigher);
		(*this->nodeMap)[nameHigher]->receive(max);

		firstChip.reset();
		secondChip.reset();
	}

	bot(std::string name, std::string nLower, std::string nHigher,
		std::shared_ptr<std::map<std::string, std::shared_ptr<node>>> nMap)
		: name(name), nameLower(nLower), nameHigher(nHigher), nodeMap(nMap){}

};


int main(){
	
	std::vector<std::tuple<int,std::string>> start_values;
	std::shared_ptr<std::map<std::string,std::shared_ptr<node>>> nodeMap(
		new std::map<std::string, std::shared_ptr<node>>()
	);

	std::string line;
	while(std::getline(std::cin, line)){

		std::regex botDecl("(bot (\\d*)) gives low to ((\\w*) (\\d*)) and high to ((\\w*) (\\d*))");
		std::regex valueDecl("value (\\d*) goes to (bot (\\d*))");

		std::smatch match;
		if(std::regex_search(line,match,botDecl)){
			std::cout << "'1:" << match.str(1) << "'" << ","
				  << "'2:" << match.str(2) << "'" << ","
				  << "'3:" << match.str(3) << "'" << ","
				  << "'4:" << match.str(4) << "'" << ","
				  << "'5:" << match.str(5) << "'" << ","
				  << "'6:" << match.str(6) << "'" << ","
				  << "'7:" << match.str(7) << "'" << ","
				  << "'8:" << match.str(8) << "'" << std::endl;


			std::string name = match.str(1);
			
			std::string lowName = match.str(3);
			std::string highName = match.str(6);
			bool lowIsOutput = boost::equals(match.str(4),"output");
			bool highIsOutput = boost::equals(match.str(7),"output");


			if(lowIsOutput && (nodeMap->find(lowName) == nodeMap->end())) (*nodeMap)[lowName]=std::shared_ptr<node>(new output(lowName));
			if(highIsOutput && (nodeMap->find(highName) == nodeMap->end())) (*nodeMap)[highName]=std::shared_ptr<node>(new output(highName));
			
			std::shared_ptr<node> nbot(new bot(name, lowName, highName, nodeMap));
			(*nodeMap)[name] = nbot;
			
		}
		else if(std::regex_search(line,match,valueDecl))
		{
			int value = boost::lexical_cast<int>(match.str(1));
			std::string output = match.str(2);
			std::tuple<int,std::string> tup{value,output};
			start_values.push_back(tup);
		}
		else throw std::runtime_error("invalid input line");

	}

	for(auto it : start_values){
		if((*nodeMap).find(std::get<1>(it)) == nodeMap->end()){
			throw std::runtime_error(std::string("cannot find ") + std::get<1>(it));
		}
		else
		{
			(*nodeMap)[std::get<1>(it)]->receive(std::get<0>(it));
		}
	}

	return 0;
}




