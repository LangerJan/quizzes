/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: jan
 *
 * Created on 4. Januar 2017, 01:08
 */

#include <cstdlib>
#include <string>
#include <iostream>
#include <vector>
#include <tuple>
#include <regex>
#include <cstdint>
#include <boost/lexical_cast.hpp>
#include <algorithm>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    
    std::vector<std::tuple<uint32_t,uint32_t>> rules;
    std::string line;
    while(std::getline(std::cin, line)){
        std::regex re("(\\d+)-(\\d+)");
        std::smatch match;
        if(std::regex_search(line, match, re)){
            uint32_t t_min = boost::lexical_cast<uint32_t>(match.str(1));
            uint32_t t_max = boost::lexical_cast<uint32_t>(match.str(2));
            std::tuple<uint32_t,uint32_t> rule{t_min, t_max};

            rules.push_back(rule);
        }
    }
    
    std::sort(rules.begin(), rules.end(), 
            [](std::tuple<uint32_t,uint32_t> left, 
                std::tuple<uint32_t,uint32_t> right){

                if(std::get<0>(left) != std::get<0>(right)){
                    return std::get<0>(left) < std::get<0>(right);
                }
                else
                {
                    return std::get<1>(left) < std::get<1>(right);
                }
                
            }
        );
        
    
    
    
    uint32_t lookup = 0;
    
    bool found = false;
    while(!found){

        bool ruleFound = false;
        for(auto rule : rules){
            if(lookup >= std::get<0>(rule) && lookup <= std::get<1>(rule)){
                lookup = std::get<1>(rule)+1;
                ruleFound = true;
                break;
            }
        }
        if(!ruleFound){
            found = true;
            break;
        }
        
    }
    
    std::cout << lookup << std::endl;

    for(auto rule : rules){
        std::cout << std::get<0>(rule) << "-" << std::get<1>(rule) << std::endl;
    }
    
    int valid = 0;

    uint32_t greatest_disallowed = 0;
    for(auto rule : rules){
        if(std::get<0>(rule) > greatest_disallowed){
            valid += (std::get<0>(rule) - greatest_disallowed - 1);
        }
        greatest_disallowed = std::max(greatest_disallowed,std::get<1>(rule));
    }
    
    std::cout << valid << std::endl;
    
    return 0;
}

