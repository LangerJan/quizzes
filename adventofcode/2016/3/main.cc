/**
 * Compile with -std=c++11
 * @author Jan Gampe
 */

#include <string>
#include <iostream>
#include <stdexcept>
#include <algorithm>
#include <array>
#include <sstream>


bool possibleTriangle(std::array<int, 3> numbers){
	std::sort(numbers.begin(), numbers.end());
	do{
		if(numbers[0] + numbers[1] <= numbers[2]){
			 return false;
		}
	}while(std::next_permutation(numbers.begin(),numbers.end()));
	return true;
}


int main(){
	int result = 0;
	std::string line;
	while(std::getline(std::cin, line)){
		std::istringstream iline(line);
		std::array<int,3> numbers;
		for(int i=0;i<3;i++){
			iline >> numbers[i];
		}
		if(possibleTriangle(numbers)){
			result++;
		}
	}
	std::cout << result << std::endl;
	return 0;
}

