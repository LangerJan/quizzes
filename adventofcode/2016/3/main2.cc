/**
 * Compile with -std=c++11
 * @author Jan Gampe
 */

#include <string>
#include <iostream>
#include <stdexcept>
#include <algorithm>
#include <array>
#include <sstream>


bool possibleTriangle(std::array<int,3> numbers){
	std::sort(numbers.begin(), numbers.end());
	do{
		if(numbers[0] + numbers[1] <= numbers[2]) return false;
	}while(std::next_permutation(numbers.begin(),numbers.end()));
	return true;
}

std::array<int,3> toNumbers(std::string line){
	std::istringstream iline(line);
	std::array<int,3> numbers;
	for(int i=0;i<3;i++){
		iline >> numbers[i];
		}
	return numbers;
}


int main(){
	int result = 0;
	std::string line,line2,line3;

	while(std::getline(std::cin, line)){
		if(!std::getline(std::cin, line2)) throw std::runtime_error("invalid input");
		if(!std::getline(std::cin, line3)) throw std::runtime_error("invalid input");
		auto numbers1 = toNumbers(line);
		auto numbers2 = toNumbers(line2);
		auto numbers3 = toNumbers(line3);

		for(int i=0;i<3;i++){
			if(possibleTriangle({numbers1[i],numbers2[i],numbers3[i]})) result++;
		}
	}
	std::cout << result << std::endl;
	return 0;
}

