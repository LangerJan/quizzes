/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: jan
 *
 * Created on 6. Januar 2017, 12:11
 */

#include <cstdlib>
#include <regex>
#include <deque>
#include <string>
#include <stdexcept>
#include <iostream>
#include <algorithm>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/utility/compare_pointees.hpp>

using namespace std;

void solution1(){
    std::string plaintext("abcdefgh");
    
    std::string line;
    while(std::getline(std::cin, line)){
        
        std::regex re_swapPos("swap position (\\d+) with position (\\d+)");
        std::regex re_swapLet("swap letter (\\w+) with letter (\\w+)");
        std::regex re_rotate("rotate (left|right) (\\d+) step");
        std::regex re_rotateBase("rotate based on position of letter (\\w)");
        std::regex re_reversePos("reverse positions (\\d+) through (\\d+)");
        std::regex re_movePos("move position (\\d+) to position (\\d+)");
        
        std::smatch match;

        std::cout << plaintext << std::endl << std::flush;
        std::cout << line << std::endl;

        
        if(regex_search(line, match, re_swapPos)){
            int posX = boost::lexical_cast<int>(match.str(1));
            int posY = boost::lexical_cast<int>(match.str(2));
            std::swap(plaintext[posX], plaintext[posY]);
        }
        else if(regex_search(line, match, re_swapLet)){
            boost::replace_all(plaintext, match.str(1), "_");
            boost::replace_all(plaintext, match.str(2), match.str(1));
            boost::replace_all(plaintext, "_", match.str(2));
        }
        else if(regex_search(line, match, re_rotate)){
            int steps = boost::lexical_cast<int>(match.str(2));
            bool left = boost::equals(match.str(1), "left");
            
            if(left){
                std::rotate(plaintext.begin(), plaintext.begin()+steps, plaintext.end());            
            }
            else {
                std::rotate(plaintext.rbegin(), plaintext.rbegin()+steps, plaintext.rend());                            
            }
        }
        else if(regex_search(line, match, re_rotateBase)){
            auto pos = plaintext.find(match.str(1));
            if(pos >= 4) pos++;
            pos++;
            
            for(int i=0;i<pos;i++){
                std::rotate(plaintext.rbegin(), plaintext.rbegin()+1, plaintext.rend());                                        
            }
        }
        else if(regex_search(line, match, re_reversePos)){
            int posX = boost::lexical_cast<int>(match.str(1));
            int posY = boost::lexical_cast<int>(match.str(2))+1;
            std::reverse(plaintext.begin()+posX, plaintext.begin()+posY);
        }
        else if(regex_search(line, match, re_movePos)){
            int posX = boost::lexical_cast<int>(match.str(1));
            int posY = boost::lexical_cast<int>(match.str(2));

            char mov = plaintext[posX];
            plaintext.erase(plaintext.begin()+posX);
            plaintext.insert(plaintext.begin()+posY, mov);
        }
        else
        {
            std::cerr << line << std::endl;
            throw std::runtime_error("Invalid op");
        }
        std::cout << plaintext << std::endl << std::flush;
        
    }
    
}


std::string solution2(std::string scrambletext){
    
    std::string line;
    while(std::getline(std::cin, line)){
        
        std::regex re_swapPos("swap position (\\d+) with position (\\d+)");
        std::regex re_swapLet("swap letter (\\w+) with letter (\\w+)");
        std::regex re_rotate("rotate (left|right) (\\d+) step");
        std::regex re_rotateBase("rotate based on position of letter (\\w)");
        std::regex re_reversePos("reverse positions (\\d+) through (\\d+)");
        std::regex re_movePos("move position (\\d+) to position (\\d+)");
        
        std::smatch match;

        std::cout << scrambletext << std::endl << std::flush;
        std::cout << line << std::endl;

        
        if(regex_search(line, match, re_swapPos)){
            int posX = boost::lexical_cast<int>(match.str(1));
            int posY = boost::lexical_cast<int>(match.str(2));
            std::swap(scrambletext[posY], scrambletext[posX]);
        }
        else if(regex_search(line, match, re_swapLet)){
            boost::replace_all(scrambletext, match.str(2), "_");
            boost::replace_all(scrambletext, match.str(1), match.str(2));
            boost::replace_all(scrambletext, "_", match.str(1));
        }
        else if(regex_search(line, match, re_rotate)){
            int steps = boost::lexical_cast<int>(match.str(2));
            bool left = boost::equals(match.str(1), "left");
            
            if(left){
                std::rotate(scrambletext.rbegin(), scrambletext.rbegin()+steps, scrambletext.rend());                            
            }
            else {
                std::rotate(scrambletext.begin(), scrambletext.begin()+steps, scrambletext.end());
            }
        }
        else if(regex_search(line, match, re_rotateBase)){
            int timesRotated = 0;
            int tst_pos;
            do{
                std::rotate(scrambletext.begin(), scrambletext.begin()+1, scrambletext.end());                                        
                timesRotated++;
                
                tst_pos = scrambletext.find(match.str(1));
                if(tst_pos >= 4) tst_pos++;
                tst_pos++;
            }
            while(tst_pos != timesRotated);
            
            
        }
        else if(regex_search(line, match, re_reversePos)){
            int posX = boost::lexical_cast<int>(match.str(1));
            int posY = boost::lexical_cast<int>(match.str(2))+1;
            std::reverse(scrambletext.begin()+posX, scrambletext.begin()+posY);
        }
        else if(regex_search(line, match, re_movePos)){
            int posX = boost::lexical_cast<int>(match.str(1));
            int posY = boost::lexical_cast<int>(match.str(2));

            char mov = scrambletext[posY];
            scrambletext.erase(scrambletext.begin()+posY);
            scrambletext.insert(scrambletext.begin()+posX, mov);
        }
        else
        {
            std::cerr << line << std::endl;
            throw std::runtime_error("Invalid op");
        }
        std::cout << scrambletext << std::endl << std::flush;
        
    }
    
    return scrambletext;
    
}


/*
 * 
 */
int main(int argc, char** argv) {

    std::string scrambletext("fbgdceah");

    std::cout << solution2(scrambletext) << std::endl;
    
    return 0;
}

