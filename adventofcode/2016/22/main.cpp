/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: jan
 *
 * Created on 6. Januar 2017, 23:01
 */

#include <cstdlib>
#include <map>
#include <vector>
#include <iostream>
#include <string>
#include <regex>
#include <boost/lexical_cast.hpp>
#include <set>

using namespace std;

class Node{
public:
    int m_posX, m_posY;
    int m_size, m_used, m_avail, m_usage; 

    Node():m_posX(0), m_posY(0), m_size(0), m_used(0),
          m_avail(0), m_usage(0){}
    Node(int posX, int posY, int size, int used, int avail, int usage)
          : m_posX(posX), m_posY(posY), m_size(size), m_used(used),
          m_avail(avail), m_usage(usage){}
    
    bool fitsIn(const Node &other) const{
        return this->m_used <= other.m_avail;
    }
    
    bool empty() const{
        return this->m_used == 0;
    }
    
};


/*
 * 
 */
int main(int argc, char** argv) {

    std::map<int, std::map<int, Node>> nodes;
    std::vector<Node> nodes_vec;
    
    std::string line;
    while(std::getline(std::cin, line)){
        std::regex re("/dev/grid/node-x(\\d+)-y(\\d+)\\s+(\\d+)T\\s+(\\d+)T\\s+(\\d+)T\\s+(\\d+)%");
        std::smatch match;
        
        if(regex_search(line, match, re)){
            int posX = boost::lexical_cast<int>(match.str(1));
            int posY = boost::lexical_cast<int>(match.str(2));
            int size = boost::lexical_cast<int>(match.str(3));
            int used = boost::lexical_cast<int>(match.str(4));
            int avail = boost::lexical_cast<int>(match.str(5));
            int usage = boost::lexical_cast<int>(match.str(6));
            
            Node node(posX, posY, size, used, avail, usage);            
            nodes[posY][posX] = node;
            nodes_vec.push_back(node);
            std::cout << posX << "." << posY << "." << size << "." << used << "." << avail
                        << "." <<usage << std::endl;

        }
        
    }

    
    int pairs = 0;
    for(int i=0;i<nodes_vec.size();i++){
        for(int j=0;j<nodes_vec.size();j++){
            if(i == j) continue;
            
            const Node &a = nodes_vec[i];            
            const Node &b = nodes_vec[j];
            
            if(!a.empty()){
                if(a.fitsIn(b)){
                    pairs++;
                }
            }
        }
    }
    
    std::cout << "Number of pairs: " << pairs << std::endl;

    
    bool finished = false;

    std::vector<std::string> currentSituation;
    for(auto nodesMap : nodes){
        std::string line;
        for(auto nodePair : nodesMap.second){
            if(nodePair.second.m_used == 0){ line += "_";}
            else if(nodePair.second.m_used < 80){ line += ".";}
            else{ line += "#";}
        }
        currentSituation.push_back(line);
    }
    currentSituation[0][currentSituation[0].size()-1] = 'G';
    
    for(auto line : currentSituation){
        std::cout << line << std::endl;
    }
    
    

    



    
    return 0;
}

