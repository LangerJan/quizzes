/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: jan
 *
 * Created on 1. Januar 2017, 15:04
 */

#include <cstdlib>
#include <string>
#include <iostream>
#include <regex>
#include <boost/lexical_cast.hpp>
#include <climits>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    std::vector<std::function<int(int)>> discs;
    
    std::string line;
    while(std::getline(std::cin, line)){
        std::regex re("Disc #(\\d+) has (\\d+) positions; at time=0, it is at position (\\d+)");
        std::smatch match;
        if(std::regex_search(line, match, re)){
            std::cout << match.str(1) << "/" << match.str(2) << "/" << match.str(3) << std::endl;
            
            int offset = boost::lexical_cast<int>(match.str(1));
            int modulus = boost::lexical_cast<int>(match.str(2));
            int start = boost::lexical_cast<int>(match.str(3));
            
            discs.push_back(
                [offset,modulus,start](int time){
                    return (start+offset+time) % modulus;
                }
            );
            
        }
    }

    for(int i=0;i<std::numeric_limits<int>::max();i++){

        bool broken = false;
        for(auto disc : discs){
            if(disc(i) != 0){broken=true; break;}
        }
        if(!broken){
            std::cout << i << std::endl;
            break;
        }
        
    }
    
    
    return 0;
}









