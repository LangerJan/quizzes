import md5

puzzle_input = "ffykfhsq"


result = list("--------")

sec = 0
while "-" in result:
	found = False
	while not found:
		testinp = "{inp}{key}".format(inp=puzzle_input, key=sec)
		hasher = md5.new()
		hasher.update(testinp)
		digest = hasher.hexdigest()
		if digest.startswith("00000"):
			print digest
			if int(digest[5], 16) < 8 and result[int(digest[5],16)] == '-':
				result[int(digest[5], 16)] = digest[6]
			found = True
			print "found! " + digest[5] + " " + digest[6]
			print testinp
		sec += 1

print result

