import md5

puzzle_input = "ffykfhsq"

result = ""

sec = 0
for i in range(0,8):
	found = False
	while not found:
		testinp = "{inp}{key}".format(inp=puzzle_input, key=sec)
		hasher = md5.new()
		hasher.update(testinp)
		digest = hasher.hexdigest()
		if digest.startswith("00000"):
			result += digest[5]
			found = True
			print "found! " + digest[5] + " " + digest
			print testinp
		sec += 1

print result

