/**
 * Compile with -std=c++14
 * @author Jan Gampe
 */
#include <string>
#include <iostream>
#include <map>
#include <algorithm>

int main(){
	std::map<int, std::map<char, int>> occurences_pos;
	
	std::string line;
	while(std::getline(std::cin, line))
		for(int i=0;i<line.size();i++)
			occurences_pos[i][line[i]]++;

	std::string result;
	for(auto keyValue : occurences_pos){
		 result += (std::min_element(keyValue.second.begin(), keyValue.second.end(),
		 			 [](auto pair_1, auto pair_2){return pair_1.second < pair_2.second;}
		 		   ))->first;
	}
	std::cout << result << std::endl;
	return 0;
}
