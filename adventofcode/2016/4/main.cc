#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
#include <stdexcept>
#include <map>
#include <tuple>
#include <set>
#include <cctype>

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

std::string getChecksum(std::string input){
	
	/* count the occurences of every character of the alphabet */
	std::map<char, int> occurences;	
	for(char c : input){
		if(::isalpha(c)) occurences[c]++;
	}

	/* sort for most occurences, break ties by alphabetic ordering */
	std::vector<std::pair<char, int>> occ_sort(occurences.begin(), occurences.end());
	std::sort(occ_sort.begin(), occ_sort.end(), [](auto foo, auto bar){
		if(foo.second != bar.second){
			return foo.second > bar.second;
		}
		else {
			return foo.first < bar.first;
		}
	});

	/* place the top 5 results into the result string */
	std::string result;
	for(auto i=0;i<5 && i<occ_sort.size();i++){
		result.append(1,occ_sort[i].first);
	}

	return result;
}

std::string rot(std::string input, int rotation){
	std::string result(input);
	std::transform(result.begin(), result.end(), result.begin(), [rotation](char ch) -> char{
		if(ch == '-') return ' ';
		else if(ch >= 'a' && ch <= 'z'){
			return (ch - 'a' + rotation) % 26 + 'a';
		}
		else return ch;

	});
	return result;
}


int main(){
	int result = 0;
	std::string line;
	while(std::getline(std::cin, line)){
		std::vector<std::string> dash_split;
		boost::split(dash_split, line, boost::is_any_of("-"));

		auto start = dash_split[dash_split.size()-1].find_first_of("[");
		auto end = dash_split[dash_split.size()-1].find_first_of("]");

		if(start == std::string::npos || end == std::string::npos) throw std::runtime_error("invalid input - no dash found");
		std::string given_checksum = dash_split[dash_split.size()-1].substr(start+1, end-start-1);
		int code = boost::lexical_cast<int>(dash_split[dash_split.size()-1].substr(0, start));

		dash_split.pop_back();
		
		std::string input = boost::algorithm::join(dash_split, "");
		std::string calced_checksum = getChecksum(boost::algorithm::join(dash_split, ""));
		
		if(boost::equals(given_checksum, calced_checksum)){
			result += code;
			std::cout << rot(line, code) << std::endl;
		}
	}
	std::cout << result << std::endl;
	return 0;
}

