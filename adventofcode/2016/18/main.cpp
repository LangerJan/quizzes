/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: jan
 *
 * Created on 2. Januar 2017, 01:02
 */

#include <cstdlib>
#include <string>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

std::string nextRow(std::string prevRow){
    
    std::string result(prevRow.size(), '.');
    for(int i=0;i<result.size();i++){
        
        char left = i==0?'.':prevRow[i-1];
        char right = i==(prevRow.size()-1)?'.':prevRow[i+1];
        char center = prevRow[i];
        
        if(
          (left == '^' && right == '.' && center == '^') ||
          (left == '.' && right == '^' && center == '^') ||

          (left == '^' && right == '.' && center == '.') ||
          (left == '.' && right == '^' && center == '.')
          
          )
        {
            result[i] = '^';            
        }
        else
        {
            result[i] = '.';
        }
    }
    return result;
}

std::vector<std::string> generateFloor(std::string firstRow, int moreRows){
    std::vector<std::string> result = {firstRow};
    
    for(int i=0;i<moreRows;i++){
        result.push_back(nextRow(result[i]));
    }
    return result;
}

void part1(){
    auto result = generateFloor(".^^^^^.^^^..^^^^^...^.^..^^^.^^....^.^...^^^...^^^^..^...^...^^.^.^.......^..^^...^.^.^^..^^^^^...^.",39);
    
    int size = 0;
    for(auto str : result){
        std::cout << str << std::endl;
        size+= std::count_if(str.begin(), str.end(), [](char x){return x == '.';});
    }
    
    std::cout << size << std::endl;
    
}

void part2(){
    auto result = generateFloor(".^^^^^.^^^..^^^^^...^.^..^^^.^^....^.^...^^^...^^^^..^...^...^^.^.^.......^..^^...^.^.^^..^^^^^...^.",399999);
    
    int size = 0;
    for(auto str : result){
        size+= std::count_if(str.begin(), str.end(), [](char x){return x == '.';});
    }
    
    std::cout << size << std::endl;
    
}

/*
 * 
 */
int main(int argc, char** argv) {

    part2();
    return 0;
}

