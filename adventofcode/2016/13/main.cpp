/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: jan
 *
 * Created on 30. Dezember 2016, 23:53
 */

#include <cstdlib>
#include <bitset>
#include <cstdint>
#include <iostream>
#include <vector>
#include <tuple>
#include <unordered_set>
#include <limits>

using namespace std;


namespace std{
    namespace
    {

        // Code from boost
        // Reciprocal of the golden ratio helps spread entropy
        //     and handles duplicates.
        // See Mike Seymour in magic-numbers-in-boosthash-combine:
        //     http://stackoverflow.com/questions/4948780

        template <class T>
        inline void hash_combine(std::size_t& seed, T const& v)
        {
            seed ^= std::hash<T>()(v) + 0x9e3779b9 + (seed<<6) + (seed>>2);
        }

        // Recursive template code derived from Matthieu M.
        template <class Tuple, size_t Index = std::tuple_size<Tuple>::value - 1>
        struct HashValueImpl
        {
          static void apply(size_t& seed, Tuple const& tuple)
          {
            HashValueImpl<Tuple, Index-1>::apply(seed, tuple);
            hash_combine(seed, std::get<Index>(tuple));
          }
        };

        template <class Tuple>
        struct HashValueImpl<Tuple,0>
        {
          static void apply(size_t& seed, Tuple const& tuple)
          {
            hash_combine(seed, std::get<0>(tuple));
          }
        };
    }

    template <typename ... TT>
    struct hash<std::tuple<TT...>> 
    {
        size_t
        operator()(std::tuple<TT...> const& tt) const
        {                                              
            size_t seed = 0;                             
            HashValueImpl<std::tuple<TT...> >::apply(seed, tt);    
            return seed;                                 
        }                                              

    };
}

bool isWall(int x, int y, int bossNr){
    uint64_t nr = (x*x + 3*x + 2*x*y + y + y*y) + bossNr;
    std::bitset<64> bitNr(nr);
    return bitNr.count() % 2 != 0;
}


std::vector<std::tuple<int,int>> findAllMoves(std::tuple<int,int> position, int bossNr){
    std::vector<std::tuple<int,int>> result;
    
    int x = std::get<0>(position);
    int y = std::get<1>(position);

    if(!isWall(x+1,y,bossNr)){
        result.push_back(std::tuple<int,int>{x+1,y});    
    }
    if(!isWall(x,y+1,bossNr)){
        result.push_back(std::tuple<int,int>{x,y+1});
    }
    if(x > 0){
        if(!isWall(x-1,y,bossNr)){
            result.push_back(std::tuple<int,int>{x-1,y});
        }
    }    
    if(y > 0){
        if(!isWall(x,y-1,bossNr)){
            result.push_back(std::tuple<int,int>{x,y-1});
        }
    }
    
    return result;
}

int solve(std::tuple<int,int> start, std::tuple<int,int> target, int bossNr){
    
    std::unordered_set<std::tuple<int,int>> currentLevel;
    std::unordered_set<std::tuple<int,int>> previousLevels;
    std::unordered_set<std::tuple<int,int>> upcomingLevels = {start};
    
    int lvl = 0;
    while(!upcomingLevels.empty()){
        previousLevels.insert(currentLevel.begin(), currentLevel.end());
        currentLevel.clear();
        currentLevel.insert(upcomingLevels.begin(), upcomingLevels.end());
        upcomingLevels.clear();
        
        for(auto position : currentLevel){
            if(position == target){
                return lvl;
            }
            else
            {
                for(auto move : findAllMoves(position, bossNr)){
                    if(currentLevel.find(move) != currentLevel.end()) continue;
                    if(previousLevels.find(move) != previousLevels.end()) continue;
                    upcomingLevels.insert(move);
                }
            }
        }
        
        lvl++;
    }
    return std::numeric_limits<int>::max();
}

int countLocations(std::tuple<int,int> start, int steps, int bossNr){
    
    std::unordered_set<std::tuple<int,int>> currentLevel = {start};
    std::unordered_set<std::tuple<int,int>> previousLevels;
    std::unordered_set<std::tuple<int,int>> upcomingLevels;
    
    for(int i=0;i<steps;i++){        
        for(auto position : currentLevel){
            for(auto move : findAllMoves(position, bossNr)){
                if(currentLevel.find(move) != currentLevel.end()) continue;
                if(previousLevels.find(move) != previousLevels.end()) continue;
                upcomingLevels.insert(move);
            }
        }
        
        previousLevels.insert(currentLevel.begin(), currentLevel.end());
        currentLevel.clear();
        currentLevel.insert(upcomingLevels.begin(), upcomingLevels.end());
        upcomingLevels.clear();
    }
    
    return previousLevels.size() + currentLevel.size();
}



/*
 * 
 */
int main(int argc, char** argv) {

    int bossNr = 1350;
    
    for(int y=0;y<40;y++){
        for(int x=0;x<40;x++){        
            std::cout << (isWall(x,y,bossNr)?"#":".");
        }
        std::cout << std::endl;
    }
    
    
    std::cout << solve(std::tuple<int,int>{1,1}, std::tuple<int,int>{31,39}, bossNr)
            << std::endl
            << countLocations(std::tuple<int,int>{1,1}, 50, bossNr)
            << std::endl;
     
    return 0;
}

