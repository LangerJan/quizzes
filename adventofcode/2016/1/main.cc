/**
 * Compile with -std=c++11 -pthread
 * @author Jan Gampe
 */

#include <iostream>
#include <string>
#include <sstream>
#include <set>
#include <tuple>
#include <thread>
#include <mutex>

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

int main()
{

    int horizontal_blocks = 0;
    int vertical_blocks = 0;
	std::once_flag flag;
	
	std::set<std::tuple<int,int>> visited_before;

	
	{
		std::tuple<int,int> new_place {horizontal_blocks, vertical_blocks};
		visited_before.insert(new_place);
	}
    char orientation = 'n';

    std::string line;
    while (std::getline(std::cin, line)) {

	std::string val;
	std::istringstream iss(line);
	while (std::getline(iss, val, ',')) {
	    boost::trim(val);
	    int steps = boost::lexical_cast < int >(val.substr(1));
	    char direction = val[0];

		int for_back = 0;
		int *block_to_move;

	    switch (orientation) {
	    case 'n':
	    for_back = (direction == 'R' ? 1 : -1);
	    block_to_move = &horizontal_blocks;
		orientation = (direction == 'R' ? 'e' : 'w');
		break;
	    case 'e':
	    for_back = (direction == 'R' ? -1 : 1);
	    block_to_move = &vertical_blocks;
		orientation = (direction == 'R' ? 's' : 'n');
		break;
	    case 's':
	    for_back = (direction == 'R' ? -1 : 1);
	    block_to_move = &horizontal_blocks;
		orientation = (direction == 'R' ? 'w' : 'e');
		break;
	    case 'w':
	    for_back = (direction == 'R' ? 1 : -1);
	    block_to_move = &vertical_blocks;
		orientation = (direction == 'R' ? 'n' : 's');
		break;
	    default:
		throw std::runtime_error("Invalid input");
	    }

		for(int i=0;i<steps;i++){
			*block_to_move += for_back;
			std::tuple<int,int> new_place {horizontal_blocks, vertical_blocks};
			int distance = ::abs(horizontal_blocks) + ::abs(vertical_blocks);
			if(visited_before.find(new_place) != visited_before.end()){
				std::call_once(flag, [new_place, distance](){
					std::cout << "First place visited twice: " << std::get<0>(new_place)
							  << "/" << std::get<1>(new_place)
							  << " - distance: " << distance << std::endl;
				});

			}
			else
			{
				visited_before.insert(new_place);
			}
		}

	}
    }

	int distance = ::abs(horizontal_blocks) + ::abs(vertical_blocks);
    std::cout << horizontal_blocks << " steps north" << std::endl
	<< vertical_blocks << " steps east. Distance of " << distance << " blocks" << std::endl;
    return 0;
}
