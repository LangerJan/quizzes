/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: jan
 *
 * Created on 31. Dezember 2016, 11:09
 */

#include <cstdlib>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <deque>
#include <map>
#include "md5.h"

#include <boost/algorithm/string.hpp>

using namespace std;

bool hasNLet(std::string &input, int n, char &c){
    for(int i=0;i<input.size()-n+1;i++){
        bool checkNLet = true;
        for(int j=1;j<n;j++){
            if(input[i+j] != input[i]){
                checkNLet = false;
                break;
            }
        }
        if(checkNLet){
            c = input[i];
            return true;
        }
    }
    return false;
}

std::string createKey(std::string secret, int salt){
        std::stringstream ss;
        ss << secret << salt;
        return ss.str();
}

void solve(std::string secret){
    std::vector<std::string> keys;
    std::vector<int> keys_indices;
    
    int i = 0;
    
    
    
    while(keys.size()<64){
    
        std::string key = createKey(secret, i);
        std::string hash = md5(key);
        char doubler;
        if(hasNLet(hash, 3, doubler)){
            std::string fivelet(5, doubler);

            for(int y=1;y<=1000;y++){
                if(boost::contains(md5(createKey(secret, y+i)), fivelet)){
                    std::cout << "found possible key " << i << endl;
                    std::cout << "index " << (y+i) << "matches" << std::endl;
                    keys.push_back(hash);
                    keys_indices.push_back(i);
                    break;
                }
            }
        }
        
        i++;
    }
    
}

std::string multihash(std::string input, int repetitions){
    std::string result = input;
    for(int i=0;i<repetitions;i++){
        result = md5(result);
    }
    return result;
}

void solve2(std::string secret){
    std::vector<std::string> keys;
    std::vector<int> keys_indices;
    
    int i = 0;
    
    std::map<int, std::string> rainbowTable;
    
    
    while(keys.size()<64){
    
        std::string key = createKey(secret, i);
        std::string hash = multihash(key, 2017);
        rainbowTable[i] = hash;
        
        
        char doubler;
        if(hasNLet(hash, 3, doubler)){
            std::string fivelet(5, doubler);

            for(int y=1;y<=1000;y++){
                
                if(rainbowTable.find(y+i) == rainbowTable.end()){
                    rainbowTable[y+i] = multihash(createKey(secret, y+i), 2017);
                }
                
                if(boost::contains(rainbowTable[y+i], fivelet)){
                    std::cout << "found possible key " << i << endl;
                    std::cout << "index " << (y+i) << "matches" << std::endl;
                    keys.push_back(hash);
                    keys_indices.push_back(i);
                    break;
                }
            }
        }
        
        i++;
    }
    
}


/*
 * 
 */
int main(int argc, char** argv) {
    
    std::string secret = "yjdafjpo";
    
    solve2(secret);
    
    
    return 0;
}

