/**
 * Compile with std=c++11
 * @author Jan Gampe
 */

#include <string>
#include <iostream>
#include <stdexcept>


const char numpad[5][5] = {
	{'0','0','1','0','0'},
	{'0','2','3','4','0'},
	{'5','6','7','8','9'},
	{'0','A','B','C','0'},
	{'0','0','D','0','0'}
};

int main(){

	int cursorX = 0;
	int cursorY = 2;

	std::string line;
	while(std::getline(std::cin, line)){
		for(char c : line){
			int nCursorY = cursorY;
			int nCursorX = cursorX;
			switch(c){
				case 'U': nCursorY=(cursorY==0?0:cursorY-1); break;
				case 'D': nCursorY=(cursorY==4?4:cursorY+1); break;
				case 'L': nCursorX=(cursorX==0?0:cursorX-1); break;
				case 'R': nCursorX=(cursorX==4?4:cursorX+1); break;
				default: throw std::runtime_error("invalid input");
			}
			if(numpad[nCursorY][nCursorX] != '0'){
				cursorX = nCursorX;
				cursorY = nCursorY;
			}
		}
		std::cout << std::string(&numpad[cursorY][cursorX],1);

	}

	std::cout << std::endl;
	return 0;
}
