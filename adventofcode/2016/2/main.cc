/**
 * Compile with std=c++11
 * @author Jan Gampe
 */

#include <string>
#include <iostream>
#include <stdexcept>

const int numpad[3][3] = {
	{1,2,3},
	{4,5,6},
	{7,8,9}
};

int main(){

	int cursorX = 1;
	int cursorY = 1;

	std::string line;
	while(std::getline(std::cin, line)){
		for(char c : line){
			switch(c){
				case 'U': cursorY=(cursorY==0?0:cursorY-1); break;
				case 'D': cursorY=(cursorY==2?2:cursorY+1); break;
				case 'L': cursorX=(cursorX==0?0:cursorX-1); break;
				case 'R': cursorX=(cursorX==2?2:cursorX+1); break;
				default: throw std::runtime_error("invalid input");
			}
		}
		std::cout << numpad[cursorY][cursorX];

	}

	std::cout << std::endl;
	return 0;
}
