#include <array>
#include <map>
#include <iostream>
#include <string>
#include <sstream>
#include <set>
#include <vector>
#include <limits>
#include <algorithm>

#include <boost/algorithm/string.hpp>

class GameState {
	public:
	int m_elevatorPos;
	int m_targetFloorPos;
	const int m_maxFloor = 4;

	std::vector<int> m_generatorPos;
	std::vector<int> m_microchipPos;

	GameState(int elevatorPos, int targetPos, std::vector<int> generator_pos, std::vector<int> microchip_pos)
		: m_elevatorPos(elevatorPos), m_targetFloorPos(targetPos), m_generatorPos(generator_pos), m_microchipPos(microchip_pos)
	{}

	GameState(const GameState &cpy) 
		: m_elevatorPos(cpy.m_elevatorPos), m_targetFloorPos(cpy.m_targetFloorPos), m_generatorPos(cpy.m_generatorPos), m_microchipPos(cpy.m_microchipPos)
	{}


	GameState& operator=(GameState cpy){
		this->m_generatorPos = cpy.m_generatorPos;
		this->m_microchipPos = cpy.m_microchipPos;
		this->m_elevatorPos = cpy.m_elevatorPos;
		this->m_targetFloorPos = cpy.m_targetFloorPos;
	}

	bool floorHasGenerator(int floorPos) const{
		for(auto it : m_generatorPos){if(it == floorPos) return true;}
		return false;
	}

	std::vector<int> unpoweredChips() const{
		std::vector<int> result;
		for(int i=0;i<m_microchipPos.size();i++){
			if(m_microchipPos[i] != m_generatorPos[i]) result.push_back(i);
		}
		return result;
	}

	std::set<int> radiatedFloors() const{
		std::set<int> result(m_generatorPos.begin(), m_generatorPos.end());
		return result;
	}

	bool isValid() const{
		std::set<int> radiatedFlrs = this->radiatedFloors();
		for(auto unpoweredChip : this->unpoweredChips()){
			if(radiatedFlrs.find(this->m_microchipPos[unpoweredChip]) != radiatedFlrs.end()){
				return false;
			}
		}
		return true;
	}

	GameState move(std::vector<int> generators, std::vector<int> microchips, int targetFloor, bool &somethingBroke){
		GameState result(*this);

		int incr = targetFloor > result.m_elevatorPos?1:-1;

		somethingBroke = false;
		do {
			result.m_elevatorPos += incr;
			for(auto gen : generators)result.m_generatorPos[gen] += incr;
			for(auto chp : microchips)result.m_microchipPos[chp] += incr;

			if(!result.isValid()){
				somethingBroke = true;
				break;
			}
		}
		while(result.m_elevatorPos != targetFloor);
		return result;
	}

	bool operator<(const GameState &rhs) const{
		std::string str = this->toString();
		std::string rhs_str = rhs.toString();
		return std::lexicographical_compare(str.begin(), str.end(),
						rhs_str.begin(), rhs_str.end());
	}

	bool operator==(const GameState &rhs) const{
		return boost::equals(this->toString(), rhs.toString());
	}

	std::string toString() const{
		std::stringstream result;
		std::set<int> radiatedFlrs = this->radiatedFloors();


		for(int i=m_maxFloor-1;i>=0;i--){
			result << "F" << i << " ";

			if(m_elevatorPos == i) result << "E";
			else result << ".";

			result << " ";

			for(int j=0;j<this->m_generatorPos.size();j++){

				if(m_generatorPos[j] == i) result << j << "G";
				else result << "..";

				result << " ";

				if(m_microchipPos[j] == i) result << j << "M";
				else result << "..";
			
				result << " ";
			}

			if(radiatedFlrs.find(i) != radiatedFlrs.end()) result << "(radiated)";
			else result << "(no radiation)";
			result << std::endl;
		}
		result << (this->isValid()?("Valid state"):("invalid state")) << std::endl;
		result << (this->isWon()?("Win!"):("No Win!")) << std::endl;
		return result.str();
	}

	std::vector<int> availableGenerators() const {
		return availableGenerators(this->m_elevatorPos);
	}

	std::vector<int> availableGenerators(int whichFloor) const {
		std::vector<int> result;
		for(int i=0;i<this->m_generatorPos.size();i++) 
			if(this->m_generatorPos[i] == whichFloor) 
				result.push_back(i);
		return result;
	}

	std::vector<int> availableChips() const{
		return availableChips(this->m_elevatorPos);
	}

	std::vector<int> availableChips(int whichFloor) const{
		std::vector<int> result;
		for(int i=0;i<this->m_microchipPos.size();i++) 
			if(this->m_microchipPos[i] == whichFloor) 
				result.push_back(i);
		return result;
	}

	bool isWon() const {
		return	this->m_elevatorPos == this->m_targetFloorPos &&
				this->availableChips().size() == this->m_microchipPos.size() &&
				this->availableGenerators().size() == this->m_generatorPos.size();
	}

	std::vector<GameState> possibleMoves(){
		std::vector<GameState> result;

		std::vector<int> availChps = this->availableChips();
		std::vector<int> availGens = this->availableGenerators();

		bool tmp_broke;

		std::vector<int> nextFloors;
		if(this->m_elevatorPos > 0 && this->m_elevatorPos < this->m_maxFloor-1){
			nextFloors.push_back(this->m_elevatorPos + 1);
			nextFloors.push_back(this->m_elevatorPos - 1);
		}
		else if(this->m_elevatorPos == 0){
			nextFloors.push_back(this->m_elevatorPos+1);
	}
		else if(this->m_elevatorPos == (this->m_maxFloor -1)){
			nextFloors.push_back(this->m_elevatorPos-1);
		}


		for(int toFloor : nextFloors){

			// Pick one chip
			for(int i=0;i<availChps.size();i++){
				// Choose only one chip
				{
					GameState moved = this->move({},{i},toFloor, tmp_broke);
					if(!tmp_broke){
						result.push_back(moved);
					}
				}
				// Pick another chip
				for(int j=i+1;j<availChps.size();j++){
					GameState moved = this->move({},{i,j},toFloor, tmp_broke);
					if(!tmp_broke) result.push_back(moved);
				}
				// Pick one generator 
				for(int j=0;j<availGens.size();j++){
					GameState moved = this->move({j},{i},toFloor, tmp_broke);
					if(!tmp_broke){
						result.push_back(moved);
					}
				}
			}
			// Pick one generator
			for(int i=0;i<availGens.size();i++){
				// Choose only one generator
				{
					GameState moved = this->move({i},{},toFloor, tmp_broke);
					if(!tmp_broke) result.push_back(moved);
				}
				// Pick another generator 
				for(int j=i+1;j<availGens.size();j++){
					GameState moved = this->move({i,j},{},toFloor, tmp_broke);
					if(!tmp_broke) result.push_back(moved);
				}
			}
		}
		std::sort(result.begin(), result.end(), [](const GameState &a, const GameState &b){
					return a.eval() > b.eval();
				});

		return result;
	}

	int eval() const {
		if(isWon()) return std::numeric_limits<int>::max();
		else
		{
			return this->availableChips(this->m_targetFloorPos).size() + this->availableGenerators(this->m_targetFloorPos).size();
		}
	}

	int solutionIn(int movesAvailable){
		std::map<GameState, std::tuple<int,int>> results;
		return solutionIn(movesAvailable, results);
	}

	int solutionIn(int movesAvailable, std::map<GameState, std::tuple<int,int>> &results){
		
		if(results.find(*this) != results.end()){
			int res_movesAvailable = std::get<0>(results[*this]);
			if(res_movesAvailable > movesAvailable){
				return std::get<1>(results[*this]);
			}
		}

		if(movesAvailable > 0){
			if(this->isWon()){
				std::cout << "Won! " << movesAvailable << std::endl;
				results[*this] = std::tuple<int,int>{movesAvailable,0};
				return 0;
			}
			else
			{
				int bestSolution, currSolution;

				movesAvailable--;
				int bestSolved = std::numeric_limits<int>::max();
				for(auto moves : this->possibleMoves()){

					int movesToGive;
					movesToGive = std::min(bestSolved, movesAvailable);
					currSolution = moves.solutionIn(movesToGive-1, results);

					if(bestSolution > currSolution)
						bestSolution = currSolution;
				}
				if(bestSolution == std::numeric_limits<int>::max()){
					return bestSolution;
				}
				else{
					return bestSolution+1;
				}
			}
		}
		else if(movesAvailable == 0){
			if(this->isWon()){
				return 0;
			}
			else{
				return std::numeric_limits<int>::max();
			}

		}
		else
		{
			return std::numeric_limits<int>::max();
		}
	}

};

int main(){

	//GameState state(0, 3, {0,1,1,1,1}, {0,2,2,2,2});
	GameState state(0, 3, {1,2}, {0,0});
	std::cout << state.toString() << std::endl;
	bool broke;


	auto solution = state.solutionIn(50);
	std::cout << "solution found in " << solution << " steps" << std::endl;



	return 0;
}



