
#include <string>
#include <iostream>
#include <stdexcept>
#include <regex>
#include <array>





class GameFloor { 
	public:
	std::array<bool,5> generators;
	std::array<bool,5> microchips;

	GameFloor(std::array<bool,5> gens, std::array<bool, 5> mchp) : generators(gens), microchips(mchp) {}

	GameFloor(){}

	std::string toString(){
		std::stringstream result;

		for(size_t i=0;i<generators.size();i++){
			if(generators[i]) result << i << "G" << "\t";
			else result << ".." << "\t";
			if(microchips[i]) result << i << "M" << "\t";
			else result << ".." << "\t";
		}
		return result.str();
	}

	bool anyGeneratorActive(){
		for(size_t i=0;i<generators.size();i++){
			if(generators[i]) return true;
		}
		return false;
	}

	bool isValid(){
		for(size_t i=0;i<microchips.size();i++){
			if(microchips[i] && !generators[i] && anyGeneratorActive()) return false;
		}
		return true;
	}

	bool isWon(){
		for(size_t i=0;i<microchips.size();i++){
			if(!(microchips[i] && generators[i])) return false;
		}
		return true;
	}

};

class GameState{
	public:
	std::array<GameFloor, 4> floors;
	size_t elevatorPos;

	int desiredPos;

	GameState(int elevPos, int desPos, std::array<GameFloor,4> floors) :
		elevatorPos(elevPos), desiredPos(desPos), floors(floors){

		
	}

	GameState(const GameState &obj){
		this->elevatorPos = obj.elevatorPos;
		this->desiredPos = obj.desiredPos;

		this->floors = std::array<GameFloor, 4>{{
			obj.floors[0],
			obj.floors[1],
			obj.floors[2],
			obj.floors[3],
			}};
	}

	GameState() : elevatorPos(0), desiredPos(0), floors(std::array<GameFloor,4>()){

	}

	GameState & operator= (const GameState &obj){
		if(this != &obj){
			
			this->elevatorPos = obj.elevatorPos;
			this->desiredPos = obj.desiredPos;

			this->floors = std::array<GameFloor, 4>{{
				obj.floors[0],
				obj.floors[1],
				obj.floors[2],
				obj.floors[3],
			}};
		}
		return *this;
	}


	std::string toString(){
		std::stringstream result;
		for(size_t i=0;i<floors.size();i++){
			result << "F" << i << "\t" << (elevatorPos==i?"E":".") << "\t" 
				<< floors.at(i).toString() << std::endl;
		}
		result << "State valid? " << isValid() << std::endl
			<< "State won? " << isWon() << std::endl;
		return result.str();
	}

	bool isValid(){
		for(auto &fl : this->floors){
			if(!fl.isValid()) return false;
		}
		return true;
	}

	bool isWon(){
		return this->floors[this->desiredPos].isWon();
	}

	std::vector<GameState> getAllNextStates(){
		std::vector<GameState> state;

		for(int newFloor=0;newFloor<this->floors.size();newFloor++){
			if(newFloor == this->elevatorPos) continue;


			GameFloor &currentFloor = this->floors[this->elevatorPos];
			for(int i=0;i<currentFloor.generators.size();i++){
				
				if(currentFloor.generators[i]){
					GameState newState(*this);
					newState.floors[this->elevatorPos].generators[i] = false;
					newState.floors[newFloor].generators[i] = true;
					newState.elevatorPos = newFloor;
					state.push_back(newState);
				}
				if(currentFloor.microchips[i]){
					GameState newState(*this);
					newState.floors[this->elevatorPos].microchips[i] = false;
					newState.floors[newFloor].microchips[i] = true;
					newState.elevatorPos = newFloor;
					state.push_back(newState);
				}
			}

		}
		return state;



	}

};

int main(){
	GameState state(0,3, std::array<GameFloor, 4>{{
			{{true, false, false, false, false},   {true, false, false, false, false}},
			{{false, true, true, true, true},      {false, false, false, false, false}},
			{{false, false, false, false, false},  {false, true, true, true, true}},
			{{false, false, false, false, false},  {false, false, false, false, false}}
			}});


	std::cout << state.toString() << std::endl;
	
	for(auto it : state.getAllNextStates()){
		std::cout << it.toString() << std::endl;
	}

	return 0;
}
