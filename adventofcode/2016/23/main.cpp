/* 
 * File:   main.cpp
 * Author: jan
 *
 * Created on 14. Januar 2017, 20:02
 */

#include <cstdlib>
#include <string>
#include <vector>
#include <iostream>
#include <sstream>
#include <map>
#include <cstdint>
#include <stdexcept>

#include <boost/optional.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>


using namespace std;

typedef enum {
    CPY, INC, DEC, JNZ, TGL
} opcode_t;

typedef struct instruction{
public:
    opcode_t m_opcode;
    std::string m_arg0;
    boost::optional<std::string> m_arg1;
} instruction;

class cpu{
public:
    int pc = 0;
    std::map<std::string,int64_t> registers={{"a",0},{"b",0},{"c",0},{"d",0}};
    
    std::string toString(){
        std::stringstream ss_result;
        ss_result << "pc: " << this->pc << std::endl;
        for(auto reg : this->registers){
            ss_result << reg.first << ":" << reg.second << std::endl;
        }
        return ss_result.str();
    }
};

inline bool isInteger(const std::string & s)
{
   if(s.empty() || ((!isdigit(s[0])) && (s[0] != '-') && (s[0] != '+'))) return false ;

   char * p ;
   strtol(s.c_str(), &p, 10) ;

   return (*p == 0) ;
}

std::vector<instruction> parseInput(){
    std::vector<instruction> result;
    std::string line;
    
    while(std::getline(std::cin, line)){
        std::vector<std::string> strs;
        boost::split(strs, line, boost::is_any_of(" "));

        std::string opcode = strs[0];
        std::string arg0 = strs[1];
        boost::optional<std::string> arg1;
        if(strs.size() > 2) arg1 = strs[2];

        opcode_t opcode_res;
        
        if(boost::equals(opcode, "cpy")){
            opcode_res = CPY;
        }
        else if(boost::equals(opcode, "inc")){
            opcode_res = INC;
        }
        else if(boost::equals(opcode, "dec")){
            opcode_res = DEC;
        }
        else if(boost::equals(opcode, "jnz")){
            opcode_res = JNZ;
        }
        else if(boost::equals(opcode, "tgl")){
            opcode_res = TGL;
        }
        else{
            std::cerr << opcode << std::endl;
            throw std::runtime_error("unknown opcode");
        }
        
        instruction elem = {opcode_res, arg0, arg1};
        result.push_back(elem);
    }
    return result;
}


void handleCPY(cpu &proc, std::vector<instruction> &instructions){
    if(isInteger(*instructions[proc.pc].m_arg1)){
        proc.pc++;
    }
    else
    {
        if(isInteger(instructions[proc.pc].m_arg0)){
            proc.registers[*instructions[proc.pc].m_arg1] 
                    = boost::lexical_cast<int>(instructions[proc.pc].m_arg0);
            proc.pc++;
        }
        else
        {
            proc.registers[*instructions[proc.pc].m_arg1] 
                    = proc.registers[instructions[proc.pc].m_arg0];
            proc.pc++;
        }
    }
}

void handleINC(cpu &proc, std::vector<instruction> &instructions){
    proc.registers[instructions[proc.pc].m_arg0]++;
    proc.pc++;
}

void handleDEC(cpu &proc, std::vector<instruction> &instructions){
    proc.registers[instructions[proc.pc].m_arg0]--;
    proc.pc++;
}

void handleJNZ(cpu &proc, std::vector<instruction> &instructions){
    int64_t arg1_int;
    if(isInteger(*instructions[proc.pc].m_arg1)){
        arg1_int = boost::lexical_cast<int>(*instructions[proc.pc].m_arg1);
    }
    else
    {
        arg1_int = proc.registers[*instructions[proc.pc].m_arg1];
    }

    if(isInteger(instructions[proc.pc].m_arg0)){
        if(boost::lexical_cast<int>(instructions[proc.pc].m_arg0) != 0){
            proc.pc += arg1_int;
        }
        else{
            proc.pc++;                    
        }                        
    }
    else
    {                    
        if(proc.registers[instructions[proc.pc].m_arg0] != 0){
            proc.pc += arg1_int;                        
        }
        else{
            proc.pc++;                    
        }                        
    }                    
}

void handleTGL(cpu &proc, std::vector<instruction> &instructions){
    int64_t arg0_int;
    if(isInteger(instructions[proc.pc].m_arg0)){
        arg0_int = boost::lexical_cast<int>(instructions[proc.pc].m_arg0);
    }
    else
    {
        arg0_int = proc.registers[instructions[proc.pc].m_arg0];
    }


    int target_pos = arg0_int + proc.pc;

    if(target_pos >= instructions.size() ||
            target_pos < 0){
        proc.pc++;
    }
    else
    {
        auto &target_instrcution = instructions[target_pos];

        if(!target_instrcution.m_arg1.is_initialized())
        {
            if(target_instrcution.m_opcode == INC){
               target_instrcution.m_opcode = DEC; 
            }
            else
            {
               target_instrcution.m_opcode = INC;                        
            }                        
        }
        else{
            if(target_instrcution.m_opcode == JNZ){
               target_instrcution.m_opcode = CPY; 
            }
            else
            {
               target_instrcution.m_opcode = JNZ;                        
            }                        

        }
        proc.pc++;
    }
}


bool specialCase(cpu &proc, std::vector<instruction> instructions){
    bool doSwitch = true;    
    if(proc.pc + 2 < instructions.size()){
        if(instructions[proc.pc + 2].m_opcode == JNZ){
            if(isInteger(*instructions[proc.pc + 2].m_arg1)){
                if(boost::lexical_cast<int>(*instructions[proc.pc + 2].m_arg1) == -2){
                    if(
                       (
                       instructions[proc.pc].m_opcode == INC ||
                       instructions[proc.pc].m_opcode == DEC
                       )
                       &&
                       (
                       instructions[proc.pc+1].m_opcode == INC ||
                       instructions[proc.pc+1].m_opcode == DEC
                       )
                      )
                    {
                        std::string mod_a = instructions[proc.pc].m_arg0;
                        std::string mod_b = instructions[proc.pc+1].m_arg0;
                        std::string chk = instructions[proc.pc+2].m_arg0;


                        int64_t loops;
                        int64_t modifier_pc;
                        if(boost::equals(chk, mod_a)){
                            if(isInteger(mod_a)){
                                loops = std::abs(boost::lexical_cast<int>(mod_a));                                
                            }
                            else
                            {
                                loops = std::abs(proc.registers[mod_a]);                                    
                            }
                            modifier_pc = proc.pc+1;
                        }
                        else if(boost::equals(chk, mod_b)){
                            if(isInteger(mod_b)){
                                loops = std::abs(boost::lexical_cast<int>(mod_b));                                
                            }
                            else
                            {
                                loops = std::abs(proc.registers[mod_b]);                                    
                            }
                            modifier_pc = proc.pc;
                        }
                        else
                        {
                            throw std::runtime_error("invalid state");
                        }

                        if(instructions[modifier_pc].m_opcode == INC){
                            if(loops == 0)loops = 1;
                            proc.registers[instructions[modifier_pc].m_arg0] += loops;
                            proc.pc += 3;
                            doSwitch = false;
                        }
                        else if(instructions[modifier_pc].m_opcode == DEC){
                            if(loops == 0)loops = 1;
                            proc.registers[instructions[modifier_pc].m_arg0] -= loops;
                            proc.pc += 3;
                            doSwitch = false;
                        }
                    }
                }
            }
        }
    }
    return doSwitch;
}


bool specialCase2(cpu &proc, std::vector<instruction> instructions){
    bool doSwitch = true;    
    if(proc.pc + 5 < instructions.size()){
        if(
          (!instructions[proc.pc].m_arg1.is_initialized() ||
           !instructions[proc.pc+3].m_arg1.is_initialized() ||     
           !instructions[proc.pc+5].m_arg1.is_initialized()
          )
          ){
            return true;
        }
        
        if(
           instructions[proc.pc].m_opcode == CPY &&
           boost::equals(instructions[proc.pc].m_arg0, "b") &&
           (instructions[proc.pc].m_arg1 && boost::equals(*instructions[proc.pc].m_arg1, "c")) &&

           instructions[proc.pc+1].m_opcode == INC &&
           boost::equals(instructions[proc.pc+1].m_arg0, "a") &&

           instructions[proc.pc+2].m_opcode == DEC &&
           boost::equals(instructions[proc.pc+2].m_arg0, "c") &&

           instructions[proc.pc+3].m_opcode == JNZ &&
           boost::equals(instructions[proc.pc+3].m_arg0, "c") &&
           (instructions[proc.pc+3].m_arg1 && boost::equals(*instructions[proc.pc+3].m_arg1, "-2")) &&

           instructions[proc.pc+4].m_opcode == DEC &&
           boost::equals(instructions[proc.pc+4].m_arg0, "d") &&

           instructions[proc.pc+5].m_opcode == JNZ &&
           boost::equals(instructions[proc.pc+5].m_arg0, "d") &&
           (instructions[proc.pc+5].m_arg1 && boost::equals(*instructions[proc.pc+5].m_arg1, "-5"))

           )
        {
            std::cout << "boom " << proc.pc << std::endl;
            std::cout << proc.registers["b"] << " * " << proc.registers["d"] << std::endl;
            proc.registers["a"] += (proc.registers["b"]) * (proc.registers["d"]); 
            proc.registers["c"] = 0;
            proc.registers["d"] = 0;

            proc.pc += 5;
            
            return false;
        }
    }
    return doSwitch;
}


void runInstructions(cpu &proc, std::vector<instruction> instructions){
    while(proc.pc < instructions.size()){
        
        bool doSwitch = true;
        
        //if(doSwitch) doSwitch = specialCase(proc, instructions);
        if(doSwitch) doSwitch = specialCase2(proc, instructions);

        if(doSwitch)
        {
            switch(instructions[proc.pc].m_opcode){
                case CPY:
                    handleCPY(proc, instructions);
                    break;
                case INC:
                    handleINC(proc, instructions);
                    break;
                case DEC:
                    handleDEC(proc, instructions);
                    break;
                case JNZ:
                    handleJNZ(proc, instructions);
                    break;
                case TGL:
                    handleTGL(proc, instructions);
                    break;
                default:
                    throw std::runtime_error("invalid opcode");
            }
        }
    }
}




/*
 * 
 */
int main(int argc, char** argv) {

    cpu proc;
    
    proc.registers["a"] = 12; 
    proc.registers["b"] = 0; 
    proc.registers["c"] = 0;
    proc.registers["d"] = 0;
    
    runInstructions(proc, parseInput());
    
    std::cout << proc.toString() << std::endl;
    
    return 0;
}

