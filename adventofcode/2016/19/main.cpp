/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: jan
 *
 * Created on 2. Januar 2017, 23:11
 */

#include <cstdlib>
#include <vector>
#include <iostream>
#include <forward_list>
#include <list>
#include <iterator>
#include <cstdint>
#include <cmath>

using namespace std;

int solve(int elves){
    std::vector<int> elvesPresents(elves, 1);
    
    int currentElf = 0;

    do{
        int nextElf = (currentElf + 1) % elvesPresents.size();
        for(;elvesPresents[nextElf] == 0;nextElf = (nextElf + 1) % elvesPresents.size()){}

        elvesPresents[currentElf] += elvesPresents[nextElf];
        elvesPresents[nextElf] = 0;

        
        currentElf = nextElf;
        for(;elvesPresents[currentElf] == 0;currentElf = (currentElf + 1) % elvesPresents.size()){}
    }
    while(elvesPresents[currentElf] != elves);
    
    return currentElf+1;
}

int solve2(int elves){
    std::vector<int> elvesPresents(elves, 1);
    
    int elvesLeft = elves;
    int currentElf = 0;

    do{
        
        int elfToStealFrom = (elvesLeft / 2);
        
        int elfToStealFromPos = currentElf;
        
        for(int i=0;i<elfToStealFrom;i++){
            do{
                elfToStealFromPos = (elfToStealFromPos+1) % elvesPresents.size();
            }while(elvesPresents[elfToStealFromPos] == 0);
        }
                
        elvesPresents[currentElf] += elvesPresents[elfToStealFromPos];
        elvesPresents[elfToStealFromPos] = 0;
        elvesLeft--;
        
        do{
            currentElf = (currentElf+1) % elvesPresents.size();
        }while(elvesPresents[currentElf] == 0);
    }
    while(elvesPresents[currentElf] != elves);
    
    return currentElf+1;
}


int itoa(int value, char *sp, int radix)
{
    char tmp[16];// be careful with the length of the buffer
    char *tp = tmp;
    int i;
    unsigned v;

    int sign = (radix == 10 && value < 0);    
    if (sign)
        v = -value;
    else
        v = (unsigned)value;

    while (v || tp == tmp)
    {
        i = v % radix;
        v /= radix; // v/=radix uses less CPU clocks than v=v/radix does
        if (i < 10)
          *tp++ = i+'0';
        else
          *tp++ = i + 'a' - 10;
    }

    int len = tp - tmp;

    if (sign) 
    {
        *sp++ = '-';
        len++;
    }

    while (tp > tmp)
        *sp++ = *--tp;

    return len;
}

int solve2better(int32_t elves){
    int32_t p = 2;

    for(int32_t i=4;i<=elves;i++) {
        p++;
        p %= (i - 1);
        if(p >= i / 2) p++;
    }
    p++;
    return p;
}

/*
 * 
 */
int main(int argc, char** argv) {

    std::cout << solve(3014387) << std::endl;
    
    std::cout << solve2better(3014387) << std::endl;
    return 0;
}

