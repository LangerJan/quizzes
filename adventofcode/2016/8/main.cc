#include <iostream>
#include <string>
#include <algorithm>
#include <array>
#include <regex>
#include <stdexcept>

#include <boost/lexical_cast.hpp>

void rect(int x, int y, std::array<std::array<bool, 50>, 6> &lcd){
	for(int xi=0;xi<x;xi++){
		for(int yi=0;yi<y;yi++){
			lcd[yi][xi] = true;
		}
	}
}

void rotateRow(int rowID, int times, std::array<std::array<bool, 50>, 6> &lcd){
	for(int i=0;i<times;i++){
		std::array<bool, 50> replace;
		for(int j=0;j<lcd[rowID].size();j++){
			int newJ = j+1 >= lcd[rowID].size()?0:j+1;
			replace[newJ] = lcd[rowID][j];
		}
		lcd[rowID] = replace;
	}
}

void rotateCol(int colID, int y, std::array<std::array<bool, 50>, 6> &lcd){
	for(int i=0;i<y;i++){
		std::array<bool, 6> replace;
		for(int j=0;j<lcd.size();j++){
			int newJ = j+1 >= lcd.size()?0:j+1;
			replace[newJ] = lcd[j][colID];
		}

		for(int j=0;j<lcd.size();j++){
			lcd[j][colID] = replace[j];
		}
	}
}

void print(std::array<std::array<bool, 50>, 6> &lcd){
	int cnt = 0;
	for(int y=0;y<lcd.size();y++){
		for(int x=0;x<lcd[y].size();x++){
			if(lcd[y][x]){ std::cout << "#"; cnt++;}
			else std::cout << ".";
		}
		std::cout << std::endl;
	}
	std::cout << "\t\t\t" << cnt << std::endl;
}


int main(){

	std::array<std::array<bool, 50>, 6> lcd;
	
	for(auto &x : lcd){
		for(auto &y : x){
			y = false;
		}
	}


	std::string line;
	while(std::getline(std::cin, line)){

		std::regex rex_rect("rect (\\d*)x(\\d*)");
		std::regex rex_rotRow("rotate row y=(\\d*) by (\\d*)");
		std::regex rex_rotCol("rotate column x=(\\d*) by (\\d*)");

		std::smatch match;
		if(std::regex_search(line, match, rex_rect)){
			int x = boost::lexical_cast<int>(match.str(1));
			int y = boost::lexical_cast<int>(match.str(2));
			rect(x,y,lcd);

		}
		else if(std::regex_search(line, match, rex_rotRow)){
			int x = boost::lexical_cast<int>(match.str(1));
			int y = boost::lexical_cast<int>(match.str(2));
			rotateRow(x,y,lcd);
		}
		else if(std::regex_search(line, match, rex_rotCol)){
			int x = boost::lexical_cast<int>(match.str(1));
			int y = boost::lexical_cast<int>(match.str(2));
			rotateCol(x,y,lcd);
		}
		else {
			throw std::runtime_error("invalid input");
		}

		print(lcd);
	}

	return 0;
}
