/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: jan
 *
 * Created on 2. Januar 2017, 00:28
 */

#include <cstdlib>
#include <vector>
#include <string>
#include <iostream>
#include <unordered_map>
#include "md5.h"

using namespace std;

class state{
public:
    int m_x;
    int m_y;
    std::string m_way;

    
    state() : m_x(0), m_y(0), m_way(std::string()){}
    state(int x, int y, std::string way) : m_x(x), m_y(y), m_way(way){}
    
    bool isWon(){
        return m_x == 3 && m_y == 3;
    }
    
    std::vector<state> validMoves(){
        std::vector<state> result;
        std::string hash = md5(m_way);
            
        if(m_y > 0){
            if(hash[0] >= 'b' && hash[0] <= 'f'){
                state move(this->m_x,this->m_y-1, this->m_way + "U");
                result.push_back(move);
            }
        }
        if(m_y < 3){
            if(hash[1] >= 'b' && hash[1] <= 'f'){
                state move(this->m_x,this->m_y+1, this->m_way + "D");
                result.push_back(move);
            }
        }
        if(m_x > 0){
            if(hash[2] >= 'b' && hash[2] <= 'f'){
                state move(this->m_x-1,this->m_y, this->m_way + "L");
                result.push_back(move);
            }
        }
        if(m_x < 3){
            if(hash[3] >= 'b' && hash[3] <= 'f'){
                state move(this->m_x+1,this->m_y, this->m_way + "R");
                result.push_back(move);
            }
        }
        return result;
    }
    
    std::string solve(){
        
        std::unordered_map<std::string, state> checkedStates;
        std::unordered_map<std::string, state> currentStates =
        {{this->m_way, *this}};
 
        std::unordered_map<std::string, state> nextStates;
 
        while(!currentStates.empty()){
            for(auto stat : currentStates){
                if(stat.second.isWon()){
                    return stat.second.m_way;
                }
                else
                {
                    for(auto move : stat.second.validMoves()){
                        if(checkedStates.find(move.m_way) != checkedStates.end()) continue;
                        if(currentStates.find(move.m_way) != currentStates.end()) continue;
                        nextStates[move.m_way] = move;
                    }
                }
            }
            checkedStates.insert(currentStates.begin(), currentStates.end());
            currentStates.clear();
            currentStates.insert(nextStates.begin(), nextStates.end());
            nextStates.clear();
        }
        
        return "nothing";
    }

    int solve2(){
        size_t longest = 0;
        
        std::unordered_map<std::string, state> checkedStates;
        std::unordered_map<std::string, state> currentStates =
        {{this->m_way, *this}};
 
        std::unordered_map<std::string, state> nextStates;
        
        while(!currentStates.empty()){
            for(auto stat : currentStates){
                if(stat.second.isWon()){
                    longest = std::max(longest,stat.second.m_way.size());
                }
                else
                {
                    for(auto move : stat.second.validMoves()){
                        if(checkedStates.find(move.m_way) != checkedStates.end()) continue;
                        if(currentStates.find(move.m_way) != currentStates.end()) continue;
                        nextStates[move.m_way] = move;
                    }
                }
            }
            checkedStates.insert(currentStates.begin(), currentStates.end());
            currentStates.clear();
            currentStates.insert(nextStates.begin(), nextStates.end());
            nextStates.clear();
        }
        
        return longest - this->m_way.size();
    }


};


/*
 * 
 */
int main(int argc, char** argv) {

    
    state stat(0, 0, std::string("rrrbmfta"));
    std::cout << stat.solve() << std::endl;
    std::cout << stat.solve2() << std::endl;
    return 0;
}

