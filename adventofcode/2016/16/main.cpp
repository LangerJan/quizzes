/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: jan
 *
 * Created on 1. Januar 2017, 23:57
 */

#include <cstdlib>
#include <string>
#include <iostream>
#include <algorithm>

using namespace std;

std::string expand(std::string input){
    std::string a(input);
    std::string b(a.rbegin(), a.rend());
    std::replace(b.begin(), b.end(), '0','2');
    std::replace(b.begin(), b.end(), '1','0');
    std::replace(b.begin(), b.end(), '2','1');
    
    a += "0";
    a += b;
    return a;
}

std::string checksum(std::string input){
    std::string result;
    
    for(int i=0;i<input.size()-1;i=i+2){
        
        if(input[i] == input[i+1]){
            result += "1";
        }
        else
        {
            result += "0";
        }
    }
    
    if(result.size() % 2 == 0){
        return checksum(result);
    }
    else {
        return result;    
    }
}

void fillDisk(std::string initialState, int size){
    while(initialState.size() < size){
        initialState = expand(initialState);
    }
    initialState = initialState.substr(0, size);
    std::string chksum = checksum(initialState);
    std::cout << "checksum is '" << chksum << "'" << std::endl;
    
}


/*
 * 
 */
int main(int argc, char** argv) {
    
    fillDisk("10000", 20);
    fillDisk("01111010110010011", 272);
    fillDisk("01111010110010011", 35651584);

    return 0;
}

