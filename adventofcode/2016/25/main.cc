/**
 * This is my puzzle input, decompiled 
 * by hand and steady eye into C++.
 * Once you see the pattern, it's obvious
 * what we are looking for...
 */

#include <iostream>

void foo(int a){
	int d = a;
	int c = 7;
	int b = 365;

	d = d + (2555);

	while(true){
		a = d;
		while(a != 0){
			b = a;
			a = 0;

			
			while(b >= 2){
				b = b - 2;
				a++;
			}
			
			c = 2 - b;
			b = 0;


			b = 2;
			b = b - c;		
			std::cout << b;
		}
		std::cout << std::endl;
	}
}

int main(){
	foo(175);
	return 0;
}


