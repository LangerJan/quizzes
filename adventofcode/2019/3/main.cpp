#include <iostream>
#include <string>
#include <map>
#include <set>
#include <tuple>
#include <vector>
#include <boost/algorithm/string.hpp>
#include <limits>

using namespace std;



int main(){
	set<tuple<int,int>> wire1;
	map<tuple<int,int>, int> wire1_step;

	int lowest = std::numeric_limits<int>::max();
	int lowest2 = std::numeric_limits<int>::max();

	{
	string line;
	getline(cin, line);
	vector<string> strs;
	boost::split(strs,line,boost::is_any_of(","));
	int xPos = 0, yPos = 0;
	int total_steps = 0;
	for(string dir : strs){
		char direction = dir.at(0);
		int steps = stoi(dir.substr(1,dir.size()-1));

		for(int step = 0;step<steps;step++){
 			switch(direction){
				case 'R':
					xPos++;
				break;
				case 'L':
					xPos--;
				break;
				case 'U':
					yPos++;
				break;
				case 'D':
					yPos--;
				break;
				default:
					cerr << "invalid direction " << direction << endl;
					return -1;
			}
			wire1.insert({xPos,yPos});
			wire1_step[{xPos,yPos}] = ++total_steps;
		}
	}
	}


	{
	string line;
	getline(cin, line);
	vector<string> strs;
	boost::split(strs,line,boost::is_any_of(","));
	int xPos = 0, yPos = 0;
	int wire2_steps = 0;
	for(string dir : strs){
		char direction = dir.at(0);
		int steps = stoi(dir.substr(1,dir.size()-1));

		for(int step = 0;step<steps;step++){
 			switch(direction){
				case 'R':
					xPos++;
				break;
				case 'L':
					xPos--;
				break;
				case 'U':
					yPos++;
				break;
				case 'D':
					yPos--;
				break;
				default:
					cerr << "invalid direction " << direction << endl;
					return -1;
			}
			wire2_steps++;

			if(wire1.find({xPos,yPos}) != wire1.end()){
				lowest = min(lowest, (abs(xPos)+abs(yPos)));

				lowest2 = min(lowest2, wire2_steps+wire1_step.at({xPos,yPos}));
			}
		}
	}
	}

	cout << lowest << endl;
	cout << lowest2 << endl;
	return 0;
}
