#include "../intcode/intcode.hpp"

#include <iostream>

using namespace std;

int main(){

	intcodeMachine machine({1,0,0,0,99});
	machine.run();

	cout << machine.m_program.at(0) << endl;

	return 0;
}
