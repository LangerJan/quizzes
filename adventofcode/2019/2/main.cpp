#include <iostream>
#include <string>
#include <vector>
#include <boost/algorithm/string.hpp>
#include <cstdint>

#include "../intcode/intcode.hpp"

using namespace std;



bool tryOut(vector<int64_t> program, int64_t noun, int64_t verb, int64_t expected_result){
	program.at(1) = noun;
	program.at(2) = verb;
	int pc=0;
	while(program.at(pc) != 99){
		switch(program.at(pc)){
			case 1:
			{
				int64_t pos1 = program.at(pc+1);
				int64_t val1 = program.at(pos1);
				int64_t pos2 = program.at(pc+2);
				int64_t val2 = program.at(pos2);
				int64_t pos3 = program.at(pc+3);
				program.at(pos3) = val1 + val2;
				pc += 4;
			}
			break;
			case 2:
			{
				int64_t pos1 = program.at(pc+1);
				int64_t val1 = program.at(pos1);
				int64_t pos2 = program.at(pc+2);
				int64_t val2 = program.at(pos2);
				int64_t pos3 = program.at(pc+3);
				program.at(pos3) = val1 * val2;
				pc += 4;
			}
			break;
			default:
			cerr << "Invlaid opcode " << program.at(pc) << endl;
			return -1;
		}
	}
	
	return (program.at(0) == expected_result);
}

void solve1_intcode(vector<int64_t> program){
	intcodeMachine machine(program);
	machine.run();
	cout << machine.m_program.at(0) << endl;
}

void solve1(vector<int64_t> program){
	int pc=0;
	while(program.at(pc) != 99){
		switch(program.at(pc)){
			case 1:
			{
				int64_t pos1 = program.at(pc+1);
				int64_t val1 = program.at(pos1);
				int64_t pos2 = program.at(pc+2);
				int64_t val2 = program.at(pos2);
				int64_t pos3 = program.at(pc+3);
				program.at(pos3) = val1 + val2;
				pc += 4;
			}
			break;
			case 2:
			{
				int64_t pos1 = program.at(pc+1);
				int64_t val1 = program.at(pos1);
				int64_t pos2 = program.at(pc+2);
				int64_t val2 = program.at(pos2);
				int64_t pos3 = program.at(pc+3);
				program.at(pos3) = val1 * val2;
				pc += 4;
			}
			break;
			default:
			cerr << "Invlaid opcode " << program.at(pc) << endl;
			return;
		}
	}

	cout << program.at(0) << endl;
}

int main(){

	string line;
	getline(cin, line);
	vector<int64_t> program;
	{
		vector<string> strs;
		boost::split(strs,line,boost::is_any_of(","));
		for(auto str : strs){
			program.push_back(stoi(str));
		}
	}	

	solve1(program);
	solve1_intcode(program);

	for(int64_t noun = 0;noun<100;noun++){
		for(int64_t verb = 0;verb<100;verb++){
			if(tryOut(program, noun, verb, 19690720)){
				cout << ((noun*100)+verb) << endl;
				return 0;
			}
		}
	}
	

	return 0;
}
