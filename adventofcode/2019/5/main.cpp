#include "../intcode/intcode.hpp"

#include <fstream>

int main(int argc, char* argv[]){
	intcodeMachine machine;

	std::ifstream stream(argv[1]);
	machine.readProgram(stream);
	machine.run();

	return 0;
}
