#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main(){

	vector<int> modules;	

	int result1, result2 = 0;
	for(string line;getline(cin, line);){
		modules.push_back(stoi(line));
	}	

	for(auto module : modules){
		result1 += module / 3 - 2;
	}

	cout << result1 << endl;
	
	for(auto module : modules){
		int totalFuelReq = 0;
		int fuelReq;
		int mod = module;
		do {
			fuelReq = (mod / 3.0) - 2;
			mod = fuelReq;
			if(fuelReq > 0) totalFuelReq += fuelReq;
		} while(fuelReq > 0);

		result2 += totalFuelReq;
	}

	cout << result2 << endl;
	return 0;
}
