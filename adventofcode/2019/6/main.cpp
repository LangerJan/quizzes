#include <iostream>
#include <set>
#include <string>
#include <vector>

#include <boost/algorithm/string.hpp>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/graphviz.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/transitive_closure.hpp>

struct Orbitable{
	std::string name;
};

struct OrbitEdge{
	int cost;
};

//Some typedefs for simplicity
typedef boost::property<boost::edge_weight_t, int> EdgeWeightProperty;
typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::bidirectionalS, Orbitable, EdgeWeightProperty> directed_graph_t;
typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS, Orbitable, EdgeWeightProperty> undirected_graph_t;
typedef boost::graph_traits<directed_graph_t>::vertex_descriptor vertex_t;



int main()
{
	directed_graph_t g;
	undirected_graph_t undG;
	std::map<std::string,vertex_t> vertices;
	{
		for(std::string line; getline(std::cin, line);){
			std::vector<std::string> strs;
			split(strs,line,boost::is_any_of(")"));
			std::string base = strs.at(0);
			std::string orbiter = strs.at(1);

			if(vertices.find(base) == vertices.end()){
			       	vertices[base] = boost::add_vertex(g);
				g[vertices[base]].name = base;
			}
			if(vertices.find(orbiter) == vertices.end()) {
				vertices[orbiter] = boost::add_vertex(g);
				g[vertices[orbiter]].name = orbiter;
			}
			boost::add_edge(vertices[orbiter], vertices[base], 1, g);
		}
	}

	{
		// Part One:
		directed_graph_t tg;
		boost::transitive_closure(g, tg);
		std::cout << num_edges(tg) << std::endl;
	}
	{
		// Part Two:
		boost::copy_graph(g, undG);
		std::vector<vertex_t> p(num_vertices(undG));
		std::vector<int> d(boost::num_vertices(undG));

		boost::dijkstra_shortest_paths(
				undG, 
				vertices["YOU"],
				predecessor_map(boost::make_iterator_property_map(p.begin(), get(boost::vertex_index, undG))).
			 	distance_map(boost::make_iterator_property_map(d.begin(), get(boost::vertex_index, undG))));
		
		boost::graph_traits < undirected_graph_t >::vertex_iterator vi, vend;
		for (boost::tie(vi, vend) = boost::vertices(g); vi != vend; ++vi) {
			if(*vi == vertices["SAN"]) std::cout << (d[*vi]-2) << std::endl;
		}
	}
	std::ofstream outf("test.dot");
	boost::write_graphviz(outf, undG,
   		boost::make_label_writer(boost::get(&Orbitable::name, undG)));
	return 0;
}

