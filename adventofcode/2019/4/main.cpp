#include <array>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

inline bool rule_twoAdjacentsSame(const array<int,6> &input){
	return adjacent_find(input.begin(), input.end()) != input.end();
}

inline bool rule_oneTwin(const array<int,6> &input){
	for(int i=0;i<5;i++){
		if(input.at(i) == input.at(i+1)){
			if(i>0){if(input.at(i-1) == input.at(i)) continue;}
			if(i<4){if(input.at(i+2) == input.at(i)) continue;}
			return true;
		}
	}
	return false;
}

inline bool rule_twoAdjacentsNeverDecrease(const array<int,6> &input){
	return adjacent_find(input.begin(), input.end(), [](auto i, auto j){return j < i;}) == input.end();
}

inline bool meetsCriteria(int inp){
	static array<int, 6> input;
	for(int j=5;j>=0;j--){
		input.at(j) = inp%10;
		inp /= 10;
	}
	return rule_twoAdjacentsSame(input) && rule_twoAdjacentsNeverDecrease(input);
}

inline bool meetsCriteriaTwo(int inp){
	static array<int, 6> input;
	for(int j=5;j>=0;j--){
		input.at(j) = inp%10;
		inp /= 10;
	}
	return rule_oneTwin(input) && rule_twoAdjacentsNeverDecrease(input);
}

int main(){
	int result = 0;
	int result2 = 0;

	for(int i=256310;i<=732736;i++){
		if(meetsCriteria(i)) result++;
		if(meetsCriteriaTwo(i)) result2++;
	}

	cout << result << endl;
	cout << result2 << endl;
	return 0;
}
