#ifndef ROBOT_HPP
#define ROBOT_HPP

#include "../intcode/intcode.hpp"

#include <array>
#include <fstream>
#include <map>
#include <memory>
#include <set>
#include <sstream>
#include <tuple>
#include <vector>
#include <valarray>

class robot{
	public:
	static const std::tuple<int,int> UP;
	static const std::tuple<int,int> DOWN;
	static const std::tuple<int,int> LEFT;
	static const std::tuple<int,int> RIGHT;
	static const std::array<std::tuple<int,int>, 4> directions;
	static const std::map<std::tuple<int,int>, char> markers;
	
	int m_currentDirectionIdx = 0;
	std::tuple<int,int> m_currentPosition = {0,0};
	std::set<std::tuple<int,int>> m_fieldsPainted;

	std::unique_ptr<intcodeMachine> m_machine;
	std::unique_ptr<std::stringstream> m_machineInput;
	std::unique_ptr<std::stringstream> m_machineOutput;
	std::set<std::tuple<int,int>> m_whiteTiles;

	void move();

	public:
	robot(std::istream &program);

	void run();
	void print();

};


#endif
