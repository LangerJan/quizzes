#include "robot.hpp"

#include <limits>

const std::tuple<int,int> robot::UP = {0,-1};
const std::tuple<int,int> robot::DOWN = {0,1};
const std::tuple<int,int> robot::LEFT = {-1,0};
const std::tuple<int,int> robot::RIGHT = {1,0};

const std::array<std::tuple<int,int>, 4> robot::directions = {
	UP, RIGHT, DOWN, LEFT
};

const std::map<std::tuple<int,int>, char> robot::markers = {
	{UP, '^'},
	{DOWN, 'v'},
	{LEFT, '<'},
	{RIGHT, '>'}
};

robot::robot(std::istream &program){
	this->m_machineInput = std::make_unique<std::stringstream>();
	this->m_machineOutput = std::make_unique<std::stringstream>();
	this->m_machine = std::make_unique<intcodeMachine>(
			*this->m_machineInput, *this->m_machineOutput);

	this->m_machine->readProgram(program);
	this->m_machine->m_waitOnEmptyRead = true;
}


void robot::run(){

	while(!this->m_machine->m_halted){
		if(this->m_whiteTiles.find(this->m_currentPosition) != this->m_whiteTiles.end()){
			(*this->m_machineInput) << "1" << std::endl;
		}
		else
		{
			(*this->m_machineInput) << "0" << std::endl;
		}
		this->m_machine->run();

		int color;
		(*this->m_machineOutput) >> color;
		int dir;
		(*this->m_machineOutput) >> dir;

		this->m_fieldsPainted.insert(this->m_currentPosition);

		if(color == 0){
			//paint black
			this->m_whiteTiles.erase(this->m_currentPosition);
		}
		else
		{
			// paint white
			this->m_whiteTiles.insert(this->m_currentPosition);
		}

		if(dir == 0){
			if(this->m_currentDirectionIdx == 0){
				this->m_currentDirectionIdx = directions.size() - 1;
			}
			else {
				this->m_currentDirectionIdx--;
			}
		}
		else
		{
			if(this->m_currentDirectionIdx+1 == directions.size()){
				this->m_currentDirectionIdx = 0;
			}
			else
			{
				this->m_currentDirectionIdx++;
			}
		}
		move();
	}
}

void robot::move(){
	std::get<0>(this->m_currentPosition) += std::get<0>(directions.at(this->m_currentDirectionIdx));
	std::get<1>(this->m_currentPosition) += std::get<1>(directions.at(this->m_currentDirectionIdx));
}

void robot::print(){
	int minX = std::numeric_limits<int>::max();
	int minY = std::numeric_limits<int>::max();
	int maxX = std::numeric_limits<int>::min();
	int maxY = std::numeric_limits<int>::min();

	for(auto tile : this->m_whiteTiles){
		minX = std::min(std::get<0>(tile), minX);
		minY = std::min(std::get<1>(tile), minY);
		maxX = std::max(std::get<0>(tile), maxX);
		maxY = std::max(std::get<1>(tile), maxY);
	}

	std::vector<std::string> lines;
	int numLines = maxY-minY+1;
	int numColumns = maxX-minX+1;

	for(int i=0;i<numLines;i++){
		lines.push_back(std::string(numColumns, '_'));
	}

	for(auto tile : this->m_whiteTiles){
		int xPos = std::get<0>(tile);
		int yPos = std::get<1>(tile);

		xPos = xPos - minX;
		yPos = yPos - minY;

		lines.at(yPos).at(xPos) = '#';
	}

	for(auto line : lines){
		std::cout << line << std::endl;
	}
}


int main(int argc, char* argv[]){

	{
		std::ifstream file(argv[1]);
		robot r(file);
		r.run();
		std::cout << ":" << r.m_fieldsPainted.size() << std::endl;
	}
	{
		std::ifstream file(argv[1]);
		robot r(file);
		r.m_whiteTiles.insert({0,0});
		r.run();
		r.print();
	}

		
	return 0;
}

