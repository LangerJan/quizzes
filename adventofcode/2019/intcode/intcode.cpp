
#include "../intcode/intcode.hpp"

#include <iostream>
#include <string>
#include <stdexcept>
#include <vector>

#include <boost/algorithm/string.hpp>

intcodeMachine::intcodeMachine(std::istream& in, std::ostream& out) :
	m_in(in),
	m_out(out){
}

intcodeMachine::intcodeMachine(const std::vector<int64_t> program, std::istream &in, std::ostream &out) : 
	m_in(in),
	m_out(out){
	this->m_program = program;

	this->m_program.resize(this->m_program.size()+1024*1024);
	std::fill(&this->m_program[program.size()], &this->m_program[this->m_program.size()-1], 0);

}

void intcodeMachine::instructionFetch(){
	this->m_curr_instr_addressing.clear();
	this->m_next_pc = -1;
	this->m_curr_instr_addr.clear();
	this->m_curr_instr_params.clear();
	this->m_curr_instr = this->m_program.at(this->m_pc);
}

void intcodeMachine::instructionDecode(){
	this->m_curr_opcode = this->m_curr_instr % 100;

	auto opcodeInfo_it = opcodes.find(this->m_curr_opcode);
	if(opcodeInfo_it == opcodes.end()){
		std::cerr << "Instr: " << this->m_curr_instr << std::endl
			<< "Opcode: " << this->m_curr_opcode << std::endl
			<< "PC: " << this->m_pc << std::endl;
		throw std::runtime_error("Instruction decode: Invalid opcode");
	}

	this->m_curr_opcode_info = opcodes.find(this->m_curr_opcode)->second;

	integer opcode = this->m_curr_instr / 100;
	for(int i=0;i<this->m_curr_opcode_info.m_argc;i++){
		int op = opcode % 10;
		opcode = opcode / 10;
		addressing_type addrType;
		auto arg = this->m_program.at(this->m_pc+i+1);
		switch(op){
			case 0:
				addrType = INDIRECT;
				this->m_curr_instr_params.push_back(this->m_program.at(arg));
				this->m_curr_instr_addr.push_back(arg);
				break;
			case 1:
				addrType = IMMEDIATE;
				this->m_curr_instr_params.push_back(arg);
				this->m_curr_instr_addr.push_back(-1);
				break;
			case 2:
				addrType = RELATIVE;
				this->m_curr_instr_params.push_back(this->m_program.at(arg+this->m_relativeBase));
				this->m_curr_instr_addr.push_back(arg+this->m_relativeBase);
				break;
			default:
				std::cerr << this->m_curr_instr << std::endl;
				std::cerr << op << std::endl;
				throw std::runtime_error("Invalid addressing type while decoding");
		}
		this->m_curr_instr_addressing.push_back(addrType);
	}
}

void intcodeMachine::execute(){
	this->m_executors[this->m_curr_opcode]();
}

void intcodeMachine::writeBack(){
	for(auto i : this->m_curr_opcode_info.m_paramsWriteBack){
		auto addr = this->m_curr_instr_addr.at(i);
		auto val = this->m_curr_instr_params.at(i);
		this->m_program.at(addr) = val;
	}

	if(this->m_next_pc == -1){
		this->m_next_pc = this->m_pc + this->m_curr_opcode_info.m_argc + 1;
	}
	this->m_pc = this->m_next_pc;
}

void intcodeMachine::step(){
	instructionFetch();
	instructionDecode();
	execute();
	writeBack();
}

void intcodeMachine::run(){
	this->m_waiting = false;
	while(!this->m_halted && !this->m_waiting){
		//this->dbg_printInts();
		this->step();
	}
}

void intcodeMachine::do_add(){
	this->m_curr_instr_params.at(2) = this->m_curr_instr_params.at(0) + this->m_curr_instr_params.at(1);
}

void intcodeMachine::do_mult(){
	this->m_curr_instr_params.at(2) = this->m_curr_instr_params.at(0) * this->m_curr_instr_params.at(1);

}

void intcodeMachine::do_readInt(){
	this->m_in.clear();
	std::string line;
	if(getline(this->m_in, line)){
		this->m_curr_instr_params.at(0) = std::stoll(line);
	}
	else
	{
		this->m_waiting = true;
		this->m_next_pc = this->m_pc;
		this->m_in.clear();
	}
}

void intcodeMachine::do_writeInt(){
	this->m_out << this->m_curr_instr_params.at(0) << std::endl;
}

void intcodeMachine::do_jumpIfTrue(){
	this->m_next_pc = (this->m_curr_instr_params.at(0) != 0)?this->m_curr_instr_params.at(1):this->m_next_pc;
}

void intcodeMachine::do_jumpIfFalse(){
	this->m_next_pc = (this->m_curr_instr_params.at(0) == 0)?this->m_curr_instr_params.at(1):this->m_next_pc;
}

void intcodeMachine::do_lessThan(){
	this->m_curr_instr_params.at(2) = (this->m_curr_instr_params.at(0) < this->m_curr_instr_params.at(1))?1:0;
}

void intcodeMachine::do_equals(){
	this->m_curr_instr_params.at(2) = (this->m_curr_instr_params.at(0) == this->m_curr_instr_params.at(1))?1:0;
}

void intcodeMachine::do_adjustRelativeBase(){
	this->m_relativeBase += this->m_curr_instr_params.at(0);	
}

void intcodeMachine::do_halt(){
	this->m_halted = true;
}

void intcodeMachine::dbg_printInts(){
	for(uint64_t i =0;i<this->m_program.size();i++){
		if(i == this->m_pc){
			std::cout << "(";
		}
		std::cout << this->m_program.at(i);
		if(i == this->m_pc){
			std::cout << ")";
		}
		std::cout << " ";
	}
	std::cout << std::endl;
}

void intcodeMachine::readProgram(std::istream &stream){
	this->m_program.clear();
	std::string line;
	std::getline(stream, line);
        {
		std::vector<std::string> strs;
                boost::split(strs,line,boost::is_any_of(","));
                for(auto str : strs){
                        this->m_program.push_back(stoll(str));
                }
        }
	auto sz = this->m_program.size();
	this->m_program.resize(sz+1024*1024);
	std::fill(&this->m_program[sz], &this->m_program[this->m_program.size()-1], 0);
}

void intcodeMachine::readProgram(std::vector<integer> prg){
	this->m_program = prg;
	auto sz = this->m_program.size();
	this->m_program.resize(sz+1024*1024);
	std::fill(&this->m_program[sz], &this->m_program[this->m_program.size()-1], 0);
}

std::string intcodeMachine::toString(){
	std::stringstream ss;

	ss << "Machine " << (this->m_halted?"halted":"not halted") << " /  " << (this->m_waiting?"waiting":"not waiting") << std::endl
	<< "at PC: " << this->m_pc << std::endl;

	return ss.str();
}
