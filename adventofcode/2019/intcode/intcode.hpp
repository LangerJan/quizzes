#ifndef INTCODE_HPP
#define INTCODE_HPP

#include <cstdint>
#include <functional>
#include <fstream>
#include <iostream>
#include <map>
#include <set>
#include <string>
#include <sstream>
#include <vector>

typedef int64_t integer;

typedef enum addressing_type{
	IMMEDIATE,
	INDIRECT,
	RELATIVE
} addressing_type;

typedef struct opcode_info{
	std::string m_name;
	integer m_opcode;
	int m_argc;
	std::set<int> m_paramsWriteBack = {};
} opcode_info;


#define OP_ADD 1
#define OP_MULT 2
#define OP_READINT 3
#define OP_WRITEINT 4
#define OP_JUMP_IF_TRUE 5
#define OP_JUMP_IF_FALSE 6
#define OP_LESS_THAN 7
#define OP_EQUALS 8
#define OP_ADJ_RELATIVE_BASE 9
#define OP_HALT 99

#define OP_GET_DE(x) (x % 100)

const std::map<integer, opcode_info> opcodes{
	{OP_ADD, {"ADD", OP_ADD, 3, {2}}},
	{OP_MULT, {"MULT", OP_MULT, 3, {2}}},
	{OP_READINT, {"READINT", OP_READINT, 1, {0}}},
	{OP_WRITEINT, {"WRITEINT", OP_WRITEINT, 1, {}}},
	{OP_JUMP_IF_TRUE, {"JUMP_IF_TRUE", OP_JUMP_IF_TRUE, 2}},
	{OP_JUMP_IF_FALSE, {"JUMP_IF_FALSE", OP_JUMP_IF_FALSE, 2}},
	{OP_LESS_THAN, {"LESS_THAN", OP_LESS_THAN, 3, {2}}},
	{OP_EQUALS, {"EQUALS", OP_EQUALS, 3, {2}}},
	{OP_ADJ_RELATIVE_BASE, {"OP_ADJ_RELATIVE_BASE", OP_ADJ_RELATIVE_BASE, 1, {}}},

	{OP_HALT, {"HALT", OP_HALT, 0, {}}}
};


class intcodeMachine{
	public:
	bool m_halted = false;
	bool m_waiting = false;
	bool m_waitOnEmptyRead = false;

	uint64_t m_relativeBase = 0;
	uint64_t m_pc = 0;
	std::vector<integer> m_program;

	std::istream &m_in;
	std::ostream &m_out;

	integer m_curr_instr;
	integer m_curr_opcode;

	opcode_info m_curr_opcode_info;
	std::vector<addressing_type> m_curr_instr_addressing;
	std::vector<integer> m_curr_instr_addr;
	std::vector<integer> m_curr_instr_params;
	integer m_next_pc = -1;

	intcodeMachine(std::istream& in=std::cin, std::ostream& out=std::cout);
	intcodeMachine(const std::vector<integer> program, std::istream &in=std::cin, std::ostream &out=std::cout);

	std::map<integer, std::function<void(void)>> m_executors = {
		{OP_ADD, std::bind(&intcodeMachine::do_add, this)},
		{OP_MULT, std::bind(&intcodeMachine::do_mult, this)},
		{OP_READINT, std::bind(&intcodeMachine::do_readInt, this)},
		{OP_WRITEINT, std::bind(&intcodeMachine::do_writeInt, this)},
		{OP_JUMP_IF_TRUE, std::bind(&intcodeMachine::do_jumpIfTrue, this)},
		{OP_JUMP_IF_FALSE, std::bind(&intcodeMachine::do_jumpIfFalse, this)},
		{OP_LESS_THAN, std::bind(&intcodeMachine::do_lessThan, this)},
		{OP_EQUALS, std::bind(&intcodeMachine::do_equals, this)},
		{OP_ADJ_RELATIVE_BASE, std::bind(&intcodeMachine::do_adjustRelativeBase, this)},
		{OP_HALT, std::bind(&intcodeMachine::do_halt, this)}
	};

	void instructionFetch();
	void instructionDecode();
	void execute();
	void writeBack();

	void do_add();
	void do_mult();
	void do_halt();

	void do_readInt();
	void do_writeInt();

	void do_jumpIfTrue();
	void do_jumpIfFalse();
	void do_lessThan();
	void do_equals();
	
	void do_adjustRelativeBase();

	void run();
	void step();
	
	void readProgram(std::istream &stream);
	void readProgram(std::vector<integer> prg);
	void dbg_printInts();

	void initializeExecutorTable();
	static intcodeMachine createMachine(const std::vector<int64_t> program, std::istream &in, std::ostream &out);

	std::string toString();
};


#endif /* INTCODE_HPP */
