#include <iostream>
#include <boost/numeric/conversion/cast.hpp> 
#include <string>
#include <set>
#include <array>
#include <vector>
#include <algorithm>
#include <cstdint>
#include <numeric>

using namespace std;

// #define TESTDATA

#ifndef TESTDATA
#define WIDTH 25
#define HEIGHT 6
#else
#define WIDTH 3
#define HEIGHT 2
#endif

vector<vector<vector<int>>> readImage(){
	string line;
	getline(cin, line);

	vector<vector<vector<int>>> image;

	image.push_back(vector<vector<int>>());
	image.at(0).push_back(vector<int>());

	for(char c : line){
		int ci = stoi(std::string(&c, 1));
	
		auto lineSize = image.back().size();
		auto lineWidth = image.back().back().size();

		if(lineWidth == WIDTH){
			if(lineSize == HEIGHT){
				image.push_back(vector<vector<int>>());
			}
			image.back().push_back(vector<int>());
			lineSize = image.back().size();
			lineWidth = image.back().back().size();
		}
		image.back().back().push_back(ci);
	}
	return image;
}

void day1(vector<vector<vector<int>>> image){
	auto result = max_element(image.begin(), image.end(), [](const vector<vector<int>> &layer1, const vector<vector<int>> &layer2){
				return accumulate(layer1.begin(), layer1.end(), 0, 
						[](auto i, auto &l){return i+count_if(l.begin(),l.end(), [](auto &i){return i == 0;});})
					>
				  	accumulate(layer2.begin(), layer2.end(), 0, 
						[](auto i, auto &l){return i+count_if(l.begin(),l.end(), [](auto &i){return i == 0;});});
			});

	int mult = accumulate(result->begin(), result->end(), 0, 
                              [](auto i, auto &l){return i+count_if(l.begin(),l.end(), [](auto &i){return i == 1;});})
			*
		   accumulate(result->begin(), result->end(), 0, 
                              [](auto i, auto &l){return i+count_if(l.begin(),l.end(), [](auto &i){return i == 2;});});

	cout << mult << endl;
}

void day2(vector<vector<vector<int>>> image){

	array<array<int, WIDTH>, HEIGHT> result;

	for(int y=0;y<result.size();y++){
		auto &line = result.at(y);
		for(int x=0;x<line.size();x++){
			int pixel = 2;
			for(auto &layer : image){
				switch(layer.at(y).at(x)){
					case 0:
					       pixel = pixel==2?0:pixel;
				       	       break;	       
					case 1:
					       pixel = pixel==2?1:pixel;
				       	       break;	       
					default:
				       	       break;	       
				}
			}
			line.at(x) = pixel;
			if(line.at(x) == 0) cout << " ";
			else if(line.at(x) == 1) cout << "X";
		}
		cout << endl;
	}


}

int main(){
	auto image = readImage();

	day1(image);
	day2(image);

	return 0;
}
