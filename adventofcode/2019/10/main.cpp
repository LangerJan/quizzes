#include <algorithm>
#include <iostream>
#include <limits>
#include <vector>
#include <set>
#include <cstdint>
#include <string>
#include <numeric>
#include <tuple>
#include <map>
#include <cmath>

#include <gmp.h>
#include <gmpxx.h>

using namespace std;

void printMap(set<tuple<int,int>> asts, int baseX, int baseY){

	int minY = numeric_limits<int>::max(),
	    maxY = numeric_limits<int>::min(),
	    minX = numeric_limits<int>::max(),
	    maxX = numeric_limits<int>::min();

	for(auto ast : asts){
		minY = min(minY, get<1>(ast));
		minX = min(minX, get<0>(ast));
		maxY = max(maxY, get<1>(ast));
		maxX = max(maxX, get<0>(ast));
	}

	for(int y=minY;y<=maxY;y++){
		for(int x=minX;x<=maxX;x++){
			if(asts.find({x,y}) != asts.end()){
				cout << "#";
			}
			else if(x == baseX && y == baseY){
				cout << "X";
			}
			else {
				cout << ".";
			}
		}
		cout << endl;
	}


}


tuple<set<tuple<int,int>>, int, int> readAsteroids(){
	set<tuple<int,int>> asteroids;
	
	int x=0;
	int y=0;
	for(string line;getline(cin, line);y++){
		x=0;
		for(auto &c : line){
			if(c == '#'){
				asteroids.insert({x,y});
			}
			x++;

		}
	}
	return {asteroids, x+1, y+1};
}


int main(){

	auto [asts, width, height] = readAsteroids();

	int bestSight = numeric_limits<int>::min();
	tuple<int,int> bestPos;

	for(auto basePos : asts){

		set<tuple<int,int>> visibleAsteroids = asts;
		visibleAsteroids.erase(basePos);
		int basePosX = get<0>(basePos);
		int basePosY = get<1>(basePos);

		for(auto asteroid : asts){
			if(asteroid == basePos) continue;
			
			int astPosX = get<0>(asteroid);
			int astPosY = get<1>(asteroid);

			int diffX = astPosX - basePosX;
			int diffY = astPosY - basePosY;

			int basicStepX, basicStepY;
			if(diffX == 0){
				basicStepX = 0;
				basicStepY = (diffY<0?-1:1);
			}
			else if(diffY == 0){
				basicStepX = (diffX<0?-1:1);
				basicStepY = 0;
			}
			else
			{
				mpq_class ratio(diffX, diffY);
				ratio.canonicalize();

				basicStepX = copysign(ratio.get_num().get_si(),diffX);
				basicStepY = copysign(ratio.get_den().get_si(),diffY);
			}
			
			int nextStepX = astPosX + basicStepX;
			int nextStepY = astPosY + basicStepY;

			while( (nextStepX >= 0 && nextStepX < width) && (nextStepY >= 0 && nextStepY < height) ){
				if(visibleAsteroids.find({nextStepX, nextStepY}) != visibleAsteroids.end()){
					visibleAsteroids.erase({nextStepX, nextStepY});
				}
				nextStepX += basicStepX;
				nextStepY += basicStepY;
			}
		}
			int sight = visibleAsteroids.size();
			if(sight > bestSight){
				bestSight = sight;
				bestPos = {basePosX, basePosY};
			}

	}

	int bestPosX = get<0>(bestPos);
	int bestPosY = get<1>(bestPos);
	cout << "Best base is at " << get<0>(bestPos) << " / " << get<1>(bestPos) << " with " << bestSight << " visible asteroids" << endl;

	asts.erase(bestPos);

	printMap(asts, bestPosX, bestPosY);

	map<double, set<tuple<int,int>>> relAsts;

	for(auto ast : asts){
		int astX = get<0>(ast);
		int astY = get<1>(ast);

		int relAstPosX = astX - bestPosX;
		int relAstPosY = astY - bestPosY;
		
		double degree = ((atan2(relAstPosY,relAstPosX) * 180 / M_PI));
		degree *= 1000;
		relAsts[floor(degree)].insert(ast);

		cout << "There is an asteroid at " << "rel(" << relAstPosX << "/" << relAstPosY << ")" 
					           << ", abs(" << astX << "/" << astY << ")"
						   << " degree(" << degree << ")" << endl;
	}

	
	int killCnt = 0;
	double startDegree = -90 * 1000;

	while(killCnt < 201){

		for(auto &relAstPr : relAsts){

			if(relAstPr.first < startDegree) continue;
			cout << "looking at " << relAstPr.first << endl;
			if(!relAstPr.second.empty()){
				auto elem = min_element(relAstPr.second.begin(), relAstPr.second.end(), [](const tuple<int, int>& p1, const tuple<int, int>& p2){
					return (get<0>(p1)+get<1>(p1)) < (get<0>(p2)+get<1>(p2));
				});

				killCnt++;
				cout << "(" << relAstPr.first << "°)" << killCnt << ":" << get<0>(*elem) << "/" << get<1>(*elem) << endl;
	
				relAstPr.second.erase(*elem);
				if(killCnt >= 201){
					break;
				}
			}
		}
		startDegree = -360 * 1000;
		cout << "Start at " << startDegree << endl;

	}

	return 0;
}
