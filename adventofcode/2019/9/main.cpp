#include "../intcode/intcode.hpp"

#include <fstream>
#include <iostream>

int main(int argc, char* argv[]){
	intcodeMachine m;

	std::ifstream file(argv[1]);
	m.readProgram(file);
	m.run();
	std::cout << m.toString() << std::endl;
	return 0;
}

