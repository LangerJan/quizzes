#include "../intcode/intcode.hpp"

#include <algorithm>

#include <cstdint>
#include <limits>
#include <memory>
#include <iostream>
#include <fstream>
#include <sstream>

#include <vector>

const std::vector<int64_t> program = {3,8,1001,8,10,8,105,1,0,0,21,30,39,64,81,102,183,264,345,426,99999,3,9,1001,9,2,9,4,9,99,3,9,1002,9,4,9,4,9,99,3,9,1002,9,5,9,101,2,9,9,102,3,9,9,1001,9,2,9,1002,9,2,9,4,9,99,3,9,1002,9,3,9,1001,9,5,9,1002,9,3,9,4,9,99,3,9,102,4,9,9,1001,9,3,9,102,4,9,9,1001,9,5,9,4,9,99,3,9,101,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,1,9,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,99,3,9,101,1,9,9,4,9,3,9,101,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,99,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,99,3,9,101,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,101,1,9,9,4,9,99};


const std::vector<int64_t> testprogram = {3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5};
const std::vector<int64_t> testprogram2 = {3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10};

int64_t day1_permute(int64_t input, std::set<int64_t> seq){
	int64_t result = std::numeric_limits<int64_t>::min();
	for(auto s : seq){
		std::stringstream in, out;
		in << s << std::endl;
		in << input << std::endl;
		intcodeMachine machine(program, in, out);
		machine.run();
		int64_t output;
		out >> output;

		std::set<int64_t> nextSeq(seq.begin(), seq.end());
		nextSeq.erase(s);

		if(!nextSeq.empty()){
			output = day1_permute(output, nextSeq);
		}

		result = std::max(result, output);
	}

	return result;
}

int64_t day2(std::vector<int64_t> phase_setting){
	const int num_pipeline = 5;
	std::vector<std::unique_ptr<intcodeMachine>> machines;
	std::vector<std::stringstream> pipes;

	for(int i=0;i<num_pipeline;i++) pipes.push_back(std::stringstream());
	for(int i=0;i<num_pipeline;i++){
		machines.push_back(std::make_unique<intcodeMachine>(program, pipes.at(i), pipes.at((i+1)%num_pipeline)));
		pipes.at(i) << phase_setting.at(i) << std::endl;
	}

	pipes.at(0) << 0 << std::endl;

	bool halted = false;
	do{
		for(auto &m : machines){
			m->run();
			halted |= m->m_halted;
		}
	}while(!halted);

	int64_t result;
	pipes.at(0) >> result;
	return result;
}

int64_t day2_permute(){
	int64_t result = std::numeric_limits<int64_t>::min();
	std::vector<int64_t> seq = {5,6,7,8,9};
	std::sort(seq.begin(), seq.end());
	do{
		result = std::max(result, day2(seq));
	}while(std::next_permutation(seq.begin(),seq.end()));
	return result;
}

int main(){
	std::cout << day1_permute(0, {0,1,2,3,4}) << std::endl;
	std::cout << day2_permute() << std::endl;
	return 0;
}
