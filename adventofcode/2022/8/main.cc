#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
#include <map>
#include <set>
#include <cstdint>
#include <valarray>

using namespace std;

using direction = tuple<int, int>;
using position = tuple<int, int>;

const vector<direction> directions{
	{ 0,-1}, // up
	{ 0, 1}, // down
	{-1, 0}, // left
	{ 1, 0}  // right
};

map<position, int> readInput(){
	map<position, int> result;
	int y = 0;
	for(string line;getline(cin,line);){
		int x = 0;
		for(auto c : line){
			result[{x++,y}] = c - '0';
		}
		y++;
	}
	return result;
}

bool isVisible(map<position, int> &forest, position tree){
	bool result = false;
	int treeVal = forest[tree];
	for(auto &direction : directions){
		position pos = tree;
		pos = {get<0>(pos)+get<0>(direction),get<1>(pos)+get<1>(direction)};
		auto it = forest.find(pos);
		result = true;
		while(it != forest.end()){
			if(it->second >= treeVal){
			       	result = false;
				break;
			}

			pos = {get<0>(pos)+get<0>(direction),get<1>(pos)+get<1>(direction)};
			it = forest.find(pos);
		}
		if(result) break;
	}
	return result;
}

void solveOne(map<position,int> forest){
	int result = 0;

	set<position> founds;
	for(auto pr : forest){
		if(isVisible(forest, pr.first)){
		       	result++;
			founds.insert(pr.first);
		}
	}

	{	
		int x,y;
	 	for(int y = 0;;y++){
			for(x = 0;;x++){
				position pos{x,y};
				if(forest.find(pos) == forest.end()) break;
				cout << forest[pos];
			}
			cout << endl;
			if(x==0) break;
	}

	}
	int x,y;
	for(int y = 0;;y++){
		for(x = 0;;x++){
			position pos{x,y};
			if(forest.find(pos) == forest.end()) break;
			if(founds.find(pos) == founds.end()) cout << '0';
			else cout << 'X';
		}
		cout << endl;
		if(x==0) break;
	}
	
	cout << result << endl;
}

uint64_t determineView(map<position,int> &forest, position tree){
	uint64_t score = 1;
	int treeVal = forest[tree];
	for(auto &direction : directions){
		uint64_t product = 0;
		position pos = tree;
		pos = {get<0>(pos)+get<0>(direction),get<1>(pos)+get<1>(direction)};

		auto it = forest.find(pos);
		while(it != forest.end()){
			product++;
			if(it->second >= treeVal){
				break;
			}
			pos = {get<0>(pos)+get<0>(direction),get<1>(pos)+get<1>(direction)};
			it = forest.find(pos);
		}
		score *= product;
	}
	return score;
}

void solveTwo(map<position,int> forest){
	uint64_t maxView = 0;

	for(auto pr : forest){
		maxView = max(maxView, determineView(forest, pr.first));
	}

	cout << maxView << endl;
}

int main(){
	auto forest = readInput();
	//solveOne(forest);
	solveTwo(forest);

	return 0;
}






