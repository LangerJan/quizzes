#include <vector>
#include <string>
#include <cstdint>
#include <set>
#include <algorithm>
#include <memory>
#include <map>
#include <iostream>

#include <boost/algorithm/string.hpp>


using namespace std;

class dir;

class inode{
	public:
		virtual size_t getSize() = 0;
		virtual vector<shared_ptr<dir>> getAllDirectories(){
			return {};
		}

		virtual bool isDir(){return false;}
};

class dir : public inode {
	public:
		dir(){}
		dir(shared_ptr<dir> p){
			this->parent = p;
		}

		shared_ptr<dir> parent;
		map<string, shared_ptr<inode>> children;

		size_t getSize() override {
			size_t result = 0;
			for(auto &inode_pr : children){
				result += inode_pr.second->getSize();
			}
			return result;
		}

		vector<shared_ptr<dir>> getAllDirectories() override{
			vector<shared_ptr<dir>> result;
			for(auto &inode_pr : children){
				if(inode_pr.second->isDir()) result.push_back(dynamic_pointer_cast<dir>(inode_pr.second));
				auto subDirs = inode_pr.second->getAllDirectories();
				result.insert(result.end(), subDirs.begin(), subDirs.end());
			}
			return result;
		}

		bool isDir() override{return true;}
};

class regularFile : public inode {
	public:
		regularFile(size_t s) : size(s){}
		size_t size;

		size_t getSize() override{
			return size;
		}
};


shared_ptr<dir> readInput(){
	shared_ptr<dir> result = make_shared<dir>();;

	shared_ptr<dir> currentDir;
	bool lsMode = false;
	for(string line;getline(cin,line);){
		vector<string> strs;
		boost::split(strs,line,boost::is_any_of(" "));

		if(strs[0] == "$"){
			lsMode = false;
			if(strs[1] == "cd"){
				if(strs[2] == "/"){
					currentDir = result;
				}
				else if(strs[2] == ".."){
					if(currentDir->parent){
						currentDir = currentDir->parent;
					}
					else
					{
						cerr << "Cannot find parent dir" << endl;
					}
				}
				else{
					auto it = currentDir->children.find(strs[2]);
					if(it != currentDir->children.end()){
						if(it->second->isDir()){
							currentDir = dynamic_pointer_cast<dir>(it->second);
						}
					}
					else
					{
						cerr << "Cannot find subdir " << strs[2] << endl;
						return result;
					}
				}
			}
			else if(strs[1] == "ls"){
				lsMode = true;
			}
		}
		else
		{
			if(lsMode){
				if(strs.size() != 2){
					cerr << "Illegal ls line" << endl;
				}
				if(strs.at(0) == "dir"){
					shared_ptr<dir> d = make_shared<dir>(currentDir);
					currentDir->children[strs.at(1)] = d;
				}
				else
				{
					shared_ptr<regularFile> file = make_shared<regularFile>(stoi(strs.at(0)));
					currentDir->children[strs.at(1)] = file;
				}
			}
		}
	}

	return result;
}


int main(){
	auto root = readInput();
	
	size_t totalDiskSpace = 70000000;
	size_t used = root->getSize();
	size_t needed = 30000000;
	size_t free = totalDiskSpace - used;
	size_t yetToFree = needed - free;


	size_t bestFit = totalDiskSpace;
	int sum = 0;
	for(auto &dir : root->getAllDirectories()){
		if(dir->getSize() < 100000){
			sum += dir->getSize();
		}

		if(dir->getSize() >= yetToFree && dir->getSize() < bestFit){
			bestFit = dir->getSize();
			
		}
	}
	cout << "Part 1: " << sum << endl;
	cout << "Part 2: " << bestFit << endl;

	return 0;
}






