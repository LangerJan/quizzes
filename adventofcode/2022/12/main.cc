#include <iostream>
#include <string>
#include <cstdint>
#include <map>
#include <set>
#include <tuple>
#include <utility>
#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <limits>

template<typename... T1, typename... T2, std::size_t... I>
constexpr auto add(const std::tuple<T1...>& t1, const std::tuple<T2...>& t2, 
                   std::index_sequence<I...>)
{
    return std::tuple{ std::get<I>(t1) + std::get<I>(t2)... };
}

template<typename... T1, typename... T2>
constexpr auto operator+(const std::tuple<T1...>& t1, const std::tuple<T2...>& t2)
{
    // make sure both tuples have the same size
    static_assert(sizeof...(T1) == sizeof...(T2));

    return add(t1, t2, std::make_index_sequence<sizeof...(T1)>{});
}

using namespace std;

using position = tuple<int,int>;
using direction = position;


direction UP{0,1}, DOWN{0,-1}, LEFT{-1,0}, RIGHT{1,0};
const vector<direction> DIRECTIONS{UP,DOWN,LEFT,RIGHT};

struct puzzle_input{
	map<position,int> height_map;
	position start;
	position end;
	int height;
	int width;
};

void drawInput(puzzle_input input){
	for(int y = 0;y<input.height;y++){
		for(int x=0;x<input.width;x++){
			position pos{x,y};
			if(input.height_map.find(pos) != input.height_map.end()){
				int elevation = input.height_map[pos];
				char elev_ch = 'a' + elevation;

				cout << elev_ch;
			}
			else
			{
				cout << "?";
			}

		}
		cout << endl;
	}
}

puzzle_input read_puzzle_input(){
	map<position,int> result;
	position start;
	position end;
	int height;
	int width;

	int y = 0;
	for(string line;getline(cin,line);){
		width = line.size();
		int x = 0;
		for(auto c : line){
			char elev_ch = c;
			position pos{x,y};
			if(c == 'S'){
				start = pos;
				elev_ch = 'a';
			}
			else if(c == 'E'){
				end = pos;
				elev_ch = 'z'; 
			}
			int elevation = elev_ch - 'a';
			result[pos] = elevation;
			x++;
		}
		y++;
	}

	return {result, start, end, y, width};
}

set<position> findNextSteps(position currPos, map<position,int> &height_map){
	set<position> result;
	int current_elevation = height_map[currPos];
	for(auto dir : DIRECTIONS){
		position nextPos = currPos + dir;
		if(height_map.find(nextPos) != height_map.end()){
			int next_elevation = height_map[nextPos];
			if(next_elevation - current_elevation <= 1){
				result.insert(nextPos);
			}
		}
	}
	return result;
}

int shortest(puzzle_input input){
	int currentStep = 0;
	set<position> earlierLevel;
	set<position> currentLevel{input.start};
	set<position> nextLevel;

	while(!currentLevel.contains(input.end) && !currentLevel.empty()){
		
		for(auto &pos : currentLevel){
			for(auto &nextPos : findNextSteps(pos, input.height_map)){
				if(!earlierLevel.contains(nextPos) && !currentLevel.contains(nextPos)){
					nextLevel.insert(nextPos);
				}
			}
		}
		currentStep++;
		
		earlierLevel.insert(currentLevel.begin(), currentLevel.end());
		currentLevel = nextLevel;
		nextLevel.clear();
	}
	return currentStep;
}

void solveOne(puzzle_input input){
	cout << shortest(input) << endl;
}

set<position> getAllAs(map<position, int> &puzzle_map){
	set<position> result;
	for(auto &pr : puzzle_map) if(pr.second == 0) result.insert(pr.first);
	return result;
}

void solveTwo(puzzle_input input){
	int shortest_moves = numeric_limits<int>::max();

	for(auto &pos : getAllAs(input.height_map)){
		cout << get<0>(pos) << "/" << get<1>(pos) << ":";
		puzzle_input input2 = input;
		input2.start = pos;
		shortest_moves = min(shortest_moves, shortest(input2));

		cout << shortest_moves << endl;
	}
}


int main(){
	auto puzzleInput = read_puzzle_input();
	drawInput(puzzleInput);


	solveOne(puzzleInput);
	solveTwo(puzzleInput);
	return 0;
}
