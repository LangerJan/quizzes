#include <iostream>
#include <string>
#include <vector>
#include <cstdint>

#include <boost/algorithm/string.hpp>
#include <boost/variant.hpp>
#include <sstream>


using namespace std;

using list_element = boost::make_recursive_variant<int,vector<boost::recursive_variant_>>::type;


list_element readElement(stringstream &ss){
	list_element result;
	char currentChar;

	auto peeked = ss.peek();
	if(peeked == '['){
		vector<list_element> res;
		ss.get(currentChar);

		peeked = ss.peek();
		while(peeked != ']' && peeked != EOF){
			res.push_back(readElement(ss));
			peeked = ss.peek();
			if(peeked == ','){
				ss.get(currentChar);
				peeked = ss.peek();
			}
		}
		ss.get(currentChar);
		result = res;
	}
	else
	{
		int res;
		ss >> res;
		result = res;
	}
	return result;
}

vector<list_element> readInput(){
	vector<list_element> result;
	for(string line;getline(cin,line);){
		if(line.empty())continue;
		stringstream ss(line);
		result.push_back(readElement(ss));
	}


	return result;

}

void print(list_element input){
	if(int* i = boost::get<int>(&input)){
		std::cout << *i << " ";
	}
	else if(vector<list_element> *i = boost::get<vector<list_element>>(&input))
	{
		cout << "[";
		for(auto &elem : *i){
			print(elem);
			cout << " ";
		}			
		cout << "]";
	}
}

void print_input(vector<list_element> &input){
	for(auto &i : input){
		print(i);
		cout << endl;
	}
}


bool isInOrder(list_element arg1, list_element arg2){

	int *arg1_i = boost::get<int>(&arg1);
	int *arg2_i = boost::get<int>(&arg2);


	vector<list_element> *arg1_v = boost::get<vector<list_element>>(&arg1);
	vector<list_element> *arg2_v = boost::get<vector<list_element>>(&arg2);
	
	list_element sub;

	if(arg1_i && arg2_i){
		if(*arg1_i <= *arg2_i) return true;
		else return false;
	}
	
	if(!(arg1_v && arg2_v)){
		if(arg1_i){
			sub = vector<list_element>{*arg1_i};
			arg1_v = boost::get<vector<list_element>>(&sub);
		}
		else if(arg2_i){
			sub = vector<list_element>{*arg2_i};
			arg2_v = boost::get<vector<list_element>>(&sub);
		}
	}


	if(arg2_v->size() < arg1_v->size()) return false;
	else
	{
		for(int elem=0;elem < arg1_v->size() && 
			elem < arg2_v->size();elem++)
		{
			if(!isInOrder(arg1_v->at(elem), arg2_v->at(elem)))
			{
				return false;
			}
		}
		return true;
	}
}

void solveOne(vector<list_element> input){

	for(int i=0;(i+1)<input.size();i=i+2){
		list_element one = input.at(i);
		list_element two = input.at(i+1);

		cout << "Compare";
		print(one);
		cout << " vs ";
		print(two);
		cout << endl;

		if(isInOrder(one,two)){
			cout << "Pair " << (i/2)+1 << " is in order" << endl;
		}
	}

}

int main(){
	auto input = readInput();
	print_input(input);
	solveOne(input);
	return 0;
}
