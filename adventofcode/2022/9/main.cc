#include <iostream>
#include <string>
#include <tuple>
#include <map>
#include <set>
#include <algorithm>
#include <vector>
#include <boost/algorithm/string.hpp>

using namespace std;

using direction = tuple<int,int>;
using position = tuple<int,int>;

const direction UP = {0,-1};
const direction DOWN = {0,1};
const direction LEFT = {-1,0};
const direction RIGHT = {1,0};

const direction UP_LEFT = {-1,-1};
const direction UP_RIGHT = {1,-1};
const direction DOWN_LEFT = {-1,1};
const direction DOWN_RIGHT = {1,1};
const direction NO_DIR = {0,0};

const set<direction> NEIGHBORS = {
	UP, DOWN, LEFT, RIGHT,
	UP_LEFT, UP_RIGHT, DOWN_LEFT, DOWN_RIGHT,
	NO_DIR
};

vector<direction> readInput(){
	vector<direction> result;
	for(string line;getline(cin,line);){
		vector<string> strs;
		boost::split(strs,line,boost::is_any_of(" "));
		for(int i=0;i<stoi(strs[1]);i++){
			direction dir;
			switch(strs[0][0]){
				case 'U': dir = UP; break; 
				case 'D': dir = DOWN; break; 
				case 'L': dir = LEFT; break; 
				case 'R': dir = RIGHT; break; 
			   	default: cerr << "FUUU" << endl; break;
			}
			result.push_back(dir);
		}
	}
	return result;
}


bool areNeighbors(position a, position b){
	int x = abs(get<0>(a) - get<0>(b));
	int y = abs(get<1>(a) - get<1>(b));

	return NEIGHBORS.contains({x,y});
}


position move(tuple<int,int> a, tuple<int,int> b){
	return {get<0>(a)+get<0>(b), get<1>(a)+get<1>(b)};
}

direction follow(position head, position tail){
	int head_x = get<0>(head);
	int head_y = get<1>(head);
	int tail_x = get<0>(tail);
	int tail_y = get<1>(tail);

	if(areNeighbors(head, tail)){
	       	return NO_DIR;
	}
	else
	{
		if(head_x == tail_x && head_y != tail_y){
			if(head_y < tail_y) return UP;
			else return DOWN;
		}
		else if(head_y == tail_y && head_x != tail_x){
			if(head_x < tail_x) return LEFT;
			else return RIGHT;
		}
		else {
			direction part1_dir = NO_DIR;
			if(head_x < tail_x) part1_dir = LEFT;
			else if(head_x > tail_x) part1_dir = RIGHT;

			direction part2_dir = NO_DIR;
			if(head_y < tail_y) part2_dir = UP;
			else if(head_y > tail_y) part2_dir = DOWN;

			return move(part1_dir,part2_dir);
		}
	}
}

void draw(int minX, int maxX, int minY, int maxY, position head, position tail){

	for(int y=minY;y<=maxY;y++){
		for(int x=minX;x<=maxX;x++){
			if(get<0>(head) == x && get<1>(head) == y) cout << "H";
			else if(get<0>(tail) == x && get<1>(tail) == y) cout << "T";
			else cout << ".";
		}
		cout << endl;
	}


	cout << "head = {" << get<0>(head) << "," << get<1>(head) << "}" << endl;
	cout << "tail = {" << get<0>(tail) << "," << get<1>(tail) << "}" << endl;
	cout << endl;	
}

void draw2(int minX, int maxX, int minY, int maxY, vector<position> pos){

	for(int y=minY;y<=maxY;y++){
		for(int x=minX;x<=maxX;x++){

			bool knotFound = false;
			for(int i=0;i<pos.size();i++){
				if(get<0>(pos[i]) == x && get<1>(pos[i]) == y){
					cout << i;
					knotFound = true;
					break;
				}
			}
			if(!knotFound) cout << ".";
		}
		cout << endl;
	}
	cout << endl;	
}

void solveOne(vector<direction> input){
	position head{0,0}, tail{0,0};

	set<position> tailPos{tail};

	for(auto &dir : input){
		head = move(head, dir);
		tail = move(tail, follow(head, tail));
		tailPos.insert(tail);
	}

	cout << tailPos.size() << endl;
}

void solveTwo(vector<direction> input){
	vector<position> knots{
		{0,0},
		{0,0},
		{0,0},
		{0,0},
		{0,0},
		{0,0},
		{0,0},
		{0,0},
		{0,0},
		{0,0}
	};

	set<position> tailPos{{0,0}};

	for(auto &dir : input){
		auto &head = knots.at(0);
		head = move(head, dir);

		for(int i=1;i<knots.size();i++){
			auto &knot_above = knots.at(i-1);
			auto &knot_curr = knots.at(i);

			knot_curr = move(knot_curr, follow(knot_above, knot_curr));
		}
		position last = knots.at(knots.size()-1);
		tailPos.insert(last);
	}
	cout << tailPos.size() << endl;
}

int main(){
	auto input = readInput();
	solveOne(input);
	solveTwo(input);
	return 0;
}

