#include <algorithm>
#include <iostream>
#include <string>
#include <cstdint>
#include <set>

using namespace std;

int main(){


	uint64_t sumPrios = 0;
	for(string line;getline(cin,line);){

		string line1, line2;
		line1 = line.substr(0,line.size()/2);
		line2 = line.substr(line.size()/2);

		set<char> line1chars(line1.begin(),line1.end());
		set<char> line2chars(line2.begin(),line2.end());
		set<char> intersect;
		set_intersection(line1chars.begin(),line1chars.end(), line2chars.begin(), line2chars.end(),
				inserter(intersect, intersect.begin()));

		if(!intersect.empty()){
			char c = *intersect.begin();
			sumPrios += (c>='a' && c<='z')?(c - 'a' + 1):(c - 'A' + 27);
		}
	}
	cout << sumPrios << endl;


	return 0;
}
