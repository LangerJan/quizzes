#include <algorithm>
#include <iostream>
#include <string>
#include <cstdint>
#include <set>

using namespace std;

int main(){


	uint64_t sumPrios = 0;
	for(string line1,line2,line3;getline(cin,line1) && getline(cin,line2) && getline(cin,line3);){

		set<char> line1chars(line1.begin(),line1.end());
		set<char> line2chars(line2.begin(),line2.end());
		set<char> line3chars(line3.begin(),line3.end());
		
		
		set<char> intersect;
		set_intersection(line1chars.begin(),line1chars.end(), line2chars.begin(), line2chars.end(),
				inserter(intersect, intersect.begin()));

		set<char> intersect2;
		set_intersection(line3chars.begin(),line3chars.end(), intersect.begin(), intersect.end(),
				inserter(intersect2, intersect2.begin()));
		
		if(!intersect2.empty()){
			char c = *intersect2.begin();
			sumPrios += (c>='a' && c<='z')?(c - 'a' + 1):(c - 'A' + 27);
		}
	}
	cout << sumPrios << endl;

	return 0;
}
