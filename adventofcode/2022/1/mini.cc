#include <iostream>
#include <string>
#include <cstdint>
#include <algorithm>
#include <vector>

using namespace std;

int main(){
	vector<int> elfs;
	int sum = 0;
	for(string l; getline(cin,l);){
		if(l.empty()){
			elfs.push_back(sum);
			sum = 0;
		}
		else
			sum += stoi(l);
	}
	sort(elfs.begin(), elfs.end());
	cout << *(elfs.end()-1) << endl;
	cout << (*(elfs.end()-1))+(*(elfs.end()-2))+(*(elfs.end()-3)) << endl;
	return 0;
}
