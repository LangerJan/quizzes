#include <iostream>
#include <string>
#include <cstdint>
#include <algorithm>
#include <vector>

using namespace std;

int main(){
	vector<int> elves;
	int currentSum = 0;
	for(string line; getline(cin,line);){
		if(line.empty()){
			elves.push_back(currentSum);
			currentSum = 0;
		}
		else
		{
			currentSum += std::stoi(line);
		}
	}
	sort(elves.begin(), elves.end());
	cout << "Part 1: " << *(elves.end()-1) << endl;
	cout << "Part 2: " << (*(elves.end()-1))+(*(elves.end()-2))+(*(elves.end()-3)) << endl;
	return 0;
}
