#include <iostream>
#include <string>
#include <cstdint>
#include <map>
#include <memory>
#include <vector>
#include <functional>
#include <sstream>

#include <boost/algorithm/string.hpp>

using namespace std;

class cpu;

struct instr_def{
	instr_def(string name, int cycles, int num_params,
		  function<void(shared_ptr<cpu>, vector<int>)> execute) : 
		m_name(name),
		m_cycles(cycles),
		m_num_params(num_params),
		m_execute(execute)
	{}
	string m_name;
	int m_cycles;
	int m_num_params;
	
	function<void(shared_ptr<cpu>, vector<int>)> m_execute;
};

struct instruction{
	instruction(shared_ptr<instr_def> definition, vector<int> params) :
		m_definition(definition),
		m_params(params){}

	shared_ptr<instr_def> m_definition;
	vector<int> m_params;
};

class cpu : public std::enable_shared_from_this<cpu>{
	public:
	cpu(vector<shared_ptr<instruction>> instructions):
		m_cycles(0),
		m_pc(0),
		m_regX(1),
		m_instructions(instructions)
	{

	}

	std::shared_ptr<cpu> getptr() {
        	return shared_from_this();
    	}

	int m_cycles;
	int m_pc;
	int m_regX;
	vector<shared_ptr<instruction>> m_instructions;

	string m_currentRow;

	void increment_cycles(){
		static int check = 20;
		static int sumSignal = 0;
		const int end = 220;

		int rasterPos = this->m_cycles % 40;

		

		if(this->m_regX -1 == rasterPos ||
		   this->m_regX == rasterPos ||
		   this->m_regX +1 == rasterPos) m_currentRow += "#";
		else m_currentRow += ".";

		cout << "During cycle " << this->m_cycles << endl;
		cout << "Current CRT row: " << m_currentRow << endl;

		if(this->m_cycles % 40 == 0){
			cerr << m_currentRow << endl;
			m_currentRow = "";
		}

		if(this->m_cycles <= end){
			if(this->m_cycles == check){
				//cout << "Cycle: " << this->m_cycles << " Register X: " << this->m_regX << endl;
				check += 40;

				sumSignal += this->m_regX * this->m_cycles;
				//cout << "Sum of Signal Strength: " << sumSignal << endl;
			}
		}

		this->m_cycles++;
	}

	void run(){

		int current_operation_remaining_cycles = 0;
		while(m_pc < m_instructions.size()){
			// fetch
			shared_ptr<instruction> instr = m_instructions.at(m_pc);
			
			cout << "Start cycle " << this->m_cycles << endl;	
			for(int i=0;i<instr->m_definition->m_cycles;i++){
				increment_cycles();
			}
			instr->m_definition->m_execute(getptr(),instr->m_params);
			m_pc++;
		}
		cerr << this->m_currentRow << endl;
	}
};




shared_ptr<instr_def> NOOP = make_shared<instr_def>(string("noop"), 1, 0, [](shared_ptr<cpu> cpu, vector<int> instrs){});
shared_ptr<instr_def> ADDX = make_shared<instr_def>(string("addx"), 2, 1, 
	[](shared_ptr<cpu> cpu, vector<int> instrs){
		cpu->m_regX += instrs[0];
	});

map<string, shared_ptr<instr_def>> instr_lookup{
	{"noop", NOOP},
	{"addx", ADDX}
};


vector<shared_ptr<instruction>> readInstructions(){
	vector<shared_ptr<instruction>> result;

	for(string line;getline(cin,line);){
		vector<string> strs;
		boost::split(strs,line,boost::is_any_of(" "));

		if(instr_lookup.find(strs[0]) == instr_lookup.end()){
			cerr << "illegal opcode " << strs[0] << endl;
			return result;
		}
		else
		{
			shared_ptr<instr_def> def = instr_lookup.at(strs[0]);
			if(def->m_num_params != strs.size()-1){
				cerr << strs[0] << " requires " << def->m_num_params << " parameters." << endl;
				cerr << "But " << strs.size()-1 << " have been provided." << endl;
			}
			vector<int> params;
			for(int i=1;i<strs.size();i++){
				params.push_back(stoi(strs.at(i)));
			}
			result.push_back(make_shared<instruction>(def, params));
		}
	}

	return result;
}

int main(){

	auto instructions = readInstructions();

	shared_ptr<cpu> my_cpu = make_shared<cpu>(instructions);
	my_cpu->run();

	return 0;
}
