#include <algorithm>
#include <string>
#include <cstdint>
#include <iostream>
#include <vector>
#include <set>
#include <deque>

#include <boost/algorithm/string.hpp>

using namespace std;


int main(){


	vector<deque<char>> stacks;
	for(string line;getline(cin,line);){
		if(line.empty()) break;
		if(stacks.empty()){
			for(int i=0;i<(line.size() / 4) + 1;i++)stacks.push_back(deque<char>());
		}
		for(int i=1;i<line.size();i+=4){
			int index = i / 4;
			if(line[i] >= '0' && line[i] <= '9') break;
			if(line[i] != ' ') stacks[index].push_back(line[i]);
		}

	}

	for(auto &stack : stacks){
		for(auto c : stack) cout << "'" << c << "'";
		cout << endl;
	}

	for(string line;getline(cin,line);){
		vector<string> strs;
		boost::split(strs,line,boost::is_any_of(" "));

		int amount = stoi(strs[1]);
		int from = stoi(strs[3]) - 1;
		int to = stoi(strs[5]) - 1;

		cout << line << endl;

		for(int i=0;i<amount;i++){
			char c = stacks.at(from).front();
			stacks.at(from).pop_front();
			stacks.at(to).push_front(c);
		}
	for(auto &stack : stacks){
		for(auto c : stack) cout << "'" << c << "'";
		cout << endl;
	}
	}


	for(auto &stack : stacks){
		for(auto c : stack) cout << "'" << c << "'";
		cout << endl;
	}

	return 0;
}
