#include <algorithm>
#include <iostream>
#include <string>
#include <cstdint>
#include <vector>
#include <set>
#include <map>


using namespace std;


int score(char a, char b){
	switch(a){
		case 'A':
		switch(b){
			case 'Y':
				return 6;
			case 'Z':
				return 0;
			default:
				return 3;
		}
		case 'B':
		switch(b){
			case 'X':
				return 0;
			case 'Z':
				return 6;
			default:
				return 3;
		}
		default:
		case 'C':
		switch(b){
			case 'X':
				return 6;
			case 'Y':
				return 0;
			default:
				return 3;
		}
	}
}

int points(char a, char b){
	int result = score(a,b);
	result += b - 'X' + 1;
	return result;
}

int main(){
	int sum = 0;
	for(string l;getline(cin,l);){
		cout << "'" << l << "'" << endl;
		sum += points(l[0],l[2]);
		cout << sum << endl;
	}



	return 0;
}

