#include <algorithm>
#include <string>
#include <sstream>
#include <iostream>
#include <cstdint>

using namespace std;



bool covered_by(int begin1, int end1, int begin2, int end2){
	return begin1 <= begin2 && end1 >= end2;
}


bool is_within(int within, int begin, int end){
	return begin <= within && end >= within;
}

bool overlap_r(int begin1, int end1, int begin2, int end2){
	return is_within(begin1, begin2, end2) || is_within(end1, begin2, end2);
}

bool overlaps(int begin1, int end1, int begin2, int end2){
	return overlap_r(begin1, end1, begin2, end2) || overlap_r(begin2, end2, begin1, end1);
}

int main(){
	int covered = 0;
	int overlapped = 0;
	for(string line;getline(cin,line);){
		stringstream line_ss(line);
		string section1_str, section2_str;
		getline(line_ss, section1_str,',');
		getline(line_ss, section2_str);
			stringstream section1(section1_str), section2(section2_str);
			string sec1_num1, sec1_num2, sec2_num1, sec2_num2;
			getline(section1, sec1_num1, '-');
			getline(section1, sec1_num2);
			getline(section2, sec2_num1, '-');
			getline(section2, sec2_num2);

			int x1 = stoi(sec1_num1);
			int x2 = stoi(sec1_num2);
			
			int x3 = stoi(sec2_num1);
			int x4 = stoi(sec2_num2);

			bool result = covered_by(x1,x2, x3,x4) || covered_by(x3,x4, x1,x2);
			if(result) covered++;

			bool overlap = overlaps(x1,x2, x3,x4);
			if(overlap) overlapped++;
	}

	cout << covered << endl;
	cout << overlapped << endl;

	return 0;
}
