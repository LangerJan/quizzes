#include <iostream>
#include <string>
#include <algorithm>
#include <tuple>
#include <functional>
#include <vector>
#include <sstream>
#include <numeric>

using namespace std;

class submarine{
	public:
		int m_aim = 0;
		int m_xPos = 0;
		int m_yPos = 0;

		void move(tuple<int,int> m){
			if(get<0>(m) == 0){
				m_aim += -get<1>(m);
			}
			else {
				m_xPos += get<0>(m);
				m_yPos += get<0>(m) * m_aim;
			}
			cout << toString() << endl;
		}

		string toString(){
			stringstream ss;
			ss << "Horizontal Pos: " << m_xPos << "; Aim: " << m_aim << "; Depth: " << m_yPos;
			return ss.str();
		}
};



vector<tuple<int, int>> readInput(){
	vector<tuple<int,int>> result;

	for(string line;getline(cin,line);){
		istringstream iss(line);
		string direction;
		getline(iss, direction, ' ');
		string steps_str;
		getline(iss, steps_str);
		int steps = stoi(steps_str);

		tuple<int,int> elem;
		if(direction.compare("forward") == 0){
			elem = {steps, 0};
		}
		else if(direction.compare("up") == 0){
			elem = {0, steps};
		}
		else if(direction.compare("down") == 0){
			elem = {0, -steps};
		}

		result.push_back(elem);
	}

	return result;
}

void solveOne(vector<tuple<int,int>> input){
	tuple<int,int> sum = accumulate(input.begin(), input.end(), tuple<int,int>({0,0}), [](tuple<int,int> a, tuple<int,int> b){
		return std::tuple<int,int>({get<0>(a)+get<0>(b), get<1>(a)+get<1>(b)});		
	});

	cout << get<0>(sum)*(-get<1>(sum)) << endl;
}

void solveTwo(vector<tuple<int,int>> input){
	submarine sub;

	for(auto i : input){
		sub.move(i);
	}

	cout << (sub.m_xPos * sub.m_yPos) << endl;
}

int main() {

	auto input = readInput();

	solveTwo(input);

	return 0;
}
