#include <algorithm>
#include <deque>
#include <functional>
#include <numeric>
#include <iostream>
#include <string>
#include <tuple>
#include <vector>

using namespace std;

vector<int> readInput(){
	vector<int> result;
	for(string line;getline(cin,line);){
		result.push_back(stoi(line));
	}
	return result;
}

int solvePart1(vector<int> input){
	std::tuple<int, int> solution = accumulate(
				next(input.begin()), input.end(), 
				tuple<int,int>({0,input.at(0)}), 
				[](tuple<int,int> res, int next){
					return tuple<int,int>{get<0>(res)+(next>get<1>(res)?1:0),next};
				});
	return get<0>(solution);
}

int solvePart2(vector<int> input){
	vector<int> transformation;
	
	deque<int> buffer;
	for(int i : input){
		buffer.push_back(i);
		while(buffer.size() > 3) buffer.pop_front();}
		if(buffer.size() == 3) transformation.push_back(accumulate(buffer.begin(), buffer.end(), 0));
	}

	return solvePart1(transformation);
}


int main(void){
	auto input = readInput();
	cout << solvePart1(input) << std::endl;
	cout << solvePart2(input) << std::endl;
	return 0;
}
