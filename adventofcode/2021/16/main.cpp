#include <iostream>
#include <bitset>
#include <memory>
#include <string>
#include <tuple>
#include <vector>
#include <set>
#include <cstdint>
#include <numeric>
#include <functional>
#include <algorithm>
#include <climits>
#include <sstream>
#include <stdexcept>

#include <gmp.h>
#include <gmpxx.h>

using namespace std;


enum class packet_type : uint8_t {
	sum = 0,
	product = 1,
	minimum = 2,
	maximum = 3,
	literal_value = 4,
	greater_than = 5,
	less_than = 6,
	equal_to = 7
};


class packet;


static shared_ptr<packet> parse(stringstream &ss);
static shared_ptr<packet> parse_literal(stringstream &ss);
static shared_ptr<packet> parse_operator(stringstream &ss);

using myInt = uint64_t;

class packet {
	public:
		uint8_t m_version;
		uint8_t m_id;

		packet(uint8_t version, uint8_t id) : m_version(version), m_id(id){}

		virtual int sumUpVersions(){
			return m_version;
		}

		virtual myInt determineValue() = 0;

		virtual void printMe(int indentation=0) = 0;

};

class operator_packet : public packet {
	public:
		uint8_t m_length_type_id;
		uint16_t m_subpackets_hint;
		vector<shared_ptr<packet>> m_subpackets;

		operator_packet(uint8_t version, uint8_t id, vector<shared_ptr<packet>> subpackets) : packet(version, id), m_subpackets(subpackets){}
		virtual ~operator_packet() = default;

		virtual int sumUpVersions() override {
			int result = this->m_version;
			for(auto sub : this->m_subpackets){
				result += sub->sumUpVersions();
			}
			return result;
		}
};

class sum_packet : public operator_packet {
	public:
		sum_packet(uint8_t version, uint8_t id, vector<shared_ptr<packet>> subpackets) : operator_packet(version, id, subpackets){}
		myInt determineValue() override {
			myInt result = std::accumulate(m_subpackets.begin(), m_subpackets.end(), myInt(0), 
					[](myInt v, shared_ptr<packet> packet){
						return v+packet->determineValue();
					});
			return result;
		}

		void printMe(int indentation) override {
			cout << endl;
			for(int i=0;i<indentation;i++) cout << " ";
			cout << "+(";
			for(auto sub : m_subpackets){
				sub->printMe(indentation+1);
				cout << ",";
			}
			cout << endl;
			for(int i=0;i<indentation;i++) cout << " ";
			cout << ") == " << determineValue() << endl;
		}
};

class product_packet : public operator_packet {
	public:
		product_packet(uint8_t version, uint8_t id, vector<shared_ptr<packet>> subpackets) : operator_packet(version, id, subpackets){}
		myInt determineValue() override {
			myInt result = std::accumulate(m_subpackets.begin(), m_subpackets.end(), myInt(1), [](myInt v, shared_ptr<packet> packet){return v*packet->determineValue();});
			return result;
		}
		
		void printMe(int indentation) override {
			cout << endl;
			for(int i=0;i<indentation;i++) cout << " ";
			cout << "*(";
			for(auto sub : m_subpackets){
				sub->printMe(indentation+1);
				cout << ",";
			}
			cout << endl;
			for(int i=0;i<indentation;i++) cout << " ";
			cout << ") == " << determineValue() << endl;
		}
};

class minimum_packet : public operator_packet {
	public:
		minimum_packet(uint8_t version, uint8_t id, vector<shared_ptr<packet>> subpackets) : operator_packet(version, id, subpackets){}
		myInt determineValue() override {
			auto val =  std::min_element(m_subpackets.begin(), m_subpackets.end(), 
					[](const shared_ptr<packet> &packet1, const shared_ptr<packet> &packet2){
						return packet1->determineValue()<packet2->determineValue();});

			return (*val)->determineValue();
		}
		
		void printMe(int indentation) override {
			cout << endl;
			for(int i=0;i<indentation;i++) cout << " ";
			cout << "min(";
			for(auto sub : m_subpackets){
				sub->printMe(indentation+1);
				cout << ",";
			}
			cout << endl;
			for(int i=0;i<indentation;i++) cout << " ";
			cout << ") == " << determineValue() << endl;
		}
		
};

class maximum_packet : public operator_packet {
	public:
		maximum_packet(uint8_t version, uint8_t id, vector<shared_ptr<packet>> subpackets) : operator_packet(version, id, subpackets){}
		myInt determineValue() override {
			auto val = std::max_element(m_subpackets.begin(), m_subpackets.end(), 
					[](const shared_ptr<packet> &packet1, const shared_ptr<packet> &packet2){
						return packet1->determineValue()<packet2->determineValue();});

			return (*val)->determineValue();
		}
		void printMe(int indentation) override {
			cout << endl;
			for(int i=0;i<indentation;i++) cout << " ";
			cout << "max(";
			for(auto sub : m_subpackets){
				sub->printMe(indentation+1);
				cout << ",";
			}
			cout << endl;
			for(int i=0;i<indentation;i++) cout << " ";
			cout << ") == " << determineValue() << endl;
		}
};

class greater_than_packet : public operator_packet {
	public:
		greater_than_packet(uint8_t version, uint8_t id, vector<shared_ptr<packet>> subpackets) : operator_packet(version, id, subpackets){}
		myInt determineValue() override {
			return this->m_subpackets.at(0)->determineValue() >
			       this->m_subpackets.at(1)->determineValue()?1:0;
		}
		
		void printMe(int indentation) override {
			cout << endl;
			for(int i=0;i<indentation;i++) cout << " ";
			cout << ">(";
			for(auto sub : m_subpackets){
				sub->printMe(indentation+1);
				cout << ",";
			}
			cout << endl;
			for(int i=0;i<indentation;i++) cout << " ";
			cout << ") == " << determineValue() << endl;
		}
};

class less_than_packet : public operator_packet {
	public:
		less_than_packet(uint8_t version, uint8_t id, vector<shared_ptr<packet>> subpackets) : operator_packet(version, id, subpackets){}
		myInt determineValue() override {
			return this->m_subpackets.at(0)->determineValue() <
			       this->m_subpackets.at(1)->determineValue()?1:0;
		}
		void printMe(int indentation) override {
			cout << endl;
			for(int i=0;i<indentation;i++) cout << " ";
			cout << "<(";
			for(auto sub : m_subpackets){
				sub->printMe(indentation+1);
				for(int i=0;i<indentation;i++) cout << " ";
				cout << ",";
			}
			cout << endl;
			for(int i=0;i<indentation;i++) cout << " ";
			cout << ") == " << determineValue() << endl;
		}
};

class equal_to_packet : public operator_packet {
	public:
		equal_to_packet(uint8_t version, uint8_t id, vector<shared_ptr<packet>> subpackets) : operator_packet(version, id, subpackets){}
		myInt determineValue() override {
			return this->m_subpackets.at(0)->determineValue() ==
			       this->m_subpackets.at(1)->determineValue()?1:0;
		}
		void printMe(int indentation) override {
			cout << endl;
			for(int i=0;i<indentation;i++) cout << " ";
			cout << "==(";
			for(auto sub : m_subpackets){
				sub->printMe(indentation+1);
				for(int i=0;i<indentation;i++) cout << " ";
				cout << ",";
			}
			cout << endl;
			for(int i=0;i<indentation;i++) cout << " ";
			cout << ") == " << determineValue() << endl;
		}
};

class literal_packet : public packet {
	public:
		myInt m_literal;
		literal_packet(uint8_t version, uint8_t id, string literal) : packet(version, id), m_literal(stoull(literal, nullptr, 2)){}
		virtual myInt determineValue() override {
			return m_literal;
		}
		void printMe(int indentation) override {
			cout << endl;
			for(int i=0;i<indentation;i++) cout << " ";
			cout << "lit(";
			cout << ") == " << determineValue() << endl;
		}
};



static string consumeBits_asString(stringstream &ss, int bits){
	char data[bits];
	ss.read(data,bits);

	string result(data,bits);

	return result;
}

static uint64_t consumeBits(stringstream &ss, int bits){
	return stoull(consumeBits_asString(ss,bits), nullptr, 2);
}

static string consumeLiteral(stringstream &ss){
	uint8_t endMarker;
	stringstream result;
	do {
		endMarker = consumeBits(ss, 1);
		result << consumeBits_asString(ss, 4);
	}while(endMarker == 1);
	return result.str();
}

static shared_ptr<packet> parse_operator(stringstream &ss, uint8_t version, uint8_t id){
	uint8_t length_type_id = consumeBits(ss, 1);
	vector<shared_ptr<packet>> packets;

	if(length_type_id == 0){
		uint16_t length_subpackets = consumeBits(ss, 15);
		string substring = consumeBits_asString(ss,length_subpackets);
		stringstream sub_ss(substring);
				

		while(sub_ss.rdbuf()->in_avail() > 0){
			packets.push_back(parse(sub_ss));
		}
	}
	else
	{
		uint16_t number_subpackets = consumeBits(ss, 11);
		
		for(uint16_t i = 0;i<number_subpackets;i++){
			packets.push_back(parse(ss));
		}
	}

	switch(static_cast<packet_type>(id)){
		case packet_type::sum:
			return std::make_shared<sum_packet>(version, id, packets);
		case packet_type::product:
			return std::make_shared<product_packet>(version, id, packets);
		case packet_type::minimum:
			return std::make_shared<minimum_packet>(version, id, packets);
		case packet_type::maximum:
			return std::make_shared<maximum_packet>(version, id, packets);
		case packet_type::greater_than:
			return std::make_shared<greater_than_packet>(version, id, packets);
		case packet_type::less_than:
			return std::make_shared<less_than_packet>(version, id, packets);
		case packet_type::equal_to:
			return std::make_shared<equal_to_packet>(version, id, packets);
		default:
			throw runtime_error("unknown id");
	}
}


static shared_ptr<packet> parse_literal(stringstream &ss, uint8_t version, uint8_t id){
	string literal = consumeLiteral(ss);
	return std::make_shared<literal_packet>(version, id, literal);
}

static shared_ptr<packet> parse(stringstream &ss){
	uint8_t version = consumeBits(ss, 3);
	uint8_t id = consumeBits(ss, 3);

	switch(static_cast<packet_type>(id)){
		case packet_type::literal_value:
			return parse_literal(ss, version, id);
		default: 
			return parse_operator(ss, version, id);
	}
};


string readInput(){
	string line;
	getline(cin,line);
	stringstream result;
	for(char c : line){
		auto hex = std::string(1,c);
		result << bitset<4>(stoi(hex, nullptr, 16)).to_string();
	}
	return result.str();
}

void solveOne(string input){
	stringstream ss(input);	
	shared_ptr<packet> rootPacket = parse(ss);
	rootPacket->printMe();
	cout << rootPacket->sumUpVersions() << endl;
	cout << rootPacket->determineValue() << endl;
}


int main(){
	auto input = readInput();


	solveOne(input);

	return 0;
}
