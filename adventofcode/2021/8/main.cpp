#include <iostream>
#include <string>
#include <cstdint>
#include <algorithm>
#include <map>
#include <vector>
#include <numeric>
#include <functional>
#include <set>
#include <sstream>
#include <iterator>

using namespace std;

using insample = tuple<set<string>, vector<string>>;


vector<insample> readInput(){
	vector<insample> result;
	

	for(string line;getline(cin,line);){
		stringstream ss(line);
		set<string> signal_patterns;
		vector<string> digits;

		{
			string patterns_str;
			getline(ss, patterns_str, '|');
			stringstream patterns_ss(patterns_str);
			for(string pattern;getline(patterns_ss, pattern, ' ');){
				if(pattern.empty()) continue;
				signal_patterns.insert(pattern);
			}
		}
		{
			string digits_str;
			getline(ss, digits_str, '|');
			stringstream digits_ss(digits_str);
			for(string digit;getline(digits_ss, digit, ' ');){
				if(digit.empty()) continue;
				digits.push_back(digit);
			}
		}

		result.push_back({signal_patterns, digits});
	}
	return result;
}

void solveOne(vector<insample> input){
	static set<int> unique_sizes = {2,3,4,7};

	int result = 0;
	for(auto it : input){
		
		for(auto str : get<1>(it)){
			if(unique_sizes.find(str.size()) != unique_sizes.end()){
				result++;
			}
		}
	}
	cout << "Part 1: " << result << endl;
}

int determine_digit(map<set<char>, int> dict, string str){
	set<char> str_chr;
	for(auto c : str) str_chr.insert(c);
	return dict[str_chr];
}

int determine_number(map<set<char>, int> dict, vector<string> strs){
	int result = 0;
	for(auto str : strs){
		result *= 10;
		result += determine_digit(dict, str);
	}
	return result;
}


int solveSample(insample sample){
	map<int, set<char>> encoding;
	map<char, char> translation;
	set<set<char>> unknown_numbers;

	for(auto str : get<0>(sample)){
		set<char> letters;
		for(auto c : str) letters.insert(c);

		switch(str.size()){
			case 2:
				encoding[1] = letters;
				break;
			case 3:
				encoding[7] = letters;
				break;
			case 4:
				encoding[4] = letters;
				break;
			case 7:
				encoding[8] = letters;
				break;
			default:
				unknown_numbers.insert(letters);
				break;
		}
	}

	// Determine number 6 and segment 'c'
	{
		for(auto letters : unknown_numbers){
			set<char> result;
			set_union(encoding[1].begin(),encoding[1].end(), letters.begin(), letters.end(),
					inserter(result, result.end()));
			if(result == encoding[8]){
				encoding[6] = letters;
				break;
			}
		}
		unknown_numbers.erase(encoding[6]);

		{
			set<char> result;
			set_difference(encoding[1].begin(), encoding[1].end(), encoding[6].begin(), encoding[6].end(),
					inserter(result, result.begin()));
			translation['c'] = *(result.begin());
		}
	}
	// Determine segment 'f'
	{
		set<char> result = encoding[1];
		result.erase(translation['c']);
		translation['f'] = *(result.begin());
	}
	// Determine number 5
	{
		for(auto letters : unknown_numbers){
			if(letters.size() != 5) continue;
			if(letters.find(translation['c']) == letters.end()){
				encoding[5] = letters;
			}
		}
		unknown_numbers.erase(encoding[5]);
	}
	// Determine segment 'e'
	{
		set<char> result;
		set_difference(encoding[6].begin(), encoding[6].end(), encoding[5].begin(), encoding[5].end(),
				inserter(result, result.begin()));
		translation['e'] = *(result.begin());
	}
	// Determine number 9
	{
		set<char> result = encoding[8];
		result.erase(translation['e']);
		encoding[9] = result;
		unknown_numbers.erase(encoding[9]);
	}
	// Determine number 0
	{
		for(auto letters : unknown_numbers){
			if(letters.size() == 6){
				encoding[0] = letters;
				break;
			}
		}
		unknown_numbers.erase(encoding[0]);
	}
	// Determine numbers 3 and 2
	{
		for(auto letters : unknown_numbers){
			if(letters.find(translation['f']) == letters.end()) encoding[2] = letters;
			else encoding[3] = letters;
		}
	}

	map<set<char>, int> reverse_encoding;
	for(auto pr : encoding){
		reverse_encoding[pr.second] = pr.first;
	}

	return determine_number(reverse_encoding, get<1>(sample));

}

void solveTwo(vector<insample> input){
	
	int result = 0;
	for(auto it : input){
		result += solveSample(it);
	}
	cout << "Part 2: " << result << endl;
}

int main(){

	auto input = readInput();
	solveOne(input);
	solveTwo(input);

	return 0;

}
