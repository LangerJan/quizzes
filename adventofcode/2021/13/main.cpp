#include <iostream>
#include <string>
#include <tuple>
#include <map>
#include <sstream>
#include <cstdint>
#include <set> 
#include <vector>
#include <algorithm>

using namespace std;

tuple<set<tuple<int,int>>, vector<tuple<char, int>>> readInput(){
	set<tuple<int,int>> holes;
	vector<tuple<char,int>> foldings;

	for(string line;getline(cin,line);){
		if(line.empty()) break;
		stringstream ss(line);
		string x, y;
		getline(ss, x, ',');
		getline(ss, y, ',');

		holes.insert({stoi(x),stoi(y)});
	}

	for(string line;getline(cin,line);){
		stringstream ss(line);
		string l;
		getline(ss, l, ' ');
		getline(ss, l, ' ');
		getline(ss, l);

		string c, value;
		stringstream ss_l(l);
		getline(ss_l, c, '=');
		getline(ss_l, value);

		foldings.push_back({c.at(0), stoi(value)});
	}

	return {holes, foldings};
}

void fold(set<tuple<int,int>> &paper, tuple<char,int> foldInstruction){
	
	set<tuple<int,int>> toDelete;
	set<tuple<int,int>> toAdd;

	char foldAxis = get<0>(foldInstruction);
	int foldValue = get<1>(foldInstruction);

	for(auto pr : paper){
		int xKoord = get<0>(pr);
		int yKoord = get<1>(pr);

		if(foldAxis == 'x'){
			if(xKoord > foldValue){
				int diff = xKoord - foldValue;
				xKoord -= (2*diff);

				toDelete.insert(pr);
				toAdd.insert({xKoord,yKoord});
			}
		}
		else {
			if(yKoord > foldValue){
				int diff = yKoord - foldValue;
				yKoord -= (2*diff);

				toDelete.insert(pr);
				toAdd.insert({xKoord,yKoord});
			}
		}
	}

	for(auto i : toDelete) paper.erase(i);
	for(auto i : toAdd) paper.insert(i);

}

void solveOne(tuple<set<tuple<int,int>>, vector<tuple<char,int>>> input){
	auto punkte = get<0>(input);
	auto foldings = get<1>(input);
	cout << "Part One: " << punkte.size() << endl;

	fold(punkte, foldings.at(0));

	cout << "Part One: " << punkte.size() << endl;
}


void print(set<tuple<int,int>> punkte){
	int maxX = 0;
	int maxY = 0;
	for(auto i : punkte){
		maxX = std::max(maxX, get<0>(i));
		maxY = std::max(maxY, get<1>(i));
	}

	for(int y=0;y<=maxY;y++){
		for(int x=0;x<=maxX;x++){
			if(punkte.find({x,y}) == punkte.end()){
				cout << ".";
			}
			else {
				cout << "#";
			}
		}
		cout << endl;
	}

}

void solveTwo(tuple<set<tuple<int,int>>, vector<tuple<char,int>>> input){
	auto punkte = get<0>(input);
	auto foldings = get<1>(input);

	for(auto folding : foldings){
		fold(punkte, folding);
	}

	print(punkte);


}

int main(){
	auto input = readInput();

	solveOne(input);
	solveTwo(input);
	return 0;
}

