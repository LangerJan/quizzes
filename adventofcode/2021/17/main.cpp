#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <map>
#include <numeric>
#include <functional>
#include <array>
#include <sstream>

using namespace std;

tuple<int, int, int, int> readInput(){
	tuple<int,int,int,int> result;


	string line;
	getline(cin, line);

	stringstream ss(line);
	string x;
	getline(ss, x, ' ');
	getline(ss, x, ' ');
	getline(ss, x, ' ');


	int x1, x2;
	{
		stringstream x_ss(x);
		string tmp;
		getline(x_ss, tmp, '=');

		x_ss >> x1;

		getline(x_ss, tmp, '.');
		getline(x_ss, tmp, '.');

		x_ss >> x2;
	}

	string y;
	getline(ss, y, ' ');

	int y1, y2;
	{
		stringstream y_ss(y);
		string tmp;
		getline(y_ss, tmp, '=');

		y_ss >> y1;

		getline(y_ss, tmp, '.');
		getline(y_ss, tmp, '.');

		y_ss >> y2;
	}

	return {x1,x2,y1,y2};
}

void solveOne(tuple<int,int,int,int> input){
	int minX = get<0>(input);
	int maxX = get<1>(input);
	int minY = get<2>(input);
	int maxY = get<3>(input);

	

	for(int yVelocity=-minY;yVelocity>=minY;yVelocity--){
		cout << yVelocity << endl;

		int currentVelocity = yVelocity;

		int yPos = 0;
		int step = 0;
		
		bool falling = (currentVelocity < 0);

		int hVelo = 0;

		while((!falling) || (falling && (yPos > minY))){
			step++;
			yPos += currentVelocity;

			hVelo = max(hVelo, yPos);
			if(yPos >= minY && yPos <= maxY){
				cout << "hit: " << hVelo << endl;
				return;
			}
			currentVelocity--;
			falling = (currentVelocity < 0);
		}
	}

	for(int xVelocity=1;xVelocity<maxX;xVelocity++){
	}



}


void solveTwo(tuple<int,int,int,int> input){
	int minX = get<0>(input);
	int maxX = get<1>(input);
	int minY = get<2>(input);
	int maxY = get<3>(input);


	cout << "SolveTwo: " << endl;
	cout << "x range: " << minX << ".." << maxX << endl;
	cout << "y range: " << minY << ".." << maxY << endl;


	map<int, set<int>> yVelocity_steps_that_hit;
	map<int, set<int>> steps_to_yVelocities;

	int maxStep = 0;

	for(int yVelocity=minY;yVelocity<=-minY;yVelocity++){
		int currentVelocity = yVelocity;
		int currentStep = 0;
		int currentYpos = 0;

		bool falling = false;
		// cout << "testing velocity " << yVelocity << endl;
		do {
			currentStep++;
			currentYpos += currentVelocity;
			falling = currentVelocity < 0;

			if(currentYpos >= minY && currentYpos <= maxY){
				yVelocity_steps_that_hit[yVelocity].insert(currentStep);
				steps_to_yVelocities[currentStep].insert(yVelocity);
				maxStep = max(maxStep, currentStep);
			}

			currentVelocity--;
		}while(!falling || (falling && currentYpos>=minY));
	}

	map<int, set<int>> steps_to_xVelocities;

	for(int xVelocity=0;xVelocity<=maxX;xVelocity++){
		int currentVelocity = xVelocity;
		int currentStep = 0;
		int currentXpos = 0;

		do{
			currentStep++;
			currentXpos += currentVelocity;

			if(currentXpos >= minX && currentXpos <= maxX){
				steps_to_xVelocities[currentStep].insert(xVelocity);
			}

			currentVelocity = currentVelocity==0?0:currentVelocity-1;


		}while(currentStep <= maxStep);

	}

	set<tuple<int,int>> velocities_that_hit;
	for(auto y_pr : steps_to_yVelocities){
		int ystep = y_pr.first;

		auto x_pr = steps_to_xVelocities.find(ystep);
		if(x_pr != steps_to_xVelocities.end()){

			for(auto y_velocity : y_pr.second){
				for(auto x_velocity : x_pr->second){
					velocities_that_hit.insert({x_velocity, y_velocity});
				}
			}
		}

	}

	cout << velocities_that_hit.size() << endl;

}
int main(){
	auto input = readInput();

	solveOne(input);
	solveTwo(input);
	return 0;
}

