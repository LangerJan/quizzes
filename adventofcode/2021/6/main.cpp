#include <string>
#include <iostream>
#include <map>
#include <vector>
#include <sstream>
#include <cstdint>

using namespace std;

map<int,int64_t> readInput(){
	map<int,int64_t> result;

	string line;
	getline(cin,line);
	stringstream ss(line);
	for(string num;getline(ss, num, ',');){
		result[stoi(num)]++;
	}

	return result;
}


map<int,int64_t> tick(const map<int,int64_t> &input){
	map<int,int64_t> result;

	for(auto it : input){
		if(it.first == 0){
			result[8] += it.second;
			result[6] += it.second;
		}
		else{
			result[it.first-1] += it.second;
		}
	}

	return result;
}

void solveOne(map<int,int64_t> input){

	for(int i=0;i<80;i++){
		map<int,int64_t> next = tick(input);
		input = next;
	}

	int64_t pop = 0;
	for(auto it : input){
		pop += it.second;
	}

	cout << "1: " << pop << endl;
}

void solveTwo(map<int,int64_t> input){

	for(int i=0;i<256;i++){
		map<int,int64_t> next = tick(input);
		input = next;
	}

	int64_t pop = 0;
	for(auto it : input){
		pop += it.second;
	}

	cout << "1: " << pop << endl;
}
int main(){
	auto input = readInput();
	for(auto it: input){
		cout << it.first << ":" << it.second << endl;
	}
	cout << endl;

	solveOne(input);
	solveTwo(input);
	return 0;
}

