#include <string>
#include <iostream>
#include <map>
#include <vector>
#include <sstream>
#include <cstdint>
#include <cmath>
#include <climits>

using namespace std;

map<int,int> readInput(){
	map<int,int> result;
	string line;
	getline(cin,line);
	stringstream ss(line);
	for(string num;getline(ss, num, ',');){
		result[stoi(num)]++;
	}

	return result;
}

int calcFuel(const map<int,int> &ships, int pos){
	int result = 0;

	for(const auto &it : ships){
		int currentPos = it.first;
		int diff = abs(currentPos-pos);
		result += diff * it.second;
	}

	return result;
}

int calcFuel2(const map<int,int> &ships, int pos){
	int result = 0;

	for(const auto &it : ships){
		int currentPos = it.first;
		int n = abs(currentPos-pos);

		int diff = n*(n+1)/2;

		result += diff * it.second;
	}

	return result;
}

void solveOne(map<int,int> input){

	int minPos = input.begin()->first;
	int maxPos = input.rbegin()->first;

	int minFuel = numeric_limits<int>::max();
	for(int i=minPos;i<=maxPos;i++){
		int fuel = calcFuel(input, i);
		minFuel = min(minFuel, fuel);
	}
	
	cout << minFuel << endl;

}


void solveTwo(map<int,int> input){

	int minPos = input.begin()->first;
	int maxPos = input.rbegin()->first;

	int minFuel = numeric_limits<int>::max();
	for(int i=minPos;i<=maxPos;i++){
		int fuel = calcFuel2(input, i);
		minFuel = min(minFuel, fuel);
	}
	
	cout << minFuel << endl;

}

int main(){
	auto input = readInput();

	solveOne(input);
	solveTwo(input);
	return 0;
}

