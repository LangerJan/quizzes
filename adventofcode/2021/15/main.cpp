#include <iostream>
#include <climits>
#include <string>
#include <map>
#include <set>
#include <vector>
#include <sstream>
#include <tuple>
#include <algorithm>
#include <numeric>
#include <functional>
#include <valarray>

#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>

using namespace std;

static const vector<tuple<int, int>> directions = {
	{1,0},   // up
	{-1,0},  // down
	{0,1},   // right
	{0,-1}   // left
};



map<tuple<int, int>, int> readInput(){
	map<tuple<int, int>, int> result;
	int y=0;
	for(string line;getline(cin,line);){
		int x=0;
		for(char c : line){
			result[{x,y}] = c - '0';
			x++;
		}
		y++;
	}

	return result;
}

void solveOne(map<tuple<int,int>, int> input){
	set<tuple<int,int>> unvisited_nodes;
	map<tuple<int,int>,int> distance;

	map<tuple<int,int>, tuple<int,int>> predecessor;

	tuple<int,int> start = {0,0};

	tuple<int,int> end;
	{
		int maxX = numeric_limits<int>::min();
		int maxY = numeric_limits<int>::min();

		for(auto pr : input){
			maxX = std::max(get<0>(pr.first), maxX);
			maxY = std::max(get<1>(pr.first), maxY);

			unvisited_nodes.insert(pr.first);
			distance[pr.first] = numeric_limits<int>::max();
		}

		end = {maxX, maxY};
	}

	distance[start] = 0;

	while(!unvisited_nodes.empty()){
		auto it = min_element(unvisited_nodes.begin(), unvisited_nodes.end(), [distance](const tuple<int,int> &a, const tuple<int,int> &b){return distance.at(a) > distance.at(b);});
		auto currentPos = *it;
		auto currentDistance = distance[currentPos];
		unvisited_nodes.erase(currentPos);


		for(auto dir : directions){
			tuple<int,int> nextPos = {
				get<0>(dir) + get<0>(currentPos),
				get<1>(dir) + get<1>(currentPos)
			};

			auto it_next = unvisited_nodes.find(nextPos);
			if(it_next != unvisited_nodes.end()){
				if(distance[nextPos] == numeric_limits<int>::max() ||
				   (input[nextPos]+currentDistance < distance[nextPos])){

					distance[nextPos] = input[nextPos]+currentDistance;
					predecessor[nextPos] = currentPos;
					
				}

			}
		}


	}

	cout << distance[end] << endl;
}


map<tuple<int,int>,int> expand(map<tuple<int,int>, int> tmap){
	map<tuple<int,int>,int> result;
	map<int, map<tuple<int,int>, int>> multiplications;

	int sizeX, sizeY;
	{
		int maxX = numeric_limits<int>::min();
		int maxY = numeric_limits<int>::min();

		for(auto pr : tmap){
			maxX = std::max(get<0>(pr.first), maxX);
			maxY = std::max(get<1>(pr.first), maxY);
		}
		sizeX = maxX+1;
		sizeY = maxY+1;
	}

	for(int i=0;i<=8;i++){		
		for(auto &pr : tmap){
			multiplications[i][pr.first] = pr.second+i>9?(pr.second+i)%9:pr.second+i;
		}
	}

	map<tuple<int,int>,int> multimap = {
		{{0,0}, 0}, {{1,0},1}, {{2,0},2}, {{3,0},3},{{4,0},4},
		{{0,1}, 1}, {{1,1},2}, {{2,1},3}, {{3,1},4},{{4,1},5},
		{{0,2}, 2}, {{1,2},3}, {{2,2},4}, {{3,2},5},{{4,2},6},
		{{0,3}, 3}, {{1,3},4}, {{2,3},5}, {{3,3},6},{{4,3},7},
		{{0,4}, 4}, {{1,4},5}, {{2,4},6}, {{3,4},7},{{4,4},8}
	};

	for(auto mm : multimap){
		int offsetX = get<0>(mm.first) * sizeX;
		int offsetY = get<1>(mm.first) * sizeY;
		int multiplicator_index = mm.second;

		for(auto orig : tmap){
			tuple<int,int> orig_pos = orig.first;
			int orig_value = orig.second;

			tuple<int,int> next_pos = {
				get<0>(orig_pos) + offsetX,
				get<1>(orig_pos) + offsetY
			};

			int multiplication = multiplications[multiplicator_index][orig_pos];

			result[next_pos] = multiplication;
		}
	}
	


	return result;
}

void printMap(map<tuple<int,int>,int> tmap){

	int sizeX, sizeY;
	{
		int maxX = numeric_limits<int>::min();
		int maxY = numeric_limits<int>::min();

		for(auto pr : tmap){
			maxX = std::max(get<0>(pr.first), maxX);
			maxY = std::max(get<1>(pr.first), maxY);
		}
		sizeX = maxX+1;
		sizeY = maxY+1;
	}


	for(int y=0;y<sizeY;y++){	
		for(int x=0;x<sizeX;x++){
			cout << tmap[{x,y}];
		}
		cout << endl;
	}
}


int main(){
	auto input = readInput();
	printMap(input);
	solveOne(input);
	auto expanded = expand(input);
	printMap(expanded);
	solveOne(expanded);

	return 0;
}








