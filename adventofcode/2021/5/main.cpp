#include <iostream>
#include <string>
#include <algorithm>
#include <tuple>
#include <map>
#include <set>
#include <sstream>
#include <vector>

using namespace std;

using coordinate = tuple<int, int>;
using cline = tuple<coordinate, coordinate>;

vector<cline> readInput(){
	vector<cline> result;

	for(string line;getline(cin,line);){
		stringstream ss(line);
		string num;
		getline(ss,num,',');
		int x1 = stoi(num);
		getline(ss,num,',');
		int y1 = stoi(num);
		coordinate from{x1,y1};

		getline(ss,num,',');
		int x2 = stoi(num);
		getline(ss,num,',');
		int y2 = stoi(num);
		coordinate to{x2,y2};

		cline l{from,to};

		result.push_back(l);
		
	}

	return result;
}

void solveTwo(vector<cline> input){
	map<coordinate, int> result;

	for(auto it : input){
		
		auto from = get<0>(it);
		auto to = get<1>(it);
		

		if(get<0>(from) == get<0>(to)){
			int x1 = get<0>(from);
			int y1 = get<1>(from);

			int x2 = get<0>(to);
			int y2 = get<1>(to);
			
			int sml_y = min(y1,y2);
			int big_y = max(y1,y2);

			for(;sml_y <= big_y;sml_y++){
				result[{x1,sml_y}]++;
			}
		}
		else if(get<1>(from) == get<1>(to)){
			int x1 = get<0>(from);
			int y1 = get<1>(from);

			int x2 = get<0>(to);
			int y2 = get<1>(to);
			
			int sml_x = min(x1,x2);
			int big_x = max(x1,x2);

			for(;sml_x <= big_x;sml_x++){
				result[{sml_x, y1}]++;
			}
		}
		else
		{
			coordinate new_from, new_to;
			if(get<0>(from) < get<0>(to)){
				new_from = from;
				new_to = to;

			}
			else {
				new_from = to;
				new_to = from;
			}


			int x1 = get<0>(new_from);
			int y1 = get<1>(new_from);
			
			int x2 = get<0>(new_to);
			int y2 = get<1>(new_to);

			int incY;
			if(get<1>(new_from) > get<1>(new_to)) incY = -1;
			else incY = 1;
			for(;x1 <= x2;x1++){
				

				result[{x1, y1}]++;
				y1 += incY;
			}
			

		}
	}

	int cnt = 0;
	for(auto it : result){
		if(it.second > 1) cnt++;
	}

#if 0
	{
		int biggest_x = 0;
		int biggest_y = 0;

		for(auto it : result){
			auto coord = it.first;
			int x1 = get<0>(coord);
			int y1 = get<1>(coord);

			biggest_x = max(x1, biggest_x);
			biggest_y = max(y1, biggest_y);
		}

		cout << "biggest y: " << biggest_y << endl;
		for(int y=0;y<=biggest_y;y++){
			for(int x=0;x<=biggest_x;x++){
				cout << result[{x,y}];
			}
			cout << endl;
		}

	}

#endif
	cout << "part 2: " << cnt << endl;
}

void solveOne(vector<cline> input){
	map<coordinate, int> result;

	for(auto it : input){
		
		auto from = get<0>(it);
		auto to = get<1>(it);
		
		int x1 = get<0>(from);
		int y1 = get<1>(from);

		int x2 = get<0>(to);
		int y2 = get<1>(to);

		if(x1 == x2){
			
			int sml_y = min(y1,y2);
			int big_y = max(y1,y2);

			for(;sml_y <= big_y;sml_y++){
				result[{x1,sml_y}]++;
			}
		}
		if(y1 == y2){
			
			int sml_x = min(x1,x2);
			int big_x = max(x1,x2);

			for(;sml_x <= big_x;sml_x++){
				result[{sml_x, y1}]++;
			}
		}
	}

	int cnt = 0;
	for(auto it : result){
		if(it.second > 1) cnt++;
	}


	{
		int biggest_x = 0;
		int biggest_y = 0;

		for(auto it : result){
			auto coord = it.first;
			int x1 = get<0>(coord);
			int y1 = get<1>(coord);

			biggest_x = max(x1, biggest_x);
			biggest_y = max(y1, biggest_y);
		}

		for(int y=0;y<=biggest_y;y++){
			cout << y << ":";
			for(int x=0;x<=biggest_x;x++){
				cout << result[{x,y}];
			}
			cout << endl;
		}

	}


	cout << "part 1: " << cnt << endl;
}


int main(){

	auto input = readInput();
	solveOne(input);
	solveTwo(input);
	return 0;
}
