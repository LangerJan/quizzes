#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <numeric>
#include <tuple>
#include <map>
#include <limits>

using namespace std;

tuple<string, map<string, char>> readInput() {
	string start, rule;
	map<string, char> rules;


	getline(cin,start);
	getline(cin,rule);
	while(getline(cin,rule)){
		string from = rule.substr(0,2);
		rules[from] = rule.at(rule.size()-1);
	}


	return {start, rules};
}

void applyRules(string &start,const map<string,char> &rules){
	string next;

	for(int i=0;i<start.size()-1;i++){
		next.append(1, start.at(i));
		next.append(1, rules.at(start.substr(i,2)));
	}
	next.append(1, start.at(start.size()-1));


	start = next;
}


void solveOne(tuple<string, map<string,char>> input, int iterations){
	string start = get<0>(input);
	map<string, char> rules = get<1>(input);

	for(int i=0;i<iterations;i++){
		applyRules(start,rules);
	}

	int64_t minCount = numeric_limits<int64_t>::max(), maxCount = 0;
	
	
	map<char, int64_t> counts;
	for(char c : start){
		counts[c]++;
	}
	for(auto pr : counts){
		minCount = std::min(pr.second, minCount);
		maxCount = std::max(pr.second, maxCount);
	}

	int64_t result = maxCount-minCount;

	cout << "Part: " << result << endl;
}



map<char, int64_t> iterate(string first, const map<string, char> &rules, int iterations){
	static map<tuple<string, map<string,char>, int>, map<char, int64_t>> cache;
	map<char, int64_t> result;

	tuple<string, map<string,char>, int> key = {first, rules, iterations};
	auto it = cache.find(key);
	if(it != cache.end()) return it->second;

	if(first.size() > 2){
		vector<string> substrings;
		for(size_t i=0;i<first.size()-1;i++){
			substrings.push_back(first.substr(i,2));
		}

		for(auto str : substrings){
			map<char, int64_t> part_result = iterate(str, rules, iterations);
			for(auto pr : part_result){
				result[pr.first] += pr.second;
			}
		}
	}
	else {
		result[rules.at(first)]++;

		if(iterations > 1){
			applyRules(first, rules);
			auto part_result = iterate(first, rules, iterations-1);
			for(auto pr : part_result){
				result[pr.first] += pr.second;
			}
		}
		
	}

	cache[key] = result;
	return result;
}

void solveTwo(tuple<string, map<string,char>> input, int iterations){
	auto counts = iterate(get<0>(input), get<1>(input), iterations);

	for(char c : get<0>(input)){
		counts[c]++;
	}

	int64_t minCount = numeric_limits<int64_t>::max(), maxCount = 0;
	
	
	for(auto pr : counts){
		minCount = std::min(pr.second, minCount);
		maxCount = std::max(pr.second, maxCount);
	}

	int64_t result = maxCount-minCount;



	for(auto pr : counts){
		cout << std::string(1,pr.first) << ":" << pr.second << endl;
	}

	cout << "min is: " << minCount << endl;
	cout << "max is: " << maxCount << endl;

	cout << "Part: " << result << endl;

}


int main(){
	auto input = readInput();


	solveOne(input,10);
	
	solveTwo(input,40);

	return 0;
}
