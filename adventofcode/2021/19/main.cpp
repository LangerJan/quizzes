#include <iostream>
#include <boost/geometry.hpp>
#include <vector>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/operation.hpp>
#include <boost/numeric/ublas/io.hpp>

#include <map>
#include <set>
#include <tuple>
#include <optional>
#include <sstream>
#include <memory>
#include <string>

using namespace std;

using coordinate = boost::numeric::ublas::matrix<int>;


bool coordinateCompare(coordinate arg0, coordinate arg1){
	if(arg0(0,0) != arg1(0,0)) return arg0(0,0) < arg1(0,0);
	else if(arg0(0,1) != arg1(0,1)) return arg0(0,1) < arg1(0,1);
	else return arg0(0,2) < arg1(0,2);
};

using cset = set<coordinate, decltype(coordinateCompare)*>;

vector<cset> allRotations(cset coords){
	vector<cset> result;

	for(int i=0;i<24;i++){
		result.push_back(cset(coordinateCompare));
	}

	for(auto &c : coords){
		int i = 0;
		coordinate x(1,3);
		x(0,0) = 	-c(0,2);
		x(0,1) = 	-c(0,1);
		x(0,2) = 	-c(0,0);
		result[i++].insert(x);

		x(0,0) =	-c(0,2);
		x(0,1) =	-c(0,0);
		x(0,2) =	 c(0,1);
		result[i++].insert(x);
		
		x(0,0) =	-c(0,2);
		x(0,1) =	 c(0,0);
		x(0,2) = 	-c(0,1);
		result[i++].insert(x);

		x(0,0) =	-c(0,2);
		x(0,1) =	 c(0,1);
		x(0,2) = 	 c(0,0);
		result[i++].insert(x);

		x(0,0) =	-c(0,1);
		x(0,1) =	-c(0,2);
		x(0,2) = 	 c(0,0);
		result[i++].insert(x);

		x(0,0) =	-c(0,1);
		x(0,1) =	-c(0,0);
		x(0,2) = 	-c(0,2);
		result[i++].insert(x);

		x(0,0) =	-c(0,1);
		x(0,1) =	 c(0,0);
		x(0,2) =	 c(0,2);
		result[i++].insert(x);

		x(0,0) =	-c(0,1);
		x(0,1) =	 c(0,2);
		x(0,2) =	-c(0,0);
		result[i++].insert(x);

		x(0,0) = 	-c(0,0);
		x(0,1) =	-c(0,2);
		x(0,2) = 	-c(0,1);
		result[i++].insert(x);

		x(0,0) = 	-c(0,0);
		x(0,1) =	-c(0,1);
		x(0,2) = 	 c(0,2);
		result[i++].insert(x);

		x(0,0) = 	-c(0,0);
		x(0,1) =	 c(0,1);
		x(0,2) = 	-c(0,2);
		result[i++].insert(x);

		x(0,0) = 	-c(0,0);
		x(0,1) =	 c(0,2);
		x(0,2) = 	 c(0,1);
		result[i++].insert(x);

		x(0,0) = 	 c(0,0);
		x(0,1) =	-c(0,2);
		x(0,2) = 	 c(0,1);
		result[i++].insert(x);

		x(0,0) =	 c(0,0);
		x(0,1) =	-c(0,1);
		x(0,2) =	-c(0,2);
		result[i++].insert(x);

		x(0,0) = 	 c(0,0);
		x(0,1) =	 c(0,1);
		x(0,2) = 	 c(0,2);
		result[i++].insert(x);

		x(0,0) = 	 c(0,0);
		x(0,1) =	 c(0,2);
		x(0,2) = 	-c(0,1);
		result[i++].insert(x);

		x(0,0) = 	 c(0,1);
		x(0,1) =	-c(0,2);
		x(0,2) = 	-c(0,0);
		result[i++].insert(x);

		x(0,0) = 	 c(0,1);
		x(0,1) =	-c(0,0);
		x(0,2) =	 c(0,2);
		result[i++].insert(x);

		x(0,0) = 	 c(0,1);
		x(0,1) =	 c(0,0);
		x(0,2) = 	-c(0,2);
		result[i++].insert(x);

		x(0,0) = 	 c(0,1);
		x(0,1) =	 c(0,2);
		x(0,2) = 	 c(0,0);
		result[i++].insert(x);

		x(0,0) =	 c(0,2);
		x(0,1) =	-c(0,1);
		x(0,2) = 	 c(0,0);
		result[i++].insert(x);

		x(0,0) =	 c(0,2);
		x(0,1) =	-c(0,0);
		x(0,2) = 	-c(0,1);
		result[i++].insert(x);

		x(0,0) = 	 c(0,2);
		x(0,1) =	 c(0,0);
		x(0,2) = 	 c(0,1);
		result[i++].insert(x);

		x(0,0) = 	 c(0,2);
		x(0,1) =	 c(0,1);
		x(0,2) = 	-c(0,0);
		result[i++].insert(x);
	}


	return result;
}

class scanner;

class scanner{
	public:
		int m_number;
		vector<cset> m_beacons_all_rotations;
		std::optional<int> m_assumed_rotation;

		map<shared_ptr<scanner>, coordinate> m_relative_coordinates;

		void merge(shared_ptr<scanner> scnr, int rotation, coordinate reference){
			for(const auto& coord : scnr->m_beacons_all_rotations.at(rotation)){
				coordinate corrected = reference + coord;

				this->m_beacons_all_rotations.at(this->m_assumed_rotation.value()).insert(corrected);
			}

			this->m_relative_coordinates[scnr] = reference;

			cout << "The relative corrdinates from scanner " << this->m_number << " to scanner " << scnr->m_number << " is "
		 		<< reference(0,0) << "," << reference(0,1) << "," << reference(0,2) << endl;
		}

		tuple<int, coordinate> matchWith(shared_ptr<scanner> scanner){
			int rot = 0;
			while(rot < scanner->m_beacons_all_rotations.size()){
			//for(const cset &rotation : scanner->m_beacons_all_rotations){
				const cset &rotation = scanner->m_beacons_all_rotations.at(rot);

				auto my_coordinates = this->m_beacons_all_rotations.at(this->m_assumed_rotation.value());
				for(const coordinate &my_coord : my_coordinates){
					for(const coordinate &your_coord : rotation){
						coordinate reference = my_coord - your_coord;


						cset found(coordinateCompare);
						for(const coordinate &your_coord2 : rotation){
							coordinate corrected = reference + your_coord2;
							
							if(my_coordinates.find(corrected) != my_coordinates.end()){
								found.insert(corrected);
							}       

							if(found.size() >= 12){
								for(auto c : found){
									cout << "found: " << c(0,0) << "," << c(0,1) << "," << c(0,2) << endl;
								}
								cout << "rotation: " << rot << endl;
								cout << "offset: " << reference(0,0) << "," << reference(0,1) << "," << reference(0,2) << endl;
								
								return {rot, reference};
							}
						}
						


					}
				}

				rot++;
			}
			
			return {-1, coordinate(1,3)};
		}


		static shared_ptr<scanner> readInput(){
			shared_ptr<scanner> result = make_shared<scanner>();
			string line;
			getline(cin, line);

			stringstream ss_line(line);
			string num;
			getline(ss_line, num, ' ');
			getline(ss_line, num, ' ');
			getline(ss_line, num, ' ');

			cset beacons(coordinateCompare);

			result->m_number = stoi(num);
			for(string input;getline(cin, input);){
				if(input.empty()) break;
				stringstream ss_line(input);
				
				coordinate elem(1,3);
				for(int i=0;i<3;i++){
					string val_str;
					getline(ss_line, val_str, ',');
					int val = stoi(val_str);
					elem(0,i) = val;
				}
				beacons.insert(elem);
			};

			result->m_beacons_all_rotations= allRotations(beacons);

			return result;
		}

		string toString(){
			stringstream ss;
			ss << "--- Scanner " << this->m_number << " ---" << endl;
			for(auto s : this->m_beacons_all_rotations.at(14)){
				ss << s(0,0) << "," << s(0,1) << "," << s(0,2) << endl;
			}
			return ss.str();
		}

};


map<int, shared_ptr<scanner>> readInput(){
	map<int, shared_ptr<scanner>> result;

	while(cin.good()){
		if(cin.peek() == EOF) break;

		auto scnr = scanner::readInput();
		result[scnr->m_number] = scnr;
	}

	return result;
}


void solveOne(map<int, shared_ptr<scanner>> input){

	auto scnr0 = input[0];
	input[0]->m_assumed_rotation = 14;

	set<shared_ptr<scanner>> sanitized_scanners{input[0]};
	
	set<shared_ptr<scanner>> yet_to_merge;
	for(auto i : input) yet_to_merge.insert(i.second);
	yet_to_merge.erase(scnr0);
	

	while(!yet_to_merge.empty()){
		
		shared_ptr<scanner> toDelete;
		for(auto scnrX : yet_to_merge){
			auto res = scnr0->matchWith(scnrX);
			if(get<0>(res) != -1){
				scnr0->merge(scnrX, get<0>(res), get<1>(res));
				toDelete = scnrX;
				break;
			}
		}
		if(toDelete) yet_to_merge.erase(toDelete);
		cout << "size: " << yet_to_merge.size() << endl;
	}

	cout << scnr0->m_beacons_all_rotations.at(scnr0->m_assumed_rotation.value()).size() << endl;

	{
		coordinate X(1,3);
		X(0,0) = 0; X(0,1) = 0; X(0,2) = 0;
		scnr0->m_relative_coordinates[scnr0] = X;
	}
	
	int maxDistance = 0;
	for(auto i : input){
		for(auto j : input){
			if(i.first == j.first) continue;

			cout << i.first << "->" << j.first << endl;
			cout << i.second << "->" << j.second << endl;
			coordinate d_i = scnr0->m_relative_coordinates[i.second];
			coordinate d_j = scnr0->m_relative_coordinates[j.second];


			int distance = abs(d_i(0,0) - d_j(0,0)) + 
				       abs(d_i(0,1) - d_j(0,1)) +
				       abs(d_i(0,2) - d_j(0,2));

			maxDistance = max(maxDistance, distance);
		}
	}

	cout << "Max Distance: " << maxDistance << endl;

}


int main()
{
	auto input = readInput();

	for(auto in : input){
		cout << in.second->toString() << endl;
	}


	solveOne(input);

	return 0;
}
