import numpy as np
import math as m

def Rx(theta):
    return np.matrix([[ 1, 0           , 0           ],
                      [ 0, m.cos(theta),-m.sin(theta)],
                      [ 0, m.sin(theta), m.cos(theta)]])
          
def Ry(theta):
    return np.matrix([[ m.cos(theta), 0, m.sin(theta)],
                        [ 0           , 1, 0           ],
                        [-m.sin(theta), 0, m.cos(theta)]])

def Rz(theta):
    return np.matrix([[ m.cos(theta), -m.sin(theta), 0 ],
                    [ m.sin(theta), m.cos(theta) , 0 ],
                    [ 0           , 0            , 1 ]])



v1 = np.array([[2],[4],[8]])

v2 = Rx(m.pi/2) * v1
print(np.round(v2, decimals=2))


v2 = Ry(m.pi/2) * v2
print(np.round(v2, decimals=2))
v2 = Ry(m.pi/2) * v2
print(np.round(v2, decimals=2))
v2 = Ry(m.pi/2) * v2
print(np.round(v2, decimals=2))
v2 = Ry(m.pi/2) * v2
print(np.round(v2, decimals=2))
