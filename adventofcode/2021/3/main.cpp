#include <algorithm>
#include <iostream>
#include <string>
#include <numeric>
#include <functional>
#include <tuple>
#include <set>
#include <vector>
#include <map>

using namespace std;

vector<string> readInput(){
	vector<string> result;
	for(string line;getline(cin,line);){
		result.push_back(line);
	}
	return result;
}

vector<string> filterInput(vector<string> input, char set, int pos){
	vector<string> result;
	for(auto str : input){
		if(str.at(pos) == set) result.push_back(str);
	}
	return result;
}

char mostCommonValue(vector<string> &input, int pos){
	int strl = input.at(0).size();
		
		int zeroCount = 0;
		int oneCount = 0;
		for(auto str : input){
			if(str.at(pos) == '0') zeroCount++;
			if(str.at(pos) == '1') oneCount++;
		}
		if(zeroCount > oneCount) return '0';
		else if(oneCount > zeroCount) return '1';
		else return ' ';
}


char leastCommonValue(vector<string> &input, int pos){
	int strl = input.at(0).size();
		
		int zeroCount = 0;
		int oneCount = 0;
		for(auto str : input){
			if(str.at(pos) == '0') zeroCount++;
			if(str.at(pos) == '1') oneCount++;
		}
		if(zeroCount > oneCount) return '1';
		else if(oneCount > zeroCount) return '0';
		else return ' ';
}


int findOxygenRating(vector<string> input){
	int strl = input.at(0).size();
	int currentPos = 0;

	while(input.size() != 1){

		char mostCommon = mostCommonValue(input, currentPos);
		if(mostCommon == ' ') mostCommon = '1';
		
		cout << "size before: " << input.size() << endl;
		remove_if(input.begin(), input.end(), [currentPos, mostCommon](string &str){return str.at(currentPos) != mostCommon;});
		cout << "size after: " << input.size() << endl;
		currentPos++;
	}

	cout << "oxygen: " << input.at(0) << ":";
	return stoi(input.at(0), nullptr, 2);
}

int findCO2Rating(vector<string> input){
	int strl = input.at(0).size();
	int currentPos = 0;

	while(input.size() != 1){

		char mostCommon = leastCommonValue(input, currentPos);
		if(mostCommon == ' ') mostCommon = '0';
		input = filterInput(input, mostCommon, currentPos);
		currentPos++;
	}

	cout << "co2: " << input.at(0) << ":";
	return stoi(input.at(0), nullptr, 2);
}


int main(){

	auto input = readInput();
	int strl = input.at(0).size();

	string gamma;

	for(int i=0;i<strl;i++){
		
		int zeroCount = 0;
		int oneCount = 0;
		for(auto str : input){
			if(str.at(i) == '0') zeroCount++;
			if(str.at(i) == '1') oneCount++;
		}
		if(zeroCount >= oneCount) gamma += '0';
		else gamma += '1';
	}


	int gammaInt = stoi(gamma, nullptr, 2);
	cout << gamma << " : " << gammaInt << endl;

	string epsilon;

	for(int i=0;i<strl;i++){
		
		int zeroCount = 0;
		int oneCount = 0;
		for(auto str : input){
			if(str.at(i) == '0') zeroCount++;
			if(str.at(i) == '1') oneCount++;
		}
		if(zeroCount >= oneCount) epsilon += '1';
		else epsilon += '0';
	}

	int epsilonInt = stoi(epsilon, nullptr, 2);
	cout << epsilon << " : " << stoi(epsilon, nullptr, 2) << endl;

	cout << (gammaInt*epsilonInt) << endl;

	int o2 = findOxygenRating(input);
	int co2 = findCO2Rating(input);

	cout << "o2:" << o2 << endl;
	cout << "co2:" << co2 << endl;
	cout << ":" << (o2*co2) << endl;
	return 0;
}
