#include <iostream>
#include <string>
#include <map>
#include <set>
#include <tuple>
#include <valarray>
#include <vector>

using namespace std;

using board = map<tuple<int,int>,int>;
using coordinate = tuple<int,int>;

board readInput(){
	board result;

	int y=0;
	for(string line;getline(cin,line);){
		int x=0;
		for(char c : line){
			result[{x,y}] = static_cast<int>(c-'0');
			x++;
		}
		y++;
	}
	return result;
}


set<int> getNeighbors(const board &brd, const coordinate &crd){
	set<int> result;

	set<coordinate> adjacents = {
		{get<0>(crd)+1, get<1>(crd)},
		{get<0>(crd)-1, get<1>(crd)},
		{get<0>(crd), get<1>(crd)+1},
		{get<0>(crd), get<1>(crd)-1},
	};

	for(auto &neighbor : adjacents){
		auto it = brd.find(neighbor);
		if(it != brd.end()){
			result.insert(it->second);
		}
	}

	return result;
}

bool isLowPoint(board &brd, const coordinate &crd){
	int pointVal = brd[crd];
	auto neighbors = getNeighbors(brd, crd);
	return all_of(neighbors.begin(), neighbors.end(), [pointVal](int i){return pointVal < i;});
}

set<coordinate> getLowPoints(board &brd){
	set<coordinate> result;
	
	for(auto pr : brd){
		if(isLowPoint(brd, pr.first)) result.insert(pr.first);
	}

	return result;
}

int getRiskLevels(board &brd, set<coordinate> &crds){
	int sum = 0;
	for(auto crd : crds){
		sum += brd[crd] + 1;
	}
	return sum;
}

void solveOne(board &brd){
	auto crds = getLowPoints(brd);

	for(int x=0;x<=100;x++){
		for(int y=0;y<=200;y++){
			auto it = brd.find({x,y});
			if(crds.find(it->first) != crds.end()){
				cout << "\033[4m";
			}
			if(it != brd.end()) cout << it->second;
			if(crds.find(it->first) != crds.end()){
				cout << "\033[0m";
			}

		}
		cout << endl;
	}

	int result = getRiskLevels(brd, crds);

	cout << "Part 1: " << result << endl;
}

set<coordinate> getBasin(board &brd, coordinate crd){
	set<coordinate> result;
	auto it = brd.find(crd);
	if(it == brd.end()) return result;
	if(it->second == 9) return result;
	
	result.insert(crd);
	brd.erase(it);

	set<coordinate> adjacents = {
		{get<0>(crd)+1, get<1>(crd)},
		{get<0>(crd)-1, get<1>(crd)},
		{get<0>(crd), get<1>(crd)+1},
		{get<0>(crd), get<1>(crd)-1},
	};

	for(auto adj : adjacents){
		auto adj_res = getBasin(brd, adj);
		result.insert(adj_res.begin(), adj_res.end());
	}

	return result;
}

set<set<coordinate>> getBasins(board brd){
	set<set<coordinate>> result;

	auto lowPoints = getLowPoints(brd);

	for(auto crd : lowPoints){
		result.insert(getBasin(brd, crd));
	}

	return result;
}

void solveTwo(board brd){
	auto basins = getBasins(brd);

	vector<set<coordinate>> basins_vec(basins.begin(), basins.end());
	std::sort(basins_vec.begin(), basins_vec.end(), [](set<coordinate> a, set<coordinate> b){return a.size() > b.size();});

	cout << basins_vec[0].size() * basins_vec[1].size() * basins_vec[2].size() << endl;
}


int main(){
	auto input = readInput();

	solveOne(input);
	solveTwo(input);

	return 0;
}







