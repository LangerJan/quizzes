#pragma once

#include <string>
#include <memory>
#include <sstream>
#include <iostream>
#include <vector>
#include <cstdint>

using namespace std;

class snailfish_number : public enable_shared_from_this<snailfish_number>{
	        public:
			shared_ptr<snailfish_number> m_parent;
			
			virtual string toString() = 0;
			static shared_ptr<snailfish_number> readNumber(stringstream &ss);
			
			virtual shared_ptr<snailfish_number> deepCopy() = 0;
			

			static shared_ptr<snailfish_number> add(shared_ptr<snailfish_number> left, shared_ptr<snailfish_number> right);

			virtual vector<shared_ptr<snailfish_number>> depth_order() = 0;
			virtual void tellParent(shared_ptr<snailfish_number> parent=shared_ptr<snailfish_number>()){
				this->m_parent = parent;
			};
			
			virtual int64_t magnitude() = 0;
			
			virtual shared_ptr<snailfish_number> getLeftLiteralNeighbor(shared_ptr<snailfish_number> caller = shared_ptr<snailfish_number>()) = 0;
			virtual shared_ptr<snailfish_number> getRightLiteralNeighbor(shared_ptr<snailfish_number> caller = shared_ptr<snailfish_number>()) = 0;
			
			virtual shared_ptr<snailfish_number> getLeftMostPairNested4Times(int depth=4){
				return shared_ptr<snailfish_number>();
			}

			virtual shared_ptr<snailfish_number> getLeftMostOverfullNumber(){
				return shared_ptr<snailfish_number>();
			}
			
			virtual void explode(){}
			virtual void split(){}
			
			virtual void replace(shared_ptr<snailfish_number> child, shared_ptr<snailfish_number> newchild){}
			
			virtual void reduce(){
				bool done = false;
				
				do{
					done = true;
					
					auto nest = getLeftMostPairNested4Times();
					if(nest){
						nest->explode();
						done = false;
						continue;
					}
					auto literal = this->getLeftMostOverfullNumber();
					if(literal){
						literal->split();
						done = false;
						continue;
					}
					
				}while(!done);
			}


			shared_ptr<snailfish_number> getPtr(){
				return shared_from_this();
			}

};
