#pragma once

#include "snailfish_number.h"
#include <sstream>
#include <cstdint>
#include <string>
#include <iostream>

using namespace std;

class snailfish_literal : public snailfish_number{
	public:
		int64_t m_number;
		snailfish_literal(int64_t num) : m_number(num) {}

		string toString() override {
			stringstream ss;
			ss << this->m_number;
			return ss.str();
		}
		
		shared_ptr<snailfish_number> deepCopy() override{
				return std::make_shared<snailfish_literal>(this->m_number);
		}

		
		virtual shared_ptr<snailfish_number> getLeftMostOverfullNumber() override{
			if(this->m_number >= 10) return this->getPtr();
			else return shared_ptr<snailfish_number>();
		}		
		
		void add(shared_ptr<snailfish_literal> arg){
				this->m_number += arg->m_number;
		}

		virtual int64_t magnitude() override{
			return this->m_number;
		}


		virtual void split() override;
		
		
		static shared_ptr<snailfish_number> readNumber(stringstream &ss);
		vector<shared_ptr<snailfish_number>> depth_order() override;
		
		shared_ptr<snailfish_number> getLeftLiteralNeighbor(shared_ptr<snailfish_number> caller = shared_ptr<snailfish_number>()) override;
		shared_ptr<snailfish_number> getRightLiteralNeighbor(shared_ptr<snailfish_number> caller = shared_ptr<snailfish_number>()) override;

};