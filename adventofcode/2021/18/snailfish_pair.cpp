#include "snailfish_pair.h"
#include "snailfish_literal.h"

#include <algorithm>
#include <iostream>

shared_ptr<snailfish_number> snailfish_pair::readNumber(stringstream &ss){
	char symbol;
	ss.readsome(&symbol, 1);
	if(symbol != '[') throw runtime_error("Unexpected character");
	shared_ptr<snailfish_number> left = snailfish_number::readNumber(ss);
	ss.readsome(&symbol, 1);
	if(symbol != ',') throw runtime_error("Unexpected character");
	shared_ptr<snailfish_number> right = snailfish_number::readNumber(ss);
	ss.readsome(&symbol, 1);
	if(symbol != ']') throw runtime_error("Unexpected character");
	return std::make_shared<snailfish_pair>(left, right);
}


vector<shared_ptr<snailfish_number>> snailfish_pair::depth_order(){
	vector<shared_ptr<snailfish_number>> result;

	auto left = this->m_left->depth_order();
	auto right = this->m_right->depth_order();

	result.insert(result.end(), left.begin(), left.end());
	result.insert(result.end(), right.begin(), right.end());
	result.push_back(this->getPtr());

	return result;
}

void snailfish_pair::explode(){
	
	auto left_of_left = this->m_left->getLeftLiteralNeighbor(this->m_left);
	if(left_of_left){
		dynamic_pointer_cast<snailfish_literal>(left_of_left)->add(dynamic_pointer_cast<snailfish_literal>(this->m_left));
	}
	
	auto right_of_right = this->m_right->getRightLiteralNeighbor(this->m_right);
	if(right_of_right){
		dynamic_pointer_cast<snailfish_literal>(right_of_right)->add(dynamic_pointer_cast<snailfish_literal>(this->m_right));				
	}
	
	this->m_parent->replace(this->getPtr(), make_shared<snailfish_literal>(0));
}

shared_ptr<snailfish_number> snailfish_pair::getLeftLiteralNeighbor(shared_ptr<snailfish_number> caller){
	if(!caller){
		return this->m_left->getLeftLiteralNeighbor(this->getPtr());
	}
	
	if(caller == m_right){
		return this->m_left->getRightLiteralNeighbor();
	}
	else if(caller == this->getPtr() ||
	   caller == this->m_left){
		   
		   if(this->m_parent) return this->m_parent->getLeftLiteralNeighbor(this->getPtr());
		   else return this->m_parent;
	}
	else if(caller == this->m_parent){
		return this->m_left->getLeftLiteralNeighbor(this->getPtr());
	}
	else
	{
		return shared_ptr<snailfish_number>();
	}
}		

shared_ptr<snailfish_number> snailfish_pair::getRightLiteralNeighbor(shared_ptr<snailfish_number> caller){
	if(!caller){
		return this->m_right->getRightLiteralNeighbor(this->getPtr());
	}

	if(caller == m_left){
		return this->m_right->getLeftLiteralNeighbor();
	}
	
	if(caller == this->getPtr() ||
	   caller == this->m_right){
		if(this->m_parent) return this->m_parent->getRightLiteralNeighbor(this->getPtr());
		else return this->m_parent;
	}
	else if(caller == this->m_parent){
		return this->m_right->getRightLiteralNeighbor(this->getPtr());
	}
	else
	{
		return shared_ptr<snailfish_number>();
	}
}		
