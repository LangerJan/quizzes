#include "snailfish_literal.h"
#include "snailfish_pair.h"

shared_ptr<snailfish_number> snailfish_literal::readNumber(stringstream &ss){
	int64_t num;
	ss >> num;
	return std::make_shared<snailfish_literal>(num);
}

vector<shared_ptr<snailfish_number>> snailfish_literal::depth_order(){
	return {};
}


shared_ptr<snailfish_number> snailfish_literal::getLeftLiteralNeighbor(shared_ptr<snailfish_number> caller){
	if(!caller) return getPtr();

	if(caller == this->m_parent){
		return getPtr();
	}
	else if(caller == this->getPtr()){
		return this->m_parent->getLeftLiteralNeighbor(this->getPtr());
	}
	else
	{
		return this->m_parent->getLeftLiteralNeighbor(this->getPtr());
	}
}		

void snailfish_literal::split(){
	int64_t leftNumber = this->m_number / 2;
	int64_t rightNumber = this->m_number - leftNumber;
	
	auto newchild = make_shared<snailfish_pair>(make_shared<snailfish_literal>(leftNumber),make_shared<snailfish_literal>(rightNumber));
	
	this->m_parent->replace(this->getPtr(), newchild);
}




shared_ptr<snailfish_number> snailfish_literal::getRightLiteralNeighbor(shared_ptr<snailfish_number> caller){
	if(!caller) return getPtr();

	if(caller == this->m_parent){
		return getPtr();
	}
	else if(caller == this->getPtr()){
		return this->m_parent->getRightLiteralNeighbor(this->getPtr());
	}
	else
	{
		return this->m_parent->getRightLiteralNeighbor(this->getPtr());
	}
}
