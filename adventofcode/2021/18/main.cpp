#include <iostream>
#include <string>
#include <cstdint>
#include <climits>
#include <map>
#include <functional>
#include <numeric>
#include <set>
#include <tuple>
#include <vector>
#include <algorithm>
#include <memory>
#include <array>
#include <sstream>
#include <stdexcept>
#include <cstdio>
#include <cctype>

#include "snailfish_number.h"

using namespace std;

vector<shared_ptr<snailfish_number>> readInput(){
	vector<shared_ptr<snailfish_number>> result;
	for(string line;getline(cin,line);){
		stringstream ss(line);
		auto elem = snailfish_number::readNumber(ss);
		elem->tellParent();
		result.push_back(elem);
	}
	
	return result;
}

int main(){
	auto input = readInput();

#if 1
	for(auto in : input){
		cout << in->toString() << endl;
		cout << in->deepCopy()->toString() << endl;
		
	}
#endif

	shared_ptr<snailfish_number> tmp;
	for(auto in : input){

		if(!tmp) tmp = in;
		else{
				auto tmp2 = snailfish_number::add(tmp, in);
				
				tmp = tmp2;
		}
		tmp->reduce();
	}

	cout << "Part One: " << tmp->magnitude() << endl;

#if 1
	int64_t maxMag = 0;
	for(auto in : input){
		for(auto in2: input){
				if(in == in2) continue;
			{
				auto res = snailfish_number::add(in, in2);
				res->reduce();
				
				cout << "reduced: " << res->toString() << endl;
				maxMag = std::max(maxMag, res->magnitude());				
			}
			{
				auto res = snailfish_number::add(in2, in);
				res->reduce();
				cout << "reduced: " << res->toString() << endl;
				maxMag = std::max(maxMag, res->magnitude());				
			}
		}
	}

	cout << "Part Two: " << maxMag << endl;
#endif
	return 0;
}
