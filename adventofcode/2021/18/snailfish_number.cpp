
#include "snailfish_number.h"
#include "snailfish_pair.h"
#include "snailfish_literal.h"
	                
//static shared_ptr<snailfish_number> readNumber(stringstream &ss);

shared_ptr<snailfish_number> snailfish_number::readNumber(stringstream &ss){
	int c = ss.peek();
	if(std::isdigit(c)){
		return snailfish_literal::readNumber(ss);
	}
	else
	{
		return snailfish_pair::readNumber(ss);
	}
}


shared_ptr<snailfish_number> snailfish_number::add(shared_ptr<snailfish_number> left, shared_ptr<snailfish_number> right){
	shared_ptr<snailfish_number> result = make_shared<snailfish_pair>(left->deepCopy(), right->deepCopy());
	result->tellParent();
	
	return result;
}


