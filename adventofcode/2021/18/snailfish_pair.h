#pragma once

#include "snailfish_number.h"
#include <sstream>
#include <string>
#include <memory>
#include <vector>

using namespace std;

class snailfish_pair : public snailfish_number{
	public:
		shared_ptr<snailfish_number> m_left;
		shared_ptr<snailfish_number> m_right;
		
		shared_ptr<snailfish_number> m_parent;
		

		snailfish_pair(shared_ptr<snailfish_number> left, shared_ptr<snailfish_number> right) 
				: m_left(left), m_right(right){}
				
		shared_ptr<snailfish_number> deepCopy() override{
				auto leftCopy = this->m_left->deepCopy();
				auto rightCopy = this->m_right->deepCopy();
			
				auto result = std::make_shared<snailfish_pair>(leftCopy, rightCopy);
				result->tellParent();
				return result;
		}
				

		string toString() override {
			stringstream ss;
			ss << "[" << this->m_left->toString() << "," 
				  << this->m_right->toString() 				  
				  << "]";

			return ss.str();
		}
		
		void tellParent(shared_ptr<snailfish_number> parent=shared_ptr<snailfish_number>()) override{
			this->m_parent = parent;
			this->m_left->tellParent(this->getPtr());
			this->m_right->tellParent(this->getPtr());
		};		
		
		virtual shared_ptr<snailfish_number> getLeftMostPairNested4Times(int depth=4) override{
			if(depth == 0) return this->getPtr();
			
			auto res = m_left->getLeftMostPairNested4Times(depth-1);
			if(!res) res = m_right->getLeftMostPairNested4Times(depth-1);
			return res;
		}
		
		virtual void replace(shared_ptr<snailfish_number> child, shared_ptr<snailfish_number> newchild) override {

			newchild->tellParent(this->getPtr());

			if(this->m_left == child){
				this->m_left = newchild;
			}
			else if(this->m_right == child){
				this->m_right = newchild;
			}
			
			
		}
		
		virtual int64_t magnitude() override{
			return 3*this->m_left->magnitude() + 2*this->m_right->magnitude();
		}

		
		virtual void explode() override;
		
		virtual shared_ptr<snailfish_number> getLeftMostOverfullNumber() override{
			auto res = this->m_left->getLeftMostOverfullNumber();
			if(!res) res = this->m_right->getLeftMostOverfullNumber();
			return res;
		}		
		
		shared_ptr<snailfish_number> getLeftLiteralNeighbor(shared_ptr<snailfish_number> caller = shared_ptr<snailfish_number>()) override;
		shared_ptr<snailfish_number> getRightLiteralNeighbor(shared_ptr<snailfish_number> caller = shared_ptr<snailfish_number>()) override;

		void explode(vector<shared_ptr<snailfish_number>> &ordered);

		static shared_ptr<snailfish_number> readNumber(stringstream &ss);
		vector<shared_ptr<snailfish_number>> depth_order() override;
};
