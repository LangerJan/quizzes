#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <algorithm>
#include <numeric>
#include <deque>
#include <stack>
#include <exception>

using namespace std;

vector<string> readInput(){
	vector<string> result;
	for(string line;getline(cin,line);) result.push_back(line);
	return result;
}

int testLine(string line){
	map<char, char> expect = {{'(',')'}, {'[',']'}, {'{','}'}, {'<','>'}};
	map<char, int> points = {{')',3}, {']',57}, {'}',1197}, {'>',25137}};

	stack<char> stck;
	for(char c : line){
		if(stck.empty()){
			if(expect.find(c) == expect.end()) return points[c];
			else stck.push(c);
		}
		else
		{
			if(expect.find(c) != expect.end()) stck.push(c);
			else {
				if(expect[stck.top()] == c) stck.pop();
				else return points[c];
			}
		}
	}
	return 0;
}


int64_t testLine2(string line){
	map<char, char> expect = {{'(',')'}, {'[',']'}, {'{','}'}, {'<','>'}};
	map<char, int> points = {{'(',1}, {'[',2}, {'{',3}, {'<',4}};

	stack<char> stck;
	for(char c : line){
		if(stck.empty()){
			if(expect.find(c) == expect.end()) return 0;
			else stck.push(c);
		}
		else
		{
			if(expect.find(c) != expect.end()) stck.push(c);
			else {
				if(expect[stck.top()] == c) stck.pop();
				else return 0;
			}
		}
	}

	int64_t result = 0;
	while(!stck.empty()){
		result *= 5;
		result += points[stck.top()];
		stck.pop();
	}
	

	return result;
}

void solveOne(vector<string> input){
	int result = accumulate(input.begin(), input.end(), 0, [](int i, string str){return i + testLine(str);});

	cout << "Part 1: " << result << endl;
}

void solveTwo(vector<string> input){

	input.erase(std::remove_if(input.begin(),input.end(), [](string line){return testLine(line) != 0;}), input.end());
	cout << "transform" << endl;
	vector<int64_t> result;
	transform(input.begin(), input.end(), back_inserter(result), testLine2);
	sort(result.begin(), result.end());

	for(auto i : result) cout << i << endl;

	cout << "Part 2: " << result.at(result.size()/2) << endl;
}

int main(){
	auto input = readInput();
	solveOne(input);
	solveTwo(input);

	return 0;
}
