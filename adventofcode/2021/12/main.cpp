#include <iostream>
#include <map>
#include <set>
#include <algorithm>
#include <numeric>
#include <functional>
#include <cstdint>
#include <string>
#include <sstream>
#include <cctype>

using namespace std;

map<string,set<string>> readInput(){
	map<string, set<string>> result;
	for(string line;getline(cin,line);){
		stringstream ss(line);
		string from, to;
		getline(ss,from,'-');
		getline(ss,to);
		result[from].insert(to);
		result[to].insert(from);
	}
	return result;
}

int countWays(map<string,set<string>> map, string currentCave, std::map<string, int> &smallCavesSeen, int maxVisits, vector<string> &pathTaken){
	if(currentCave.compare("end") == 0){
#if 0
		for(string str : pathTaken){
			cout << str << ",";
		}
		cout << "end" << endl;
#endif
	       	return 1;
	}
	if((currentCave.compare("start") == 0) && smallCavesSeen["start"] == 1) return 0;

	// We've been here before, and shouldn't be
	if(std::islower(currentCave.at(0)) != 0){
		if(smallCavesSeen[currentCave] == maxVisits) return 0;
		smallCavesSeen[currentCave]++;
	}

	int result = 0;
	pathTaken.push_back(currentCave);

	for(string nextCave : map[currentCave]){
		result += countWays(map, nextCave, smallCavesSeen, maxVisits, pathTaken);
	}
	pathTaken.pop_back();

	if(std::islower(currentCave.at(0)) != 0){
		smallCavesSeen[currentCave]--;
	}
	
	return result;
}


int countWays2(map<string,set<string>> map, string currentCave, std::map<string, int> &smallCavesSeen, int maxVisits, vector<string> &pathTaken){
	if(currentCave.compare("end") == 0){
#if  0
		for(string str : pathTaken){
			cout << str << ",";
		}
		cout << "end" << endl;
#endif
	       	return 1;
	}
	if((currentCave.compare("start") == 0) && smallCavesSeen["start"] == 1) return 0;

	// We've been here before, and shouldn't be
	if(std::islower(currentCave.at(0)) != 0){
		smallCavesSeen[currentCave]++;

		if(smallCavesSeen[currentCave] > 2){
			smallCavesSeen[currentCave]--;
			return 0;
		}

		if(smallCavesSeen[currentCave] == 2){
			bool check = true;
			for(auto pr : smallCavesSeen){
				if(pr.first.compare(currentCave) != 0){
					if(pr.second > 1){
						check = false;
						break;
					}
				}
			}
			if(!check){
				smallCavesSeen[currentCave]--;
				return 0;
			}
		}
	}

	int result = 0;
	pathTaken.push_back(currentCave);

	for(string nextCave : map[currentCave]){
		result += countWays2(map, nextCave, smallCavesSeen, maxVisits, pathTaken);
	}
	pathTaken.pop_back();

	if(std::islower(currentCave.at(0)) != 0){
		smallCavesSeen[currentCave]--;
	}
	
	return result;
}

void solveOne(std::map<string,set<string>> mp){
	std::map<string, int> smallCavesSeen;
	vector<string> pathTaken;
	cout << countWays(mp, "start", smallCavesSeen, 1, pathTaken) << endl;
}

void solveTwo(std::map<string,set<string>> map){
	std::map<string, int> smallCavesSeen;
	vector<string> pathTaken;
	cout << countWays2(map, "start", smallCavesSeen, 2, pathTaken) << endl;
}

int main(){

	auto input = readInput();

	solveOne(input);
	solveTwo(input);

	return 0;
}
