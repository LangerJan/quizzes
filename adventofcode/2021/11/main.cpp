#include <iostream>
#include <string>
#include <algorithm>
#include <map>
#include <set>
#include <tuple>
#include <cstdint>
#include <numeric>

using namespace std;

map<tuple<int,int>, int> readInput(){
	map<tuple<int,int>, int> result;

	int y = 0;
	for(string line;getline(cin,line);){
		int x=0;
		for(char c : line){
			result[{x,y}] = c - '0';
			x++;
		}
		y++;
	}

	return result;
}

set<tuple<int,int>> allNeighbors(map<tuple<int,int>, int> mp, tuple<int,int> pos){
	set<tuple<int,int>> result;

	set<tuple<int,int>> tst = {
		{get<0>(pos)-1, get<1>(pos)},
		{get<0>(pos)+1, get<1>(pos)},
		{get<0>(pos), get<1>(pos)+1},
		{get<0>(pos), get<1>(pos)-1},
		
		{get<0>(pos)+1, get<1>(pos)+1},
		{get<0>(pos)-1, get<1>(pos)-1},
		{get<0>(pos)+1, get<1>(pos)-1},
		{get<0>(pos)-1, get<1>(pos)+1}
	};

	for(auto pos : tst){
		auto it = mp.find(pos);
		if(it != mp.end()){
			if(it->second != 0){
				result.insert(it->first);
			}
		}
	}

	return result;
}

int tick(map<tuple<int,int>, int> &map){
	int result = 0;

	set<tuple<int,int>> to_flash;

	for(auto &pr : map){
		pr.second++;
		if(pr.second > 9){
			to_flash.insert(pr.first);
			pr.second = 0;
		}
	}


	while(!to_flash.empty()){
		result += to_flash.size();
		set<tuple<int,int>> next_to_flash;
		for(auto f : to_flash){
			for(auto neighbor : allNeighbors(map, f)){
				if(++map[neighbor] > 9){
					next_to_flash.insert(neighbor);
					map[neighbor] = 0;
				}
			}
		}
		to_flash = next_to_flash;
	}

	return result;
}

void solveOne(map<tuple<int,int>, int> map){
	int64_t result = 0;
	for(int i=0;i<100;i++){
		result += tick(map);
	}


	cout << "Part One: " << result << endl;
}

void solveTwo(map<tuple<int,int>, int> map){
	int result = 0;
	do{
		tick(map);
		result++;
	}while(std::accumulate(map.begin(), map.end(), 0, [](int value, auto &pr){return value + pr.second;}) != 0);
	cout << "Part Two: " << result << endl;
}


int main(){
	auto input = readInput();
	solveOne(input);
	solveTwo(input);
	return 0;
}

