#include <string>
#include <set>
#include <iostream>
#include <vector>
#include <algorithm>
#include <sstream>
#include <tuple>
#include <map>
#include <memory>
#include <algorithm>
#include <numeric>

using namespace std;

class bingocard {
	public:
		set<shared_ptr<set<int>>> m_rows_and_columns;

		string toString(){
			stringstream ss;

			for(auto it : this->m_rows_and_columns){
				for(auto num :*it){
					ss << num << ",";
				}
				ss << endl;
			}

			return ss.str();
		}

		bool bingo(){
			for(auto &b : this->m_rows_and_columns){
				if(b->empty()) return true;
			}
			return false;
		}

		void draw(int num){
			for(auto &s : this->m_rows_and_columns){
				s->erase(num);
			}
		}

		int sum(){
			int result = 0;

			set<int> s;

			for(auto &st : this->m_rows_and_columns){
				s.insert(st->begin(), st->end());
			}

			result = std::accumulate(s.begin(), s.end(), 0);

			return result;
		}

		static bingocard readCard(){
			bingocard result;
			
			map<int, set<int>> rows;
			map<int, set<int>> columns;

			int yPos = 0;
			for(string line;getline(cin,line);){
				if(line.empty()) break;

				stringstream ss(line);
				int xPos = 0;

				cout << "row " << yPos << ":";

				set<int> currentRow;
				for(string col;getline(ss, col, ' ');){
					if(col.empty()) continue;
					int num = stoi(col);

					cout << "R" << num << ",";
					currentRow.insert(num);
					columns[xPos].insert(num);
					xPos++;
				}
				cout << endl;
				rows[yPos] = currentRow;
				yPos++;
			}

			cout << "rows: " <<  rows.size() << endl;

			for(auto it : rows){
				result.m_rows_and_columns.insert(make_shared<set<int>>(it.second.begin(), it.second.end()));
			}
			for(auto it : columns){
				result.m_rows_and_columns.insert(make_shared<set<int>>(it.second.begin(), it.second.end()));
			}

			return result;
		}
};

vector<bingocard> read_incoming_cards(){
	vector<bingocard> result;
	
	for(;cin.good();){
		result.push_back(bingocard::readCard());
	}
	return result;
}

vector<int> read_incoming_numbers(){
	std::vector<int> numbers_called;
	string line;
	getline(cin,line);
	stringstream ss(line);
	for(string num;getline(ss, num, ',');){
		numbers_called.push_back(stoi(num));
	}
	return numbers_called;
}

tuple<vector<bingocard>, vector<int>> readInput(){
	vector<int> incoming_numbers = read_incoming_numbers();
	vector<bingocard> incoming_cards = read_incoming_cards();
	return {incoming_cards, incoming_numbers};
}

void solveOne(vector<bingocard> cards, vector<int> numbers){
	for(int num : numbers){
		for(auto &card : cards){
			if(card.bingo()) continue;
			card.draw(num);
			if(card.bingo()){
				cout << "bingo: " << card.sum() * num << endl;
			}
		}
	}
}

void solveTwo(vector<bingocard> cards, vector<int> numbers){
	for(int num : numbers){
		for(auto &card : cards){
			if(card.bingo()) continue;
			card.draw(num);
			if(card.bingo()){
				cout << card.sum() * num << endl;
			}
		}
		
	}
}

int main(void){

	auto input = readInput();
	solveOne(get<0>(input), get<1>(input));

	return 0;

}
