#include <algorithm>
#include <iostream>
#include <regex>
#include <string>

using namespace std;

bool validPassword(int min, int max, char character, string password){
	int cnt = std::count(password.begin(), password.end(), character);
	return cnt >= min && cnt <= max;
}

bool validPassword2(int pos1, int pos2, char character, string password){
	return ((password.at(pos1-1) == character)?1:0)+((password.at(pos2-1) == character)?1:0) == 1;
}

int main(){
	int result_one = 0;
	int result_two = 0;

	const std::regex pieces_regex("(\\d+)-(\\d+) ([a-z]): ([a-z]+)");
	smatch pieces_match;
	for(string line;getline(cin, line);){
        	if(std::regex_search(line, pieces_match, pieces_regex)) {
			int pos1 = std::stoi(pieces_match[1]);
			int pos2 = std::stoi(pieces_match[2]);
			char character = std::string(pieces_match[3]).at(0);
			string password = pieces_match[4];

			result_one += validPassword(pos1, pos2, character, password)?1:0;
			result_two += validPassword2(pos1, pos2, character, password)?1:0;
		}
	}	

	cout << result_one << endl;
	cout << result_two << endl;

	return 0;
}
