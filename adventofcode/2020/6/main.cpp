
#include <algorithm>
#include <string>
#include <iostream>
#include <iterator>
#include <set>
#include <vector>

using namespace std;

int main(){	

	int part1 = 0;
	int part2 = 0;
	set<char> group;

	vector<set<char>> answers;

	for(string line;getline(cin,line);){
		cout << line << endl;
		if(line.empty()){
			part1 += group.size();
			group.clear();

			set<char> result = answers.at(0);
			for(int i=1;i<answers.size();i++){

				set<char> &it = answers.at(i);
				set<char> tmp;

				set_intersection(result.begin(), result.end(),
						it.begin(), it.end(),
						inserter(tmp, tmp.end()));

				result = tmp;
			}
			part2 += result.size();
			answers.clear();
		}
		else
		{
			set<char> answer;
			for(char &c : line){
				group.insert(c);
				answer.insert(c);
			}
			answers.push_back(answer);
		}
	}
	cout << "Part 1: " << part1 << endl;
	cout << "Part 2: " << part2 << endl;

	return 0;
}
