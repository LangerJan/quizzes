#include <string>
#include <algorithm>
#include <vector>
#include <set>
#include <map>
#include <sstream>
#include <tuple>
#include <iostream>

using namespace std;

map<string, set<tuple<int, string>>> contains_relation;
map<string, set<string>> contained_by_relation;

int part1(string bag){
	set<string> toCheck, alreadyChecked;

	toCheck = contained_by_relation[bag];

	while(!toCheck.empty()){
		alreadyChecked.insert(toCheck.begin(),toCheck.end());

		set<string> nextToCheck;
		for(const string &it : toCheck){
			set<string> next = contained_by_relation[it];

			for(const string &nextIt : next){
				if(alreadyChecked.find(nextIt) == alreadyChecked.end()){
					nextToCheck.insert(nextIt);
				}
			}
		}
		toCheck = nextToCheck;
	}

	return alreadyChecked.size();
}



int64_t part2(string bag){
	int64_t result = 0;
	static map<string, int64_t> cache;
	if(cache.find(bag) != cache.end()) return cache[bag];
	
	set<tuple<int,string>> relations = contains_relation[bag];

	for(auto &it : relations){
		int64_t times = get<0>(it);
		string containingBag = get<1>(it);
		times = times + (times * part2(containingBag));
		result += times;
	}

	cache[bag] = result;
	return result;
}

int main(){


	for(string line;getline(cin,line);){
		stringstream line_ss(line);

		string key, name1, name2;
		line_ss >> name1; line_ss >> name2;
		key = name1 + " " + name2;

		string ignore;
		line_ss >> ignore;
		string contain;
		line_ss >> contain;

		string bags;
		string number;

		do{
			line_ss >> number;
			if(number.compare("no") == 0){
				break;
			}
			int num = stoi(number);
			string bagname, bagname1, bagname2;
			line_ss >> bagname1; line_ss >> bagname2;
			bagname = bagname1 + " " + bagname2;
			line_ss >> bags;
	
			contains_relation[key].insert({num, bagname});
			contained_by_relation[bagname].insert(key);
		}while(bags[bags.size()-1] == ',');
	}

	cout << part1("shiny gold") << endl;
	cout << part2("shiny gold") << endl;
	return 0;


}
