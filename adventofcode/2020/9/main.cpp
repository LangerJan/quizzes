#include <iostream>
#include <string>
#include <deque>
#include <vector>
#include <algorithm>

using namespace std;

bool isValidNumber(deque<int64_t> &sums, int64_t number){
	for(size_t a=0;a<sums.size();a++){
		for(size_t b=a+1;b<sums.size();b++){
			if(sums.at(a)+sums.at(b) == number) return true;
		}
	}
	return false;
}

int main(){

	vector<int64_t> numbers;
	deque<int64_t> sums;
	for(string line;getline(cin, line);){
		int64_t number = stoll(line);
		numbers.push_back(number);

		if(sums.size() < 25){
			sums.push_back(number);
		}
		else
		{
			if(isValidNumber(sums,number)){
				sums.push_back(number);
				sums.pop_front();
			}
			else {
				cout << "Part 1: " << number << endl;


				for(size_t i=0;i<numbers.size();i++){
					vector<int64_t> tmp;

					int64_t partSum = numbers.at(i);
					tmp.push_back(numbers.at(i));

					for(size_t j=i+1;j<numbers.size();j++){
						int64_t n = numbers.at(j);
						tmp.push_back(numbers.at(j));

						partSum += n;

						if(partSum == number){
							sort(tmp.begin(), tmp.end());

							cout << "Part 2: " << (tmp.at(0) + tmp.at(tmp.size()-1)) << endl;
							return 0;
						}
						if(partSum > number) break;
					}

				}
				break;
			}
		}
	}

	return 0;
}
