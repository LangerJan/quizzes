#include <string>
#include <iostream>
#include <vector>
#include <tuple>
#include <set>
#include <sstream>

using namespace std;

typedef enum opcode{
	OP_NOP,
	OP_ACC,
	OP_JMP
} opcode_t;

class handheld_state{
	public:
		size_t m_pc = 0;
		vector<tuple<opcode_t, int>> m_instructions;

		int64_t m_acc = 0;

		set<size_t> m_visited;
	

		void load(){
			for(string line;getline(cin, line);){
				stringstream ss_line(line);
				string opcode_str;
				int arg;

				ss_line >> opcode_str;
				ss_line >> arg;
				
				opcode_t opcode;
				if(opcode_str.compare("nop") == 0) opcode = OP_NOP;
				else if(opcode_str.compare("acc") == 0) opcode = OP_ACC;
				else if(opcode_str.compare("jmp") == 0) opcode = OP_JMP;
				else cerr << "Unknown opcode '" << opcode_str << "'" << endl;

				this->m_instructions.push_back({opcode, arg});
			}
		}

		void run(){


			while(this->m_visited.find(this->m_pc) == this->m_visited.end() && this->m_pc < this->m_instructions.size()){
				this->m_visited.insert(this->m_pc);
				
				auto line = this->m_instructions.at(this->m_pc);
				auto opcode = get<0>(line);
				auto arg0 = get<1>(line);

				switch(opcode){
					case OP_NOP:
						this->m_pc++;
						break;
					case OP_ACC:
						this->m_acc += arg0;
						this->m_pc++;
						break;
					case OP_JMP:
						this->m_pc += arg0;
						break;
					default:
						break;
				}

			}
		}
};


int main(){
	
	handheld_state hd;
	hd.load();
	

	for(size_t i=0;i<hd.m_instructions.size();i++){
		auto line = hd.m_instructions.at(i);
		opcode_t op = get<0>(line);

		handheld_state next_hd(hd);

		if(op == OP_NOP || op == OP_JMP){
			if(op == OP_NOP){
				get<0>(next_hd.m_instructions.at(i)) = OP_JMP;
			}
			else
			{
				get<0>(next_hd.m_instructions.at(i)) = OP_NOP;
			}

			next_hd.run();
			
			if(next_hd.m_pc >= next_hd.m_instructions.size()){
				cout << "Part 2: "  << next_hd.m_acc << endl;
				break;
			}

		}
	}

	hd.run();

	cout << "Part 1: " << hd.m_acc << endl;

	return 0;
}



