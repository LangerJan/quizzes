#include <string>
#include <iostream>

using namespace std;

int main(){

	char lastbit = ' ';
	for(string line;getline(cin,line);){
		char currentbit = line.at(9);
		if(lastbit == currentbit){
			cout << (stoi(line,nullptr,2)-1) << endl;
			return 0;
		}
		lastbit = currentbit;
	}
	return 0;
}
