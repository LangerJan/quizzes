#include <algorithm>
#include <iostream>
#include <map>
#include <cstdint>
#include <string>
#include <tuple>
#include <vector>

using namespace std;


const char TREE = '#';
const char FREE = '.';

vector<string> readInput(){
	vector<string> result;

	for(string line;getline(cin, line);){
		result.push_back(line);
	}

	return result;
}



int countTrees(vector<string> &input, int startX, int startY, int right, int down){
	int currentX = startX;
	int currentY = startY;
	int result = 0;

	while(currentY < input.size()){
		currentX = currentX % input.at(currentY).size();
		if(input.at(currentY).at(currentX) == TREE){
			result++;
		}
		currentX += right;
		currentY += down;
	}

	return result;
}

void solve_part1(vector<string> &input){
	cout << "Part 1: " << countTrees(input,0,0,3,1) << endl;
}

void solve_part2(vector<string> &input){
	vector<tuple<int,int>> vectors = {{1,1}, {3,1}, {5,1}, {7,1}, {1,2}};

	int64_t result = 1;
	for(auto vec : vectors){
		result *= countTrees(input,0,0,get<0>(vec), get<1>(vec));
	}
	cout << "Part 2: " << result << endl;
}

int main(){
	
	vector<string> input = readInput();
	solve_part1(input);
	solve_part2(input);

	return 0;
}




