#include <iostream>
#include <string>
#include <vector>

using namespace std;

int part1(std::vector<int> &invoices, int expSum){
	for(size_t i=0;i<invoices.size();i++){
		for(size_t j=i+1;j<invoices.size();j++){
			int sum = invoices.at(i)+invoices.at(j);
			if(sum == expSum){
				return (invoices.at(i)*invoices.at(j));
			}
		}
	}
	return -1;
}

int part2(std::vector<int> &invoices, int expSum){
	for(size_t i=0;i<invoices.size();i++){
		for(size_t j=i+1;j<invoices.size();j++){
			for(size_t k=j+1;k<invoices.size();k++){
				int sum = invoices.at(i)+invoices.at(j)+invoices.at(k);
				if(sum == expSum){
					return (invoices.at(i)*invoices.at(j)*invoices.at(k));
				}
			}
		}
	}
	return -1;
}

int main(){
	std::vector<int> invoices;
	for(std::string line;getline(cin,line);){
		invoices.push_back(std::stoi(line));
	}

	cout << part1(invoices, 2020) << endl;
	cout << part2(invoices, 2020) << endl;

	return 0;
}
