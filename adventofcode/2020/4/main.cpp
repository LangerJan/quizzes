#include <functional>
#include <iostream>
#include <map>
#include <regex>
#include <set>
#include <sstream>
#include <string>
#include <vector>

using namespace std;

bool isValid(map<string,string> &passport){
	set<string> fields = {"byr","iyr","eyr","hgt","hcl","ecl","pid"};
	for(auto key : fields){
		if(passport.find(key) == passport.end()) return false;
	}
	return true;
}

bool isValid2(map<string,string> &passport){
	const static vector<function<bool(map<string,string>&)>> validators = {
		[](map<string,string> &passport){
		if(passport.find("byr") == passport.end()) return false;
		int byr = stoi(passport["byr"]);
		return byr >= 1920 && byr <= 2002;
	},
		[](map<string,string> &passport){
		if(passport.find("iyr") == passport.end()) return false;
		int iyr = stoi(passport["iyr"]);
		return iyr >= 2010 && iyr <= 2020;
	},
		[](map<string,string> &passport){
		if(passport.find("eyr") == passport.end()) return false;
		int eyr = stoi(passport["eyr"]);
		return eyr >= 2020 && eyr <= 2030;
	},
		[](map<string,string> &passport){
		string hgt = passport["hgt"];
		stringstream ss_hgt(hgt);
		int num;
		string unit;
		ss_hgt >> num;
		ss_hgt >> unit;
		if(unit.compare("cm") == 0){
			return num >= 150 && num <=193;
		}
		else if(unit.compare("in") == 0){
			return num >= 59 && num <= 76;
		}
		else
		{
			cout << "Err: " << unit << endl;
			return false;
		}
	},
		[](map<string,string> &passport){
		string hcl = passport["hcl"];
		regex rex("#[0-9a-f]{6}");
		return regex_match(hcl, rex);
	},
		[](map<string,string> &passport){
		set<string> x = {"amb","blu","brn","gry","grn","hzl","oth"};
		return x.find(passport["ecl"]) != x.end();
	},
		[](map<string,string> &passport){
		string pid = passport["pid"];
		regex rex("[0-9]{9}");
		return regex_match(pid, rex);
	}
	};


	for(auto field : validators){
		if(!field(passport)) return false;
	}
	return true;
}


int main(){
	int valid = 0;
	int valid2 = 0;
	map<string, string> passport;


	for(string line;getline(cin, line);){
		if(line.empty()){
			if(isValid(passport)) valid++;

			try{
				if(isValid2(passport)) valid2++;
			}
			catch(invalid_argument &ex){
				cerr << "invalid argument: " << ex.what() << endl;
				return -1;
			}
			passport.clear();
		}
		else
		{
			stringstream line_ss(line);
			for(string field;getline(line_ss, field, ' ');){
				stringstream kv_ss(field);
				string key, value;
				getline(kv_ss, key, ':');
				getline(kv_ss, value, ':');
				passport[key] = value;
			}
		}
	}

	cout << valid << endl;
	cout << valid2 << endl;
	return 0;
}

