#include <iostream>
#include <string>

int main(){
    std::string line;
    int result = 0;
    while(std::getline(std::cin, line)){
        
        std::string formatted;
        formatted += "\"";


        for(char x : line){
           
            if(x == '\\'){
                formatted += "\\\\";
            }
            else if(x == '\"'){
                formatted += "\\\"";
            }
            else
            {
                formatted += x;
            }
        }

        formatted += "\"";

        int code_orig = line.size();
        int code_new = formatted.size();

        std::cout << code_orig << "/" << code_new << std::endl;

        result += code_new - code_orig;
    }

    std::cout << result << std::endl;
    return 0;
}
