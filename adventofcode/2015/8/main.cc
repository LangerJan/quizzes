#include <iostream>
#include <string>


int main(){

    int result = 0;

    std::string line;
    while(std::getline(std::cin, line)){

        bool within_quotation = false;
        bool escaped = false;
        bool within_hex = false;
        int hex_to_read = 2;

        int num_chars_code = line.size();
        int num_chars_memory = 0;

        for(char x : line){
            
            if(within_hex){
                if(hex_to_read > 0){
                    hex_to_read--;
                    continue;
                }
                else
                {
                    within_hex = false;
                    num_chars_memory++;
                }
            }

            if(x == '"'){
                if(within_quotation){
                    if(escaped){
                        num_chars_memory++;
                        escaped = false;
                    }
                    else
                    {
                        within_quotation = false;
                    }
                }
                else
                {
                    within_quotation = true;
                }
            }
            else if(x == '\\'){
                if(escaped){
                    num_chars_memory++;
                    escaped = false;
                }
                else
                {
                    escaped = true;
                }
            }
            else if(x == 'x'){
                if(escaped){
                    within_hex = true;
                    hex_to_read = 2;
                    escaped = false;
                }
                else
                {
                    num_chars_memory++;
                }
            }
            else
            {
                num_chars_memory++;
            }

        }

        result += num_chars_code - num_chars_memory;
        std::cout << num_chars_code << "/" << num_chars_memory << std::endl;
    }

    std::cout << result << std::endl;
    return 0;
}
