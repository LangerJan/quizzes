#include <string>
#include <iostream>
#include <sstream>
#include <functional>
#include <set>
#include <memory>

struct insta_effect {
	string m_name;
	int m_cost;
	int m_damage;
	int m_heal;
};

const insta_effect magic_missile("Magic Missile", 53, 4, 0);
const insta_effect drain("Drain", 73, 2, 2);

vector<const insta_effect> insta_effects{
	magic_missile, drain
};

struct effect {
	string m_name;
	int m_cost;
	int m_ttl;
	int m_armor;
	int m_damage;
	int m_extra_mana;
};

const effect shield("Shield",113,6,7,0,0);
const effect poison("Poison",173,6,0,3,0);
const effect recharge("Recharge",229,5,0,0,101);

vector<const effect> effects{
	shield, poison, recharge
};


class player {
	public:
	int m_hp;
	int m_armor;
	int m_mana;

	

	player() : 
		m_hp(50),
		m_armor(0),
		m_mana(500)
	{
	}

	


	int getCurrentArmor(){
		int result = 0;
		
		return result;
	}

	string toString(){
		stringstream ss;
		ss << "Player has " << m_hp << " hit points, " << getCurrentArmor() << " armor, " << m_mana << "mana";
		return ss.str();
	}
};

class boss {
	public:
	int m_hp;
	int m_damage;

	boss() :
		m_hp(58),
		m_damage(9)
	{

	}
	
	string toString(){
		stringstream ss;
		ss << "Boss has " << m_hp << " hit points";
		return ss.str();
	}

	void attack(shared_ptr<player> player){
		int actualDamage = m_damage - player->getCurrentArmor();
		if(actualDamage < 1) actualDamage = 1;
		cout << "Boss attacks for " << m_damage << " - " player->getCurrentArmor() << " = " << actualDamage << " damage!" << endl;
		player->m_hp -= actualDamage;
	}
}






