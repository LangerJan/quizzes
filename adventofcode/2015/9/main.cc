#include <string>
#include <regex>
#include <iostream>
#include <stdexcept>
#include <tuple>
#include <set>
#include <vector>
#include <algorithm>
#include <limits>

#include <boost/lexical_cast.hpp>

int main(){

    std::map<std::tuple<std::string, std::string>, int> distances;
    std::set<std::string> points;

    std::string line;
    while(std::getline(std::cin, line)){

        std::regex rex("(\\w+) to (\\w+) = (\\d+)");
        std::smatch smatch;

        std::regex_search(line, smatch, rex);

        if(smatch.size() != 4){
            std::cout << "invalid line '" << line << "'" << std::endl;
            throw std::runtime_error("invalid line in input");
        }

        std::string pointA = smatch[1];
        std::string pointB = smatch[2];
        int distance = boost::lexical_cast<int>(smatch[3]);

        distances[{pointA, pointB}] = distance;
        distances[{pointB, pointA}] = distance;
        points.insert(pointA);
        points.insert(pointB);
    }

    std::vector<std::string> points_vec(points.begin(), points.end());
    std::sort(points_vec.begin(), points_vec.end());

    std::cout << points_vec.size() << " points" << std::endl;

    std::vector<std::string> shortest_points;

    int shortest = std::numeric_limits<int>::max();
    do{ 

        int distance = 0;

        for(int i=0;i<points_vec.size()-1;i++){
            std::string pointA = points_vec[i];
            std::string pointB = points_vec[i+1];

            if(distances.find({pointA, pointB}) == distances.end()){
                std::cout << "cannot find way from " << pointA << " to " << pointB << std::endl;
                throw std::runtime_error("error");
            }
            distance += distances[{pointA, pointB}];
        }

        if(distance < shortest){
            shortest = distance;
            shortest_points = std::vector<std::string>(points_vec.begin(), points_vec.end());
        }

    }while(std::next_permutation(points_vec.begin(), points_vec.end()));

    int dist = 0;
    for(int i=0;i<shortest_points.size()-1;i++){
        auto pointA = shortest_points[i];
        auto pointB = shortest_points[i+1];
        int pdist = distances[{pointA, pointB}];
        std::cout << pointA << " -> " << pointB << " = " << pdist << std::endl;
        dist += pdist;
    }
    std::cout << dist << std::endl;

    return 0;
}
