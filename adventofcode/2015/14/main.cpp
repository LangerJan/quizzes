#include <regex>
#include <iostream>
#include <cstdint>
#include <sstream>
#include <string>
#include <stdexcept>
#include <vector>
#include <map>
#include <tuple>

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

using namespace std;

class deer{
	public:
	string m_name;
	int64_t m_speed;
	
	int64_t m_max_dexterity;
	int64_t m_curr_dexterity;

	int64_t m_total_resting;
	int64_t m_curr_resting;

	bool m_isRunning;

	int64_t m_distance;
	int64_t m_points;

	string toString(){
		stringstream ss;
		ss << m_name 
			<< "\t" << (m_isRunning?"running":"resting")
			<< "\t" << m_distance
			<< "\t" << m_speed
			<< "\t" << m_curr_dexterity << "/" << m_max_dexterity
			<< "\t" << m_curr_resting << "/" << m_total_resting
			<< "\t" << m_points;

		return ss.str();
	}

	void tick(){
		if(m_isRunning){
			m_curr_dexterity--;
			m_distance += m_speed;

			if(m_curr_dexterity == 0){
				m_isRunning = false;
				m_curr_resting = m_total_resting;
			}
		}
		else {
			m_curr_resting--;
			if(m_curr_resting == 0){
				m_isRunning = true;
				m_curr_dexterity = m_max_dexterity;
			}
		}
	}

};

vector<deer> readDeer(){
	vector<deer> result;

	for(string line;getline(cin, line);){
		regex re("(\\S+) can fly (\\d+) km/s for (\\d+) seconds, but then must rest for (\\d+) seconds.");
		smatch match;

		if(regex_match(line, match, re)){
			string name(match[1]);
			int64_t speed = boost::lexical_cast<int64_t>(match[2]);
			int64_t dex = boost::lexical_cast<int64_t>(match[3]);
			int64_t rest = boost::lexical_cast<int64_t>(match[4]);
			result.push_back({name, speed, dex, dex, rest, rest, true, 0, 0});
		}
	}

	return result;
}

int main(void){

	auto reindeers = readDeer();

	for(int s=0;s<2503;s++){

		for(auto &d : reindeers)d.tick();

		int64_t lead_distance = 0;
		vector<deer*> lead;

		for(auto &d : reindeers){

			if(d.m_distance > lead_distance){
				lead.clear();
				lead_distance = d.m_distance;
			}
			if(d.m_distance >= lead_distance){
				lead.push_back(&d);
			}
		}

		for(auto &d : lead){
			d->m_points++;
		}

	}
	for(auto &d : reindeers) cout << d.toString() << endl;

	return 0;
}










