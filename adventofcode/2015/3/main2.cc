/**
 * Compile with -std=c++11 -pthread
 * @author Jan Gampe
 */

#include <iostream>
#include <string>
#include <stdexcept>
#include <map>
#include <tuple>


int main(){

	std::map<std::tuple<int,int>,int> houses;
	
	int cursorX_santa = 0, cursorX_robo = 0;
	int cursorY_santa = 0, cursorY_robo = 0;
	
	
	int *otherX_ptr = &cursorX_robo;
	int *otherY_ptr = &cursorY_robo;

	int *currentX_ptr = &cursorX_santa;
	int *currentY_ptr = &cursorY_santa;

	{
		std::tuple<int,int> current_house{*currentX_ptr,*currentY_ptr};
		houses[current_house] += 1;
	}

	std::string line;
	while(std::getline(std::cin, line)){
		for(char c : line){
			*currentY_ptr = (c=='^'?*currentY_ptr+1:(c=='v'?*currentY_ptr-1:*currentY_ptr));
			*currentX_ptr = (c=='>'?*currentX_ptr+1:(c=='<'?*currentX_ptr-1:*currentX_ptr));

			std::tuple<int,int> current_house{*currentX_ptr,*currentY_ptr};
			houses[current_house] += 1;

			std::swap(currentX_ptr,otherX_ptr);
			std::swap(currentY_ptr,otherY_ptr);
		}
	}
	
	std::cout << houses.size() << std::endl;
	return 0;
}


