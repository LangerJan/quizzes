/**
 * Compile with -std=c++11
 * @author Jan Gampe
 */

#include <iostream>
#include <string>
#include <stdexcept>
#include <map>
#include <tuple>


int main(){

	std::map<std::tuple<int,int>,int> houses;

	int cursorX = 0;
	int cursorY = 0;

	std::tuple<int,int> current_house{cursorX,cursorY};
	houses[current_house] += 1;

	std::string line;
	while(std::getline(std::cin, line)){
		for(char c : line){
			cursorY = (c=='^'?cursorY+1:(c=='v'?cursorY-1:cursorY));
			cursorX = (c=='>'?cursorX+1:(c=='<'?cursorX-1:cursorX));

			std::tuple<int,int> current_house{cursorX,cursorY};
			houses[current_house] += 1;
		}
	}
	
	std::cout << houses.size() << std::endl;
	return 0;
}


