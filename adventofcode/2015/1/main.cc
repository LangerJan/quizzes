/**
 * compile with std=c++11 -pthread
 * @author Jan Gampe
 */

#include <iostream>
#include <string>
#include <stdexcept>
#include <cstdint>

#include <mutex>

int main(){
	std::string line;
	std::once_flag flag;
	int i = 1;

	int64_t result = 0;
	while(std::getline(std::cin, line)){
		for(char c : line){
			switch(c){
				case '(': result++; break;
				case ')': result--; break;
				default: throw std::runtime_error("invalid input");
			}
			if(result == -1){
				std::call_once(flag, [i](){std::cout << "Basement hit first at " << i << std::endl;});
			}

			i++;
		}
		std::cout << result << std::endl;
	}
	return 0;
}

