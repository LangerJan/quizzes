#include <algorithm>
#include <cstdint>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <limits>
#include <map>
#include <memory>
#include <numeric>
#include <regex>
#include <set>
#include <stdexcept>
#include <string>
#include <tuple>
#include <vector>

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

using namespace std;

tuple<map<string, set<string>>, string> readInput(){
	tuple<map<string,set<string>>, string> result;

	string line;
	while(getline(cin, line)){
		if(line.empty()) continue;
		regex re("(\\S+) => (\\S+)");
		smatch match;
		if(regex_match(line, match, re)){
			get<0>(result)[match[1]].insert(match[2]);
		}
		else
		{
			get<1>(result) = line;
		}
	}

	return result;
}

// returns count of non-overlapping occurrences of 'sub' in 'str'
int countSubstring(const string& str, const string& sub)
{
	if (sub.length() == 0) return 0;
	int count = 0;
	for (size_t offset = str.find(sub); offset != string::npos;offset = str.find(sub, offset + sub.length()))
	{
		count++;
	}
	return count;
}



set<string> doAllReplacements(const string& str, string key, set<string> replacements){
	set<string> result;


	for(auto rep : replacements){
		for(int cnt = countSubstring(str, key)-1;cnt >= 0; cnt--){
			result.insert(boost::algorithm::replace_nth_copy(str, key, cnt, rep));
		}
	}

	return result;
}



int main(void){

	auto input = readInput();
	
	map<string, set<string>> rules = get<0>(input);
	string line =  get<1>(input);

	set<string> result;
	for(auto kV : rules){
		set<string> res = doAllReplacements(line, kV.first, kV.second);
		result.insert(res.begin(), res.end());
	}

	cout << result.size() << endl;

	result.clear();
	result.insert(string("e"));
	int step = 0;

	cout << "--------------" << endl;
	set<string> nextResult;
	while(result.find(line) == result.end() && result.size() >= 0){
	
		cout << result.size() << endl;

		for(string nextLine : result){
			for(auto kV : rules){
				set<string> res = doAllReplacements(nextLine, kV.first, kV.second);

				for(auto res_it : res){
					if(res_it.size() <= line.size()){
						nextResult.insert(res_it);
					}
				}	
			}
		}
		step++;
		result = nextResult;
		nextResult.clear();
	}

	cout << step << endl;

	return EXIT_SUCCESS;
}



