
#include <algorithm>
#include <cstdint>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <limits>
#include <map>
#include <memory>
#include <numeric>
#include <regex>
#include <set>
#include <stdexcept>
#include <string>
#include <vector>

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

using namespace std;

vector<int> readContainers(){
	vector<int> result;
	for(string line;getline(cin, line);){
		result.push_back(boost::lexical_cast<int>(line));
	}

	sort(result.begin(), result.end());
	
	for(auto x : result) cout << x << endl;

	return result;
}

int howManyFits(vector<int> container, int idx, int liters){

	if(idx >= container.size()) return 0;

	// Use it
	int cap = container.at(idx);

	if(cap > liters){
		return howManyFits(container, idx+1, liters);
	}
	else if(cap < liters){
		return howManyFits(container, idx+1, liters-cap) +
		       howManyFits(container, idx+1, liters);
	}
	else {
		return 1 +
		       howManyFits(container, idx+1, liters);
	}

}


void howManyFits_Two(vector<int> container, int idx, int liters, int currently_used, int &minimum, int &result){

	if(currently_used > minimum) return;
	if(idx >= container.size()) return;

	int cap = container.at(idx);

	if(cap > liters){
		howManyFits_Two(container, idx+1, liters, currently_used, minimum, result);
		return;
	}
	else if(cap < liters){
		howManyFits_Two(container, idx+1, liters-cap, currently_used+1, minimum, result);
		howManyFits_Two(container, idx+1, liters, currently_used, minimum, result);
		return;
	}
	else {
		if(currently_used == minimum){
			result++;
		}
		else if(currently_used < minimum){
			minimum = currently_used;
			result = 1;
		}

		howManyFits_Two(container, idx+1, liters, currently_used, minimum, result);
	}
}

int main(void){

	auto containers = readContainers();

	cout << howManyFits(containers, 0, 150) << endl;

	int minimum = numeric_limits<int>::max();
	int result = 0;
	howManyFits_Two(containers, 0, 150, 0, minimum, result);

	cout << minimum << "\t" << result << endl;

	return EXIT_SUCCESS;
}
