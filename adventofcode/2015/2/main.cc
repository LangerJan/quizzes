/**
 * Compile with -std=c++11 -pthread
 * @author Jan Gampe
 */

#include <string>
#include <iostream>
#include <vector>
#include <cmath>
#include <cstdint>

#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>

int main(){
	int64_t result = 0;
	int64_t result_ribbon = 0;
	std::string line;
	while(std::getline(std::cin, line)){
		boost::trim(line);
		if(line.empty())continue;
		int length, width, height;
		{
			std::vector<std::string> strs;
			boost::split(strs,line,boost::is_any_of("x"));
			length = boost::lexical_cast<int>(strs[0]);
			width = boost::lexical_cast<int>(strs[1]);
			height = boost::lexical_cast<int>(strs[2]);
		}
		
		int area_lw = length * width;
		int area_lh = length * height;
		int area_hw = height * width;

		int volume = area_lw * height;
		int ribbon_sides = std::min(std::min(2*(length+width), 2*(length+height)), 2*(height+width));
		int ribbon_total = ribbon_sides + volume;
		result_ribbon += ribbon_total;

		int min_area = std::min(std::min(area_lw, area_lh), area_hw);
		result += 2*area_lw + 2*area_lh + 2*area_hw + min_area;
	}
	std::cout << result << " square feet of wrapping paper" << std::endl;
	std::cout << result_ribbon << " feet of ribbon" << std::endl;
	return 0;
}
