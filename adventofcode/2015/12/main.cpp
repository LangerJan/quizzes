#include <iostream>
#include <string>
#include <regex>
#include <algorithm>

#include <boost/lexical_cast.hpp>

using namespace std;

int main(void){

	regex re("[-]?\\d+");
	smatch match;

	string line;
	getline(cin, line);

	int result = 0;
	for(std::sregex_iterator i = std::sregex_iterator(line.begin(), line.end(), re);
		i != std::sregex_iterator();
		i++ )
	{
		std::smatch m = *i;
		result += boost::lexical_cast<int>(m.str());
	}

	cout << result << endl;

	return 0;
}
