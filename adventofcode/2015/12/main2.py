import json
from numbers import Number

with open("input", "r") as read_file:
    data = json.load(read_file)

def countNums(data):
    if isinstance(data, list):
        result = 0
        for v in data:
            if isinstance(v, Number):
                result = result + v
            elif isinstance(v, list) or isinstance(v, dict):
                result = result + countNums(v)
        return result
    elif isinstance(data, dict):
        result = 0
        if 'red' in data.values():
            return 0
        for k,v in data.items():
            if isinstance(v, Number):
                result = result + v
            elif isinstance(v, list) or isinstance(v, dict):
                result = result + countNums(v)
        return result

print countNums(data)
