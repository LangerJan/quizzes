#include <iostream>
#include <string>
#include <bitset>
#include <vector>
#include <algorithm>
#include <stdexcept>

#include <boost/algorithm/string.hpp>

int main(){

	std::vector<std::bitset<1000>> lights;

	std::string line;
	while(std::getline(std::cin, line)){
		if(boost::starts_with(line, "turn on")){
			turnOn(line, lights);
		}
		else if(boost::starts_with(line, "turn off"){
			turnOff(line, lights);
		}
		else if(boost::starts_with(line, "toggle"){
			toggle(line, lights);
		}
		else {
			throw std
		}
	}
	return 0;
}

