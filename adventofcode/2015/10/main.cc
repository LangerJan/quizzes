#include <string>
#include <iostream>
#include <sstream>

std::string process(std::string input){
	std::stringstream result;

	for(int i=0;i<input.size();){


		char currentChar = input[i];
		int occurrences = 0;

		while(i < input.size() && input[i] == currentChar){
			i++;
			occurrences++;
		}

		result << occurrences << currentChar;
		


	}

	return result.str();
}


int main(){

	std::string input("1113222113");

	for(int i=0;i<50;i++){
		input = process(input);
	}

	std::cout << input.size() << std::endl;

	return 0;


}
