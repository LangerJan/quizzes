#include <iostream>
#include <string>
#include <regex>
#include <cstdint>
#include <map>
#include <functional>
#include <vector>

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

using namespace std;

class gate{
	public:
	vector<string> m_needed;
	std::function<uint16_t(std::vector<uint16_t>)> m_function;
	bool m_resolved = false;
	uint16_t m_result = -1;

	uint16_t solve(std::map<std::string, gate> & gateMap){
		
		if(m_resolved){
			return m_result;
		}
		else
		{
			std::vector<uint16_t> params;
			for(auto strParam : m_needed){
				params.push_back(gateMap[strParam].solve(gateMap));
			}
			
			m_result = m_function(params);
			m_resolved = true;
			return m_result;
		}
	}

	gate(){
		m_needed = {};
		m_resolved = false;
		m_result = 0;
	}

	gate(uint16_t result){
		m_needed = {};
		m_resolved = true;
		m_result = result;
	}

	gate(vector<string> needed, std::function<uint16_t(vector<uint16_t>)> fkt){
		m_needed = needed;
		m_function = fkt;
		m_resolved = false;
		m_result = 0;
	}

	void reset(){
		if(m_function){
			m_resolved = false;
			m_result = 0;
		}
	}
};

uint16_t gate_and(std::vector<uint16_t> vec){return vec[0] & vec[1];}
uint16_t gate_or(std::vector<uint16_t> vec){return vec[0] | vec[1];}
uint16_t gate_rshift(std::vector<uint16_t> vec){return vec[0] >> vec[1];}
uint16_t gate_lshift(std::vector<uint16_t> vec){return vec[0] << vec[1];}
uint16_t gate_identity(std::vector<uint16_t> vec){return vec[0];}
uint16_t gate_not(std::vector<uint16_t> vec){return ~vec[0];}


std::map<std::string, gate> produceGates(){
	std::map<std::string, gate> gates;

	string line;
	while(getline(cin, line)){

		{
			std::regex rex("^(\\S+) -> (\\w+)");
			std::smatch match;
			if(std::regex_search(line, match, rex)){
				try{
					uint16_t num = boost::lexical_cast<uint16_t>(match.str(1));
					gates[match.str(1)] = gate(num);
				} catch(boost::bad_lexical_cast){}

				gate tmp_gate {
					{match.str(1)},
					gate_identity,
				};
				gates[match.str(2)] = tmp_gate;
			}
		}
		{
			std::regex rex("^NOT (\\S+) -> (\\w+)");
			std::smatch match;
			if(std::regex_search(line, match, rex)){
				try{
					uint16_t num = boost::lexical_cast<uint16_t>(match.str(1));
					gates[match.str(1)] = gate(num);
				} catch(boost::bad_lexical_cast){}

				gate tmp_gate {
					{match.str(1)},
					gate_not,
				};
				gates[match.str(2)] = tmp_gate;
			}

		}
		{
			std::regex rex("^(\\S+) (\\w+) (\\S+) -> (\\w+)");
			std::smatch match;
			if(std::regex_search(line, match, rex)){

				std::string op1 = match.str(1);
				std::string op2 = match.str(3);
				std::string oper = match.str(2);
				std::string retOp = match.str(4);


				for(std::string s : {op1, op2}){
					try{
						uint16_t num = boost::lexical_cast<uint16_t>(s);
						gates[s] = gate(num);
					} catch(boost::bad_lexical_cast){}
				}

				std::function<uint16_t(std::vector<uint16_t>)> function;

				if(boost::equals(oper, "RSHIFT")){function = gate_rshift;}
				else if(boost::equals(oper, "LSHIFT")){function = gate_lshift;}
				else if(boost::equals(oper, "AND")){function = gate_and;}
				else if(boost::equals(oper, "OR")){function = gate_or;}
				else {
					cerr << "Unknown binary operand " << oper << endl;
					throw std::runtime_error("Unknown operand");
				}

				gate tmp_gate {
					{op1, op2},
					function,
				};
				gates[retOp] = tmp_gate;
			}

		}


	}

	return gates;
}



int main(void){

	std::map<std::string, gate> gates = produceGates();
	
	uint16_t solutionPartOne = gates["a"].solve(gates);
	cout << solutionPartOne << endl;

	for(auto & gate_pair : gates){
		gate_pair.second.reset();
	}

	gates["b"].m_resolved = true;
	gates["b"].m_result = solutionPartOne;

	uint16_t solutionPartTwo = gates["a"].solve(gates);
	cout << solutionPartTwo << endl;

	return 0;
}







