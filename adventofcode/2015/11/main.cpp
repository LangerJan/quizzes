#include <cstdint>
#include <iostream>
#include <string>
#include <stdexcept>
#include <vector>
#include <algorithm>

using namespace std;

class password{
	public:

	string m_currentPW;


	bool req_threeLetters(){
		for(unsigned int i=0;i<m_currentPW.size()-2;i++){
			char first = m_currentPW.at(i);
			char second = m_currentPW.at(i+1);
			char third = m_currentPW.at(i+2);
			if((third - second) == 1 && (second - first) == 1){
				return true;
			}
		}
		return false;
	}

	bool req_forbiddenCharacters(size_t &lastOffence){
		lastOffence = m_currentPW.find_last_of("iol");
		return lastOffence == m_currentPW.npos;
	}

	bool req_doubleCharacters(){
		char firstCh = '\0';
		bool foundFirst = false;

		for(size_t i=0;i<m_currentPW.size()-1;i++){
			char first = m_currentPW.at(i);
			char second = m_currentPW.at(i+1);

			if(first == second){
				if(foundFirst && firstCh != first){
					return true;
				}
				else if(!foundFirst){
					foundFirst = true;
					firstCh = first;
					i++;
				}
			}
		}

		return false;
	}

	void inc(){
		increment(m_currentPW.size()-1);
	}

	void increment(int at){

		if(at == -1){
			std::string x("a");
			m_currentPW = x + m_currentPW;
		}
		else
		{
			char &ch = m_currentPW.at(at);
			if(ch == 'z'){
				ch = 'a';
				increment(at-1); 
			}
			else
			{
				ch++;
			}
		}
	}

	bool isValidPW(){
		size_t at;
		return req_threeLetters() && req_forbiddenCharacters(at) && req_doubleCharacters();
	}

	void nextPW(){
		do{
			inc();
		}
		while(!isValidPW());
		
	}

};


int main(void){
	
	password pw{"hepxcrrq"};

	pw.nextPW();
	cout << pw.m_currentPW << endl;
	pw.nextPW();
	cout << pw.m_currentPW << endl;

	return 0;
}
