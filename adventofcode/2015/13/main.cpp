#include <regex>
#include <iostream>
#include <cstdint>
#include <string>
#include <stdexcept>
#include <vector>
#include <map>
#include <tuple>

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

using namespace std;

map<string, map<string, int>> readPeople(){
	
	map<string, map<string, int>> result;

	for(string line;getline(cin, line);){
		regex re("(\\S+) would (gain|lose) (\\d+) happiness units by sitting next to (\\S+)\\.");
		smatch match;

		if(regex_match(line, match, re)){

			int num = boost::lexical_cast<int>(match[3]);
			if(boost::equals(string(match[2]), string("lose"))){
				num = -num;
			}
			result[match[1]][match[4]] = num;
		}
	}

	return result;
}

int calculateLove(vector<string> &peopleStr, map<string, map<string, int>> &loveMap){
	
	int love = 0;
	
	for(size_t i=0;i<peopleStr.size();i++){

		int idxLeft, idxRight;
		if(i==0){
		       	idxLeft = peopleStr.size()-1;
		} else {
			idxLeft = i-1;
		}
		
		if(i==peopleStr.size()-1){
		      	idxRight = 0;
		} else {
			idxRight = i+1;
		}

		love += loveMap[peopleStr[i]][peopleStr[idxLeft]];
		love += loveMap[peopleStr[i]][peopleStr[idxRight]];
	}

	return love;
}

int main(void){

	auto ppl = readPeople();

	vector<string> peopleStr;
	for(auto p : ppl){
		peopleStr.push_back(p.first);
	}
	sort(peopleStr.begin(), peopleStr.end());

	int max_love = 0;
	do{
		int currentLove = calculateLove(peopleStr, ppl);

		max_love = max(currentLove, max_love);
	}while(next_permutation(peopleStr.begin(), peopleStr.end()));

	cout << max_love << endl;

	return 0;
}










