
#include <algorithm>
#include <cstdint>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <limits>
#include <map>
#include <memory>
#include <numeric>
#include <regex>
#include <set>
#include <stdexcept>
#include <string>
#include <tuple>
#include <vector>

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

using namespace std;


map<tuple<int,int>, int> readInput(){
	map<tuple<int,int>, int> result;
	
	int y = 0;
	for(string line;getline(cin, line);){
		int x = 0;
		for(auto c: line){
			result[{x,y}] = (c == '#'?1:0);
			x++;
		}
		y++;
	}

	return result;
}

int read(map<tuple<int,int>,int> &arena, const tuple<int,int> pos){

	int x = get<0>(pos);
	int y = get<1>(pos);

	if((x == 0 && y == 0) ||
   	  (x == 99 && y == 0) ||
          (x == 99 && y == 99) ||
	  (x == 0 && y == 99)){
		return 1;
	}

	auto it = arena.find(pos);
	if(it == arena.end()) return 0;
	else return it->second;
}

int count(map<tuple<int,int>,int> &arena, tuple<int,int> pos){
	static vector<tuple<int,int>> directions{
		{0,1},
		{1,0},
		{1,1},
		{0,-1},
		{-1,0},
		{-1,-1},
		{-1,1},
		{1,-1}
	};

	int result = 0;
	for(auto dir : directions){
		tuple<int,int> neighPos{get<0>(dir)+get<0>(pos), get<1>(dir)+get<1>(pos)};
		result += read(arena, neighPos);
	}

	return result;
}

map<tuple<int,int>, int> step(map<tuple<int,int>, int> arena){
	map<tuple<int,int>,int> result;

	for(auto kV : arena){
		auto pos = kV.first;
		int status = read(arena, pos);

		int neigh = count(arena, pos);

		int followUp;
		if(status == 1){
			if(neigh == 2 || neigh == 3){
				followUp = 1;
			}
			else {
				followUp = 0;
			}
		}
		else
		{
			if(neigh == 3){
				followUp = 1;
			}
			else
			{
				followUp = 0;
			}
		}

		result[pos] = followUp;
	}
	


	return result;
}


int main(void){

	auto input = readInput();

	map<tuple<int,int>,int> next = input;

	for(int i=0;i<100;i++){
		next = step(next);
	}

	int result = 0;
	for(auto kV : next){
		result += read(next, kV.first);
	}

	cout << result << endl;

	return EXIT_SUCCESS;
}
