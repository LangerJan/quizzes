#include <regex>
#include <iostream>
#include <cstdint>
#include <sstream>
#include <string>
#include <stdexcept>
#include <vector>
#include <map>
#include <tuple>
#include <functional>
#include <numeric>


#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

using namespace std;

class sue{
	public:
	int m_name;
	map<string, int> m_contents;

	string toString(){
		stringstream ss;
		ss << "Sue " << m_name << ": ";
		for(auto cnt : m_contents){
			ss << cnt.first << ": " << cnt.second << ", ";
		}
		return ss.str();
	}
};

vector<sue> readSues(){
	vector<sue> result;

	for(string line;getline(cin, line);){
		regex re("Sue (\\d+): (\\S+): (\\d+), (\\S+): (\\d+), (\\S+): (\\d+)");
		smatch match;

		if(regex_match(line, match, re)){
			result.push_back(
				{
					boost::lexical_cast<int>(match[1]),
					{
						{match[2],boost::lexical_cast<int>(match[3])},
						{match[4],boost::lexical_cast<int>(match[5])},
						{match[6],boost::lexical_cast<int>(match[7])}
					}
				}
			);
		}
	}

	return result;
}

vector<sue> eliminateSues(vector<sue> sues, string key, int value, function<bool(int,int)> test){

	sues.erase(remove_if(sues.begin(), sues.end(), [key, value, test](sue &s){
		if(s.m_contents.find(key) == s.m_contents.end()){
			return false;
		}
		else
		{
			return test(s.m_contents[key], value);
		}
	}), sues.end());


	return sues;
}




int main(void){

	auto sues = readSues();

	map<string, int> facts{
		{"children", 3},
		{"cats", 7},
		{"samoyeds", 2},
		{"pomeranians", 3},
		{"akitas", 0},
		{"vizslas", 0},
		{"goldfish", 5},
		{"trees", 3},
		{"cars", 2},
		{"perfumes", 1}
	};

	{
		vector<sue> result = sues;
		for(auto f : facts){
			result = eliminateSues(result, f.first, f.second, not_equal_to<int>());
		}
		cout << result.at(0).toString() << endl;
	}

	{
		vector<sue> result = sues;
		for(auto f : facts){

			function<bool(int,int)> test;
			if(boost::equals(f.first, "cats") || boost::equals(f.first, "trees")){
				test = less_equal<int>();
			}
			else if(boost::equals(f.first, "pomeranians") || boost::equals(f.first, "goldfish")){
				test = greater_equal<int>();
			}
			else {
				test = not_equal_to<int>();
			}

			result = eliminateSues(result, f.first, f.second, test);
		}
		cout << result.at(0).toString() << endl;
	}

	return 0;
}










