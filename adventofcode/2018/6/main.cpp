#include <algorithm>
#include <iostream>
#include <iterator>
#include <map>
#include <string>
#include <set>
#include <tuple>
#include <vector>
#include <unordered_set>
#include <cmath>

using namespace std;

typedef tuple<int,int> coords;

set<coords> getAllStartingPoints(){
	set<coords> result;
	for(string line;getline(cin, line);){
		int xPos, yPos;
		sscanf(line.c_str(), "%d, %d", &xPos, &yPos);
		result.insert({xPos,yPos});
	}
	return result;
}

template <size_t X>
auto cmp = [](const coords &arg0, const coords &arg1){return get<X>(arg0) < get<X>(arg1);};

class santaMap{
	public:
	/*
	 * Possible values:
	 * * -1  occupied by more than one
	 * * 0   unoccupied
	 * * x>0 occupied by x
	 */
	map<coords, int> m_occupation;
	set<int> m_occupants;
	set<coords> m_startingPoints;

	/*
	 * Beyond these borders, nothing changes any more.
	 * Every occupant at these borders is having an invinite area
	 */
	int m_minX, m_maxX, m_minY, m_maxY;

	santaMap(set<coords> &startingPoints){
		this->m_startingPoints = startingPoints;
		int i = 1;
		for(auto point : startingPoints){
			this->m_occupants.insert(i);
			this->m_occupation[point] = i++;
		}

		this->m_minX = get<0>(*min_element(startingPoints.begin(), startingPoints.end(), cmp<0>));
		this->m_minY = get<1>(*min_element(startingPoints.begin(), startingPoints.end(), cmp<1>));
		this->m_maxX = get<0>(*max_element(startingPoints.begin(), startingPoints.end(), cmp<0>));
		this->m_maxY = get<1>(*max_element(startingPoints.begin(), startingPoints.end(), cmp<1>));
	}

	bool isWithinBorders(coords &point){
		auto [posX, posY] = point;
		return posX >= this->m_minX && posX <= this->m_maxX &&
		       posY >= this->m_minX && posY <= this->m_maxY;
	}

	set<coords> getFreeNeighbors(const coords &point){
		static vector<coords> directions{
			{-1,0},{1,0},{0,-1},{0,1}
		};

		// cout << "get neighbors of " << get<0>(point) << "/" << get<1>(point) << endl;

		auto [posX, posY] = point;
		set<coords> result;

		for(auto dir : directions){
			auto [dirX, dirY] = dir;
			coords next{posX+dirX, posY+dirY};
			if(this->isWithinBorders(next)){
				if(this->m_occupation[next] == 0){
					result.insert(next);
				}
			}
		}
		return result;
	}

	void populate(){
		map<coords, int> nextClaims;
		do{
			nextClaims.clear();

			for(auto point_pr : this->m_occupation){
				int occupant = point_pr.second;
				if(occupant > 0){
					for(auto neighborPoint : this->getFreeNeighbors(point_pr.first)){
						auto claim = nextClaims[neighborPoint];
						if(claim == 0){
							nextClaims[neighborPoint] = occupant;
						}
						else if(claim > 0 && claim != occupant){
							nextClaims[neighborPoint] = -1;
						}
					}
				}
			}
			for(auto point_pr : nextClaims){
				if(point_pr.second != 0){
					this->m_occupation[point_pr.first] = point_pr.second;
				}
			}
		}while(!nextClaims.empty());
	}

	set<int> getFiniteNumberings(){
		set<int> inf_result;

		for(int posX = this->m_minX;posX <= this->m_maxX;posX++){
			for(int posY : {this->m_minY, this->m_maxY}){
				if(this->m_occupation[{posX,posY}] > 0){
					inf_result.insert(this->m_occupation[{posX,posY}]);
				}
			}
		}

		for(int posY = this->m_minY;posY <= this->m_maxY;posY++){
			for(int posX : {this->m_minX, this->m_maxX}){
				if(this->m_occupation[{posX,posY}] > 0){
					inf_result.insert(this->m_occupation[{posX,posY}]);
				}
			}
		}


		set<int> result;
		set_difference(this->m_occupants.begin(), this->m_occupants.end(), inf_result.begin(), inf_result.end(), 
				std::inserter(result, result.end()));

		return result;
	}

	int getBiggestArea(){
		int result = 0;

		for(int id : this->getFiniteNumberings()){
			
			int id_res = count_if(this->m_occupation.begin(), this->m_occupation.end(), [id](auto &elem){
						return elem.second == id;
					});
			result = max(result, id_res);
		}

		return result;
	}


	int manhattanDist(coords start, coords end){
		auto [startX, startY] = start;
		auto [endX, endY] = end;
		return abs(startX-endX) + abs(startY-endY);
	}

	set<coords> getAllSteps(coords start, int maxDist, set<coords> startPoints){
		set<coords> result;
		auto [posX, posY] = start;
		for(int stepH=0;stepH<maxDist;stepH++){
			for(int stepV=0;stepV<maxDist-stepH;stepV++){
				vector<coords> allDirs = {{posX+stepH, posY+stepV},
							{posX+stepH, posY-stepV},
							{posX-stepH, posY+stepV},
							{posX-stepH, posY-stepV}};

				for(auto coord : allDirs)
				{
					int totalDist = 0;
					for(auto point : startPoints){
						totalDist += this->manhattanDist(coord, point);
					}

					//cout << "\t" << get<0>(coord) << "/" << get<1>(coord) << endl;


					if(totalDist < maxDist){
						result.insert(coord);
					}
				}


			}
		}

		return result;
	}

	
	int getPart2Area(int maxSteps){

		vector<coords> starts(this->m_startingPoints.begin(), this->m_startingPoints.end());
		coords start = starts[0];

		auto result = this->getAllSteps(start, maxSteps, this->m_startingPoints);

		return result.size();

	}

};


int main(){

	auto startingPoints = getAllStartingPoints();

	santaMap sMap(startingPoints);
	sMap.populate();

	cout << sMap.getBiggestArea() << endl;

	cout << sMap.getPart2Area(10000) << endl;

	return 0;
}












