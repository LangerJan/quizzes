#include <algorithm>
#include <cstdint>
#include <iostream>
#include <numeric>
#include <vector>

using namespace std;

class node{
	public:
	vector<uint32_t> m_metadata;
	vector<node> m_childNodes;

	node(std::istream &input){
		int childNodes, metaNodes;
		input >> childNodes;
		input >> metaNodes;
		for(int i=0;i<childNodes;i++){
			m_childNodes.push_back(node(input));
		}
		for(int i=0;i<metaNodes;i++){
			int metaNode;
			input >> metaNode;
			m_metadata.push_back(metaNode);
		}
	}

	int sumMeta(){
		int result = accumulate(this->m_metadata.begin(), this->m_metadata.end(), 0);
		for(auto &child : m_childNodes) result += child.sumMeta();
		return result;
	}

	int value(){
		if(this->m_childNodes.empty()){
			return accumulate(this->m_metadata.begin(), this->m_metadata.end(), 0);
		}
		else {
			int result = 0;
			for(auto meta : this->m_metadata){
				if(meta-1 < this->m_childNodes.size()){
					result += this->m_childNodes[meta-1].value();
				}
			}
			return result;
		}
	}
};

int main(){
	node root(cin);
	cout << "Part 1: " << root.sumMeta() << endl;
	cout << "Part 2: " << root.value() << endl;
	return 0;
}
