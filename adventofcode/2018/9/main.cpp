#include <algorithm>
#include <cstdint>
#include <iostream>
#include <list>
#include <sstream>
#include <string>
#include <vector>

using namespace std;

struct circle{
	list<uint64_t>::iterator m_currentMarble;
	list<uint64_t> m_marbles;

	uint64_t m_lowestMarbleAvailable;
	uint64_t m_highestMarble;

	circle(uint64_t highestMarble){
		this->m_highestMarble = highestMarble;
		this->m_marbles.push_back(0);
		this->m_currentMarble = this->m_marbles.begin();
		this->m_lowestMarbleAvailable = 1;
	}

	uint64_t takeAvailableMarble(){
		return this->m_lowestMarbleAvailable++;
	}

	void placeMarble(uint64_t marble){
		for(int i=0;i<2;i++){
			if(this->m_currentMarble == this->m_marbles.end()) this->m_currentMarble = this->m_marbles.begin();
			this->m_currentMarble++;
		}

		this->m_currentMarble = this->m_marbles.insert(this->m_currentMarble, marble);
	}

	uint64_t takeMarble(){
		for(int i=0;i<7;i++){
			if(this->m_currentMarble == this->m_marbles.begin()) this->m_currentMarble = this->m_marbles.end();
			this->m_currentMarble--;
		}
		
		uint64_t result = *(this->m_currentMarble);
		this->m_currentMarble = this->m_marbles.erase(this->m_currentMarble);
		return result;
	}

	bool marblesEmpty(){
		return this->m_lowestMarbleAvailable > this->m_highestMarble;
	}
};

struct player{
	uint64_t m_points = 0;
	void step(circle &c_circle){
		int myMarble = c_circle.takeAvailableMarble();
		if(myMarble % 23 != 0){
			c_circle.placeMarble(myMarble);
		}
		else
		{
			this->m_points += myMarble;
			this->m_points += c_circle.takeMarble();
		}
	}
};

void solve(int numPlayers, int highestMarble){
	circle circ(highestMarble);
	int currentPlayerIdx = 0;
	vector<player> players(numPlayers);

	while(!circ.marblesEmpty()){
		players.at(currentPlayerIdx).step(circ);
		currentPlayerIdx = (currentPlayerIdx+1)%players.size();
	}

	cout << max_element(players.begin(), players.end(), [](auto &arg0, auto &arg1){return arg0.m_points < arg1.m_points;})->m_points << endl;
}

int main(){
	string line;
	getline(cin, line);

	int numPlayers, highestMarble;
	sscanf(line.c_str(), "%d players; last marble is worth %d points", &numPlayers, &highestMarble);

	solve(numPlayers, highestMarble);
	solve(numPlayers, highestMarble*100);

	return 0;
}

