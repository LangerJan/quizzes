#include <cstdint>
#include <iostream>
#include <mutex>
#include <set>
#include <stdexcept>
#include <string>
#include <vector>

#include <boost/lexical_cast.hpp>

using namespace std;

int main(){

	int64_t result = 0;
	bool found = false;
	bool once = false;
	set<int64_t> results = {result};
	vector<int64_t> input;
	for(string line;getline(cin,line);){
		input.push_back(boost::lexical_cast<int64_t>(line));
	}
	
	while(!found){

		for(auto i : input){
			result += i;
			if(results.find(result) != results.end() && !found){
				found = true;
				cout << "Part 2: " << result << endl;
			}
			results.insert(result);
		}

		if(!once){
			cout << "Part 1: " << result << endl;
			once = true;
		}
	}

	return 0;
}
