#include <string>
#include <iostream>
#include <vector>
#include <algorithm>

#include <boost/algorithm/string.hpp>

using namespace std;

string react(string line){
	vector<string> reactionPairs;
	for(char ch='A';ch<='Z';ch++){
		char ch2 = ch + ('a'-'A');
		reactionPairs.push_back({ch,ch2});
		reactionPairs.push_back({ch2,ch});
	}

	bool reactionHappened;
	do{
		reactionHappened = false;
		for(string lookout : reactionPairs){
			if(boost::algorithm::contains(line, lookout)){
				boost::algorithm::replace_all(line,lookout,"");
				reactionHappened = true;
			}
		}
	} while(reactionHappened);

	return line;
}

string removeReact(string line, char poly){
	string upStr{::toupper(poly)};
	string lowStr{::tolower(poly)};
	boost::algorithm::replace_all(line, upStr, "");
	boost::algorithm::replace_all(line, lowStr, "");
	return react(line);
}

int main(){
	string line;
	if(getline(cin, line)){
		cout << "Part 1: " << react(line).size() << endl;

		vector<string> reactions;
		for(char c='a';c<='z';c++){
			string result = removeReact(line, c);
			reactions.push_back(removeReact(line, c));
		}
		auto smallest = min_element(reactions.begin(), reactions.end(), [](auto &arg0, auto &arg1){return arg0.size() < arg1.size();});

		cout << "Part 2: " << smallest->size() << endl;

	}

	return 0;
}

