#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <set>
#include <regex>
#include <unordered_map>
#include <map>
#include <tuple>
#include <boost/lexical_cast.hpp>
#include <vector>

using namespace std;

int main(){
	regex rex("#(\\d+) @ (\\d+),(\\d+): (\\d+)x(\\d+)");
	vector<tuple<int,int,int,int,int>> claims;
	map<tuple<int,int>, int> laps;

	/* 
	 * Read the input into
	 * * A set of tuple containing all information
	 * * A map of two-dimensional coordinates with an overlap counter
	 */
	for(string line;getline(cin, line);){
		smatch match;
		if(regex_match(line, match, rex)){
			int id = boost::lexical_cast<int>(match[1]);
			int posX = boost::lexical_cast<int>(match[2]);
			int posY = boost::lexical_cast<int>(match[3]);
			int width = boost::lexical_cast<int>(match[4]);
			int height = boost::lexical_cast<int>(match[5]);

			for(int x_offset=0;x_offset<width;x_offset++){
				for(int y_offset=0;y_offset<height;y_offset++){
					laps[{posX+x_offset, posY+y_offset}]++;
				}
			}
			claims.push_back({id, posX, posY, width, height});
		}
	}
	
	/*
	 * Count how many overlaping square inches we have
	 */
	cout << "Part 1: " 
	     << count_if(begin(laps), end(laps), [](auto pair){return pair.second > 1;})
	     << endl;

	/*
	 * Iterate over all claims, check every field for overlaps
	 */
	auto notOverlapped = find_if(claims.begin(), claims.end(), [&laps](auto &claim){
		auto [_,posX,posY,width,height] = claim;

		for(int x_offset=0;x_offset<width;x_offset++){
			for(int y_offset=0;y_offset<height;y_offset++){
				if(laps[{posX+x_offset, posY+y_offset}] != 1){
					return false;
				}
			}
		}
		return true;
	});

	cout << "Part 2: "
	     << get<0>(*notOverlapped) 
	     << endl;

	return 0;
}
