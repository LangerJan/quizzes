#include <iostream>
#include <algorithm>
#include <string>
#include <vector>

using namespace std;


bool differBy(string &arg0, string &arg1, int by){
	int diff = 0;
	for(int i=0;i<arg0.size();i++){
		if(diff > by) return false;

		if(i >= arg1.size()){
			diff++;
			continue;
		}

		if(arg0.at(i) != arg1.at(i)){
			diff++;
		}
	}

	return diff == by;
}


int main(){

	vector<string> input;
	for(string line;getline(cin, line);){
		input.push_back(line);
	}

	for(int i=0;i<input.size();i++){
		for(int j=i+1;j<input.size();j++){
			string &arg0 = input.at(i);
			string &arg1 = input.at(j);

			if(differBy(arg0,arg1,1)){
				for(int i=0;i<arg0.size();i++){
					if(arg0[i] == arg1[i]){
						cout << arg0[i];
					}
				}
				cout << endl;
				return 0;
			}
		}
	}
	return 0;
}
