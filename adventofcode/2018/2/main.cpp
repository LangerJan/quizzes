#include <algorithm>
#include <iostream>
#include <set>
#include <string>
#include <stdexcept>

using namespace std;

int main(){

	int num_twos = 0;
	int num_threes = 0;

	for(string line;getline(cin, line);){
		
		bool two = false;
		bool three = false;
		for(char ch='a';ch<='z';ch++){
			int items = count(line.begin(), line.end(), ch);
			if(items == 2) two = true;
			else if(items == 3) three = true;
		}

		if(two) num_twos++;
		if(three) num_threes++;
	}

	cout << num_twos * num_threes << endl;

	return 0;
}
