#include <algorithm>
#include <iostream>
#include <string>
#include <cstdint>
#include <map>
#include <tuple>
#include <set>
#include <regex>
#include <limits>
#include <numeric>

using namespace std;

tuple<set<int64_t>, map<tuple<bool,bool,bool,bool,bool>, bool>> readInput(){
	set<int64_t> alive;
	map<tuple<bool,bool,bool,bool,bool>, bool> rules;

	string line;
	{
		regex rex("initial state: ([#.]+)");
		smatch match;
		getline(cin, line);
		regex_match(line, match, rex);
		string state = match[1];
		for(int64_t i=0;i<(int64_t)state.size();i++){
			if(state[i] == '#') alive.insert(i);
		}
	}

	while(getline(cin, line)){
		if(line.empty()) continue;

		regex rex("([#.]{5}) => ([#.]{1})");
		smatch match;
		regex_match(line, match, rex);
		string kmatch = match[1];
		string vmatch = match[2];

		rules[{
			kmatch[0] == '#',
			kmatch[1] == '#',
			kmatch[2] == '#',
			kmatch[3] == '#',
			kmatch[4] == '#'
			}] = vmatch[0] == '#';
	}

	return {alive, rules};
}

set<int64_t> generate(set<int64_t> currState, map<tuple<bool,bool,bool,bool,bool>, bool> rules, int64_t generations){
	
	set<int64_t> nextState;

	for(int64_t i=0;i<generations;i++){
		if(i % 1000000 == 0){
			cout << i << " : " << currState.size() << endl;

			for(auto x : currState) cout << x << ", ";
			cout << endl;
		}

		int64_t alreadyWatchedMinPos = numeric_limits<int64_t>::min();

		for(auto alive : currState){
			for(int64_t pos = (-2+alive);pos<=(2+alive);pos++){
				if(pos < alreadyWatchedMinPos) continue;

				if(rules[{
					currState.find(pos-2) != currState.end(),
					currState.find(pos-1) != currState.end(),
					currState.find(pos-0) != currState.end(),
					currState.find(pos+1) != currState.end(),
					currState.find(pos+2) != currState.end()}]){
					nextState.insert(pos);
				}
				alreadyWatchedMinPos = pos;
			}
		}

		currState = nextState;
		nextState.clear();
	}

	return currState;
}


int main(){
	auto [startState, rules] = readInput();
	cout << startState.size() << "/" << rules.size() << endl;

	auto resultOne = generate(startState, rules, 20);
	cout << accumulate(resultOne.begin(), resultOne.end(), 0) << endl;

	auto resultTwo = generate(startState, rules, 50000000000);
	cout << accumulate(resultTwo.begin(), resultTwo.end(), 0) << endl;

	return 0;
}













