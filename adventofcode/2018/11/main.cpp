#include <algorithm>
#include <string>
#include <iostream>
#include <set>
#include <map>
#include <tuple>
#include <numeric>
#include <cstdint>

using namespace std;

static inline int determinePowerLevel(int x, int y, int serialNo){
	// Find the fuel cell's rack ID, which is its X coordinate plus 10.
	int rackID = x + 10;
	// Begin with a power level of the rack ID times the Y coordinate.
	int powerLevel = rackID * y;
	// Increase the power level by the value of the grid serial number
	powerLevel += serialNo;
	// Set the power level to itself multiplied by the rack ID.
	powerLevel *= rackID;
	// Keep only the hundreds digit of the power level 
	// numbers with no hundreds digit become 0
	if(powerLevel < 100) powerLevel = 0;
	else powerLevel = (powerLevel / 100)%10;
	// Subtract 5 from the power level.
	powerLevel -= 5;

	return powerLevel;
}

map<tuple<int, int>, int> readGrid(int serialNo){
	map<tuple<int,int>, int> result;

	for(int x=1;x<=300;x++){
		for(int y=1;y<=300;y++){
			result[{x,y}] = determinePowerLevel(x,y, serialNo);
		}
	}

	return result;
}


int determineGridPower(map<tuple<int,int>,int> &grid, int centerX, int centerY){
	return grid[{centerX-1, centerY-1}] +
	       grid[{centerX  , centerY-1}] +
	       grid[{centerX+1, centerY-1}] +
	       grid[{centerX-1, centerY  }] +
	       grid[{centerX  , centerY  }] +
	       grid[{centerX+1, centerY  }] +
	       grid[{centerX-1, centerY+1}] +
	       grid[{centerX  , centerY+1}] +
	       grid[{centerX+1, centerY+1}];
}

int determineBestPower(map<tuple<int,int>,int> &grid, int upLeftX, int upLeftY, int &best_n){
	int n = 1;

	int precomputedSum = 0;
	int currentBest = 0;

	while(!(upLeftX+n > 300 || upLeftY+n > 300)){
		int posX;
		int posY= upLeftY+(n-1);

		// Compute the last horizontal
		for(posX = upLeftX; posX < upLeftX+(n-1); posX++){
			precomputedSum += grid[{posX, posY}];
		}
		// And then go up
		for(posY=upLeftY+(n-1);posY>=upLeftY;posY--){
			precomputedSum += grid[{posX, posY}];
		}
		
		if(precomputedSum > currentBest){
			currentBest = precomputedSum;
			best_n = n;
		}


		n++;
	}

	return currentBest;
}



void solveTwo(int serialNo){
	auto grid = readGrid(serialNo);

	int bestX = -1, bestY = -1;
	int bestN = -1;
	int power = -1;


	for(int x=1;x<=300;x++){
		for(int y=1;y<=300;y++){
			int best_n_here = -1;
			int bestPowerHere = determineBestPower(grid, x, y, best_n_here);

			if(bestPowerHere > power){
				power = bestPowerHere;
				bestN = best_n_here;
				bestX = x;
				bestY = y;
			}

		}
	}

	cout << "Part 2: " << bestX << "," << bestY << "," << bestN << endl;
}

void solveOne(int serialNo){

	auto grid = readGrid(serialNo);

	int bestX = -1, bestY = -1;
	int power = -1;

	for(int centerX=2;centerX<=299;centerX++){
		for(int centerY=2;centerY<=299;centerY++){
			int thisPower = determineGridPower(grid, centerX, centerY);
			if(thisPower > power){
				power = thisPower;
				bestX = centerX;
				bestY = centerY;
			}
		}
	}

	cout << "Part 1: " << (bestX-1) << "," << (bestY-1) << endl;
}


int main(){

	solveOne(7400);
	solveTwo(7400);
	return 0;
}



