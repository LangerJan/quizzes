#include <SFML/Graphics.hpp>

#include <iostream>
#include <sstream>
#include <string>
#include <vector>

using namespace std;

struct star{
	sf::Vertex m_vertex;
	sf::Vector2f m_vel;

	void move(int times){
		this->m_vertex.position += m_vel*(float)times;
	}

	string toString(){
		stringstream ss;
		ss << m_vertex.position.x << "/" << m_vertex.position.y;
		return ss.str();
	}
};

vector<star> readInput(){
	vector<star> result;
	for(string line;getline(cin, line);){
		star it;
		sscanf(line.c_str(), "position=<%f,%f> velocity=<%f,%f>", &it.m_vertex.position.x, &it.m_vertex.position.y, &it.m_vel.x, &it.m_vel.y);
		result.push_back(it);
	}
	return result;
}


int main(void)
{
	auto stars = readInput();
	int currentSecond = 0;

	// create the window
	sf::RenderWindow window(sf::VideoMode(800, 600), "");
	window.setFramerateLimit(60);

	// run the program as long as the window is open
	while (window.isOpen())
	{
		// check all the window's events that were triggered since the last iteration of the loop
        	sf::Event event;
        	while (window.pollEvent(event))
        	{
			switch(event.type){
           			// "close requested" event: we close the window
            			case sf::Event::Closed:
 					window.close();
					break;
				case sf::Event::KeyPressed:
				{
					int movement = 0;
					if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))movement = -1;
					else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) movement = +1;
					else if (sf::Keyboard::isKeyPressed(sf::Keyboard::PageUp)) movement = -100;
					else if (sf::Keyboard::isKeyPressed(sf::Keyboard::PageDown)) movement = +100;
				
					for(auto &star : stars) star.move(movement);
					currentSecond += movement;
					string title = string("Pos of Star 1: ") + stars[0].toString() + ", " + string("Current second: ") + to_string(currentSecond);
					window.setTitle(title.c_str());
					break;
				}
			default:
				break;
		}
	}

        // clear the window with black color
        window.clear(sf::Color::Black);
        // draw everything here...
	for(auto &star : stars){
		window.draw(&star.m_vertex, 1, sf::Points);
	}
        // end the current frame
        window.display();
    }

    return 0;
}
