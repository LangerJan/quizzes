#include <algorithm>
#include <iostream>
#include <vector>
#include <cstdint>
#include <set>
#include <map>
#include <numeric>
#include <tuple>
#include <string>
#include <iterator>
#include <sstream>

using namespace std;

int readInput(){
	int input;
	string line;
	getline(cin, line);
	sscanf(line.c_str(), "%d", &input);
	return input;
}


void solveOne(int max_recepies){

	vector<int> recepies;

	int elf_one_idx = 0;
	int elf_two_idx = 1;

	int result = 37;

	do{
		// Split the result, if needed
		if(result > 9){
			int res_2 = result % 10;
			recepies.push_back((result / 10) % 10);
			recepies.push_back(res_2);
		} else {
			recepies.push_back(result % 10);
		}

		elf_one_idx = (elf_one_idx + 1 + recepies[elf_one_idx]) % recepies.size();
		elf_two_idx = (elf_two_idx + 1 + recepies[elf_two_idx]) % recepies.size();

		result = recepies[elf_one_idx] + recepies[elf_two_idx];

	}while(recepies.size() < max_recepies+10);


	for(int i=0;i<10;i++)
		cout << recepies[max_recepies+i];
	cout << endl;

}

bool checkSuffix(vector<int> &recepies, vector<int> &input){
	if(recepies.size() < input.size()) return false;

	for(int i=0;i<input.size();i++){
		if(recepies[recepies.size()-1-i] != input[input.size()-1-i]) return false;
	}

	return true;
}

void solveTwo(vector<int> input){

	vector<int> recepies;

	int elf_one_idx = 0;
	int elf_two_idx = 1;

	int result = 37;

	bool found = false;

	do{
		// Split the result, if needed
		if(result > 9){
			int res_2 = result % 10;
			recepies.push_back((result / 10) % 10);
			if(checkSuffix(recepies, input)){
				found = true;
				break;
			}
			recepies.push_back(res_2);
		} else {
			recepies.push_back(result % 10);
		}
		if(checkSuffix(recepies, input)){
			found = true;
			break;
		}

		elf_one_idx = (elf_one_idx + 1 + recepies[elf_one_idx]) % recepies.size();
		elf_two_idx = (elf_two_idx + 1 + recepies[elf_two_idx]) % recepies.size();

		result = recepies[elf_one_idx] + recepies[elf_two_idx];
	}while(!found);

	cout << "Appears after " << (recepies.size()-input.size()) << " recepies" << endl;

}

int main(){
	int input;
	string line;
	getline(cin, line);
	sscanf(line.c_str(), "%d", &input);

	solveOne(input);

	vector<int> search;
	for(char c : line){
		search.push_back(c-'0');
	}
	solveTwo(search);

	return 0;
}



