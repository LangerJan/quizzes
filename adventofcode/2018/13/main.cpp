#include <algorithm>
#include <string>
#include <iostream>
#include <numeric>
#include <climits>
#include <set>
#include <map>
#include <tuple>
#include <cstdint>
#include <vector>
#include <sstream>

using namespace std;

typedef tuple<int,int> position;
typedef tuple<int,int> direction;

tuple<int,int> 
	dir_up{0,-1},
	dir_down{0,1},
	dir_left{-1,0},
	dir_right{1,0};


typedef enum rotation{
	rotate_left,
	rotate_not,
	rotate_right
} rotation;



struct cart{
	int m_posX, m_posY;
	direction m_dir;
	bool collision = false;

	rotation m_nextRotation = rotate_left;

	void rotate(){
		switch(m_nextRotation){
			case rotate_left:
			{
				if(m_dir == dir_up) m_dir = dir_left;
				else if(m_dir == dir_down) m_dir = dir_right;
				else if(m_dir == dir_left) m_dir = dir_down;
				else m_dir = dir_up;

				m_nextRotation = rotate_not;
				break;
			}
			case rotate_right:
			{
				if(m_dir == dir_up) m_dir = dir_right;
				else if(m_dir == dir_down) m_dir = dir_left;
				else if(m_dir == dir_left) m_dir = dir_up;
				else m_dir = dir_down;

				m_nextRotation = rotate_left;
				break;
			}
			default:
			{
				m_nextRotation = rotate_right;
				break;
			}
		}
	}

};

tuple<vector<cart>, map<position, char>> readInput(){
	vector<cart> carts;
	map<position, char> tracks;

	int y=0;
	for(string line;getline(cin, line);){
		for(int x=0;x<(int)line.size();x++){
			
			// Read the tracks. Even those where a cart is on
			switch(line[x]){
				case 'v':
					tracks[{x,y}] = '|';
					carts.push_back({x,y,dir_down});
					break;
				case '^':
					tracks[{x,y}] = '|';
					carts.push_back({x,y,dir_up});
					break;
				case '>':
					tracks[{x,y}] = '-';
					carts.push_back({x,y,dir_right});
					break;
				case '<':
					tracks[{x,y}] = '-';
					carts.push_back({x,y,dir_left});
					break;
				default:
					tracks[{x,y}]  = line[x];
					break;
			}
		}
		y++;
	}

	return {carts, tracks};
}

void move(vector<cart> &carts, map<position, char> tracks, bool &collision, position &crashSite, bool remove=false){
	sort(carts.begin(), carts.end(), [](auto &cart1, auto &cart2){
			return (cart1.m_posY != cart2.m_posY ? cart1.m_posY < cart2.m_posY : cart1.m_posX < cart2.m_posX);}
	    );
	
	for(auto &cart : carts){
		if(cart.collision) continue;

		auto [dirX, dirY] = cart.m_dir;
		cart.m_posX += dirX;
		cart.m_posY += dirY;

		auto cnt = count_if(carts.begin(), carts.end(), [cart](auto &car){
				return car.m_posX == cart.m_posX && car.m_posY == cart.m_posY;});

		if(cnt == 2){
			collision = true;
			crashSite = {cart.m_posX, cart.m_posY};

			for(auto &car : carts){
				if(car.m_posX == cart.m_posX && car.m_posY == cart.m_posY){
					car.collision = true;
					cart.collision = true;
				}
			}
		}
		else
		{

			switch(tracks[{cart.m_posX, cart.m_posY}]){
				case '-':
					break;
				case '|':
					break;
				case '/':
					if(cart.m_dir == dir_down) cart.m_dir = dir_left;
					else if(cart.m_dir == dir_up) cart.m_dir = dir_right;
					else if(cart.m_dir == dir_right) cart.m_dir = dir_up;
					else cart.m_dir = dir_down;
					break;
				case '\\':
					if(cart.m_dir == dir_down) cart.m_dir = dir_right;
					else if(cart.m_dir == dir_up) cart.m_dir = dir_left;
					else if(cart.m_dir == dir_left) cart.m_dir = dir_up;
					else cart.m_dir = dir_down;
					break;
				case '+':
					cart.rotate();
					break;
				default:
					break;
			}

		}

	}

	if(remove){
		carts.erase(remove_if(carts.begin(), carts.end(), 
				[](auto &car){return car.collision;}),
				carts.end());
	}
}

#define HEIGHT 7
#define WIDTH 15

string toString(vector<cart> &carts, map<position, char> tracks){
	char output[HEIGHT][WIDTH];
	
	for(int y=0;y<HEIGHT;y++){
		for(int x = 0;x<WIDTH;x++){
			output[y][x] = ' ';
		}
		output[y][WIDTH-1] = '\0'; 
	}


	for(auto trk_pr : tracks){
		auto [x,y] = trk_pr.first;
		output[y][x] = trk_pr.second;
	}

	for(auto &cart : carts){
		char c = '?';
		if(cart.collision) c = 'X';
		else {
			if(cart.m_dir == dir_up) c = '^';
			else if(cart.m_dir == dir_down) c = 'v';
			else if(cart.m_dir == dir_left) c = '<';
			else if(cart.m_dir == dir_right) c = '>';
		}
		output[cart.m_posY][cart.m_posX] = c;
	}

	stringstream ss;

	for(int y=0;y<HEIGHT;y++){
		string line(output[y]);
		ss << line << endl;
	}
	return ss.str();

}

void solveOne(vector<cart> carts, map<position, char> tracks){
	bool collision = false;
	int moves = 0;
	position crashSite;
	do {
		moves++;
		move(carts, tracks, collision, crashSite);
	} while(!collision);
	auto [cx, cy] = crashSite;
	cout << "Part 1: " << cx << "," << cy << endl;

}

void solveTwo(vector<cart> carts, map<position, char> tracks){
	bool collision = false;
	position crashSite;

	int moves = 0;
	while(carts.size() > 1){
		moves++;
		move(carts, tracks, collision, crashSite, true);

		if(moves % 100 == 0){
			cout << moves << "\t" << carts.size() << endl;
		}
	}

	cout << carts[0].m_posX << "," << carts[0].m_posY << endl;
}

int main(){
	auto [carts, tracks] = readInput();

	cout << carts.size() << " carts " << endl;
	cout << tracks.size() << " tracks " << endl;

	solveOne(carts, tracks);
	solveTwo(carts, tracks);

	return 0;
}
