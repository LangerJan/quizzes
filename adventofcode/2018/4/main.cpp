#include <algorithm>
#include <iostream>
#include <string>
#include <regex>

#include <map>
#include <set>

using namespace std;


int main(){

	
	map<int, map<int,int>> guard_min_asleep;
	map<int, int> guard_totalAsleep;
	
	int currentGuard = -1;
	int currentFallsAsleep = -1;
	int currentWakesUp = -1;

	string line;



	while(getline(cin,line)){
		regex rex_start_shift("\\[\\d+-\\d+-\\d+ \\d+:\\d+\\] Guard #(\\d+) begins shift");
		regex rex_falls_asleep("\\[\\d+-\\d+-\\d+ \\d+:(\\d+)\\] falls asleep");
		regex rex_wakes_up("\\[\\d+-\\d+-\\d+ \\d+:(\\d+)\\] wakes up");
		smatch match;
		if(regex_match(line, match, rex_start_shift)){
			currentGuard = stoi(match[1]);
		}
		else if(regex_match(line, match, rex_falls_asleep)){
			currentFallsAsleep = stoi(match[1]);
		}
		else if(regex_match(line, match, rex_wakes_up)){
			currentWakesUp = stoi(match[1]);

			for(int i=currentFallsAsleep;i<currentWakesUp;i++){
				guard_min_asleep[currentGuard][i]++;
			}
			guard_totalAsleep[currentGuard] += currentWakesUp - currentFallsAsleep;
		}
	}

	
	auto guard_it = std::max_element(guard_totalAsleep.begin(), guard_totalAsleep.end(),
    			[](const pair<int, int>& p1, const pair<int, int>& p2) {
        			return p1.second < p2.second; });

	cout << "Guard " << guard_it->first << " has the been mostly asleep" << endl;

	auto &minute_map = guard_min_asleep[guard_it->first];

	auto worst_minute = std::max_element(minute_map.begin(), minute_map.end(),
    			[](const pair<int, int>& p1, const pair<int, int>& p2) {
        			return p1.second < p2.second; });

	cout << "Worst at minute " << worst_minute->first << " " << worst_minute->second << " times " << endl;

	cout << "Part 1: " << (guard_it->first * worst_minute->first) << endl;


	int worst_p2_guard = -1;
	int worst_p2_minute = -1;
	int worst_p2_times_asleep = -1;

	for(auto &guard_pr : guard_min_asleep){

		auto worst_minute_pr = std::max_element(guard_pr.second.begin(), guard_pr.second.end(),
    				[](const pair<int, int>& p1, const pair<int, int>& p2) {
        				return p1.second < p2.second; });

		if(worst_minute_pr->second > worst_p2_times_asleep){
			worst_p2_guard = guard_pr.first;
			worst_p2_minute = worst_minute_pr->first;
			worst_p2_times_asleep = worst_minute_pr->second;
		}
	}

	cout << "Guard " << worst_p2_guard << " was worst at minute " << worst_p2_minute << " with " << worst_p2_times_asleep << " times" << endl;
	cout << "Part 2: " << (worst_p2_guard*worst_p2_minute) << endl;

	return 0;
}


