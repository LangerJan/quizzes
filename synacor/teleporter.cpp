



#include <vector>
#include <iostream>
#include <deque>
#include <cstdint>
#include <tuple>
#include <map>
#include <limits>
#include <shared_mutex>
#include <mutex>
#include <thread>
#include <cstdlib>

#include <boost/lexical_cast.hpp>

using namespace std;

/**
 * Pseudo-Assembly from challenge.bin
	6027:JT IF REG[0] != 0 JUMP TO VAL[6035]
	6030:ADD REG[0] = REG[1] + VAL[1]
	6034:RET
	6035:JT IF REG[1] != 0 JUMP TO VAL[6048]
	6038:ADD REG[0] = REG[0] + VAL[32767]
	6042:SET REG[1] TO REG[7]
	6045:CALL VAL[6027]
	6047:RET
	6048:PUSH REG[0]
	6050:ADD REG[1] = REG[1] + VAL[32767]
	6054:CALL VAL[6027]
	6056:SET REG[1] TO REG[0]
	6059:POP REG[0]
	6061:ADD REG[0] = REG[0] + VAL[32767]
	6065:CALL VAL[6027]
	6067:RET
 */


/**
 * First translation to c++
 */
std::deque<uint16_t> stack;
uint16_t reg[8];
void fuckery(){
	if(reg[0] == 0){
		reg[0] = (reg[1] + 1) % 32768;
		return;
	}
	else if(reg[1] == 0)
	{
		reg[0] = (reg[0] + 32767) % 32768;
		reg[1] = reg[7];
		fuckery();
		return;
	}
	else{
		stack.push_back(reg[0]);
		reg[1]--;
		fuckery();
		reg[1] = reg[0];
		reg[0] = stack.back();
		stack.pop_back();
		reg[0] = (reg[0] + 32767) % 32768;
		fuckery();
		return;
	}

}

/**
 * First optimization round
 */
void fuckery_it(uint16_t &reg0, uint16_t &reg1, uint16_t &reg7){
	if(reg0 == 0){
		reg0 = (reg1 + 1) % 32768;
		return;
	}
	else if(reg1 == 0)
	{
		reg0 = (reg0 + 32767) % 32768;
		reg1 = reg7;
		fuckery_it(reg0, reg1, reg7);
		return;
	}
	else{
		uint16_t tmp = reg0;
		reg1--;
		fuckery_it(reg0, reg1, reg7);
		reg1 = reg0;
		reg0 = tmp;
		reg0 = (reg0 + 32767) % 32768;
		fuckery_it(reg0, reg1, reg7);
		return;
	}
}


/**
 * Second optimization round
 */
tuple<uint16_t, uint16_t> fuckery_it2(uint16_t reg0, uint16_t reg1, uint16_t reg7){

	if(reg0 == 0){
		reg0 = (reg1 + 1) % 32768;
		return {reg0, reg1};
	}
	else if(reg1 == 0)
	{
		reg0 = (reg0 + 32767) % 32768;
		reg1 = reg7;
		return fuckery_it2(reg0, reg1, reg7);
	}
	else{
		auto result = fuckery_it2(reg0, reg1-1, reg7);
		reg1 = get<0>(result);
		return fuckery_it2(reg0-1, reg1, reg7);
	}
}

typedef std::tuple<uint16_t, uint16_t, uint16_t> inputTuple;
typedef std::tuple<uint16_t, uint16_t> outputTuple;

/**
 * Final optimization by memorizing results
 */
outputTuple fuckery_it3(inputTuple args, map<inputTuple, outputTuple> &results){
	
	auto found_it = results.find(args);
	if(found_it != results.end()){
		return found_it->second;
	}

	uint16_t reg0 = std::get<0>(args), reg1 = std::get<1>(args), reg7 = std::get<2>(args);

	if(reg0 == 0){
		reg0 = (reg1 + 1) % 32768;
		outputTuple result = {reg0, reg1};
		results[args] = result;
		return result;
	}
	else if(reg1 == 0)
	{
		reg0 = (reg0 + 32767) % 32768;
		inputTuple newInput{reg0, reg7, reg7};
		outputTuple result = fuckery_it3(newInput, results);
		results[newInput] = result;
		results[args] = result;
		return result;
	}
	else{
		inputTuple newInput{reg0, reg1-1, reg7};
		auto result = fuckery_it3(newInput, results);
		results[newInput] = result;
		reg1 = get<0>(result);

		result = fuckery_it3({reg0-1, reg1, reg7}, results);
		results[args] = result;
		return result;
	}
}



void test(uint16_t reg0, uint16_t reg1, uint16_t reg7){
	reg[0] = reg0;
	reg[1] = reg1;
	reg[7] = reg7;
	fuckery();
	std::cout << reg[0] << "\t" << reg[1] << "\t" << reg[7] << std::endl;
}

void test2(uint16_t reg0, uint16_t reg1, uint16_t reg7){
	fuckery_it(reg0, reg1, reg7);
	std::cout << reg0 << "\t" << reg1 << "\t" << reg7 << std::endl;
}

void test3(uint16_t reg0, uint16_t reg1, uint16_t reg7){
	auto res = fuckery_it2(reg0, reg1, reg7);
	std::cout << std::get<0>(res) << "\t" << std::get<1>(res) << "\t" << reg7 << std::endl;
}

void solveFromTo(uint16_t start, uint16_t step){
	std::mutex cout_mutex;


	for(uint16_t i = start;i<=32768;i=i+step){
		std::map<inputTuple, outputTuple> map;
		auto res = fuckery_it3({4,1,i}, map);

		{
			std::lock_guard<std::mutex> lock(cout_mutex);
			std::cout << "f(4,1," << i << ")\t" << std::get<0>(res) << "\t" << std::get<1>(res) << "\t" << i << std::endl;
		}

		if(std::get<0>(res) == 6){
			{
				std::lock_guard<std::mutex> lock(cout_mutex);
				std::cout << "FOUND IT: " << i << std::endl;
			}
			exit(EXIT_SUCCESS);
			break;
		}
	}
}

void solve(int numthreads){
	
	std::vector<thread*> threads;
	
	for(int i=0;i<numthreads;i++){
		thread *thr = new thread(solveFromTo, i+10000, numthreads);
		threads.push_back(thr);
	}

	for(auto thr : threads){
		thr->join();
	}


}




int main(int, char* argv[]){
	int threads = boost::lexical_cast<uint16_t>(argv[1]);
	solve(threads);
	return 0;
}

