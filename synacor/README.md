# My run on the Synacor Challenge

This describes my approach to the synacor challenge. It's completely solved in C++.

It is not indented to be the fastest emulation. I aimed for

1. no casting whatsoever
2. maximum stability

## 1. The Virtual Machine

So, it's a VM. Seen one, seen'em all. There are a few design decisions I want to point out here:

It's got separate fields for stack, memory and registers. All numbers are **uint16_t**, which corresponds nicely with the spec. Why not **int16_t**? Well, you can use that too, but a) the abstract machine has unsigned numbers only and b) I wanted to avoid to run into a signed integer overflow.

Opcodes are defined as uint16_t constants. The corresponding concepts are not separate Instruction-Classes, but different methods in the CPU-class. A map translates from the constant to the method:

**std::map<uint16_t, std::function<void()>>**

This is, obviously, not the fastest way of emulating a machine. But with checking the opcodes in an extra map gives me a well-defined point to check for invalid opcodes.

### Bottom line

Nothing beats the feeling of the first time seeing the _challenge.bin_ running! I absolutely loved it.

Eric, if you read this: Thank you! And since you wanted to hear ideas how to improve it:
* Write explicitly that Jump-Instructions are absolute, not relative adresses. 

## 2. Up until the coin riddle

The text adventure is really nice. I had a bit of a hard time getting through the maze, how to get a lit lantern, but it worked out well. The coin riddle is pretty much straightforward, as is it's solution.

My solution is in [coins.cpp](./coins.cpp). It basically tries all combinations.

## 3. Teleport!

Arriving at Synacor Headquarters, the strange book was a nice read :D I'm not bragging about how I solved this in a rush. In fact, I run into some dead-ends:

### Improving the VM with a debugger

So, it became clear that I need a debugging mechanism here. Just as **gdb**, ctrl+c (SIGINT) will now break out into a simple command line, letting me fiddle with the VM.

### Try out all values of register 7

So, the first thing I tried was:

* Set register 7 to 1. 
* Run into assumed fail-state, mark this as a program counter I don't want to be
* Roll back
* Increment register
* If I run into the fail-sate, roll back again...

I thought, that it would suffice to find the correct register 7 and the code would then lead me somewhere else. Bad idea. 

### Improving the VM with listing-output

It became clear that I need to have a look at the binary. This is where I had to hack a bit: If I went fully object-oriented with Instructions here I might have had an easier time here, but anyway...

As I skimmed through the binary, some of the strings were easily decoded, but the later ones are somehow encoded. There seems to be a print function at PC 1841 and a decode-print function at PC 1841.

### Have a look at the listing

So, if you decide to hack register 7 to something else than 0, the following happens

```
5451:JF IF REG[7] == 0 JUMP TO VAL[5605] // Down the rabbit hole we go
5454:PUSH REG[0]                         // Save registers, before print-call
5456:PUSH REG[1]                         // --"-- 
5458:PUSH REG[2]                         // --"--
5460:SET REG[0] TO VAL[28844]            // Prepare print-call
5463:SET REG[1] TO VAL[1531]             // Prepare print-call
5466:ADD REG[2] = VAL[1649] + VAL[3590]  // Prepare print-call
5470:CALL VAL[1458]  										 // Print something
5472:POP REG[2]                          // Restore registers
5474:POP REG[1]                          // --"--
5476:POP REG[0]                          // --"--
5478:NOOP                                // A NOOP-Slide? I suppose Erik 
5479:NOOP                                // did something here and it left a gap
5480:NOOP                                // because he didn't want to change all jump-addresses
5481:NOOP
5482:NOOP
5483:SET REG[0] TO VAL[4]                 
5486:SET REG[1] TO VAL[1]
5489:CALL VAL[6027]												// Call 6027 with 4,1 and REG[7]
5491:EQ REG[1] IS 1 IF REG[0] == VAL[6]   // Is Register[0] == 6?
5495:JF IF REG[1] == 0 JUMP TO VAL[5579]  // If not, jump somewhere else
5498:PUSH REG[0]                          // Save registers, before print-call 
5500:PUSH REG[1]                          // --"-- 
5502:PUSH REG[2]                          // --"--
5504:SET REG[0] TO VAL[29014]             // Prepare print-call
5507:SET REG[1] TO VAL[1531]              // --"--
5510:ADD REG[2] = VAL[17296] + VAL[11112] // Prepare print-call
5514:CALL VAL[1458]                       // Print something
5516:POP REG[2]                           // Restore registers
5518:POP REG[1]                           // --"--
5520:POP REG[0]                           // --"--
5522:SET REG[0] TO REG[7]                 // Prepare decode-print function with Register 7
5525:SET REG[1] TO VAL[25866]
5528:SET REG[2] TO VAL[32767]
5531:PUSH REG[3]
5533:SET REG[3] TO VAL[29241]
5536:CALL VAL[1841]                       // Decode Print something
```

```
6027:JT IF REG[0] != 0 JUMP TO VAL[6035]
6030:ADD REG[0] = REG[1] + VAL[1]
6034:RET
6035:JT IF REG[1] != 0 JUMP TO VAL[6048]
6038:ADD REG[0] = REG[0] + VAL[32767]
6042:SET REG[1] TO REG[7]
6045:CALL VAL[6027]
6047:RET
6048:PUSH REG[0]
6050:ADD REG[1] = REG[1] + VAL[32767]
6054:CALL VAL[6027]
6056:SET REG[1] TO REG[0]
6059:POP REG[0]
6061:ADD REG[0] = REG[0] + VAL[32767]
6065:CALL VAL[6027]
6067:RET
```

So, there is a function being called, never to return. And after that, the result register 1 is being checked if it has a value of 6. So why not skip it entirely and set reg[1]=6 and go on with your life? Because the achievement-code you're going to get will be wrong: At PC 5522 the code will be decrypted with whatever you gave at register 7. Dang!

So, what do we have here?

A function f(4,1,x) = 6, and we need to find a suitable x for the function to return 6. But you cannot just try every possible value, since this is a modified **Ackermann-Function**. The program is desgined, as promised, not to return for a billion years, regardless of the input. So we need to improve it. I basically went with writing it in C++, optimize it, memorize results and parallelize it heavily. You can find it in [teleporter.cpp](./teleporter.cpp).

### Bottom line

The great thing about this part is, that you cannot cheat it. The challenge-code is encrypted, and you cannot simply try out every code since you have no idea how the plaintext looks like. Now that I think of it, maybe one could rule out some wrong values because the output wouldnt be valid ASCII characters any more, hmm...

## 3. The Vault

Whew, what could possibly follow after a riddle like that? A nice riddle about how to move in a vault in order to fulfill a certain formula.

I tried to make it fancy, using only tuples and c++ **< functional>** objects. Even though tuples help a lot (instantly usable as keys in sets and maps) it gets hardly readable fast. A breadth-search over all possible moves leads to the solution here. You can find it in [vaultSolver.cpp](./vaultSolver.cpp).

### Bottom line

While calulating the solution I stumbled over a way reaching the desired orb-value by walking to the final room two times. Alas, this is not allowed in the game. Shame.


## 4. Conclusion

Again, thank you for this brilliant coding challenge! I had a wonderful time solving it. It was never unfair, I can tell that you put a lot of effort into it to make it water-proof.
