/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   opcodes.h
 * Author: basis
 *
 * Created on 5. Dezember 2017, 13:00
 */

#ifndef OPCODES_H
#define OPCODES_H

namespace opcode{
    const uint16_t HALT = 0,
        SET = 1,
        PUSH = 2,
        POP = 3,
        EQ = 4,
        GT = 5,
        JMP = 6,
        JT = 7,
        JF = 8,
        ADD = 9,
        MULT = 10,
        MOD = 11,
        AND = 12,
        OR = 13,
        NOT = 14,
        RMEM = 15,
        WMEM = 16,
        CALL = 17,
        RET = 18,
        OUT = 19,
        IN = 20,
        NOOP = 21;
}


#endif /* OPCODES_H */

