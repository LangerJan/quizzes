#include <csignal>
#include <cstdlib>

#include "CPU.h"

using namespace std;


CPU g_cpu;

void signal_handler(int)
{
    g_cpu.debug_interrupt();
}

/*
 * 
 */
int main(int argc, char** argv) {
    std::signal(SIGINT, signal_handler);
    
    g_cpu.load(std::string(argv[1]));
    g_cpu.run();
    
    return 0;
}

