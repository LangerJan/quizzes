#include "CPU.h"

#include <functional>
#include <iostream>
#include <iterator>
#include <fstream>
#include <vector>
#include <cstdint>
#include <stdexcept>
#include <sstream>
#include <cstdint>

#include <boost/numeric/conversion/cast.hpp> 
#include <boost/algorithm/string.hpp>

CPU::CPU() 
{
}

CPU::CPU(const CPU& orig) {
}

CPU::~CPU() {
}

void CPU::load(std::string file){
    std::ifstream in(file, ::std::ios::binary);
    in.read(reinterpret_cast<char*>(this->m_memory.data()), this->m_memory.size()*sizeof(uint16_t));
}

void CPU::run(){
    this->m_debug_interrupted = false;
    this->m_halted = false;
    this->m_cheatMode = false;
    this->m_verboseMode = false;
    
    bool reRun;
    
    do{
        reRun = false;
        
        while(!this->m_halted && !this->m_debug_interrupted){
            
            this->step();
            
            if(this->m_cheatMode_pc_failurePoint == this->m_programCounter){
                if(!this->m_cheatMode){
                    std::cout << "fail point found" << std::endl;
                    this->m_debug_interrupted = true;                    
                }
                else
                {
                    int regVal = this->m_registers[7];
                    this->popState();
                    this->pushState();
                    
                    this->m_registers[7] = (regVal+1) % 32768;
                    std::cout << "increased register 7 to " << this->m_registers[7] << std::endl;
                }
            }
            
            if(this->m_breakPoints.find(this->m_programCounter) != this->m_breakPoints.end()){
                std::cout << "Break at " << this->m_programCounter << std::endl;
                reRun = this->debugDialogue();
                break;
            }
            
        }

        if(this->m_debug_interrupted){
            reRun = this->debugDialogue();
        }        
    }while(reRun);
}

bool CPU::debugDialogue(){
    for(std::string line;std::getline(std::cin, line);){

        if(boost::equals(line, "continue")){
            std::cout << "continuing..." << std::endl;
            this->m_debug_interrupted = false;
            return true;
        }
        else if(boost::equals(line, "quit")){
            std::cout << "quitting..." << std::endl;
            return false;
        }
        else if(boost::equals(line, "show reg")){
            this->printRegisters();
        }
        else if(boost::equals(line, "show stack")){
            this->printStack();
        }
        else if(boost::equals(line, "show pc")){
            std::cout << this->m_programCounter << std::endl;
        }
        else if(boost::equals(line, "push state")){
            this->pushState();
        }
        else if(boost::equals(line, "pop state")){
            this->popState();
        }
        else if(boost::equals(line, "set reg")){
            std::cout << "reg:";
            std::cout.flush();
            uint16_t reg;
            std::cin >> reg;
            std::cout << "val:";
            uint16_t reg_val;
            std::cin >> reg_val;
            
            if(reg >= 0 && reg <= 7){
                if(this->isNumerical(reg_val)){
                    this->m_registers[reg] = reg_val;
                    std::cout << "set " << reg << " to " << reg_val << std::endl;            
                }
            }
        }
        else if(boost::equals(line, "set failurepoint")){
            this->m_cheatMode_pc_failurePoint = this->m_programCounter;
            std::cout << "set failurepoint at " << this->m_programCounter << std::endl;
        }
        else if(boost::equals(line, "set input")){
            std::cout << "input:";
            std::getline(std::cin, this->m_cheatMode_input);
            std::cout << "set permanent output to '" << this->m_cheatMode_input << "'" << std::endl;
        }
        else if(boost::equals(line, "cheatmode on")){
            this->m_cheatMode = true;
            std::cout << "cheatmode activated" << std::endl;
        }
        else if(boost::equals(line, "cheatmode off")){
            this->m_cheatMode = false;
            std::cout << "cheatmode deactivated" << std::endl;
        }
        else if(boost::equals(line, "verbose on")){
            this->m_verboseMode = true;
            std::cout << "verbose mode activated" << std::endl;            
        }
        else if(boost::equals(line, "verbose off")){
            this->m_verboseMode = false;
            std::cout << "verbose mode deactivated" << std::endl;            
        }
        else if(boost::equals(line, "listing")){
            this->printListing();
        }
        else if(boost::equals(line, "break")){
            std::cout << "at pc:";
            std::cout.flush();
            uint16_t bp;
            std::cin >> bp;
            this->m_breakPoints.insert(bp);
            std::cout << "Breakpoint set" << std::endl;
        }
        else if(boost::equals(line, "jump")){
            std::cout << "to pc:";
            std::cout.flush();
            uint16_t bp;
            std::cin >> bp;
            this->m_programCounter = bp;
            std::cout << "Done" << std::endl;
        }
        else if(boost::equals(line, "store dump")){
            this->saveDump("core.dump");
        }
        else if(boost::equals(line, "load dump")){
            this->loadDump("core.dump");
        }
        else
        {
            std::cout << "unknown command" << std::endl;
        }
        
        
    }    
    return false;
}

void CPU::pushState(){
    this->m_state_stack.push_back({this->m_programCounter, this->m_registers, this->m_stack, this->m_memory});
    std::cout << "state pushed" << std::endl;
}

void CPU::popState(){
    if(!this->m_state_stack.empty())
    {
        auto state = this->m_state_stack.back();
        this->m_state_stack.pop_back();

        this->m_programCounter = std::get<0>(state);
        this->m_registers = std::get<1>(state);
        this->m_stack = std::get<2>(state);
        this->m_memory = std::get<3>(state);
        std::cout << "state popped" << std::endl;
    }
}


void CPU::printRegisters(){
    for(auto reg : this->m_registers){
        std::cout << "->" << reg << std::endl;
    }
}

void CPU::printStack(){
    if(this->m_stack.empty()){
        std::cout << "Stack is empty" << std::endl;
    }
    else
    {
        for(auto elem : this->m_stack){
            std::cout << elem << " " << std::endl;
        }        
    }
}

void CPU::loadDump(std::string file){
    std::ifstream dumpfile(file);
    if(dumpfile.is_open()){
        this->m_stack.clear();
        dumpfile >> this->m_programCounter;
        for(int i=0;i<this->m_registers.size();i++){
            dumpfile >> this->m_registers[i];
        }
        for(int i=0;i<this->m_memory.size();i++){
            dumpfile >> this->m_memory.at(i);
        }
        while(dumpfile.good()){
            uint16_t elem;
            dumpfile >> elem;
            this->m_stack.push_back(elem);
        }
        dumpfile.close();
        std::cout << "Loading dump complete" << std::endl;
    }
    else
    {
        std::cout << "Unable to load dump" << std::endl;
    }
    
    
}

void CPU::saveDump(std::string file){
    std::ofstream dumpfile(file);
    if(dumpfile.is_open()){
        dumpfile << this->m_programCounter << " ";
        for(int i=0;i<this->m_registers.size();i++){
            dumpfile << this->m_registers[i] << " ";
        }
        for(int i=0;i<this->m_memory.size();i++){
            dumpfile << this->m_memory.at(i) << " ";
        }
        for(int i=0;i<this->m_stack.size();i++){
            dumpfile << this->m_stack.at(i);
            if(i != this->m_stack.size()-1){
                dumpfile << " ";
            }
        }
        dumpfile.close();
        std::cout << "Dump complete" << std::endl;
    }
    else
    {
        std::cout << "Unable to dump" << std::endl;
    }
}


void CPU::step(){
    if(this->m_verboseMode){
        std::cout << this->m_programCounter << ": ";
        std::cout.flush();
    }

    uint16_t opcode = this->pcFetch();
    std::function<void()> fkt = this->instructionDecode(opcode);
    fkt();
}

void CPU::debug_interrupt(){
    this->m_debug_interrupted = true;
}


uint16_t CPU::pcFetch(){
    return this->pcFetch(this->m_programCounter);
}

uint16_t CPU::pcFetch(uint16_t &pc){
    if(pc >= 0 && pc < this->m_memory.size()){
        return this->m_memory[pc++];
    }
    else
    {
        throw std::runtime_error("PC out of bounds");
    }    
}

uint16_t CPU::numFetch(){
    return numFetch(this->m_programCounter);
}

uint16_t CPU::numFetch(uint16_t &pc){
    uint16_t a = this->pcFetch(pc);
    uint16_t result;
    if(this->isRegister(a)){
        if(this->m_verboseMode){
            std::cout << "FETCH FROM REGISTER " << numToRegister(a) << std::endl;            
        }
        
        result = this->m_registers[numToRegister(a)];
    }
    else if(this->isNumerical(a))
    {
        result = a;
    }
    else 
    {
        std::stringstream ss_err;
        ss_err << "Illegal numerical value: " << a;
        throw std::runtime_error(ss_err.str());
    }
    return result;
}


uint16_t CPU::numToRegister(uint16_t num){
    if(isRegister(num)){
        return num-32768;
    }
    else
    {
        std::stringstream ss_err;
        ss_err << "Illegal register access: " << num;
        throw std::runtime_error(ss_err.str());
    }
}

bool CPU::isRegister(uint16_t num){
    return (num >= 32768 && num <= 32775);
}

bool CPU::isNumerical(uint16_t num){
    return (num >= 0 && num <= 32767);
}

uint16_t CPU::getNextInput(){
    if(this->m_input.empty()){
        std::string line;
        
        if(this->m_cheatMode_input.empty()){
            if(std::cin.fail()){
                std::cin.clear();
                std::cin.ignore();
            }
            std::getline(std::cin, line);
        }
        else
        {
            line = this->m_cheatMode_input;
        }
        
        line += std::string("\n");
        for(auto c : line){
            this->m_input.push_back(c);
        }
    }

    uint16_t result = this->m_input.front();
    this->m_input.pop_front();
    return result;
}


std::function<void()> CPU::instructionDecode(uint16_t opcode){
    auto fkt_it = this->m_opsmap.find(opcode);
    if(fkt_it == this->m_opsmap.end()){
        std::stringstream ss_err;
        ss_err << "Illegal opcode at PC: " << (this->m_programCounter-1) << " : " << opcode;
        throw std::runtime_error(ss_err.str());
    }
    return fkt_it->second;
}



void CPU::execHALT(){
    this->m_halted = true;

    if(this->m_verboseMode){
        std::cout << "HALT" << std::endl;
    }

}
void CPU::execSET(){
    uint16_t register_a = numToRegister(this->pcFetch());
    this->m_registers[register_a] = this->numFetch();

    if(this->m_verboseMode){
        std::cout << "SET " << register_a << " -> " << "REG[" << register_a << "]" << this->m_registers[register_a] << std::endl;
    }
}
void CPU::execPUSH(){
    this->m_stack.push_back(this->numFetch());
    if(this->m_verboseMode){
        std::cout << "PUSH " << this->m_stack.back() << std::endl;
    }
}
void CPU::execPOP(){
    uint16_t register_a = numToRegister(this->pcFetch());
    
    if(this->m_stack.empty()){
        throw std::runtime_error("POP: empty stack");
    }
    else
    {
        uint16_t val = this->m_stack.back();
        this->m_stack.pop_back();
        this->m_registers[register_a] = val;
        
        if(this->m_verboseMode){
            std::cout << "POP " << "REG[" << register_a << "]" << val << std::endl;
        }        
    }
}
void CPU::execEQ(){
    uint16_t register_a = numToRegister(this->pcFetch());

    uint16_t num_b = this->numFetch();
    uint16_t num_c = this->numFetch();
    
    this->m_registers[register_a] = (num_b == num_c)?1:0;

    if(this->m_verboseMode){
        std::cout << "EQ " << num_b << "==" << num_c << "?" << "REG[" << register_a << "]" << this->m_registers[register_a] << std::endl;
    }        
}
void CPU::execGT(){
    uint16_t register_a = numToRegister(this->pcFetch());

    uint16_t num_b = this->numFetch();
    uint16_t num_c = this->numFetch();
    
    this->m_registers[register_a] = (num_b > num_c)?1:0;    
    if(this->m_verboseMode){
        std::cout << "GT " << num_b << ">" << num_c << "?" << "REG[" << register_a << "]" << this->m_registers[register_a] << std::endl;
    }        
}
void CPU::execJMP(){
    this->m_programCounter = this->numFetch();
    if(this->m_verboseMode){
        std::cout << "JMP " << this->m_programCounter << std::endl;
    }        
}
void CPU::execJT(){
    uint16_t num_a = this->numFetch();
    uint16_t num_b = this->numFetch();
    this->m_programCounter = num_a!=0?num_b:this->m_programCounter;
    if(this->m_verboseMode){
        std::cout << "JT " << num_a << "!= 0" << "?" << "->" << this->m_programCounter << std::endl;
    }        
}
void CPU::execJF(){
    uint16_t num_a = this->numFetch();
    uint16_t num_b = this->numFetch();
    this->m_programCounter = num_a==0?num_b:this->m_programCounter;    
    if(this->m_verboseMode){
        std::cout << "JF " << num_a << "== 0" << "?" << "->" << num_b << "->" << this->m_programCounter << std::endl;
    }        
}
void CPU::execADD(){
    uint16_t register_a = numToRegister(this->pcFetch());
    uint16_t num_b = this->numFetch();
    uint16_t num_c = this->numFetch();
    
    this->m_registers[register_a] = (num_b + num_c) % 32768;
    if(this->m_verboseMode){
        std::cout << "ADD " << num_b << "+" << num_c << "=" << "REG[" << register_a << "]" << this->m_registers[register_a] << std::endl;
    }        
}
void CPU::execMULT(){
    uint16_t register_a = numToRegister(this->pcFetch());
    uint16_t num_b = this->numFetch();
    uint16_t num_c = this->numFetch();
    
    this->m_registers[register_a] = (num_b * num_c) % 32768;    
    if(this->m_verboseMode){
        std::cout << "MULT " << num_b << "*" << num_c << "=" << "REG[" << register_a << "]" << this->m_registers[register_a] << std::endl;
    }        
}
void CPU::execMOD(){
    uint16_t register_a = numToRegister(this->pcFetch());
    uint16_t num_b = this->numFetch();
    uint16_t num_c = this->numFetch();
    
    this->m_registers[register_a] = (num_b % num_c);
    if(this->m_verboseMode){
        std::cout << "MOD " << num_b << "%" << num_c << "=" << "REG[" << register_a << "]" << this->m_registers[register_a] << std::endl;
    }
}
void CPU::execAND(){
    uint16_t register_a = numToRegister(this->pcFetch());
    uint16_t num_b = this->numFetch();
    uint16_t num_c = this->numFetch();
    
    this->m_registers[register_a] = (num_b & num_c);    
    if(this->m_verboseMode){
        std::cout << "AND " << num_b << "&" << num_c << "=" << "REG[" << register_a << "]" << this->m_registers[register_a] << std::endl;
    }        
}
void CPU::execOR(){
    uint16_t register_a = numToRegister(this->pcFetch());
    uint16_t num_b = this->numFetch();
    uint16_t num_c = this->numFetch();
    
    this->m_registers[register_a] = (num_b | num_c);        
    if(this->m_verboseMode){
        std::cout << "OR " << num_b << "|" << num_c << "=" << "REG[" << register_a << "]" << this->m_registers[register_a] << std::endl;
    }        
}
void CPU::execNOT(){
    uint16_t register_a = numToRegister(this->pcFetch());
    uint16_t num_b = this->numFetch();
    
    this->m_registers[register_a] = num_b ^ 32767;        
    if(this->m_verboseMode){
        std::cout << "NOT " << num_b << "=" << "REG[" << register_a << "]" << this->m_registers[register_a] << std::endl;
    }            
}
void CPU::execRMEM(){
    uint16_t register_a = numToRegister(this->pcFetch());
    uint16_t num_b = this->numFetch();

    this->m_registers[register_a] = this->m_memory[num_b];
    if(this->m_verboseMode){
        std::cout << "RMEM FROM " << num_b << "(=" << this->m_memory[num_b] << ")"
                  << "TO " << register_a << "(=" << this->m_registers[register_a] << ")" << std::endl;
    }
}
void CPU::execWMEM(){
    uint16_t num_a = this->numFetch();
    uint16_t num_b = this->numFetch();

    this->m_memory[num_a] = num_b;
    if(this->m_verboseMode){
        std::cout << "WMEM " << num_b << " INTO " << num_a << std::endl;
    }            
}
void CPU::execCALL(){
    uint16_t num_a = this->numFetch();

    this->m_stack.push_back(this->m_programCounter);
    this->m_programCounter = num_a;
    if(this->m_verboseMode){
        std::cout << "CALL " << num_a << std::endl;
    }            
}
void CPU::execRET(){
    
    if(this->m_stack.empty()){
        this->m_halted = true;
    }
    else
    {
        uint16_t top = this->m_stack.back();
        this->m_stack.pop_back();
        this->m_programCounter = top;
        if(this->m_verboseMode){
            std::cout << "RET " << this->m_programCounter << std::endl;
        }            
    }
}
void CPU::execOUT(){
    uint16_t a = this->numFetch();
    char arg;
    
    try{
        arg = boost::numeric_cast<char>(a);
    }
    catch(boost::numeric::bad_numeric_cast &ex){
        std::stringstream ss_err;
        ss_err << "Bad numeric cast of " << a << " at " << (this->m_programCounter-1);
        throw std::runtime_error(ss_err.str());
    }
    
    if(this->m_verboseMode){
        std::cout << "OUT: '" << arg << "'" << std::endl;
    }
    else
    {
        std::cout << arg;
    }
    std::cout.flush();
}

void CPU::execIN(){
    uint16_t register_a = numToRegister(this->pcFetch());

    this->m_registers[register_a] = this->getNextInput();
}
void CPU::execNOOP(){
    if(this->m_verboseMode){
        std::cout << "NOOP" << std::endl;
    }                
}

std::string CPU::valueToString(uint16_t val){
    std::stringstream ss;
    
    if(this->isRegister(val)){
        ss << "REG["<<this->numToRegister(val)<<"]";
    }
    else
    {
        ss << "VAL["<<val<<"]";
    }
    return ss.str();
}


void CPU::printListing(){
    
    for(uint16_t i_pc=0;i_pc<this->m_memory.size();){
        
        std::cout << i_pc << ":";
        uint16_t opcode = this->pcFetch(i_pc);
        
        
        if(this->m_opsmap.find(opcode) != this->m_opsmap.end()){
            uint16_t old_pc = i_pc;
            try{
                switch(opcode){
                    case opcode::HALT:
                        std::cout << "HALT";
                        break;
                    case opcode::SET:
                    {
                        std::string val_a = this->valueToString(this->pcFetch(i_pc));
                        std::string val_b = this->valueToString(this->pcFetch(i_pc));
                        std::cout << "SET " << val_a << " TO " << val_b;
                        break;
                    }
                    case opcode::PUSH:
                    {
                        std::string val_a = this->valueToString(this->pcFetch(i_pc));
                        std::cout << "PUSH " << val_a;
                        break;                        
                    }
                    case opcode::POP:
                    {
                        std::string val_a = this->valueToString(this->pcFetch(i_pc));
                        std::cout << "POP " << val_a;
                        break;
                    }
                    case opcode::EQ:
                    {
                        std::string val_a = this->valueToString(this->pcFetch(i_pc));
                        std::string val_b = this->valueToString(this->pcFetch(i_pc));
                        std::string val_c = this->valueToString(this->pcFetch(i_pc));
                        std::cout << "EQ " << val_a << " IS 1 IF " << val_b << " == " << val_c;
                        break;                        
                    }
                    case opcode::GT:
                    {
                        std::string val_a = this->valueToString(this->pcFetch(i_pc));
                        std::string val_b = this->valueToString(this->pcFetch(i_pc));
                        std::string val_c = this->valueToString(this->pcFetch(i_pc));
                        std::cout << "GT " << val_a << " IS 1 IF " << val_b << " > " << val_c;
                        break;                        
                    }
                        break;
                    case opcode::JMP:
                    {
                        std::string val_a = this->valueToString(this->pcFetch(i_pc));
                        std::cout << "JMP " << val_a;
                        break;
                    }
                    case opcode::JT:
                    {
                        std::string val_a = this->valueToString(this->pcFetch(i_pc));
                        std::string val_b = this->valueToString(this->pcFetch(i_pc));
                        std::cout << "JT IF " << val_a << " != 0 JUMP TO " << val_b;                        
                        break;
                    }
                    case opcode::JF:
                    {
                        std::string val_a = this->valueToString(this->pcFetch(i_pc));
                        std::string val_b = this->valueToString(this->pcFetch(i_pc));
                        std::cout << "JF IF " << val_a << " == 0 JUMP TO " << val_b;
                        break;
                    }
                    case opcode::ADD:
                    {
                        std::string val_a = this->valueToString(this->pcFetch(i_pc));
                        std::string val_b = this->valueToString(this->pcFetch(i_pc));
                        std::string val_c = this->valueToString(this->pcFetch(i_pc));
                        std::cout << "ADD " << val_a << " = " << val_b << " + " << val_c;
                        break;
                    }
                    case opcode::MULT:
                    {
                        std::string val_a = this->valueToString(this->pcFetch(i_pc));
                        std::string val_b = this->valueToString(this->pcFetch(i_pc));
                        std::string val_c = this->valueToString(this->pcFetch(i_pc));
                        std::cout << "MULT " << val_a << " = " << val_b << " * " << val_c;
                        break;
                    }
                    case opcode::MOD:
                    {
                        std::string val_a = this->valueToString(this->pcFetch(i_pc));
                        std::string val_b = this->valueToString(this->pcFetch(i_pc));
                        std::string val_c = this->valueToString(this->pcFetch(i_pc));
                        std::cout << "MOD " << val_a << " = " << val_b << " % " << val_c;
                        break;
                    }
                    case opcode::AND:
                    {
                        std::string val_a = this->valueToString(this->pcFetch(i_pc));
                        std::string val_b = this->valueToString(this->pcFetch(i_pc));
                        std::string val_c = this->valueToString(this->pcFetch(i_pc));
                        std::cout << "AND " << val_a << " = " << val_b << " & " << val_c;
                        break;
                    }
                    case opcode::OR:
                    {
                        std::string val_a = this->valueToString(this->pcFetch(i_pc));
                        std::string val_b = this->valueToString(this->pcFetch(i_pc));
                        std::string val_c = this->valueToString(this->pcFetch(i_pc));
                        std::cout << "OR " << val_a << " = " << val_b << " | " << val_c;
                        break;
                    }
                    case opcode::NOT:
                    {
                        std::string val_a = this->valueToString(this->pcFetch(i_pc));
                        std::string val_b = this->valueToString(this->pcFetch(i_pc));
                        std::cout << "NOT " << val_a << " = !" << val_b;
                        break;
                    }
                    case opcode::RMEM:
                    {
                        std::string val_a = this->valueToString(this->pcFetch(i_pc));
                        std::string val_b = this->valueToString(this->pcFetch(i_pc));
                        std::cout << "RMEM " << val_a << " = " << val_b;
                        break;
                    }
                    case opcode::WMEM:
                    {
                        std::string val_a = this->valueToString(this->pcFetch(i_pc));
                        std::string val_b = this->valueToString(this->pcFetch(i_pc));
                        std::cout << "WMEM " << val_a << " = " << val_b;
                        break;
                    }
                    case opcode::CALL:
                    {
                        std::string val_a = this->valueToString(this->pcFetch(i_pc));
                        std::cout << "CALL " << val_a;
                        break;
                    }
                    case opcode::RET:
                    {
                        std::cout << "RET";
                        break;                        
                    }
                    case opcode::OUT:
                    {
                        uint16_t val = this->pcFetch(i_pc);
                        try{
                            char c = boost::numeric_cast<char>(val);
                            std::cout << "OUT '" << c << "'";
                        }
                        catch(boost::numeric::bad_numeric_cast &ex)
                        {
                            std::cout << "OUT " << this->valueToString(val);                        
                        }
                        break;                        
                    }
                    case opcode::IN:
                    {
                        std::string val_a = this->valueToString(this->pcFetch(i_pc));
                        std::cout << "IN " << val_a;
                        break;                        
                    }
                    case opcode::NOOP:        
                        std::cout << "NOOP";
                        break;
                }
            }
            catch(std::runtime_error &err){
                i_pc = old_pc;
                
                try{
                    char c = boost::numeric_cast<char>(opcode);
                    std::cout << "DATA: '" << c << "'";
                }
                catch(boost::numeric::bad_numeric_cast &ex)
                {
                    std::cout << "DATA: " << opcode;                  
                }
            }
        }
        else
        {
            static std::string data;
            
            
            try{
                char c = boost::numeric_cast<char>(opcode);
                std::cout << "DATA: '" << c << "'";
                data+= c;
            }
            catch(boost::numeric::bad_numeric_cast &ex)
            {
                if(!data.empty()){
                    std::cout << "Collected string: '" << data << "'" << std::endl;
                    data.clear();
                }
                std::cout << "DATA: " << opcode;                  
            }
        }
        
        std::cout << std::endl;
    }
    
    
}
