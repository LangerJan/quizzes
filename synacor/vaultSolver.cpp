#include <iostream>
#include <functional>
#include <set>
#include <string>
#include <map>
#include <tuple>
#include <algorithm>

using namespace std;

typedef tuple<int,int> position;
typedef tuple<string, position> movement;
typedef tuple<position, int, string> state;
typedef function<int(int, int)> roomFunc;

/**
 * A map of all rooms with an arithmetic operation
 */
map<position,roomFunc> functionRooms{
	{{0,3}, multiplies<int>()},	{{2,3}, minus<int>()},
	{{1,2}, multiplies<int>()},	{{3,2}, multiplies<int>()},
	{{0,1}, plus<int>()},		{{2,1}, minus<int>()},
	{{1,0}, minus<int>()},		{{3,0}, multiplies<int>()}
};

/**
 * A map of all rooms with a number
 */
map<position, int> numberRooms{
	{{1,3}, 8},		{{3,3}, 1},
	{{0,2}, 4},		{{2,2}, 11},
	{{1,1}, 4}, 	{{3,1}, 18},
	{{0,0}, 22}, 	{{2,0}, 9}
};

/**
 * Given a certain position and a map of rooms, find all valid movements.
 */
template<class RoomType>
set<movement> findRooms(position pos, map<position, RoomType> rooms){
	int posX = get<0>(pos);
	int posY = get<1>(pos);

	set<movement> possible{	{string("E"),{posX+1,posY}}, 
				{string("W"),{posX-1,posY}}, 
				{string("N"),{posX,posY+1}}, 
				{string("S"),{posX,posY-1}}};
	set<movement> result;

	for(auto move : possible){if(rooms.find(get<1>(move))!=rooms.end())result.insert(move);}
	return result;
}

/**
 * Given a state, find all states which can result from here.
 * Note that is is always a double-move (move to an operator-room and to the next number-room)
 */
set<state> followUpStates(state &stat){
	set<state> result;	
	for(movement funcRoomMove : findRooms<roomFunc>(get<0>(stat), functionRooms)){

		for(movement numRoomMove : findRooms<int>(get<1>(funcRoomMove), numberRooms)){
			result.insert({get<1>(numRoomMove), functionRooms[get<1>(funcRoomMove)](get<1>(stat), numberRooms[get<1>(numRoomMove)]),
					get<2>(stat)+get<0>(funcRoomMove)+get<0>(numRoomMove)});
		}
	}	
	return result;
}

/**
 * A state is valid if the orb's value does not reach zero,
 * the start postion may not be returned to
 * the end position may only be entered if the orb-vaule of 30 is reached.
 */
bool isValidState(state &stat){
	return  get<1>(stat) > 0 && 
		!(get<0>(get<0>(stat)) == 0 && get<1>(get<0>(stat)) == 0) &&
		(!(get<0>(get<0>(stat)) == 3 && get<1>(get<0>(stat)) == 3) || get<1>(stat) == 30);
}

/**
 * A state is won if we are at the vault room with the correct orb-value of 30 
 */
bool isWonState(state &stat){
	return get<0>(get<0>(stat)) == 3 &&
	       get<1>(get<0>(stat)) == 3 &&
	       get<1>(stat) == 30;
}

/**
 * Breadth-search. Make all possible moves in round 1, then in round 2, ... until
 * a winning state is found.
 */
state solve(state startState){
	set<state> toDo, toDoNext;

	toDo = followUpStates(startState);

	while(!toDo.empty()){
		for(state it : toDo){
			if(isValidState(it)){
				if(isWonState(it)){
					return it;
				}
				else
				{
					set<state> tmpToDoNext = followUpStates(it);
					toDoNext.insert(tmpToDoNext.begin(), tmpToDoNext.end());
				}
			}
		}
		toDo = toDoNext;
	}

	return {{-1,-1},-1, "-"};
}

int main(){
	state startState{{0,0},22,""};

	state solution = solve(startState);
	cout << get<0>(get<0>(solution)) << "/" << get<1>(get<0>(solution)) 
		<< " " << get<1>(solution) << " " << get<2>(solution) << endl;

	return 0;
}



