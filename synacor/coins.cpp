#include <vector>
#include <algorithm>
#include <iostream>

using namespace std;

int main(){
	std::vector<int> coins{2, 3, 5, 7, 9};
	std::sort(coins.begin(), coins.end());
	do{
		int result = coins[0] + 
				coins[1] * (coins[2]*coins[2]) +
				(coins[3]*coins[3]*coins[3]) -
				coins[4];
		if(result == 399){
			for(auto coin : coins) cout << coin << " ";
			cout << endl;
		}
	}while(next_permutation(coins.begin(), coins.end()));
	return 0;
}

