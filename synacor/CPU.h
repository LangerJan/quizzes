#ifndef CPU_H
#define CPU_H

#include <cstdint>
#include <array>
#include <deque>
#include <map>
#include <set>
#include <functional>
#include <csignal>
#include <atomic>

#include <tuple>

#include "opcodes.h"

class CPU {
public:
    CPU();
    CPU(const CPU& orig);
    virtual ~CPU();
    void load(std::string file);
    void run();
    void step();
    
    void debug_interrupt();
private:
    
    bool debugDialogue();
    
    void printRegisters();
    void printStack();
    
    void loadDump(std::string file);
    void saveDump(std::string file);

    
    uint16_t m_programCounter;
    bool m_halted;
    std::atomic<bool> m_debug_interrupted;
    
    bool m_verboseMode;
    bool m_cheatMode;
    
    std::set<uint16_t> m_breakPoints;
    uint16_t m_cheatMode_pc_failurePoint;
    std::string m_cheatMode_input;
    
    std::array<uint16_t, 8> m_registers;
    std::deque<uint16_t> m_stack;
    std::array<uint16_t,32768> m_memory;
    
    std::deque<std::tuple<uint16_t, std::array<uint16_t, 8>, std::deque<uint16_t>, std::array<uint16_t,32768> >> m_state_stack;
    
    std::deque<uint16_t> m_input;

    
    std::map<uint16_t, std::function<void()>> m_opsmap{
        {opcode::HALT, std::bind(&CPU::execHALT, this)},
        {opcode::SET, std::bind(&CPU::execSET, this)},
        {opcode::PUSH, std::bind(&CPU::execPUSH, this)},
        {opcode::POP, std::bind(&CPU::execPOP, this)},
        {opcode::EQ, std::bind(&CPU::execEQ, this)},
        {opcode::GT, std::bind(&CPU::execGT, this)},
        {opcode::JMP, std::bind(&CPU::execJMP, this)},
        {opcode::JT, std::bind(&CPU::execJT, this)},
        {opcode::JF, std::bind(&CPU::execJF, this)},
        {opcode::ADD, std::bind(&CPU::execADD, this)},
        {opcode::MULT, std::bind(&CPU::execMULT, this)},
        {opcode::MOD, std::bind(&CPU::execMOD, this)},
        {opcode::AND, std::bind(&CPU::execAND, this)},
        {opcode::OR, std::bind(&CPU::execOR, this)},
        {opcode::NOT, std::bind(&CPU::execNOT, this)},
        {opcode::RMEM, std::bind(&CPU::execRMEM, this)},
        {opcode::WMEM, std::bind(&CPU::execWMEM, this)},
        {opcode::CALL, std::bind(&CPU::execCALL, this)},
        {opcode::RET, std::bind(&CPU::execRET, this)},
        {opcode::OUT, std::bind(&CPU::execOUT, this)},
        {opcode::IN, std::bind(&CPU::execIN, this)},
        {opcode::NOOP, std::bind(&CPU::execNOOP, this)}
    };
    
    uint16_t pcFetch();
    uint16_t pcFetch(uint16_t &pc);
    uint16_t numFetch();
    uint16_t numFetch(uint16_t &pc);
    
    std::function<void()> instructionDecode(uint16_t opcode);
    uint16_t numToRegister(uint16_t num);
    bool isRegister(uint16_t num);
    bool isNumerical(uint16_t num);
    
    uint16_t getNextInput();
  
    void pushState();
    void popState();
    
    std::string valueToString(uint16_t val);
    void printListing();
    
    void execHALT();
    void execSET();
    void execPUSH();
    void execPOP();
    void execEQ();
    void execGT();
    void execJMP();
    void execJT();
    void execJF();
    void execADD();
    void execMULT();
    void execMOD();
    void execAND();
    void execOR();
    void execNOT();
    void execRMEM();
    void execWMEM();
    void execCALL();
    void execRET();
    void execOUT();
    void execIN();
    void execNOOP();
};

#endif /* CPU_H */

