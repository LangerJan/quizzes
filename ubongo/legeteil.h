#pragma once

#include <set>
#include <tuple>


using basiskoordinate = tuple<int,int>;
using form = std::set<basiskoordinate>;

class legeteil {
	public:
		legeteil(form grundform);
	private:
		void erzeugeFormen();
		std::set<form> m_formen;
}
